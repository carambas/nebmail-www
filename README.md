## Nebmail
Site web pour les joueurs d'une partie de [Nébula](http://nebula.ludimail.net/).

* gérer ses préférences (adresse mail, mot de passe, copie des messages)
* récupérer son compte-rendu
* saisir et envoyer ses ordres. Possibilité d'effectuer une simulation d'évaluation.
* envoyer et lire des messages échangés avec les autres joueurs et l'arbitre

Concernant l'arbitre d'une partie d'autres fonctions lui sont proposées :

* importer la liste des joueurs à partir d'un fichier .nba
* voir les ordres rendus
* possibilité d'envoyer des ordres vides pour un joueur
* gérer la date limite du tour suivant

Le site nebmail s'interface avec les logiciels tournant sous Windows nebutil et le programme nebula utilisé par  l'arbitre.

## Développement du site

Le site web utilise Laravel 8, Livewire et alpinejs. Le bloc de saisie des ordres d'appuie sur codemirror. L'interface utilisteur repose sur bootstrap 4 avec jQuery et poper.js pour l'utilisation de popover et modals.
L'utilisation des notifications temps réel passe par pusher.com où il faut se créer un compte.

## Paramétrage
* DB_* pour paramétrer la base de données. L'utilisation d'une vue nécessite l'utilisation de MariaDB et ne fonctionne pas avec Mysql.
* BROADCAST_DRIVER : log si on ne veut pas de notifications temps réel, pusher sinon. Dans ce cas renseigner les 4 lignes PUSHER_*
* MAIL_ADMIN : permet d'envoyer les notifications en cas d'erreur à l'exécution d'un job
* EVAL_CMD et CR_CMD : ligne de commande à appeler pour faire l'éval d'un tour et générer le CR à partir d'un fichier .nbt ou .nba. A récupérer par ailleurs. Sans cela pas de possiblité de simulation d'éval, de récupération de CR ainsi que d'envoi de CR.
* PARTIE_MESSAGE : liste des parties pour lesquelles activer la fonction de messagerie (limitation à supprimer)
* LOG_SLACK_WEBHOOK_URL : Webhook slack pour l'envoi des jobs en erreur et des ordres rendus
* DISCORD_WEBHOOK_URL : Webhook Discord s'il n'y en a pas de précisé dans la partie. Pour les notifications par le bot.

## Dépendances
Il est nécessaire d'avoir nodejs en version 12 ou plus, npm en version 7, [php 8](https://php.watch/articles/php-8.0-installation-update-guide-debian-ubuntu) et [composer 2](https://blog.packagist.com/composer-2-0-is-now-available/), ainsi qu'un client mysql local si on utilise un serveur distant.

Il faut également les modules php suivants : php-zip,php-curl,php-xml

## Installation de l'environnement de développement
Il faut utiliser [Composer](http://getcomposer.org/) pour installer le site nebmail.

```
composer install
```
configurer le fichier .env

```
APP_NAME=Nebmail
APP_ENV=local
APP_KEY=(généré automatiquement par composer install)
APP_DEBUG=false
APP_URL=http://localhost
STAT_ORDRES_URL=http://nebmail.ludimail.net/cgi-bin/ordres.pl
STAT_RUM_URL=http://nebmail.ludimail.net/cgi-bin/rumeurs.pl
STAT_NBMESSAGES=http://nebmail.ludimail.net/cgi-bin/statSM.pl
DATA_PATH=
DATA_EXPORT_PATH=
SITE_CHARSET=UTF-8
FILES_ENCODING=ISO-8859-1
LOG_CHANNEL=stack
MAIL_ADMIN=xxx
EVAL_CMD="/home/nebmail/console/nebula eval"
CR_CMD="/home/nebmail/console/nebula cr"
PARTIE_MESSAGE=TEST

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nebmail
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=database
QUEUE_CONNECTION=sync
SESSION_DRIVER=database
SESSION_LIFETIME=1440

MAIL_MAILER=smtp
MAIL_HOST=localhost
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=noreply@ludimail.net
MAIL_FROM_NAME="SM Nébula dev ignorer"

PUSHER_APP_ID=xxx
PUSHER_APP_KEY=xxx
PUSHER_APP_SECRET=xxx
PUSHER_APP_CLUSTER=eu

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

LOG_SLACK_WEBHOOK_URL=xxx
SLACK_WEBHOOK_URL="${LOG_SLACK_WEBHOOK_URL}"
DISCORD_WEBHOOK_URL=xxx
```

Création du schéma de la base de donnée :
```
php artisan migrate
```

Initialisation d'une partie TEST basée sur storage/app/data :
```
php artisan db:seed
```

Dépendances JavaScript :
```
npm install
npm run dev
```
