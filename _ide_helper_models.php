<?php

// @formatter:off
// phpcs:ignoreFile
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * 
 *
 * @property string $user_id
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Coordonnees newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Coordonnees newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Coordonnees query()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Coordonnees whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Coordonnees whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Coordonnees whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Coordonnees whereValue($value)
 */
	class Coordonnees extends \Eloquent {}
}

namespace App\Models{
/**
 * 
 *
 * @property int $id
 * @property string $user_id
 * @property int $tour
 * @property int $monde_id
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait query()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait whereMondeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait whereTour($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Extrait whereUserId($value)
 */
	class Extrait extends \Eloquent {}
}

namespace App\Models{
/**
 * 
 *
 * @property int $id
 * @property string $user_id
 * @property int $tour
 * @property int $brouillon
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\User $user
 * @method static \Database\Factories\OrdreFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre query()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre whereBrouillon($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre whereTour($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Ordre withoutTrashed()
 */
	class Ordre extends \Eloquent {}
}

namespace App\Models{
/**
 * 
 *
 * @property string $name
 * @property string $password
 * @property int $tour
 * @property int|null $tour_final
 * @property int $codesm
 * @property int $visible
 * @property int $en_cours false si non débutée ou terminée
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deadline
 * @property \Illuminate\Support\Carbon|null $derniere_relance
 * @property int $diplomatie
 * @property string|null $discord_url
 * @property string|null $webhook_ordres_rendus
 * @property-read mixed $arbitre
 * @property-read mixed $joueurs_sans_arbitre
 * @property-read mixed $status
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $joueur
 * @property-read int|null $joueur_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Ordre> $ordre
 * @property-read int|null $ordre_count
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie query()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereCodesm($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereDerniereRelance($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereDiplomatie($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereDiscordUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereEnCours($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereTour($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereTourFinal($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Partie whereWebhookOrdresRendus($value)
 */
	class Partie extends \Eloquent {}
}

namespace App\Models{
/**
 * 
 *
 * @property string|null $id
 * @property string $pseudo
 * @property string|null $name
 * @property int $numjou
 * @property int|null $equipe
 * @property string $partie_name
 * @property string|null $password
 * @property string|null $api_token
 * @property string|null $password_plain
 * @property array<array-key, mixed> $email
 * @property string|null $first_name
 * @property string|null $last_name
 * @property int $codesm
 * @property bool $cc
 * @property bool $rumeurs
 * @property bool $msg_gen
 * @property array<array-key, mixed> $relations_custom
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Coordonnees|null $coordonnees
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Extrait> $extraits
 * @property-read int|null $extraits_count
 * @property-read mixed $cc_str
 * @property-read mixed $msg_gen_str
 * @property-read mixed $pseudo_with_num_jou
 * @property-read mixed $rumeurs_str
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Cmgmyr\Messenger\Models\Message> $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection<int, \Illuminate\Notifications\DatabaseNotification> $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Ordre|null $ordre
 * @property-read \App\Models\Ordre|null $ordreBrouillon
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Ordre> $ordresBrouillon
 * @property-read int|null $ordres_brouillon_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Cmgmyr\Messenger\Models\Participant> $participants
 * @property-read int|null $participants_count
 * @property-read \App\Models\Partie $partie
 * @property-read \Illuminate\Database\Eloquent\Collection<int, User> $relations
 * @property-read int|null $relations_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Cmgmyr\Messenger\Models\Thread> $threads
 * @property-read int|null $threads_count
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User arbitre()
 * @method static \Database\Factories\UserFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User ofEquipe($numequipe)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User ofPartie($partie)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User partieSession()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User query()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereCc($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereCodesm($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereEquipe($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereMsgGen($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereNumjou($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User wherePartieName($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User wherePasswordPlain($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User wherePseudo($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereRelationsCustom($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereRumeurs($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace Cmgmyr\Messenger\Models{
/**
 * 
 *
 * @property int $id
 * @property int $thread_id
 * @property string $user_id
 * @property string $body
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Cmgmyr\Messenger\Models\Participant> $participants
 * @property-read int|null $participants_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Cmgmyr\Messenger\Models\Participant> $recipients
 * @property-read int|null $recipients_count
 * @property-read \Cmgmyr\Messenger\Models\Thread|null $thread
 * @property-read \App\Models\User|null $user
 * @method static \Database\Factories\Cmgmyr\Messenger\Models\MessageFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message query()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message unreadForUser($userId)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message whereThreadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Message withoutTrashed()
 */
	class Message extends \Eloquent {}
}

namespace Cmgmyr\Messenger\Models{
/**
 * 
 *
 * @property int $id
 * @property int $thread_id
 * @property string $user_id
 * @property string|null $last_read
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int|null $ignore_non_repondu
 * @property-read \Cmgmyr\Messenger\Models\Thread|null $thread
 * @property-read \App\Models\User|null $user
 * @method static \Database\Factories\Cmgmyr\Messenger\Models\ParticipantFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant query()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant whereIgnoreNonRepondu($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant whereLastRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant whereThreadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Participant withoutTrashed()
 */
	class Participant extends \Eloquent {}
}

namespace Cmgmyr\Messenger\Models{
/**
 * 
 *
 * @property int $id
 * @property string $subject
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $anonymous
 * @property int $broadcast
 * @property-read mixed $updated_at_human
 * @property-read \Cmgmyr\Messenger\Models\Message|null $lastMessage
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Cmgmyr\Messenger\Models\Message> $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Cmgmyr\Messenger\Models\Participant> $participants
 * @property-read int|null $participants_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread between(array $participants)
 * @method static \Database\Factories\Cmgmyr\Messenger\Models\ThreadFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread forDeletedUser($userId)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread forUser($userId)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread forUserWithNewMessages($userId)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread query()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread whereAnonymous($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread whereBroadcast($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread withLastMessage()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder<static>|Thread withoutTrashed()
 */
	class Thread extends \Eloquent {}
}

