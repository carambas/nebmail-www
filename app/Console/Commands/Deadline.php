<?php

namespace App\Console\Commands;

use Notification;
use Carbon\Carbon;
use App\Models\Partie;
use App\Mail\DeadlineMail;
use App\Events\DeadlineEvent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Notifications\DeadlineJoueur;
use App\Notifications\DeadlineArbitre;

class Deadline extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nebula:deadline';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function relance(Partie $partie)
    {
        // Liste des joueurs n'ayant pas rendu leurs ordres
        $users = $partie->joueurs_sans_arbitre->doesntHave('ordre')->get();

        // Notification envoyée aux joueurs ONR
        Notification::send($users, new DeadlineJoueur());

        // Copie envoyée à l'arbitre
        $partie->arbitre->notify(new DeadlineArbitre($users));

        $partie->derniere_relance = Carbon::now();
        $partie->save();

        Log::debug(__METHOD__);
        event(new DeadlineEvent($partie));
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // On cherche toutes les parties avec une date limite fixée dans le futur
        $parties = Partie::where('deadline', '>', Carbon::now())->get();

        if ($parties->count() === 0) {
            $this->warn('Aucune partie trouvée disposant d\'une date limite à venir');
            return 1;
        }

        foreach ($parties as $partie) {
            $delaiAvantDeadline = (int) Carbon::now()->diffInDays($partie->deadline);
            $delaiDepuisDerniereRelance = (int) $partie->derniere_relance->diffInDays(Carbon::now());
            if ($delaiDepuisDerniereRelance >= $delaiAvantDeadline) {
                $this->info("{$partie->name} : relance");
                $this->info("{$partie->name} : Il reste $delaiAvantDeadline jours avant la date limite : envoi de relances");
                $this->relance($partie);
            } else {
                $this->info("{$partie->name} : pas de relance");
                $this->info("{$partie->name} : Il reste $delaiAvantDeadline jours avant la date limite. Pas de relance.");
            }
        }

        return 0;
    }
}
