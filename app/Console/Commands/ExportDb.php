<?php

namespace App\Console\Commands;

use App\Models\Partie;
use App\Services\Utils;
use Arr;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ExportDb extends Command
{
    private $storage;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:export {partie_name?} {--ignore_relations}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export database to nebmail files. Reverse of db:seed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->storage = Storage::disk('export_data');

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('partie_name')) {
            $partie = Partie::findOrFail($this->argument('partie_name'));
            $this->writePartieFiles($partie);
        }
        else {
            $this->writePartiesFile();

            foreach (Partie::withoutGlobalScopes()->get() as $partie) {
                $this->writePartieFiles($partie);
            }
        }

        return 0;
    }

    private function writePartiesFile()
    {
        $this->info('Création fichier PARTIES');

        $this->storage->put('PARTIES', Utils::strConvertOut('MELUZINE:engreve:0:H:'));

        foreach (Partie::withoutGlobalScopes()->get() as $partie) {
            $ligne = implode(':', array_values($partie->only('name', 'password', 'codesm', 'status')));
            $this->storage->append('PARTIES', Utils::strConvertOut("$ligne:"));
        }
    }

    private function writePartieFiles(Partie $partie)
    {
        $this->info("Fichiers répertoire {$partie->name}");
        $this->storage->makeDirectory($partie->name);

        $this->writeUsersFile($partie);
        $this->writeConfFile($partie);

        if (!$this->option('ignore_relations')) {
            $this->writeRelatFile($partie);
        }
    }

    private function writeUsersFile(Partie $partie)
    {
        $userFileName = "{$partie->name}/{$partie->name}";
        $this->storage->delete($userFileName);
        foreach ($partie->joueur as $user) {
            $ligne = implode(':', array_values($user->only(
                'numjou', 'pseudo', 'email_str', 'password_plain', 'codesm', 'cc_str', 'rumeurs_str', 'msg_gen_str'
            )));
            $this->storage->append($userFileName, Utils::strConvertOut("$ligne"));
        }
        $this->storage->append($userFileName, Utils::strConvertOut("end:"));
    }

    private function writeConfFile(Partie $partie)
    {
        $confFileName = "{$partie->name}/CONF";
        $this->storage->put($confFileName, Utils::strConvertOut("DernierTourEnStock={$partie->tour}"));
        $this->storage->append($confFileName, Utils::strConvertOut("TourFinal={$partie->tour_final}"));
    }

    private function writeRelatFile(Partie $partie)
    {
        $relatFileName = "{$partie->name}/RELAT";
        $this->info($relatFileName);
        $this->storage->delete($relatFileName);

        foreach ($partie->joueur->where('numjou', '>', '0') as $joueur) {
            $relat = $joueur->relations()->get()->map(function ($user) {
                return $user->numjou;
            });
            if ($relat->count() > 0) {
                $this->storage->append($relatFileName, $joueur->numjou . '|' . implode(':', $relat->toArray()));
            }
        }
    }
}
