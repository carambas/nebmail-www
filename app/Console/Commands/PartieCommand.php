<?php

namespace App\Console\Commands;

use App\Models\Partie;
use App\Services\Utils;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class PartieCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nebula:partie {nom_partie} {email_arbitre}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Créer une nouvelle partie et envoie par email le mot de passe à l\'arbitre';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $nomPartie = $this->argument('nom_partie');
        $emailArbitre = $this->argument('email_arbitre');


        $validator = Validator::make([
            'nom_partie' => $nomPartie,
            'email_arbitre' => $emailArbitre
        ], [
            'nom_partie' => ['required', 'unique:parties,name', 'regex:/^[a-z]+[0-9]*$/i', 'max:6'],
            'email_arbitre' => 'required|email:rfc',
        ],
        $messages = [
            'regex' => 'Le nom de partie doit débuter par des caractères alphabétiques non accentués (de A à Z). Les chiffres sont possible en suffique.',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nom_partie')) {
                $this->error($errors->first('nom_partie'));
                return(1);
            }
            if ($errors->has('email_arbitre')) {
                $this->error($errors->first('email_arbitre'));
                return(2);
            }
        }

        $partie = Partie::Create([
            'name' => $nomPartie,
            'password' => '',
            'codesm' => 0,
            'tour' => 0,
            'visible' => true,
            'en_cours' => true,
        ]);

        // Générer un password
        $partie->joueur()->create([
            'numjou' => 0,
            'pseudo' => 'Arbitre',
            'email' => [$this->argument('email_arbitre')],
            'codesm' => 0,
            'cc' => true,
            'rumeurs' => true,
            'msg_gen' => true,
        ]);

        $this->info("Partie {$partie->name} créée avec succès");

        Utils::genereMdpPartie($partie);

        $this->info("Le mot de passe a été envoyé à l'adresse {$partie->arbitre->email[0]}");

        return 0;
    }
}
