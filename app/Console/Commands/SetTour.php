<?php

namespace App\Console\Commands;

use App\Models\Partie;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SetTour extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:tour {partie_name} {tour?} {--inc}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Incrémente le numéro du tour d\'une partie';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $partie = Partie::FindOrFail($this->argument('partie_name'));
        } catch(ModelNotFoundException $e) {
            $this->error('Partie ' . $this->argument('partie_name') . ' non trouvée');
            return;
        }

        $tour = $this->argument('tour');
        if (!(isset($tour) && is_numeric($tour))) {
            if ($this->option('inc')) {
                $tour = $partie->tour + 1;
            }
            else {
                $this->error('Il faut préciser un numéro de tour ou utiliser l\'option --inc');
                return;
            }
        }

        $partie->tour = $tour;
        $partie->save();
        $this->info("Nouveau tour pour la partie {$partie->name} : {$partie->tour}");
    }
}
