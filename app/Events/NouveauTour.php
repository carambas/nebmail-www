<?php

namespace App\Events;

use App\Models\Partie;
use Illuminate\Support\Facades\Log;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NouveauTour implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $partie_name;
    public $tour;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Partie $partie)
    {
        $this->partie_name = $partie->name;
        $this->tour = $partie->tour;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        Log::debug(__METHOD__);
        return new Channel('nebmail');
    }
}
