<?php

namespace App\Events;

use App\Models\Ordre;
use App\Models\Partie;
use Illuminate\Support\Facades\Log;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrdresRendus implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $partie_name;
    public $nb_ordres, $nb_jou, $old_count;
    public  $discord_url;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Partie $partie, int $old_count)
    {
        $this->partie_name = $partie->name;
        $this->nb_ordres = $partie->ordre->count();
        $this->old_count = $old_count;
        $this->nb_jou = $partie->joueurs_sans_arbitre->count();
        $this->discord_url = $partie->discord_url;
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        // On ne retourne pas discord_url et user_id qui sont secrets
        return [
            'partie_name' => $this->partie_name,
            'nb_ordres' => $this->nb_ordres,
            'nb_jou' => $this->nb_jou,
            'old_count' => $this->old_count,
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        Log::debug(__METHOD__, [$this]);
        return new Channel('nebmail');
    }
}
