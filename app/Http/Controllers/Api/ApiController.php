<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ApiController extends Controller
{
    public function user(Request $request)
    {
        return $request->user();
    }

    public function partie(Request $request)
    {
        Gate::authorize('view-partie');

        return $request->user()->partie;
    }
}
