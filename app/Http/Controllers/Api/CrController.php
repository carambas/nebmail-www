<?php

namespace App\Http\Controllers\Api;

use Log;
use Gate;
use Validator;
use App\Models\User;
use App\Services\Cr;
use App\Jobs\EnvoiCR;
use App\Models\Extrait;
use App\Services\Utils;
use App\Events\NouveauTour;
use Illuminate\Http\Request;
use App\Services\ExtraitService;
use App\Services\NebData\NebData;
use App\Http\Requests\GetCrRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ParseNbtRequest;


class CrController extends Controller
{
    /**
     * Stocke le fichier .nba envoyé. Si demandé génère et envoie les CR aux joueurs
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request)
    {
        Gate::authorize('store-cr');

        $partie = $request->user()->partie;
        $tour_actuel = $partie->tour;
        $tour_prochain = $tour_actuel + 1;

        $request->validate([
            'nba' => 'required|file',
            'json' => 'required|json',
            'json.*.data' => 'required',
        ]);

        $json = json_decode($request->input('json'), true);

        Validator::make($json, [
            'data' => 'required|array',
            'data.tour' => "required|int|between:$tour_actuel,$tour_prochain",
            'data.envoi_cr' => "required|int|between:0,1",
        ])->validate();

        $data = $json['data'];
        $tour = $data['tour'];
        $envoi_cr = $data['envoi_cr'];

        // Stockage du fichier .nba
        if ($request->hasFile('nba')) {
            $file = $request->file('nba');
            $filename = $file->getClientOriginalName();
            $file->storeAs('', $partie->getNbaFileName($tour), 'data');
            Log::debug(__METHOD__, ['Chemin fichier', $file->getRealPath()]);
            $result['file'] = $filename;
            Log::debug(__METHOD__, ['fichier .nba stocké', $partie->getNbaFileName($tour)]);
        } else {
            Log::error(__METHOD__, ['fichier .nba non stocké', null]);
        }

        // Mise à jour du numéro du tour si le tour du CR = tour de la partie + 1
        if ($tour > $tour_actuel) {
            $partie->tour = $tour;
            $partie->derniere_relance = now();
            $partie->save();

            // Archivage des ordres
            $partie->ordre->each->delete();
            $partie->ordresBrouillon->each->forceDelete();

            $partie->refresh();

            event(new NouveauTour($partie));
        }

        // Envoi des CR si demandé
        if ($envoi_cr) {
            EnvoiCR::dispatch($partie);
        }

        $result['message'] = 'OK';
        $result['tour'] = $tour;
        $result['envoi_cr'] = $envoi_cr;
        return $this->jsonResponse($result);
    }

    /**
     * Récupère un ou des CR txt et nbt
     * pour chaque tour retourne le ou les types de CR demandés
     * champs attendu dans la requête json :
     * user: joueur dont on veut récupérer le CR
     * nbt: bool
     * txt: bool
     * tours: array of integer
     * extraits:      boolean - indique si on veut des extraits de CR
     * extraitsAfter: string - on veut les extraits mis à jour après ce timestamp
     *
     * réponse json
     *  data : array
     *      cr
     *          tour :     integer
     *          txt :      cr texte
     *          nbt :      nbt porsé
     *      extraits:      array - extraits demandés
     *      lastExtraitAt: string - timestamp indiquant la date du dernier extrait fourni
     */
    function get(GetCrRequest $request)
    {
        Log::debug(__METHOD__, ['for user' => Auth::user()->id]);

        $validated = $request->validated();
        Log::debug($validated);

        $usercr = User::with('partie')->find($validated['data']['user']);

        Gate::authorize('view-cr', $usercr);

        // On vérifie que les numéros de tours fournis existent
        $tours = collect($validated['data']['tours']);
        if ($tours->max() > $usercr->partie->tour) {
            abort(404, "Numéro de tour " . $tours->max() . " trop grand");
        }

        $cr = collect();

        // On récupère les CR
        foreach ($tours as $tour) {
            $tourContent = [];
            $tourContent['tour'] = $tour;

            if ($validated['data']['txt']) {
                $crtxt = Cr::getCr(user: $usercr, type: 'txt', tour: (int) $tour, tousLesMondes: $usercr->numjou === 0);
                $tourContent['txt'] = $crtxt;
            }

            if ($validated['data']['nbt']) {
                $nbtFileName = Cr::getCr(user: $usercr, type: 'nbt', tour: (int) $tour, file_output: true);
                $nebdata = new NebData($nbtFileName);
                $tourContent['nbt'] = $nebdata;
            }

            $cr->push($tourContent);
        }

        $result = [];
        $result = ['cr' => $cr];

        // On récupère les extraits de CR stockés pour le joueur
        if ($validated['data']['extraits'] ?? false) {
            $extraits = ExtraitService::getExtraits($validated['data']['extraitsAfter'], $usercr);
            $lastExtraitAt = Extrait::whereBelongsTo($usercr)->max('updated_at');
            $result['extraits'] = $extraits;
            $result['lastExtraitAt'] = $lastExtraitAt;
        }

        return ['data' => $result];
    }

    /**
     * Stocke le fichier .nba envoyé. Si demandé génère et envoie les CR aux joueurs
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function parseNbt(ParseNbtRequest $request)
    {
        $nebData = (object) $request->nbt_parsed;
        $stockeNbt = $request->stocke_nbt ?? false;

        if ($stockeNbt) {
            // On stocke le fichier .nbt
            $request->file('nbt')->storeAs(
                Cr::getNbtDirName($request->user()->partie->name, true),
                Cr::getCRNbtName($request->user()->partie->name, $request->user()->numjou, $nebData->tour, true),
                'data'
            );

            $nbtFilePath = Cr::getNbtFilePath($request->user()->partie->name, $request->user()->numjou, $nebData->tour, true);
        } else {
            // On met une extension .nbt au nom de fichier temporaire sans quoi cmdNebulaCr ne fonctionnera pas
            $nbtFilePath = $request->nbt->path() . '.nbt';
            rename($request->nbt->path(), $nbtFilePath);
        }

        // Générer le CR txt
        $crtxt = utils::strConvertIn(Cr::cmdNebulaCR('txt', $nbtFilePath, $nebData->numJoueur, $nebData->tour));

        $response = [
            'nbt_stocke' => (int) $stockeNbt,
            'nbt' => $nebData,
            'crtxt' => $crtxt,
        ];

        if (!$stockeNbt) {
            unlink($nbtFilePath);
        }

        return $this->jsonResponse($response);
    }
}
