<?php

namespace App\Http\Controllers\Api;

use Validator;
use App\Models\Partie;
use App\Events\OrdresRendus;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Rules\IntegerKeyAttribute;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\OrdreStorage;
use App\Http\Resources\OrdreCollection;
use Illuminate\Auth\Access\AuthorizationException;

/**
 * Class OrdreController
 * @package App\Http\Controllers\Api
 */
class OrdreController extends Controller
{

    /**
     * Affiche l'ensemble des ordres stockés pour le tour à venir
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */

    use OrdreStorage;

    public function index(Request $request)
    {
        Gate::authorize('access-all-ordres');

        $user = $request->user();

        $result = new OrdreCollection($user->partie->ordre);
        Log::debug(__FUNCTION__, [$result]);
        return new JsonResponse($result);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param integer $numjou
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, int $numjou)
    {
        Gate::authorize('access-ordre', $numjou);

        $user = $request->user();

        //On vérifie l'existence du joueur
        $user_ordres = $user->partie->joueurs_sans_arbitre->firstWhere('numjou', $numjou);

        if (!isset($user_ordres)) {
            return $this->jsonResponse(['message' => "Joueur $numjou non trouvé"], 404);
        }

        if (!isset($user_ordres->ordre)) {
            $result = OrdreCollection::ordresVides($numjou, $user->partie->tour + 1);
        } else {
            $result = new OrdreCollection([$user_ordres->ordre]);
        }
        Log::debug(__FUNCTION__, [$result]);
        return new JsonResponse($result);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */

    public function updateMany(Request $request)
    {
        Gate::authorize('access-all-ordres');

        $data = json_decode($request->getContent(), true);

        Validator::make($data, [
            'ordres' => 'required|array',
            'ordres.*' => ['string', new IntegerKeyAttribute],
            'tour' => 'integer',
        ])->validate();

        Log::debug(__METHOD__, ['data' => $data]);

        $user = $request->user();

        $this->checkRequestTour($user->partie, $data['tour']);

        $ordres = $data['ordres'];

        $this->updateFromArray($user->partie, $ordres);

        return $this->jsonResponse(['message' => 'OK']);
    }

    /**
     * @param Partie $partie
     * @param int $tour
     * @return int
     */
    private function checkRequestTour(Partie $partie, int $tour)
    {
        if (isset($tour)) {
            $expected_tour = $partie->tour + 1;

            if ($tour <> $expected_tour) {
                abort(422, "Ordres attendus pour le tour $expected_tour. Tour reçu : $tour");
            }
        }

        return $tour;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $numjou
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $numjou)
    {
        Gate::authorize('access-ordre', $numjou);

        $data = json_decode($request->getContent(), true);

        Validator::make($data, [
            'ordres' => 'required|string',
            'tour' => 'integer',
        ])->validate();

        Log::debug(__METHOD__, ['data' => $data]);

        $user = $request->user();

        $this->checkRequestTour($user->partie, $data['tour']);

        $ordres[$numjou] = $data['ordres'];

        $this->updateFromArray($user->partie, $ordres);

        return $this->jsonResponse(['message' => 'OK']);
    }

    public function count($partie_name = '')
    {
        // On vérifie que la partie existe
        if (($partie_name <> '') && !Partie::where('name', $partie_name)->exists()) {
            abort(404, "Partie $partie_name non trouvée");
        }

        if ($partie_name <> '') {
            return $this->jsonResponse([$partie_name => Partie::find($partie_name)->ordre()->count()]);
        } else {
            $ordres = array();
            foreach (Partie::all() as $p) {
                $ordres[$p->name] = $p->ordre()->count();
            }
            return $this->jsonResponse($ordres);
        }

    }


}
