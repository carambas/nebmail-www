<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Arr;
use Artisan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Log;
use Validator;

class RelationController extends Controller
{
    public function index(Request $request)
    {
        Gate::authorize('access-relation');

        $relations = $request->user()->partie->joueur
            ->reject(fn ($user) => $user->isArbitre() === true)
            ->map(fn ($user) => [
                'id' => $user->id,
                'relations' => $user->relations->map(fn ($rel) => ['id' => $rel->id]),
            ]);

        return($relations);
    }

    public function update(Request $request)
    {
        Gate::authorize('access-relation');
        $partie_name = $request->user()->partie_name;

        $data = json_decode($request->getContent(), true);
        Log::debug(__METHOD__, ['request' => $request->getContent()]);

        Validator::make($data, [
            '*.id' => "required|exists:users|starts_with:$partie_name-",
            '*.relations' => 'array',
            '*.relations.*.id' => "exists:users|starts_with:$partie_name-"
        ])->validate();

        Log::debug(__METHOD__, compact('data'));
        foreach($data as $d) {
            $user1 = User::find($d['id']);

            foreach($d['relations'] as $r) {
                $user2 = User::find(($r['id']));

                // On insère la nouvelle relation
                if (!$user1->relations->contains($user2->id)) {
                    $user1->relations()->attach($user2->id);
                }
            }
        }

        // Mise à jour du fichier RELAT (et par effet de bord du fichier de PARTIE et CONF)
        Artisan::call('db:export', ['partie_name' => $partie_name]);

        return $this->jsonResponse(['message' => 'OK']);

    }
}
