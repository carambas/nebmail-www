<?php

namespace App\Http\Controllers\Auth;

use App\Models\Partie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use App\Providers\RouteServiceProvider;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    //private $parties;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectTo()
    {
        Cookie::queue('partie_name', Auth::user()->partie_name, 60 * 24 * 180); // 180 jours

        if (Auth::user()->isArbitre())
            return RouteServiceProvider::DASHBOARD;
        else
            return RouteServiceProvider::HOME;
    }

    /**
     * Validate the user login request.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            'partie_name' => 'required|string|max:6',
            'numjou' => 'required|numeric',
            'password' => 'required|string',
        ]);
    }

    protected function sendFailedLoginResponse()
    {
        throw ValidationException::withMessages([
            'status' => [trans('auth.failed')],
        ]);
    }


    protected function credentials(Request $request)
    {
        return $request->only('partie_name', 'numjou', 'password');
    }

    public function showLoginForm()
    {
        $parties = Partie::all();
        $partiesNames = $parties->map(function($partie) {
            return $partie->name;
        });
        return view('auth.login')->with('parties', $partiesNames);
    }


}
