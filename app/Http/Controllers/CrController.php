<?php

namespace App\Http\Controllers;

use Log;
use ZipArchive;
use App\Models\User;
use App\Services\Cr;
use App\Services\Utils;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;


class CrController extends Controller
{
    public function crTexte(User $user = null)
    {
        if (!isset($user)) $user = Auth::user();
        Gate::authorize('view-cr', $user);

        $crTxt = Cr::getCr($user, 'txt');

        if (isset($crTxt)) {
            return view('crtexte')->with(['cr' => $crTxt, 'user' => $user]);
        } else {
            return view('crtexte')->with(['error' => 'Compte-rendu non trouvé sur le serveur']);
        }
    }

    public function crZip()
    {
        $partie_name = Auth::user()->partie->name;
        $tour = Auth::user()->partie->tour;
        $numjou = Auth::user()->numjou;

        $crTxt = Cr::getCr(Auth::user(), 'txt');
        $crNbt = Cr::getCr(Auth::user(), 'nbt');
        $zipFile = tempnam(sys_get_temp_dir(), 'Neb') . '.zip';

        $zip = new ZipArchive;

        if ($zip->open($zipFile, ZipArchive::CREATE)!==TRUE) {
            exit("Impossible d'ouvrir le fichier <$zipFile>\n");
        }

        $zip->addFromString(Cr::getCRNbtName($partie_name, $numjou, $tour), Utils::strConvertOut($crNbt));
        $zip->addFromString(Cr::getCRTxtName($partie_name, $numjou, $tour), Utils::strConvertOut($crTxt));
        $zip->close();

        if (file_exists($zipFile)) {
            return response()->download($zipFile, "$partie_name-J$numjou.zip")->deleteFileAfterSend();
        }

        return redirect('home')->withError('Erreur dans la génération du CR');
    }

    public function crNba()
    {
        $fic_nba = Auth::user()->partie->getNbaFileName();
        $fic = Storage::disk('data')->path($fic_nba);

        return response()->download($fic, basename($fic_nba));
    }

    public function plan()
    {
        $user = (request()->user ? User::find(request()->user) : null) ?? Auth::user();

        $userId = $user->id;
        $numJoueur = $user->numjou;

        return view('plan', compact('userId','numJoueur'));
    }

    public function nebutil()
    {
        $user = (request()->user ? User::find(request()->user) : null) ?? Auth::user();

        $userId = $user->id;
        $numJoueur = $user->numjou;
        $partieName = $user->partie->name;
        $tour = $user->partie->tour;
        $show_ordres = (!Auth::user()->isArbitre() || isset(request()->user));
        $apiToken = $user->api_token;

        return view('nebutil', compact('show_ordres', 'userId', 'numJoueur', 'partieName', 'tour', 'apiToken'));
    }

}
