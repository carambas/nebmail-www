<?php

namespace App\Http\Controllers;

use App\Services\OrdresService;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nbOrdres = null;
        $equipeAvecOrdresCount = collect();
        if (Auth::check()) {
            $nbOrdres = Auth::user()->partie->ordre()->count();
            $equipeAvecOrdresCount = OrdresService::getCountOrdresRendusEquipe(Auth::user());
        }
        return view('home')->with(compact('nbOrdres', 'equipeAvecOrdresCount'));
    }

}
