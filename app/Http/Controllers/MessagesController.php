<?php

namespace App\Http\Controllers;

use App\Notifications\MessageNotification;
use App\Models\User;
use Arr;
use Cache;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Notification;
use Gate;

/**
 * Class MessagesController
 * @package App\Http\Controllers
 */
class MessagesController extends Controller
{

    /**
     * MessagesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all of the message threads to the user.
     * TODO à supprimer car réécrit avec Livewire
     *
     * @return mixed
     */
    public function index()
    {
        return view('messenger.index');
    }

    /**
     * Shows a message thread.
     * TODO à supprimer lorsque MessageShow sera SPA
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        // TODO : essayer de supprimer ce code. Pour le moment un bug livewire empêche ces tests d'y fonctionner
        try {
            $thread = Thread::withCount('participants')->with(['participants.user', 'messages.user'])->findOrFail($id);

        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'Le fil de discussion numéro ' . $id . ' est introuvable.');

            return redirect()->route('messages');
        }

        try {
            $this->authorize('view', $thread);
        } catch (AuthorizationException $e) {
            Session::flash('error_message', $e->getMessage());

            return redirect()->route('messages');
        }

        // don't show the current user in list
        $user_id = Auth::id();
        // $users = Auth::user()->partie->joueur->whereNotIn('id', $thread->participantsUserIds($user_id));

        return view('messenger.show', compact('user_id', 'thread'));
    }

    /**
     * Génère la liste des joueurs connus de noms et coéquipiers
     * @return User[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getJoueursData()
    {
        if (Auth::user()->isArbitre())
            return Auth::user()->partie->joueurs_sans_arbitre->get();
        else {
            $relations = collect()
                ->push(Auth::user()->partie->arbitre)
                ->merge(isset(Auth::user()->equipe) ? Auth::user()->coequipiers()->get() : null)
                ->merge(Auth::user()->partie->diplomatie ? Auth::user()->relations()->get() : null)
                ->where('id', '<>', Auth::user()->id)
                ->unique('id');

            return $relations->sortBy('numjou');
        }
    }
    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create($typeMessage = 'message')
    {
        $this->authorize('create', Thread::class);
        if ($typeMessage == 'mg') {
             Gate::authorize('msg_general');
        }

        if ($typeMessage === '') {
            $typeMessage = 'message';
        }

        // On regarde si un destinataire est fourni en paramètre
        $dest = null;
        if (request()->input('dest')) {
            // $dest est un numéro de joueur
            if (Auth::user()->partie->joueur->where('numjou', request()->input('dest'))->count() == 1) {
                $dest = (int) request()->input('dest');
            };
        }

        $users = $this->getJoueursData();
        $diplomatie = Auth::user()->partie->diplomatie;

        // Initialisation objet
        if (empty(request()->input('subject', old('subject')))) {
            request()->merge(["subject" => 'Message de ' . Auth::user()->pseudoWithNumJou]);
        }

        return view('messenger.create', compact('users', 'typeMessage', 'dest', 'diplomatie'));
    }

    /**
     * Stores a new message thread.
     *
     * @param Request $request
     * @param User $user
     * @return mixed
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', Thread::class);

        $user = Auth::user();

        $request->validate(['type_message' => 'in:message,mg,rumeur']);

        $typeMessage = $request->input('type_message');

        if ($typeMessage === 'message') {
            //$array = $user->relationsId(true);
            $joueursConnusEtCoequipiers = $this->getJoueursData()->pluck('id');

            // On ajoute les clés du tableau recipients en valeur pour la validation qui va suivre
            $recipients = $request->input('recipients');

            if (isset($recipients)) {
                $keys = array_keys($recipients);
                foreach($keys as $key) {
                    $recipients[$key] = $key;
                }
                $request->merge([
                    "recipients" => $recipients
                ]);
            }

            $request->validate([
                'recipients' => 'required',
                'subject' => 'required',
                'message' => 'required',
                'recipients.*' => Rule::in($joueursConnusEtCoequipiers)
            ]);
        } else {
            $request->validate([
                    'message' => 'required',
            ]);
        }

        $input = $request->all();
        $subject = '';

        switch ($typeMessage) {
            case 'message':
                $subject = $input['subject'];
                break;
            case 'mg':
                $subject = "Message général de " . $user->pseudoWithNumJou;
                break;
            case 'rumeur':
                $subject = "RUMEUR";
                break;
        }

        $broadcast = (($typeMessage === 'rumeur') || ($typeMessage === 'mg'));
        $thread = Thread::create([
            'subject' => $subject,
            'anonymous' => ($typeMessage === 'rumeur'),
            'broadcast' => $broadcast,
        ]);

        // Message
        $message = Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $input['message'],
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'last_read' => new Carbon,
        ]);

        // Recipients
        if ($broadcast) {
            $thread->addParticipant(Arr::pluck($user->partie->joueur, 'id'));
        }
        elseif ($request->has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }

        $thread->updateUnreadMessageCountCache();

        $this->notify($message);

        return redirect()->route('messages');
    }

    private function notify(Message $message)
    {
        // Notification des destinataires
        $recipients = $message->recipients();
        $users = $recipients->get()->map(fn($recipient) => $recipient->user);

        // Si le joueur a demandé de recevoir une copie des messages envoyés sous forme de notification
        if (Auth::user()->cc) {
            $users->push(Auth::user());
        }

        if ($message->thread->isMessageGeneral()) {
            // Filtrage joueurs ne désirant pas recevoir de notification pour les messages généraux
            $users = $users->where('msg_gen', true);
        }

        if ($message->thread->isRumeur()) {
            // Filtrage joueurs ne désirant pas recevoir de notification pour les rumeurs
            $users = $users->where('rumeurs', true);
        }

        Notification::send($users, new MessageNotification($message, false));

        // Notification de l'arbitre
        $hasArbitre = $message->participants()->where('user_id', '=', Auth::user()->partie->arbitre->id)->exists();
        if (Auth::user()->partie->arbitre->cc && (! $hasArbitre)) {
            Notification::send(Auth::user()->partie->arbitre, new MessageNotification($message, true));
        }
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'La discussion ' . $id . ' n\' pas été trouvée.');

            return redirect()->route('messages');
        }

        try {
            $this->authorize('update', $thread);
        } catch (AuthorizationException $e) {
            Session::flash('error_message', $e->getMessage());

            return redirect()->route('messages');
        }

        $thread->activateAllParticipants();

        // Message
        $message = Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $request->input('message'),
        ]);

        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
        ]);
        $participant->last_read = new Carbon;
        $participant->save();

        // Recipients
        if ($request->has('recipients')) {
            $thread->addParticipant($request->input('recipients'));
        }

        $this->supprimeFlagIgnoreNonrepondus($thread);

        $thread->updateUnreadMessageCountCache();

        $this->notify($message);

        return redirect()->route('messages.show', $id)->withFragment('latest');
    }

    public function supprimeFlagIgnoreNonrepondus(Thread $thread) {
        Participant::whereBelongsTo($thread)->update(['ignore_non_repondu' => false]);
    }
}
