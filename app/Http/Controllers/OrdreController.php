<?php

namespace App\Http\Controllers;

use Session;
use App\Models\User;
use App\Services\Cr;
use App\Services\Utils;
use App\Events\OrdresRendus;
use Illuminate\Http\Request;
use App\Services\OrdresService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;

class OrdreController extends Controller
{

    use OrdreStorage;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function getNumJou(Request $request, $numjou)
    {
        return (! isset($numjou))
        ? (int) $request->user()->numjou
        : (int) $numjou;
    }

    private function getNumJouPour(Request $request, $numjou)
    {
        return ($numjou <> $request->user()->numjou) ? $numjou : null;
    }

    public function edit(User $user = null)
    {
        if (! isset($user)) $user = Auth::user();

        Gate::authorize('edit-ordre', $user);

        $ordre = $user->ordre;
        $ordres_txt = $ordre->content ?? '';

        if ($ordres_txt === '') {
            $ordres_txt = OrdresService::generateCartouche($user);
            $ordres_txt .= "\n\n" . OrdresService::generateSquelette($user);
        }

        return view('ordres')
            ->with([
                'nbOrdres' => $user->partie->ordre()->count(),
                'dateOrdres' => $ordre->updated_at ?? null,
                'ordres' => $ordres_txt ?? '',
                'joueur_pour' => $user
            ]);
    }

    public function update(Request $request, User $user)
    {
        Gate::authorize('edit-ordre', $user);

        $request->validate([
            'ordres' => 'required|string'
        ]);

        // En cas de simulation d'éval
        if ($request->has('simul')) {
            return $this->simulOrdres($request, $user);
        }

        $ordres_txt = $request->input('ordres');
        $this->updateFromArray($user->partie, [$user->numjou => $ordres_txt], 'WEB');

        return redirect()->route('ordres', ['user' => ($user->id <> $request->user()->id) ? $user->id : null])->withInput()
            ->with('status', 'Vos ordres ont été stockés. Un accusé de réception vient de vous être envoyé par email.');
    }

    private function simulOrdres(Request $request, $user)
    {
        $simul = Cr::simulOrdres($user, Utils::strConvertOut($request->input('ordres')));

        if (!isset($simul)) {
            return Redirect::back()->with('error', 'Compte-rendu non trouvé sur le serveur');
        }

        return view('ordres')->with([
            'ordres' => $request->input('ordres'),
            'nbOrdres' => $request->session()->get('nbOrdres'),
            'dateOrdres' => $request->session()->get('dateOrdres'),
            'joueur_pour' => $user,
            'cr' => $simul,
            'boutonRetour' => true
        ]);

    }


}
