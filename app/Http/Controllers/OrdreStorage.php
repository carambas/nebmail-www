<?php


namespace App\Http\Controllers;


use App\Models\User;
use App\Mail\OrdresAR;
use App\Models\Partie;
use App\Jobs\CallSlack;
use App\Services\Utils;
use App\Events\OrdresRendus;
use App\Scopes\NotDraftScope;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/**
 * Trait OrdreStorage
 * @package App\Http\Controllers
 */
trait OrdreStorage
{
    /**
     * updateFromArray
     * Update ordres into DB from array $ordres. Appelé par update et updateMany
     *
     * @param Partie $partie
     * @param array $ordres
     * @param string $canal
     */
    private function updateFromArray(Partie $partie, array $ordres, string $canal = 'API')
    {
        $old_count = $partie->ordre()->count();

        foreach ($ordres as $j => $ordre_jou) {
            $user_ordre = $partie->joueur->firstWhere('numjou', $j);

            if (!isset($user_ordre)) {
                $message = "Joueur $j non trouvé.";
                if (count($ordres) > 1) {
                    $message .= " Les ordres des joueurs précédents ont été stockés.";
                }
                Abort(404, $message);
            }

            // Si ordre présent, même brouillon, on le remplace
            $ordre = $user_ordre->ordre()
                ->withoutGlobalScope(NotDraftScope::class)
                ->firstOrCreate([
                    'tour' => $partie->tour + 1,
                    'brouillon' => false
                ]);
            $ordre->brouillon = false;
            $ordre->content = $ordre_jou;
            $ordre->save();

            // On supprime les brouillons du joueur s'il y en a
            $user_ordre->ordresBrouillon->each->forceDelete();

            $this->sendEmailAR($user_ordre, $ordre_jou, $partie->tour + 1);

            $this->logOK("[{$user_ordre->partie_name}] [$canal] Le joueur {$user_ordre->pseudo}:{$user_ordre->numjou} a rendu ses ordres");
        }

        $partie->refresh();
        event(new OrdresRendus($partie, $old_count));
    }

    /**
     * sendEmailAR
     *
     * @param User $user
     * @param mixed $ordres
     * @param mixed $tour
     * @return void
     */
    private function sendEmailAR(User $user, $ordres, $tour)
    {
        Mail::to($user->email)
            ->queue(new OrdresAR($user, $ordres, $tour));

        if ($user->partie->arbitre->cc) {
            Mail::to($user->partie->arbitre->email)
                ->queue(new OrdresAR($user, $ordres, $tour, true));
        }
    }

    /** Envoi un message de log via le job CallSlack
     * @param string $message
     */
    private function logOK(string $message)
    {
        CallSlack::dispatch($message, Auth::user()->partie);
    }

}
