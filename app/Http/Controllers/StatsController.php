<?php

namespace App\Http\Controllers;

use App\Models\Partie;

class StatsController extends Controller
{
    public function messages()
    {
        return view('stats.messages');
    }

    public function rumeurs()
    {
        return view('stats.rumeurs');
    }

}
