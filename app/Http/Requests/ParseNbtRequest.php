<?php

namespace App\Http\Requests;

use App\Rules\NbtFormatRule;
use App\Rules\NbtJoueurRule;
use App\Services\NebData\NebData;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Http\FormRequest;

class ParseNbtRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nbt' => 'required|file',
            'stocke_nbt' => 'boolean',
            'nbt_parsed' => [new NbtFormatRule, new NbtJoueurRule($this->user())],
        ];
    }

    protected function prepareForValidation()
    {
        if ($this->nbt) {
            $this->merge([
                'nbt_parsed' => (array) new NebData($this->nbt->path()),
            ]);
        }
    }
}
