<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Log;

class OrdreCollection extends ResourceCollection
{
    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;

    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'ordres';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $ordre_array = array();

        foreach ($this->collection as $ordre) {
            $ordre_array[$ordre->user->numjou] = $ordre->content;
        }

        return [
            'ordres' => $ordre_array,
            'tour' => isset($ordre) ? $ordre->user->partie->tour + 1 : null,
        ];

    }

    public static function ordresVides(int $numjou, $tour): array
    {
        return [
            'ordres' => [$numjou => ''],
            'tour' => $tour,
        ];
    }

}
