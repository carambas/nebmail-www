<?php

namespace App\Jobs;

use App\Models\Partie;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CallSlack implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @param string $message
     */
    public function __construct(
        private string $message,
        private ?Partie $partie = null,
        private ?string $url = null,
    ) {
        if (! $this->url) {
            $url = $this->partie?->webhook_ordres_rendus;
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle():void
    {
        if (empty($this->url)) {
            Log::info($this->message);
        }

        $channel = Log::build([
            'driver' => 'slack',
            'username' => 'BOT Nebmail',
            'emoji' => ':boom:',
            'level' => 'debug',
            'url' => $this->url,
        ]);

        Log::stack(['single', $channel])?->info($this->message);
    }
}
