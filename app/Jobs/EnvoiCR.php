<?php

namespace App\Jobs;

use Log;
use App\Models\User;
use App\Services\Cr;
use App\Events\CrSent;
use App\Models\Partie;
use App\Services\Utils;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class EnvoiCR implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Partie
     */
    public Partie $partie;

    public string $nbaFileName;
    public string $temp_file_txt;
    public string $file_nbt;
    public $crTxt;
    public $users;
    public $oneUser = false;

    /**
     * Create a new job instance.
     *
     * @param Partie $partie
     */
    public function __construct(Partie|User $partieOrUser)
    {
        if (get_class($partieOrUser) === Partie::class) {
            $this->partie = $partieOrUser;
            $this->users = $partieOrUser->joueurs_sans_arbitre->get();
        } else {
            $this->partie = $partieOrUser->partie;
            $this->users = collect([$partieOrUser]);
            $this->oneUser = true;
        }

        $nbaFileName = $this->partie->getNbaFileName();

        if (!Storage::disk('data')->exists($nbaFileName)) {
            Log::error(__METHOD__, ['erreur' => "$nbaFileName non trouvé"]);
            return;
        }

        $this->nbaFileName = Storage::disk('data')->path($nbaFileName);
    }

    /**
     * Supprime les fichiers temporaires utilisés pour les CR
     */
    public function cleanTmpFiles()
    {
        Log::debug(__METHOD__);

        if (isset($this->temp_file_txt)) {
            Storage::disk('data')->delete($this->temp_file_txt);
            //unlink($this->temp_file_txt);
            unset($this->temp_file_txt);
        }
    }

    /**
     * Génère les CR txt et nbt
     * @param string $type 'txt' ou 'nbt'
     * @param User $joueur
     */
    private function genereCR(string $type, User $joueur)
    {
        $crFile = Cr::getCr($joueur, $type, true);

        switch ($type) {
            case 'txt':
                $this->crTxt = Utils::strConvertIn(file_get_contents($crFile));
                $this->temp_file_txt = $crFile;
                break;
            case 'nbt':
                $this->file_nbt = $crFile;
                break;
        }
    }

    /**
     * @param User $joueur
     */
    private function envoiCRMail(User $joueur)
    {
        Mail::raw($this->crTxt, function ($message) use ($joueur) {
            $message->to($joueur->email)
                ->subject("NEB {$joueur->partie->name} Compte rendu du tour {$joueur->partie->tour}")
                ->attach($this->temp_file_txt, [
                    'as' => Cr::getCRTxtName($this->partie->name, $joueur->numjou, $this->partie->tour)
                ])
                ->attach($this->file_nbt, [
                    'as' => Cr::getCRNbtName($this->partie->name, $joueur->numjou, $this->partie->tour)
                ]);
        });

    }

    /**
     * Génère les fichiers de compte-rendus d'un tour à partir du fichier .nba et envoie les CR par mail aux joueurs.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->oneUser) {
            Cr::cleanNbtFile($this->users[0]);
        } else {
            Cr::cleanNbtFiles($this->partie);
        }

        foreach ($this->users as $joueur) {
            $this->genereCR('txt', $joueur);
            $this->genereCR('nbt', $joueur);

            $this->envoiCRMail($joueur);

            $this->cleanTmpFiles();
        }

        if ($this->oneUser) {
            $message = "Le compte-rendu du tour {$this->partie->tour} du joueur {$this->users[0]->name} a été renvoyé";
            $subject = "NEB {$this->partie->name} CR renvoyé";
            $webhook_url = config('nebmail.slack_webhook_url');

        } else {
            $message = "Les compte-rendus du tour {$this->partie->tour} de la partie {$this->partie->name} ont été envoyés";
            $subject = "NEB {$this->partie->name} CRs envoyés aux joueurs";
            $webhook_url = $this->partie->discord_url;
        }

        event(new CrSent($subject, $message, $this->partie->arbitre->email, $webhook_url));
    }
}
