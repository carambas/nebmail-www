<?php

namespace App\Listeners;

use App\Events\CrSent;
use App\Services\Utils;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrSentNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CrSent $event)
    {
        extract(get_object_vars($event));

        Utils::sendMailTo(
            $arbitreEmail,
            $subject,
            $message
        );

        // TODO ajouter champ pour indiquer s'il faut envoyer une notification discord ou pas
        Utils::postToDiscord($message, $discordUrl);
    }
}
