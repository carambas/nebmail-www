<?php

namespace App\Listeners;

use App\Models\Partie;
use App\Services\Utils;
use App\Events\OrdresRendus;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ListenOrdresRendus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(OrdresRendus $event)
    {
        extract(get_object_vars($event));

        // Notification sur discord
        if ($nb_ordres > $old_count) {
            $message = "";
            if ($nb_ordres == $nb_jou - 2) $message = "{$nb_ordres} joueurs ont rendu leurs ordres dans la partie {$partie_name}. Ça se précise ©.";
            elseif ($nb_ordres == $nb_jou - 1) $message = "{$nb_ordres} joueurs ont rendu leurs ordres dans la partie {$partie_name}. Il est temps de relire ses ordres.";
            elseif ($nb_ordres === $nb_jou) $message = "Tous les joueurs ont rendu leurs ordres dans la partie {$partie_name} !";

            if ($message != "") {
                dispatch(function () use ($discord_url, $message) {
                    Utils::postToDiscord($message, $discord_url);
                });
            }
        }
    }
}
