<?php

namespace App\Livewire\Arbitre;

use Carbon\Carbon;
use App\Models\User;
use App\Services\Cr;
use App\Jobs\EnvoiCR;
use App\Models\Partie;
use App\Jobs\CallSlack;
use App\Services\Utils;
use Livewire\Component;
use Illuminate\Support\Arr;
use Livewire\WithFileUploads;
use App\Services\NebData\NebData;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\OrdreStorage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class Dashboard extends Component
{
    use WithFileUploads, OrdreStorage;

    public Partie $partie;
    public ?User $user = null;
    public $label_upload = "Selectionnez votre fichier .nba";
    public $fichier_nba;
    public $fichier_nba_dutour;
    public $test_input = "Modifiez-moi";
    public $ordres = '';
    public $showOrdresButton = false;
    public $ordresIdJou = 0;
    public $deadline_date;
    public $deadline_heure;
    public $partie_par_equipe = false;
    public $boutonImportActif = false;

    protected $listeners = [
        'echo:nebmail,OrdresRendus' => 'eventOrdresRendus',
        'echo:nebmail,NouveauTour' => 'eventNouveauTour',
        'echo:nebmail,DeadlineEvent' => 'eventDeadline',
    ];

    protected $rules = [
        'fichier_nba' => 'required|file',
        'partie.deadline' => 'sometimes',
        'deadline_date' => 'required|date',
        'deadline_heure' => 'required|date_format:H:i',
    ];

    private function setPartieParEquipe()
    {
        $this->partie_par_equipe = ($this->partie->joueurs_sans_arbitre->where('equipe', '<>', null)->count() > 0);
    }

    public function mount()
    {
        $this->partie = Auth::user()->partie;
        $this->deadline_date = optional($this->partie->deadline)->toDateString();
        $this->deadline_heure = optional($this->partie->deadline)->toTimeString('minute');

        // On vérifie que le fichier nba du tour est bien présent
        $fic =  $this->partie->getNbaFileName();
        $this->fichier_nba_dutour = Storage::disk('data')->exists($fic) ? $fic : null;

        $this->setPartieParEquipe();
    }

    public function render()
    {
        return view('livewire.arbitre.dashboard');
    }

    public function delete(User $user)
    {
        $user->delete();
        $this->user = null;
        $this->partie->refresh();
    }

    public function details(User $user)
    {
        $this->user = $user;
    }

    public function updatedFichierNba()
    {
        $this->validateOnly('fichier_nba');
        $this->label_upload = 'Fichier bien reçu';
        $this->boutonImportActif = true;
    }

    private function removeInvalidEmailAdresses(array $emails): array
    {
        return collect($emails)->map(function ($email) {
            $validator = Validator::make([
                'email' => $email
            ], [
                'email' => 'email:rfc',
            ]);

            if (!$validator->fails()) return $email;
        })->toArray();
    }

    public function uploadNba()
    {
        $this->validateOnly('fichier_nba');

        $this->fichier_nba->store('nba');

        $joueurs = NebData::parseNbaGetJoueurs($this->fichier_nba->path());
        $partie_infos = NebData::parseNbaGetPartie($this->fichier_nba->path());
        $this->fichier_nba->delete();

        if (!isset($joueurs)) {
            $this->label_upload = "Format de fichier incorrect";
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'fichier_nba' => ['Format de fichier incorrect'],
            ]);
            throw $error;
        }

        // On efface les joueurs déjà présents sauf l'arbitre
        $this->partie->joueurs_sans_arbitre->delete();

        // On ajoute ceux importés
        foreach ($joueurs as $numjou => $value) {
            $user = new User;
            $user->numjou = $numjou;
            $user->equipe = $value['equipe'] ?? null;
            $user->pseudo = $value['pseudo'];
            // TODO vérifier format email et ignorer ceux dans un format non reconnu
            $emails = $this->removeInvalidEmailAdresses($value['email']);

            $user->email = $emails;
            $user->fill([
                'codesm' => 0,
                'cc' => true,
                'rumeurs' => true,
                'msg_gen' => true
            ]);
            $this->partie->joueur()->where('numjou', $numjou)->delete();
            $this->partie->joueur()->save($user);
        }

        $this->setPartieParEquipe();
        $this->partie->diplomatie = ($partie_infos['DiploInterdite'] == 0);
        $this->partie->save();
        $this->dispatch('close-upload-modal');
        $this->label_upload = 'Sélectionnez votre fichier .nba';
        $this->boutonImportActif = false;
    }

    public function genererMdp()
    {
        Utils::genereMdpPartie($this->partie);
    }

    public function updatedDeadlineDate()
    {
        $this->validateOnly('deadline_date');
    }

    public function updatedDeadlineHeure()
    {
        $this->validateOnly('deadline_heure');
    }

    public function deadline()
    {
        $this->validate(Arr::only($this->rules, ['deadline_date', 'deadline_heure']));

        $this->partie->deadline = Carbon::createFromDate($this->deadline_date)
            ->setTimeFromTimeString($this->deadline_heure);

        $this->partie->save();

        $this->dispatch('close-deadline');
    }

    public function eventOrdresRendus($event)
    {
        if ($event['partie_name'] === $this->partie->name) {
            $this->partie->refresh();
            $this->notifyBrowser("Des ordres viennent d'être rendus. {$event['nb_ordres']} joueurs ont rendus leurs ordres.");
        }
    }

    public function eventNouveauTour($event)
    {
        if ($event['partie_name'] === $this->partie->name) {
            $this->partie->refresh();
            $fic =  $this->partie->getNbaFileName();
            $this->fichier_nba_dutour = Storage::disk('data')->exists($fic) ? $fic : null;
            $this->notifyBrowser("Un nouveau tour vient d'être stocké");
        }
    }

    public function eventDeadline($event)
    {
        if ($event['partie_name'] === $this->partie->name) {
            $this->partie->refresh();
            $this->notifyBrowser("Une relance de date limite vient d'être envoyée pour la partie {$event['partie_name']}");
        }
    }

    public function afficheOrdres(User $user)
    {
        $this->ordresIdJou = $user->id;
        $this->ordres = $user->ordre->content;
        $this->showOrdresButton = false;
    }

    public function simulOrdres(User $user)
    {
        $this->ordres = Cr::simulOrdres($user, Utils::strConvertOut($user->ordre->content));
        $this->showOrdresButton = true;
    }

    public function renvoiCR(User $user)
    {
        EnvoiCR::dispatch($user);
        $this->notifyBrowser("CR renvoyé à {$user->pseudo}");
    }

    private function notifyBrowser(string $message, string $type = 'success')
    {
        $this->dispatch('notify',
            typeMsg: $type,
            msg: $message,
        );
    }
}
