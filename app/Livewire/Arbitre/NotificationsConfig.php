<?php

namespace App\Livewire\Arbitre;

use App\Jobs\CallSlack;
use App\Services\Utils;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class NotificationsConfig extends Component
{

    public string $testTexte = "";
    public string $webhook_ordres_rendus = "";
    public string $webhook_bot = "";
    public $partie;

    protected $rules = [
        'webhook_ordres_rendus' => 'url',
        'webhook_bot' => 'url',
    ];

    public function mount()
    {
        $this->partie = Auth::user()->partie;
        $this->readHooksFromPartie();
    }

    public function render()
    {
        return view('livewire.arbitre.notifications-config');
    }

    public function readHooksFromPartie()
    {
        $this->webhook_ordres_rendus = $this->partie->webhook_ordres_rendus ?? config('nebmail.slack_webhook_url');
        $this->webhook_bot = $this->partie->discord_url ?? config('nebmail.discord_webhook_url');
    }

    public function notificationsFormSubmit()
    {
        $this->validate();
        $this->partie->webhook_ordres_rendus = $this->webhook_ordres_rendus;
        $this->partie->discord_url = $this->webhook_bot;
        $this->partie->save();
    }

    public function testBot()
    {
        $texte = $this->testTexte;
        if (empty($texte)) {
            $texte = 'Test bot';
        }

        Utils::postToDiscord($texte, $this->webhook_bot);
    }

    public function testNotificationMessage()
    {
        $texte = $this->testTexte;
        if (empty($texte)) {
            $texte = 'Test notification ordres rendus';
        }

        CallSlack::dispatch(message: $texte, url: $this->webhook_ordres_rendus);
    }

    public function notificationReset()
    {
        $this->reset('testTexte');
        $this->readHooksFromPartie();
    }
}
