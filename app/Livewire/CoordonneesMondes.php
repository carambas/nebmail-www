<?php

namespace App\Livewire;

use App\Models\User;
use Livewire\Component;
use App\Models\Coordonnees;
use Illuminate\Support\Facades\Log;
use App\Rules\CoordonneesMondesRule;
use App\Services\CoordonneesService;
use Illuminate\Support\Facades\Auth;

class CoordonneesMondes extends Component
{
    public string $coordonneesTexte;
    public User $user;

    protected $rules = [
        'coordonneesTexte' => ['string']
    ];

    protected $listeners = ['storeCoordonneesFromPlan'];

    public function mount()
    {
        $this->user = Auth::user();
        $this->readUserCoordinates();
    }

    private function readUserCoordinates()
    {
        $this->coordonneesTexte = $this->user->coordonnees?->value ?? '';
    }

    public function render()
    {
        return view('livewire.coordonnees-mondes');
    }

    public function save()
    {
        $this->validate([
            'coordonneesTexte' => ['string', new CoordonneesMondesRule],
        ]);

        $this->user->coordonnees()->updateOrCreate([], ['value' => $this->coordonneesTexte]);
        $coordonneesArray = (new CoordonneesService($this->coordonneesTexte))->mondesAsKeys()->toArray();
        $this->dispatch('coordonnes-updated', coord: $coordonneesArray);
    }

    public function storeCoordonneesFromPlan(array $coordonneesArray)
    {
        $customCoord = (new CoordonneesService($this->user->coordonnees?->value))
            ->mondesAsKeys()
            ->mergeArrayFromJS($coordonneesArray)
            ->coordonneesAsString();

        Coordonnees::updateOrCreate(['user_id' => $this->user->id], ['value' => $customCoord]);

        $this->user->refresh();
        $this->readUserCoordinates();

        $this->dispatch('disable-save-button');

        $this->dispatch('notify',
            typeMsg: 'success',
            msg: 'Coordonnées enregistrées'
        );
    }
}
