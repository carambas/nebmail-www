<?php

namespace App\Livewire;

use App\Models\User;
use App\Services\Cr;
use App\Services\Utils;
use Livewire\Component;
use App\Scopes\NotDraftScope;
use App\Services\OrdresService;
use App\Services\NebData\NebData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\OrdreStorage;

class EditionOrdres extends Component
{
    use OrdreStorage;

    public $userId;
    public $ordres_txt;
    public $mode_simul = false;
    public $brouillon;
    public $nomPartie;
    public $tour;
    public $apiToken;
    public $crApiUrl;
    public $numJou;

    public function mount()
    {
        $user = User::findOrFail($this->userId);
        $this->numJou = $user->numjou;
        $partie = $user->partie;
        $this->nomPartie = $partie->name;
        $this->tour = $partie->tour;
        $this->apiToken = Auth::user()->api_token;
        $this->crApiUrl = route('api.get.cr');

        Gate::authorize('edit-ordre', $user);

        $ordre = $user->ordre()->withoutGlobalScope(NotDraftScope::class)->first();

        $ordres_txt = $ordre->content ?? '';

        if ($ordres_txt === '') {
            $ordres_txt = OrdresService::generateCartouche($user);
            $ordres_txt .= "\n\n" . OrdresService::generateSquelette($user);
        }

        $this->ordres_txt = $ordres_txt;
        $this->brouillon = $ordre?->brouillon ?? false;
    }

    public function render()
    {
        return view('livewire.edition-ordres');
    }

    public function save(string $ordres_txt, $brouillon = false)
    {
        $user = User::findOrFail($this->userId);
        if ($brouillon) {
            $ordre = $user->ordre()
                ->withoutGlobalScope(NotDraftScope::class)
                ->firstOrCreate([
                    'tour' => $user->partie->tour + 1,
                    'brouillon' => true,
                ]);
            $ordre->content = $ordres_txt;
            $ordre->brouillon = true;
            $this->brouillon = true;
            $ordre->save();
        } else {
            $this->updateFromArray($user->partie, [$user->numjou => $ordres_txt], 'WEB');
            $this->brouillon = false;
        }

        $this->ordres_txt = $ordres_txt;

        $msg = $brouillon ? 'Brouillon enregistré' : 'Ordres envoyés';

        $this->dispatch('notify',
            typeMsg: 'success',
            msg: $msg
        );
    }

    public function simul(string $ordres_txt, $simul = true)
    {
        $user = User::findOrFail($this->userId);
        $tour = $this->tour;
        $this->mode_simul = $simul;
        if ($simul) {
            // Simulation d'éval
            $crtxt = Cr::simulOrdres(user: $user, ordres: Utils::strConvertOut($ordres_txt), creeNbt: true);
            $tour = $this->tour + 1;
        } else {
            // Mode retour à l'affichage CR du tour courant
            $crtxt = Cr::getCr(user: $user, type: 'txt');
        }

        $nbtFileName = Cr::getNbtFilePath($this->nomPartie, $this->numJou, $tour);
        $crtxtFileName = Cr::getCRTxtFilePath($this->nomPartie, $this->numJou, $tour);

        $nebdata = new NebData($nbtFileName);

        if ($simul) {
            unlink($crtxtFileName);
            unlink($nbtFileName);
        }

        $this->dispatch('dessine-plan',
            nebdata: $nebdata,
            crtxt: $crtxt,
            simul: $simul,
            ancientour: false
        );

    }
}
