<?php

namespace App\Livewire;

use App\Services\Cr;
use App\Models\Extrait;
use Livewire\Component;
use Illuminate\Support\Collection;

class ExtraitsModal extends Component
{
    public string $mondesTrouves = '';
    public string $extraitsRaw = '';
    public array $extraits = [];

    public function mount()
    {
        // $this->getExtraits();
    }

    public function render()
    {
        return view('livewire.extraits-modal');
    }

    public function sendExtraits()
    {
        $mondes = Cr::convertCrToArray($this->extraitsRaw);
        $tour = auth()->user()->partie->tour;

        $extraits = $mondes->map(function ($extrait) use ($tour) {
            return  [
                'monde_id' => $extrait['numero'],
                'user_id' => auth()->id(),
                'tour' => empty($extrait['tour'] ?? '') ? $tour : $extrait['tour'],
                'content' => $extrait['monde'],
            ];
        });

        Extrait::upsert(
            $extraits->toArray(),
            ['user_id', 'tour', 'monde_id']
        );

        $this->extraitsRaw = '';
        $this->mondesTrouves = '';
        $this->dispatch('fermer-extraits-modal');

        $extraits = $extraits->map(function ($extrait) {
            return [
                'tour' => $extrait['tour'],
                'numero' => $extrait['monde_id'],
                'content' => $extrait['content']
            ];
        })->values();

        $lastExtraitAt = Extrait::whereBelongsTo(auth()->user())->max('updated_at');

        $this->dispatch('nouveaux-extraits-cr', extraits: $extraits, lastExtraitAt: $lastExtraitAt);
    }

    public function updatedExtraitsRaw()
    {
        $mondes = Cr::convertCrToArray($this->extraitsRaw);
        $this->mondesTrouves = $mondes
            ->map(function ($extrait) {
                return $extrait['numero'] . ($extrait['tour'] ? "(Tour {$extrait['tour']})" : '');
            })
            ->join(', ');
    }

    public function deleteExtrait(int $tour, int $numero)
    {
        Extrait::query()
            ->WhereBelongsTo(auth()->user())
            ->where('tour', $tour)
            ->where('monde_id', $numero)
            ->delete();

        $this->dispatch('supprimer-extrait-cr', tour: $tour, numero: $numero);
    }
}
