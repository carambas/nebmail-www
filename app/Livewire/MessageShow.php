<?php

namespace App\Livewire;

use App\Models\User;
use Livewire\Component;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

// TODO passer à composant SPA
class MessageShow extends Component
{
    use AuthorizesRequests;

    public Thread $thread;
    public User $user;

    public function getListeners()
    {
        return [
            "echo-private:App.Models.User.{$this->user->id},.Illuminate\\Notifications\\Events\\BroadcastNotificationCreated" => 'notifyMessage',
            'refresh' => '$refresh',
        ];
    }

    public function mount(string $userId, Thread $thread)
    {
        $this->user = User::findOrFail($userId);
        $this->thread = $thread;

        try {
            $this->authorize('view', $this->thread);
        } catch (AuthorizationException $e) {
            Session::flash('error_message', $e->getMessage());

            $this->redirect('messages');
        }

    }

    public function render()
    {
        // On ne montre pas l'utilisateur courant dans la liste
        // $userId = Auth::id();
        // $users = Auth::user()->partie->joueur->whereNotIn('id', $this->thread->participantsUserIds($userId));

        $this->thread->markAsRead($this->user->id);
        $this->thread->updateUnreadMessageCountCache($this->user);

        // dd($this->thread);

        Log::debug(__METHOD__);

        return view('livewire.message-show');
    }

    public function notifyMessage($event)
    {
        if ($event['type'] === "App\\Notifications\\MessageNotification") {
            Log::debug(__METHOD__);
            $this->dispatch('refresh');
            $this->dispatch('refreshCount');
        }

    }

}
