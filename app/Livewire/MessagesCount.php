<?php

namespace App\Livewire;

use Auth;
use Cache;
use Livewire\Component;
use Illuminate\Support\Facades\Log;

class MessagesCount extends Component
{
    public int $count;
    public $user_id;

    public function mount()
    {
        $this->user_id = Auth::user()->id;
        $this->refreshCount();
    }

    public function getListeners()
    {
        return [
            "echo-private:App.Models.User.{$this->user_id},.Illuminate\\Notifications\\Events\\BroadcastNotificationCreated" => 'notifyMessage',
            'refreshCount' => 'refreshCount',
        ];
    }

    public function refreshCount()
    {
        $this->count = Cache::rememberForever('Unread:' . Auth::id(), function () {
            return Auth::user()->unreadMessagesCount();
        });

        Log::debug(__METHOD__, ['count' => $this->count]);
    }

    public function render()
    {
        return view('livewire.messages-count');
    }

    public function notifyMessage($event)
    {
        Log::debug(__METHOD__);

        if ($event['type'] === "App\\Notifications\\MessageNotification") {
            Log::debug(__METHOD__ . " => MessageNotification");
            $this->refreshCount();
        }
    }

}
