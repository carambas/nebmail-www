<?php

namespace App\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class MessagesIndex extends Component
{
    use WithPagination, AuthorizesRequests;

    public User $user;
    public bool $showDeleted;
    protected $paginationTheme = 'bootstrap';

    public function getListeners()
    {
        return [
            "echo-private:App.Models.User.{$this->user->id},.Illuminate\\Notifications\\Events\\BroadcastNotificationCreated" => 'notifyMessage',
            'refresh' => '$refresh',
        ];
    }

    public function mount()
    {
        $this->user = Auth::user();
        $this->showDeleted = false;
    }

    public function render()
    {
        $this->authorize('viewAny', Thread::class);

        $threadsQuery = $this->showDeleted
             ? Thread::forDeletedUser($this->user->id)
             : Thread::forUser($this->user->id);

        $threads = $threadsQuery
            ->withLastMessage()
            ->with(['participants.user',
                'participants'])
            ->latest('updated_at')->paginate(15);


        return view('livewire.messages-index', compact('threads'));
    }

    public function notifyMessage($event)
    {
        if ($event['type'] === "App\\Notifications\\MessageNotification") {
            $this->dispatch('refresh');
        }

    }

    public function delete(Thread $thread)
    {
        try {
            $this->authorize('delete', $thread);
        } catch (AuthorizationException $e) {
            $this->dispatch('notify',
                typeMsg: 'danger',
                msg: $e->getMessage(),
            );

            return;
        }

        $thread->removeParticipant($this->user->id);

        $thread->updateUnreadMessageCountCache($this->user);
        $this->dispatch('refreshCount');
    }

    public function restore(Thread $thread)
    {
        try {
            $this->authorize('restore', $thread);
        } catch (AuthorizationException $e) {
            $this->dispatch('notify',
                typeMsg: 'danger',
                msg: $e->getMessage(),
            );

            return;
        }

        $thread->restoreParticipant($this->user->id);

        $thread->updateUnreadMessageCountCache($this->user);
        $this->dispatch('refreshCount');
    }

    public function toggleDeleted()
    {
        $this->showDeleted = !$this->showDeleted;
        $this->resetPage();
    }

    public function hydrate()
    {
        $this->dispatch('refresh-icons');
    }

    public function removeNonLu(Thread $thread)
    {
        try {
            $this->authorize('update', $thread);
        } catch (AuthorizationException $e) {
            $this->dispatch('notify',
                typeMsg: 'danger',
                msg: $e->getMessage(),
            );

            return;
        }

        $participant = $thread->getParticipantFromUser($this->user->id);
        $participant->ignore_non_repondu = true;
        $participant->save();
        $this->dispatch('refresh');
    }
}
