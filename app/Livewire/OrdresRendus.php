<?php

namespace App\Livewire;

use App\Models\Partie;
use Livewire\Component;

class OrdresRendus extends Component
{
    public array $ordres;

    protected $listeners = [
        'echo:ordres.rendus,OrdresRendus' => 'eventOrdresRendus',
        'echo:nouveau.tour,NouveauTour' => 'eventNouveauTour',
    ];

    public function mount()
    {
        $this->ordres = Partie::withCount('ordre')->pluck('ordre_count', 'name')->toArray();

    }

    public function render()
    {
        return view('livewire.ordres-rendus');
    }

    public function eventOrdresRendus($event)
    {
        $this->ordres[$event['partie_name']] = $event['nb_ordres'];
    }

    public function eventNouveauTour($event)
    {
        $this->ordres[$event['partie_name']] = 0;
    }
}
