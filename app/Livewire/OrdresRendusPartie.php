<?php

namespace App\Livewire;

use Livewire\Component;

class OrdresRendusPartie extends Component
{
    public $partieName;
    public $nbOrdres;

    protected $listeners = [
        'echo:ordres.rendus,OrdresRendus' => 'eventOrdresRendus',
        'echo:nouveau.tour,NouveauTour' => 'eventNouveauTour',
    ];

    public function render()
    {
        return view('livewire.ordres-rendus-partie');
    }

    public function eventOrdresRendus($event)
    {
        if ($event['partie_name'] === $this->partieName) {
            $this->nbOrdres = $event['nb_ordres'];
        }
    }

    public function eventNouveauTour($event)
    {
        if ($event['partie_name'] === $this->partieName) {
            $this->nbOrdres = 0;
        }
    }
}
