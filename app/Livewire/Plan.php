<?php

namespace App\Livewire;

use App\Models\User;
use App\Models\Extrait;
use Livewire\Component;
use App\Services\CoordonneesService;
use Illuminate\Support\Facades\Auth;

class Plan extends Component
{
    public string $apiToken;
    public string $tour;
    public string $nomPartie;
    public string $userId;
    public string $numJoueur;
    public string $crApiUrl;
    public string $parseNbtUrl;
    public array $customCoord = [];

    public function mount()
    {
        $this->apiToken = Auth::user()->api_token;
        $partie = Auth::user()->partie;
        $this->tour = $partie->tour;
        $this->nomPartie = $partie->name;
        $this->crApiUrl = route('api.get.cr');

        // Coordonnées personnalisées
        $customCoord = Auth::user()->coordonnees?->value;
        if ($customCoord) {
            $this->customCoord = (new CoordonneesService($customCoord))->mondesAsKeys()->toArray();
        }
    }

    public function render()
    {
        return view('livewire.plan');
    }

}
