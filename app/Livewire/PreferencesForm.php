<?php

namespace App\Livewire;

use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Session;

class PreferencesForm extends Component
{
    public User $user;
    public string $password;

    public string $valide_dns;

    protected $rules = [
        'user.msg_gen' => ['boolean'],
        'user.cc' => ['boolean'],
        'user.rumeurs' => ['boolean'],
        'password' => ['required', 'min:8'],
        'user.email.*' => ['nullable', "email:rfc"],
        ];

    public function render()
    {
        return view('livewire.preferences-form');
    }

    public function mount()
    {
        $this->valide_dns = config('app.debug') ? '' : ',dns';
        $this->user = Auth::user();
        $this->password = $this->user->password_plain;
    }

    public function updatedUserPasswordPlain()
    {
        $this->validate(['password' => ['required', 'min:8']]);
    }

    public function updatedUserEmail0()
    {
        $this->validate([
            'user.email.0' => ['required', "email:rfc{$this->valide_dns}"],
        ]);
    }

    public function updatedUserEmail1()
    {
        $this->validate([
            'user.email.1' => ['nullable', "email:rfc{$this->valide_dns}"]
        ]);
    }

    public function save()
    {
        // validation de la requête
        $valide_dns = config('app.debug') ? '' : ',dns';
        $this->validate([
            'user.msg_gen' => ['boolean'],
            'user.cc' => ['boolean'],
            'user.rumeurs' => ['boolean'],
            'password' => ['required', 'min:8'],
            'user.email.0' => ['required', "email:rfc$valide_dns"],
            'user.email.1' => ['nullable', "email:rfc$valide_dns"],
        ]);

        // Supprime du tableau des emails les valeurs vides
        // On supporte de facto plus de deux adresses email
        $this->user->email = array_values(array_filter((array) $this->user->email, fn($val) => !empty($val)));

        $this->user->password_plain = $this->password;
        $this->user->password = Hash::make($this->password);

        $this->user->save();

        $this->dispatch('notify',
            typeMsg: 'success',
            msg: 'Préférences enregistrées',
        );

        Artisan::call('db:export', [
                'partie_name' => $this->user->partie_name,
                '--ignore_relations' => 'true']
        );

    }

}
