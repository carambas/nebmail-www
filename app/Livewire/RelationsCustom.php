<?php

namespace App\Livewire;

use Illuminate\Database\Eloquent\Relations\Relation;
use Livewire\Component;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class RelationsCustom extends Component
{
    public array $relationsCustom;
    public int $numJou;

    public function mount()
    {
        $user = Auth::user();
        $this->relationsCustom = $user->relations_custom;
        $this->numJou = $user->numjou;
    }

    public function render()
    {
        return view('livewire.relations-custom');
    }

    public function updateCouleur(int $numJou, string $couleur)
    {
        $user = Auth::user();
        Arr::set($this->relationsCustom, "$numJou.couleur", $couleur);
        $user->relations_custom = $this->relationsCustom;
        $user->save();
    }

    public function updateRelations(array $relations)
    {
        $this->relationsCustom = collect($relations)->mapWithKeys(function ($rel) {
            return [$rel['numJou'] => array_filter($rel, array($this, 'cleanRelation'), ARRAY_FILTER_USE_BOTH)];
        })->toArray();
        $user = Auth::user();
        $user->relations_custom = $this->relationsCustom;
        $user->save();
    }

    private function cleanRelation ($val, $key) {
        if (in_array($key, ['pseudo', 'numJou'])) return false;
        if ($key === 'classe' && $val === 0) return false;
        if ($key === 'confiance' && $val === 3) return false;
        if (in_array($key, ['att', 'def', 'declAllie', 'declChargeur', 'declChargeurPop', 'declDechargeurPop']) && $val == 0) return false;

        return true;
    }
}
