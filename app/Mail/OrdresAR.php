<?php

namespace App\Mail;

use App\Models\User;
use App\Services\Cr;
use Illuminate\Support\Arr;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrdresAR extends Mailable
{
    use Queueable, SerializesModels;

    public string $partie;
    public string $pseudo;
    public int $numjou;
    public string $ordres;
    public bool $cc_arbitre;
    private int $tour;
    public $ordres_checked;


    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $ordres
     * @param int $tour
     * @param bool $cc_arbitre
     */
    public function __construct(User $user, string $ordres, int $tour, bool $cc_arbitre = false)
    {
        $this->partie = $user->partie_name;
        $this->pseudo = $user->pseudo;
        $this->numjou = $user->numjou;
        $this->tour = $tour;
        $this->ordres = $ordres;
        $this->ordres_checked = Cr::checkOrdres($user, $ordres);
        $this->cc_arbitre = $cc_arbitre;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tourch = "";

        if (isset($this->tour)) {
            $tourch = " tour {$this->tour}";
        }

        $erreursCh = count(Arr::flatten($this->ordres_checked['erreurs'])) === 1 ?
            ' (erreur trouvée)' :
            (count(Arr::flatten($this->ordres_checked['erreurs'])) > 1 ?
                ' (erreurs trouvées)' :
                '');

        if (!$this->cc_arbitre) {
            $subject = "{$this->partie}$tourch - Accusé de réception de vos ordres" . $erreursCh;
        }
        else {
            $subject = "{$this->partie}$tourch - Ordres du joueur {$this->pseudo}:{$this->numjou}";
        }

        return $this->markdown('emails.ordres_ar')
            ->text('emails.ordres_ar_plain')
            ->subject($subject);
    }
}
