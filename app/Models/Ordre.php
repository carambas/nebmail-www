<?php

namespace App\Models;

use App\Scopes\NotDraftScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ordre extends Model
{
    use SoftDeletes, HasFactory;

    protected $fillable = ['user_id', 'tour', 'brouillon', 'content', 'deleted_at'];


    protected static function booted()
    {
        static::addGlobalScope(new NotDraftScope);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}




