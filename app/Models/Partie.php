<?php

namespace App\Models;

use App\Services\Cr;
use App\Scopes\VisibleScope;
use App\Scopes\NotDraftScope;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Nullable;

class Partie extends Model
{

    protected $primaryKey = 'name';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $casts = [
        'deadline' => 'datetime',
        'derniere_relance' => 'datetime',
    ];

    protected $guarded = [];

    public function joueur()
    {
        return $this->hasMany(User::class, 'partie_name', 'name');
    }

    public function ordre()
    {
        return $this->hasManyThrough(
            Ordre::class,
            User::class,
            'partie_name', // Foreign key on users table...
            'user_id', // Foreign key on Ordre table...
            'name', // local key on partie
            'id' // local key on user table
        );
    }

    public function ordresBrouillon()
    {
        return $this->ordre()->withoutGlobalScope(NotDraftScope::class)->where('brouillon', true);
    }


    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }

    protected static function booted()
    {
        static::addGlobalScope(new VisibleScope);
    }

    public function getNbJou()
    {
        return $this->joueur->count() - 1;
    }

    public function getStatusAttribute()
    {
        $status = '';
        if (!$this->attributes['visible']) {
            $status = 'H';
        } else {
            $status = ($this->attributes['en_cours']) ? 'C' : 'T';
        }

        return $status;
    }

    public function getArbitreAttribute()
    {
        return $this->joueur()->where('numjou', 0)->first();
    }

    public function getJoueursSansArbitreAttribute()
    {
        return $this->joueur()->where('numjou', '>', 0);
    }

    public function getNbaFileName(int $tour = null)
    {
        if (!isset($tour))
            $tour = $this->tour;
        return Cr::getNbaFileName($this->name, $tour);

    }

    public function getNbtDirName()
    {
        return CR::getNbtDirName($this->name);

    }

}
