<?php

namespace App\Models;

use App\Services\Cr;
use App\Models\Extrait;
use App\Services\Utils;
use Illuminate\Support\Str;
use App\Scopes\NotDraftScope;
use Illuminate\Support\Facades\Log;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
    use Messagable;
    use HasFactory;

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
       'pseudo', 'email', 'numjou', 'partie_name', 'password', 'api_token', 'password_plain', 'codesm', 'msg_gen', 'cc', 'rumeurs'
   ];

    // protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token', 'password_plain', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'cc' => 'boolean',
        'rumeurs' => 'boolean',
        'msg_gen' => 'boolean',
        'email' => 'array',
        'relations_custom' => 'array',
    ];

    public static function boot()
    {
        parent::boot();

        // Événements pour générer automatiquement l'id à partir du nom de partie et du numéro de joueur
        User::creating(function($user)
        {
            $user->id = "{$user->partie_name}-{$user->numjou}";
        });

        User::updating(function($user)
        {
            if ($user->id !== "{$user->partie_name}-{$user->numjou}")
                $user->id = "{$user->partie_name}-{$user->numjou}";
        });
    }

    public function partie()
    {
        return $this->belongsTo(Partie::class, 'partie_name', 'name');
    }

    public function relations()
    {
        return $this->belongsToMany(User::class, 'relations', 'user_id', 'relation_id')
            ->withTimestamps();
    }

    public function relationsId($include_arbitre = false)
    {
        if ($this->isArbitre()) {
            return $this->partie->joueur()->get()->map(fn ($value) => $value->id);
        }

        $users = $this->relations()->select('users.id as id')->get()->map(fn ($value) => $value->id);

        if ($include_arbitre) {
            $users->prepend($this->partie->arbitre->id);
        }

        return $users->toArray();
    }

    public function ordre()
    {
        return $this->hasOne(Ordre::class)->orderBy('id', 'desc');
    }

    public function ordresBrouillon()
    {
        return $this->hasMany(Ordre::class)->withoutGlobalScope(NotDraftScope::class)->where('brouillon', true);
    }

    public function ordreBrouillon()
    {
        // return $this->hasOne(Ordre::class)->withoutGlobalScope(NotDraftScope::class)->where('brouillon', true)->latestOfMany();
        return $this->hasOne(Ordre::class)->withoutGlobalScope(NotDraftScope::class)->where('brouillon', true)->orderBy('id', 'desc');
    }

    public function coordonnees()
    {
        return $this->hasOne(Coordonnees::class);
    }

    public function extraits()
    {
        return $this->hasMany(Extrait::class);
    }

    public function isArbitre()
    {
        return $this->numjou === 0;
    }

    public function scopeArbitre($query)
    {
        return $query->where('numjou', 0);
    }

    public function scopePartieSession($query)
    {
        return $query->where('partie_name', Session('partie'));
    }

    public function scopeOfPartie($query, $partie)
    {
        return $query->where('partie_name', $partie);
    }

    public function scopeOfEquipe($query, $numequipe)
    {
        return $query->where('equipe', $numequipe);
    }

    public function coequipiers()
    {
        return $this->partie->joueur()->ofEquipe($this->attributes['equipe']);
    }

    public function getCcStrAttribute()
    {
        return $this->attributes['cc'] ? 'Y' : 'N';
    }

    public function getRumeursStrAttribute()
    {
        return $this->attributes['rumeurs'] ? 'Y' : 'N';
    }

    public function getMsgGenStrAttribute()
    {
        return $this->attributes['msg_gen'] ? 'Y' : 'N';
    }

    public function getPseudoWithNumJouAttribute()
    {
        return "{$this->pseudo}:{$this->numjou}";
    }

    public function getNameAttribute()
    {
        return $this->attributes['pseudo'];
    }

    public function getNbtDirName()
    {
        return $this->partie->getNbtDirName();
    }

    public function getNbtFileName(int $tour = null)
    {
        if (!isset($tour))
            $tour = $this->partie->tour;

        return Cr::getNbtFileName($this->partie->name, $this->numjou ,$tour);
    }

}
