<?php

namespace App\Notifications;

use App\Models\Partie;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class DeadlineArbitre
 * @package App\Notifications
 */
class DeadlineArbitre extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Collection
     */
    private Collection $users;

    /**
     * Create a new notification instance.
     *
     * @param Collection $users
     */
    public function __construct(Collection $users)
    {
        $this->users = $users;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $tour = $notifiable->partie->tour + 1;
        $date = (new Carbon($notifiable->partie->deadline))->locale('fr_FR')->isoFormat('LLLL');

        return (new MailMessage)
            ->subject("NEB {$notifiable->partie_name} commande deadline")
            ->greeting('Mail de relance')
            ->line("Un mail de relance a été envoyé aux joueurs. Date limite indiquée : $date.")
            ->line('Joueurs concernés : ' . $this->users->pluck('pseudoWithNumJou')->implode(','))
            ->salutation('L\'équipe Nébula');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
