<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DeadlineJoueur extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @throws \Exception
     */
    public function toMail($notifiable)
    {
        $tour = $notifiable->partie->tour + 1;
        $date = (new Carbon($notifiable->partie->deadline))->locale('fr_FR')->isoFormat('LLLL');
        return (new MailMessage)
            ->subject("NEB {$notifiable->partie_name} Vous n'avez pas rendu vos ordres pour le tour $tour")
            ->greeting('Vous n\'avez pas rendu vos ordres')
            ->line("{$notifiable->pseudo}, vos ordres pour le tour $tour de la partie {$notifiable->partie_name} ne sont actuellement pas stockés sur le serveur.")
            ->line("Si vous ne les avez pas envoyés, n'oubliez pas la date limite : $date")
            ->line('Si vous les avez envoyés, contactez l\'arbitre.');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
