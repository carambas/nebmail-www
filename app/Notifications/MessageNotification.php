<?php

namespace App\Notifications;

use Log;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Cmgmyr\Messenger\Models\Message;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class MessageNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Message
     */
    private Message $message;
    /**
     * @var false
     */
    private bool $cc_arbitre;

    /**
     * Create a new notification instance.
     *
     * @param Message $message
     * @param bool $cc_arbitre
     */
    public function __construct(Message $message, bool $cc_arbitre = false)
    {
        //
        $this->message = $message;
        $this->cc_arbitre = $cc_arbitre;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ($notifiable->id !== $this->message->user_id)
            ? ['mail', 'broadcast']
            : ['mail'];

    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $pseudo = $this->message->user->pseudoWithNumJou;
        $destinatairesStr = $this->message->thread->participantsString($this->message->user_id);

        $strCcArbitre = $this->cc_arbitre ? ' CC:arbitre' : '';

        $boutonRepondre = true;

        if ($this->message->thread->anonymous) {
            $subject = "NEB {$notifiable->partie_name}{$strCcArbitre} Rumeur";
        } elseif ($this->message->thread->broadcast) {
            $subject = "NEB {$notifiable->partie_name}{$strCcArbitre} Message général de $pseudo";
            if (!$notifiable->partie->diplomatie) {
                $boutonRepondre = false;
            }
        } else {
            $subject = "NEB {$notifiable->partie_name}{$strCcArbitre} conversation entre {$this->message->thread->participantsString()}";
        }

        return (new MailMessage)
            ->subject($subject)
            ->markdown('emails.message_notification', [
                'message' => $this->message,
                'dest_str' => $destinatairesStr,
                'anonymous' => $this->message->thread->anonymous,
                'broadcast' => $this->message->thread->broadcast,
                'bouton_repondre' => $boutonRepondre,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message_subject' => $this->message->thread->subject,
            'message_body' => Str::limit($this->message->body),
            'dest_str' => $this->message->thread->participantsString($this->message->user_id),
            'sender' => $this->message->user_id,
            'anonymous' => $this->message->thread->anonymous,
            'broadcast' => $this->message->thread->broadcast,
        ];

    }
}
