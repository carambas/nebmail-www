<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $partie = $notifiable->partie_name;
        $password = $notifiable->password_plain;
        return (new MailMessage)
                    ->subject("NEB $partie - Votre mot de passe")
                    ->greeting($notifiable->pseudo)
                    ->line("Votre mot de passe pour la partie $partie est : **$password**.")
                    ->line('Ce mot de passe vous sera nécessaire pour envoyer vos ordres à partir de Nebutil et pour vous connecter au site nebmail.')
                    ->action('Me connecter et modifier mon mot de passe', route('confpref'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
