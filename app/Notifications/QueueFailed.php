<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackAttachment;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\Events\JobFailed;
use Log;

class QueueFailed extends Notification
{
    use Queueable;

    /**
     * @var JobFailed
     */
    private JobFailed $event;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobFailed $event)
    {
        //
        $this->event = $event;
        dump($event);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        Log::debug(__METHOD__);
        $jobname = $this->event->job->resolveName();

        $trace = array_filter(explode("\n", $this->event->exception->getTraceAsString()), fn ($line) => strpos($line, 'vendor') === false);

        $mail =  (new MailMessage)
            ->error()
            ->subject("Nebmail Job $jobname en erreur")
            ->line("Le job **$jobname** est en erreur")
            ->line($this->event->exception->getMessage())
            ->attachData($this->event->exception->getTraceAsString(), 'trace.txt', [
                'mime' => 'text/plain',
        ]);

        foreach ($trace as $line) {
            $mail->line("$line");
        }

        return $mail;
    }

    /**
     * @param $notifiable
     * @return mixed
     */
    public function toSlack($notifiable)
    {
        Log::debug(__METHOD__);
        $jobname = $this->event->job->resolveName();

        return (new SlackMessage())
            ->to("#general")
            ->error()
            ->content("Nebmail Job $jobname en erreur\n")
            ->attachment(function(SlackAttachment $attachment) use ($notifiable) {
                $attachment->content($this->event->exception->getMessage());
            //     $attachment->fields([
            //     //     'Payload' => $this->event->job->getRawBody(),
            //     //    'Exception' => $this->event->exception->getTraceAsString(),
            //     //'Job' => $this->event->job->resolveName(),
            //     'Exception' => $this->event->exception->getMessage(),
            //    ]);
            });
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
