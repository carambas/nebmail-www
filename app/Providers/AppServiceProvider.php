<?php

namespace App\Providers;

use App\Notifications\QueueFailed;
use Illuminate\Pagination\Paginator;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\ServiceProvider;
use Log;
use Notification;
use Queue;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        \App\Lbc\LaravelBootstrapComponents::init();

        Queue::failing(function (JobFailed $event) {
            Notification::route('mail', config('nebmail.mail_admin'))
                ->route('slack', config('nebmail.slack_webhook_url'))
                    ->notify(new QueueFailed($event));
        });
    }
}
