<?php

namespace App\Providers;

use App\Policies\ThreadPolicy;
use App\Services\Auth\BasicAuthGuard;
use App\Models\User;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Auth\Access\Response;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Thread::class => ThreadPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        Auth::provider('nebmail', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...

            return new NebmailUserProvider();
        });


        Auth::extend('basic.auth', function ($app, $name, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\Guard...

            return new BasicAuthGuard(Auth::createUserProvider($config['provider']), $app->make('request'));
        });

        Gate::define('view-cr', function (User $user, $usercr) {
            return $user->isArbitre() || ($user->id === $usercr->id)
                ? Response::allow()
                : Response::deny('Refusé : autre joueur');
        });

        Gate::define('view-partie', function (User $user) {
            return $user->isArbitre();
        });

        Gate::define('access-relation', function (User $user) {
            return $user->isArbitre();
        });

        Gate::define('access-all-ordres', function (User $user) {
            return $user->isArbitre()
                ? Response::allow()
                : Response::deny('Action réservée à l\'arbitre');
        });

        Gate::define('access-ordre', function (User $user, int $numjou) {
            return $user->isArbitre() || ($user->numjou === $numjou)
                ? Response::allow()
                : Response::deny("Refusé : autre joueur");
        });

        Gate::define('edit-ordre', function (User $user, $userordres) {
            if ($user->partie->name != $userordres->partie->name)
                return Response::deny("Refusé : joueur d'une autre partie");

            return ($user->isArbitre() || $user->id === $userordres->id)
                ? Response::allow()
                : Response::deny("Refusé : autre joueur");
        });

        Gate::define('store-cr', function (User $user) {
            return $user->isArbitre()
                ? Response::allow()
                : Response::deny('Action réservée à l\'arbitre');
        });

        Gate::define('msg_general', function (User $user) {
            return $user->partie->diplomatie || $user->isArbitre()
                ? Response::allow()
                : Response::deny('Partie sans diplomatie');
        });

        Gate::define('msg_rumeur', function (User $user) {
            return ! $user->isArbitre();
        });
    }
}
