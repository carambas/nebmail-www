<?php

namespace App\Providers;

use App\Events\CrSent;
use App\Events\OrdresRendus;
use App\Listeners\LogFailedLogin;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Failed;
use App\Listeners\LogAuthenticated;
use App\Listeners\CrSentNotification;
use App\Listeners\ListenOrdresRendus;
use App\Listeners\LogSuccessfulLogin;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Authenticated;
use App\Listeners\LogAuthenticationAttempt;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // Attempting::class => [LogAuthenticationAttempt::class],
        // Authenticated::class => [LogAuthenticated::class],
        Login::class => [LogSuccessfulLogin::class],
        Failed::class => [LogFailedLogin::class],
        OrdresRendus::class => [ListenOrdresRendus::class],
        CrSent::class => [CrSentNotification::class],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
