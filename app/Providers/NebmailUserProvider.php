<?php

namespace App\Providers;

use App\Services\Joueurs;
use \Illuminate\Contracts\Auth\UserProvider as UserProvider;

use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Auth\GenericUser;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class NebmailUserProvider implements UserProvider
{

    public function validateCredentials(UserContract $user, array $credentials)
    {
        Log::debug(__FUNCTION__, [
            'user' => $user,
            'credentials' => $credentials,
            'pass' => ($user->getAuthPassword() === $credentials['password']) ? 'OK' : 'KO']);
        return ($user->getAuthPassword() === $credentials['password']);
    }

    /**
     * @inheritDoc
     */
    public function retrieveById($identifier)
    {
        $parts = explode('-', $identifier);
        if (count($parts) < 2) {
            return null;
        }
        list ($partie, $numjou) = $parts;
        Log::debug(__FUNCTION__, ['identifier' => $identifier]);
        Log::info(__FUNCTION__, ['lecture fichier ' . storage_path("$partie/$partie")]);

        $joueurs = Joueurs::getPartieFileContentArray($partie);
        if (in_array($numjou, array_keys($joueurs))) {
            $joueur = $joueurs[$numjou];
        } else {
            return null;
        }

        $user = $this->nebmailUser($identifier, $partie, $numjou, $joueur['pseudo'], $joueur['email'],
            $joueur['password'], $joueur['codesm'], $joueur['cc'], $joueur['rumeurs'], $joueur['msgGen'], null);

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function retrieveByToken($identifier, $token)
    {
        Log::debug(__FUNCTION__, ['identifier' => $identifier, 'token' => $token]);
        if (!Storage::disk('local')->exists("token/$identifier")) {
            return null;
        }
        $content = Storage::disk('local')->get("token/$identifier");
        Log::debug(__FUNCTION__, ['content' => $content, 'token' => $token]);
        if ($content === $token) {
            return $this->retrieveById($identifier);
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function updateRememberToken(UserContract $user, $token)
    {
        Log::debug(__FUNCTION__, ['user' => $user, 'token' => $token]);
        Storage::disk('local')->makeDirectory('token');
        Storage::disk('local')->put("token/{$user->id}", $token);
    }

    /**
     * @inheritDoc
     */
    public function retrieveByCredentials(array $credentials)
    {
        Log::debug(__FUNCTION__, ['credentials' => $credentials]);
        if (!in_array('partie', array_keys($credentials)) ||
            !in_array('numjou', array_keys($credentials))) {
            return null;
        }
        $id = strtoupper($credentials['partie']) . '-' . $credentials['numjou'];
        return $this->retrieveById($id);
    }


    /**
     * Return a nebmail user
     */
    protected function nebmailUser($identifier, $partie, $numjou, $pseudo, $email, $password, $codesm, $cc, $rumeurs,
                                   $msgGen, $remember_token)
    {
        $attributes = array(
            'id' => $identifier,
            'partie' => $partie,
            'numjou' => (int)$numjou,
            'username' => $numjou,
            'password' => $password,
            'name' => $pseudo,
            'email' => $email,
            'remember_token' => $remember_token,
            'codesm' => (int)$codesm,
            'cc' => strtoupper($cc) === 'Y',
            'rumeurs' => strtoupper($rumeurs) === 'Y',
            'msgGen' => strtoupper($msgGen) === 'Y',
            'arbitre' => (int)$numjou === 0
        );

        Log::debug(__FUNCTION__, ['attributes' => $attributes]);

        return new GenericUser($attributes);
    }
}
