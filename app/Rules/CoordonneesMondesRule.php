<?php

namespace App\Rules;

use App\Services\CoordonneesService;
use Illuminate\Support\Str;
use Illuminate\Contracts\Validation\Rule;

class CoordonneesMondesRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return ((new CoordonneesService($value))->checkFormat());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "La liste des coordonnées n'est pas au format attendu : m=x,y sur plusieurs lignes";
    }
}
