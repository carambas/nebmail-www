<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IntegerKeyAttribute implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $key = null;
        $array = explode('.', $attribute);
        if (count($array) >= 2) {
            $key = array_pop($array);
        }
        return ctype_digit($key);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'L\'index du tableau doit être un entier';
    }
}
