<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NbtFormatRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = (object) $value;
        return ($value->numJoueur ?? 0) > 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Le fichier n'est au format .nbt (pas de numéro de joueur de trouvé)";
    }
}
