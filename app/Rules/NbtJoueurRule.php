<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class NbtJoueurRule implements Rule
{
    private int $numJou;
    private string $nomPartie;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->numJou = $user->numjou;
        $this->nomPartie = $user->partie->name;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = (object) $value;
        return ($value->nomPartie === $this->nomPartie) && ($value->numJoueur === $this->numJou);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Le fichier .nbt n'appartient pas au joueur {$this->numJou} de la partie {$this->nomPartie}";
    }
}
