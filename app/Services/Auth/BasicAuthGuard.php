<?php


namespace App\Services\Auth;


use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class BasicAuthGuard implements Guard
{
    use GuardHelpers;

    protected $request;

    public function __construct(
        UserProvider $provider,
        Request $request)
    {
        $this->request = $request;
        $this->provider = $provider;
        $this->user = null;
    }

    public function user()
    {
        if (! is_null($this->user)) {
            return $this->user;
        }

        $user = null;
        $partie = null;
        $numjou = null;

        $id = explode('-', $this->request->getUser());
        if (count($id) > 1) {
            list($partie, $numjou) = $id;
        }

        $credentials = [
            'partie_name' => $partie,
            'numjou' => $numjou,
            'password' => $this->request->getPassword(),
        ];

        $user = $this->provider->retrieveByCredentials($credentials);

        if (isset($user) && $this->provider->validateCredentials($user, $credentials)) {
            return $this->user = $user;
        }

        return null;
    }

    public function validate(array $credentials = [])
    {
        if (empty($credentials['partie']) || empty($credentials['numjou']) || empty($credentials['password'])) {
            return false;
        }

        if ($user = $this->provider->retrieveByCredentials($credentials)) {
            return $this->provider->validateCredentials($user, $credentials);
        }


        return false;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }



}
