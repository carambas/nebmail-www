<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class CoordonneesService
{
    private Collection $value;
    private $linePattern = '/^(\d+)=(\d+),(\d+)$/';

    public function __construct(string $value)
    {
        $this->value = collect(preg_split("/\n\r?/", $value ?? ''));
        $this->trim();
    }

    /**
     * Vérifie le format d'un fichier de coordonnées : plusieurs lignes de la forme m=x,y
     *
     * @param [type] $value
     * @return void
     */
    public function checkFormat(): bool
    {
        return $this->value
            ->every(fn ($line) => preg_match($this->linePattern, $line));
    }

    public function trim()
    {
        $this->value = $this->value
            ->map(fn ($line) => Str::replace(" ", "", $line))
            ->filter(fn ($line) => trim($line) !== '')
            ->values();
    }

    public function mondesAsKeys(): static
    {
        $this->value = $this->value
            ->mapWithKeys(function ($line) {
                $matches = [];
                preg_match($this->linePattern, $line, $matches);
                return [(int) $matches[1] => [(int) $matches[2], (int) $matches[3]]];
            });

        return $this;
    }

    public function coordonneesAsString(): string
    {
        return $this->value
            ->map(fn ($monde, $key) => "$key={$monde[0]},{$monde[1]}")
            ->join("\n");
    }

    public function toArray(): array
    {
        return $this->value->toArray();
    }

    public function toString(): string
    {
        return $this->value->join("\n");
    }

    public function toJson(): string
    {
        return $this->value->toJson();
    }

    /**
     * Merge avec array dont les champs sont numero, x et y
     *
     * @param [type] $coordArray
     * @return static
     */
    public function mergeArrayFromJS($coordArray): static
    {
        foreach ($coordArray as $monde) {
            $this->value[(int) $monde['numero']] = [$monde['x'], $monde['y']];
        }
        return $this;
    }
}
