<?php

namespace App\Services;

use App\Models\User;
use App\Models\Partie;
use App\Models\Extrait;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Cr
{
    /**
     * cmdNebulaCR

     *
     * @param  string $type
     * @param  string $nbxFileName
     * @param  int $numjou
     * @return string
     */
    public static function cmdNebulaCR(string $type, string $nbxFileName, int $numjou, bool $tousLesMondes = false)
    {
        $numjouStr = $tousLesMondes ? 'all' : $numjou;
        $commande = config('nebmail.cr') . " $type $nbxFileName $numjouStr";
        Log::debug(__METHOD__, ['commande' => $commande]);

        unset($output);
        exec($commande, $output, $err);

        return implode("\r\n", $output);

    }

    public static function getCRTxtName(string $partie, int $numjou, int $tour)
    {
        return sprintf('%sR%02d%02d.txt', mb_substr($partie, 0, 3), $numjou, $tour);
    }

    public static function getNbtDirName($partie_name, $modifie = false)
    {
        return "{$partie_name}/STOCK/NBT" . ($modifie ? '_modifies' : '');

    }

    public static function getCRNbtName(string $partie_name, int $numjou, int $tour)
    {
        return sprintf('%s%02d%02d.nbt', $partie_name, $numjou, $tour);
    }

    public static function getNbtFileName(string $partie_name, int $numjou, int $tour, $modifie = false): string
    {
        return sprintf('%s/%s', self::getNbtDirName($partie_name, $modifie), self::getCRNbtName($partie_name, $numjou ,$tour));
    }

    public static function getCRTxtFileName(string $partie_name, int $numjou, int $tour, $modifie = false)
    {
        return sprintf('%s/%s', self::getNbtDirName($partie_name, $modifie), self::getCRTxtName($partie_name, $numjou ,$tour));
    }

    public static function getNbtFilePath(string $partie_name, int $numjou, int $tour, $modifie = false)
    {
        $path = Storage::disk('data')->path(self::getNbtFileName($partie_name, $numjou, $tour, true));
        if (!file_exists($path)) {
            $path = Storage::disk('data')->path(self::getNbtFileName($partie_name, $numjou, $tour, $modifie));
        }
        return $path;
    }

    public static function getCRTxtFilePath(string $partie_name, int $numjou, int $tour)
    {
        return Storage::disk('data')->path(
            self::getCRTxtFileName($partie_name, $numjou, $tour,
                file_exists(Storage::disk('data')->path(self::getNbtFileName($partie_name, $numjou, $tour, true))))
        );
    }

    public static function getNbaFileName($partie_name, $tour)
    {
        return sprintf('%1$s/STOCK/%1$s%2$02d.nba', $partie_name, $tour);
    }

    public static function getNbaFilePath($partie_name, $tour)
    {
        return Storage::disk('data')->path(self::getNbaFileName($partie_name, $tour));
    }

    /**
     * Récupère le CR txt ou nbt. Contenu ou emplacement du fichier (doublon avec extractCR)
     * Ecriture dans un fichier = on conserve l'envodage "disque"
     * Ecriture dans chaine de caractère : on converti en utf-8
     * @param User $user joueur
     * @param string $type 'txt' ou 'nbt'
     * @param bool $file_output false : renvoie le contenu CR, true : renvoie le nom d'un fichier le contenant
     * @return string|null
     */
    public static function getCr(User $user, string $type, bool $file_output = false, bool $tousLesMondes = false, ?int $tour = null)
    {
        if (!isset($tour)) {
            $tour = $user->partie->tour;
        }

        // Si on a un fichier .nbt déjà présent on le retourne
        if ($type == 'nbt') {
            $nbtFileName = self::getNbtFilePath($user->partie->name, $user->numjou, $tour);
            if (file_exists($nbtFileName)) {
                if ($file_output) {
                    return $nbtFileName;
                }
                else {
                    return Utils::strConvertIn(file_get_contents($nbtFileName));
                }
            }
        }

        // ... sinon on part du fichier .nba
        $nbaFileName = self::getNbaFilePath($user->partie->name, $tour);

        if (!file_exists($nbaFileName)) {
            throw new \Exception("Fichier $nbaFileName non trouvé");
        }

        $numjou = $user->numjou;

        $cr = self::cmdNebulaCR($type, $nbaFileName, $numjou, $tousLesMondes);

        //On vérifie qu'on a un début de fichier .nbt
        if ($cr === '') {
            throw new \Exception("Impossible de générer le fichier .nbt partie {$user->partie_name} joueur {$user->numjou} tour {$tour} : CR vide");
        }

        if (!$file_output) {
            $cr = Utils::strConvertIn($cr);
            return $cr;
        }

        // Ici on retourne le nom du fichier contenant le CR
        if ($type === 'nbt') {
            // Si fichier .nbt les stocker dans le répertoire STOCK/NBT
            Storage::disk('data')->makeDirectory($user->getNbtDirName());
            $file_name = $nbtFileName;
        }
        else {
            $file_name = tempnam(sys_get_temp_dir(), 'Neb') . '.txt';
        }

        file_put_contents($file_name, $cr);
        return $file_name;
    }

    public static function convertCrToArray(string $cr): Collection
    {
        $cr = str_replace("\r", '', $cr);
        $cr = explode("Ordres du joueur", $cr)[0];
        preg_match_all('/(?:^### ?Informations? datant du tour (?<tour>\d+)\R)?(?<monde>(^".*?"\R)?^Md?[#_]?(?<numero>\d+)\s[\s\S]*?)(\R\s*?\R|\z)/mi', $cr, $matches, PREG_SET_ORDER);

        // return collect($matches)->map(fn ($e) => collect($e)->only('monde', 'tour', 'numero')->toArray());
        return collect($matches)->map(function ($e) {
            return [
                'monde' => $e['monde'],
                'tour' => (int) $e['tour'],
                'numero' => (int) $e['numero'],
            ];
        });
    }

    public static function cleanNbtFiles(Partie $partie)
    {
        $dir = $partie->getNbtDirName();
        $files = Storage::disk('data')->files($dir);
        foreach ($files as $file) {
            if (Str::lower(pathinfo($file, PATHINFO_EXTENSION)) === 'nbt') {
                Storage::disk('data')->delete($file);
            }
        }
    }

    public static function cleanNbtFile(User $user)
    {
        $filename = self::getNbtFileName($user->partie->name, $user->numjou, $user->partie->tour);

        if (Storage::disk('data')->exists($filename)) {
            Storage::disk('data')->delete($filename);
        }
    }

    private static function getOrdresCoequipiers(User $user, string $ordresUser):string
    {
        return $user->partie
            ->joueurs_sans_arbitre
            ->where('equipe', $user->equipe)
            ->where('id', '!=', $user->id)
            ->get()
            ->where('ordre', '!=', null)
            ->pluck('ordre')
            ->map(fn ($ordre) => "@Begin Player {$ordre->user->numjou}\n{$ordre->content}\n@End")
            ->prepend("@Begin Player {$user->numjou}\n{$ordresUser}\n@End")
            ->join("\n");
    }

    /**
     * Effectue une simulation d'éval du joueur avec les ordres passés en paramètre
     * La simulation s'effectue sur la base du fichier .nbt stocké sur le serveur
     *
     * @param  User $user
     * @param  string $ordres
     * @return string
     */
    public static function simulOrdres(User $user, string $ordres, bool $creeNbt = false): string
    {
        // Extraction fichier .nbt
        $crNbt = Cr::getCr($user, 'nbt', true);

        if (!isset($crNbt)) {
            // TODO générer une exception
            return null;
        }

        $genNbt = $creeNbt ? '-genNBT' : '';

        // Partie par équipes => on récupère les ordres des coéquipiers
        if ($user->equipe) {
            $ordres = Self::getOrdresCoequipiers($user, $ordres);
        }

        // Sauvegarde des ordres dans un fichier temporaire
        $temp_file = tempnam(sys_get_temp_dir(), 'Neb');
        file_put_contents($temp_file, $ordres);

        // Simulation d'éval
        $commande = config('nebmail.eval') . " $genNbt $crNbt $temp_file";
        Log::debug(__METHOD__, ['commande' => $commande]);
        exec($commande, $output, $err);

        $simul = Utils::strConvertIn(implode("\n", $output));

        // Nettoyage
        unlink($temp_file);

        return $simul;
    }

    public static function checkOrdres(User $user, string $ordres)
    {
        // Ici on retourne la liste des ordres non exécutés ainsi que le total des ordres envoyés
        $ordresChecked = [
            'nbordres' => 0,
            'erreurs'  => []
        ];

        $simul = self::simulOrdres($user, $ordres);

        $matches = [];

        // On récupère le nombre d'ordres
        if (preg_match("/Nombre d'ordres : (\d+)/s", $simul, $matches)) {
            $ordresChecked['nbordres'] = $matches[1];
        }

        // On extrait les ordres non exécutés du CR
        if (preg_match("/(L'ordre suivant n'a pu être exécuté|Les ordres suivants n'ont pu être exécutés) :\n(.*)Nombre d'ordres :/s", $simul, $matches)) {
            $ordresKO = array_filter(explode("\n", $matches[2]), fn($value) => !is_null($value) && $value !== '' && $value !== "\n");

            foreach  ($ordresKO as $ordreKO) {
                if (str_contains($ordreKO, ' <- ')) {
                    $tmp = explode(' <- ', $ordreKO);
                    $ordresChecked['erreurs'][$tmp[1]][] = $tmp[0];
                }
            }
        }

        return $ordresChecked;
    }
}
