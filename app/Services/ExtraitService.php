<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Extrait;
use Illuminate\Support\Collection;

class ExtraitService
{
    public static function getExtraits(string $extraitsAfter, User $user): array
    {
        return Extrait::select(['tour', 'monde_id as numero', 'content'])
            ->whereBelongsTo($user)
            ->where('updated_at', '>', $extraitsAfter)
            ->get()
            ->sortBy(['tour', 'numero'])
            ->toArray();
    }
}
