<?php


namespace App\Services\Files;


class Joueur
{
    private $attributes = array();

    public $attrNames = array(
        'numjou',
        'pseudo',
        'email',
        'password',
        'codesm',
        'cc',
        'rumeurs',
        'msgGen'
    );
    private $relations = array();

    function __construct(array $attributes, $relations = null)
    {
        // On vérifie que tous les attribus attendus sont bien présents
        foreach ($this->attrNames as $attrName) {
            if (!key_exists($attrName, $attributes)) {
                throw new \Exception('Attribut $attrName non trouvé dans Joueur');
            }
        }

        $this->attributes = $attributes;
        $this->attributes['cc'] = ($attributes['cc'] === 'Y');
        $this->attributes['rumeurs'] = ($attributes['rumeurs'] === 'Y');
        $this->attributes['msgGen'] = ($attributes['msgGen'] === 'Y');

        if (isset($relations)) {
            $this->relations = $relations;
        }
    }

    function get($attrName)
    {
        return $this->attributes[$attrName];
    }

//    function getRelations()
//    {
//        return $this->relations;
//    }

}
