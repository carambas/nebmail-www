<?php


namespace App\Services\Files;


use App\Services\Utils;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Joueurs
{

    private array $joueurs;
    private array $relations = [];
    private $partie;
    private $dernierTour;

    function __construct($partie)
    {
        $this->partie = $partie;
        $this->getJoueursFromFile();
        $this->getConfFromFile();
    }

    public function get($numjou = null)
    {
        if (isset($numjou))
            return $this->joueurs[$numjou];
        else
            return $this->joueurs;
    }

    public function getNbJou()
    {
        if (isset($this->joueurs)) {
            return max(array_keys($this->joueurs));
        }

        return 0;
    }

    public function exists($numjou) {
        if (!isset($this->joueurs))
            return false;

        return in_array($numjou, array_keys($this->joueurs));
    }

    private function getJoueursFromFile()
    {

        $this->getRelationsFromFile();
        $joueurs = $this->getPartieFileContentArray($this->partie);

        foreach ($joueurs as $joueur) {
            $relations = array();
            $numjou = $joueur['numjou'];
            if (isset($this->relations) && array_key_exists($numjou, $this->relations)) {
                $relations = $this->relations[$numjou];
            }
            $this->joueurs[$joueur['numjou']] = new \App\Services\Files\Joueur($joueur, $relations);
        }

        Log::debug("Joueurs : récupération par lecture du fichier");

    }

    private function getRelationsFromFile()
    {
        Log::debug("Joueurs.relations : récupération dans le fichier");
        $relations = array();
        $contents = $this->getRelationsFileContents($this->partie);

        if (isset($contents)) {
            $pattern = "/^(\d+)\|(.*$)/m";
            preg_match_all($pattern, $contents, $matches);

            foreach($matches[1] as $key => $jou) {
                $relations[$jou] = explode(':', chop($matches[2][$key]));
            }
        }

        $this->relations = $relations;
    }

    private function getConfFromFile()
    {

//        $contents = $this->getConfFileContents($this->partie);
//
//        $pattern = "/DernierTourEnStock=(\d+)/m";
//        preg_match($pattern, $contents, $matches);
//        $this->dernierTour = chop($matches[1]);
//
//        Log::debug("Joueurs.dernierTour : récupération par lecture du fichier");
//        Session::put('dernierTour', $this->dernierTour);
    }

    public function getDernierTour()
    {
        return $this->dernierTour;
    }


    static function getPartieFileContents($partie)
    {
        $res = null;
        try {
            $res = Utils::strConvertIn(Storage::disk('data')->get("$partie/$partie"));
        } finally {
            return $res;
        }
    }

    static function getRelationsFileContents($partie)
    {
        if (Storage::disk('data')->exists("$partie/RELAT")) {
            return Storage::disk('data')->get("$partie/RELAT");
        }

        return null;
    }

    static function getConfFileContents($partie)
    {
        return Storage::disk('data')->get("$partie/CONF");
    }

    static function getPartieFileContentArray($partie)
    {
        $joueurs = array();

        $contents = Joueurs::getPartieFileContents($partie);

        $pattern = "/^(\d+:.*)$/m";
        preg_match_all($pattern, $contents, $matches);

        foreach ($matches[0] as $jou) {
            $ligne = rtrim($jou);
            list($numjou, $pseudo, $email, $password, $codesm, $cc, $rumeurs, $msgGen) = explode(':', $ligne);
            if (! is_numeric($codesm)) {
                $codesm = 0;
            }
            $joueurs[] = compact('numjou', 'pseudo', 'email', 'password', 'codesm', 'cc', 'rumeurs', 'msgGen');
        }
        return $joueurs;
    }

    public function estEnRelation($numjou1, $numjou2)
    {
        return in_array($numjou2, $this->relations[$numjou1]);
    }

    public function getRelations($numjou = null)
    {
        if (isset($numjou))
            return $this->relations[$numjou];

        return $this->relations;
    }

}
