<?php

namespace App\Services\Files;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class Parties
{
    private array $parties;

    public function __construct()
    {
        $this->parties = array();
        $contents = Storage::disk('data')->get('PARTIES');
        preg_match_all('/^(.*)$/m', $contents, $matches);
        foreach ($matches[0] as $ligne) {
            list ($nompartie, $password, $codesm, $statut) = explode(':', $ligne);

            if (! is_numeric($codesm)) {
                $codesm = 0;
            }

            if ($nompartie !== 'MELUZINE') {
                // Lecture fichier CONF
                if (Storage::disk('data')->exists("$nompartie/CONF")) {
                    $contents = Storage::disk('data')->get("$nompartie/CONF");

                    preg_match('/TourFinal=(\d+)/', $contents, $matches2);
                    $tourFinal = $matches2[1] ?? null;

                    preg_match('/DernierTourEnStock=(\d+)/', $contents, $matches2);
                    $tour = $matches2[1];
                }
                else {
                    $tourFinal = null;
                    $tour = 0;
                }


                $partie = [
                    'nom' => $nompartie,
                    'password' => $password,
                    'codesm' => $codesm,
                    'statut' => $statut,
                    'tour' => $tour,
                    'tour_final' => $tourFinal,
                ];
                $this->parties[] = $partie;
            }

        }
        Log::debug("Parties : récupération par lecture du fichier");
    }

    public function getParties()
    {
        Log::debug(__METHOD__);
        return $this->parties;
    }

    public function getPartiePassword()
    {
        if (Auth::check()) {
            $partieName = Auth::user()->partie_name;
            foreach ($this->parties as $partie) {
                if ($partie['nom'] === $partieName) {
                    return $partie['password'];
                }
            }
        }
        return null;
    }

    public function getPartiesName($displayHidden = false)
    {
        Log::debug(__METHOD__);
        $partiesName = array();
        foreach ($this->parties as $partie) {
            if (($partie['statut'] != 'H') || ($displayHidden))
                $partiesName[] = $partie['nom'];
        }
        return $partiesName;

    }

    static public function countOrdres($partie)
    {
        // $files contient la liste des fichiers d'ordres rendus afin de les compter
        $files = Storage::disk('data')->files($partie);
        $files = preg_grep('|.*/\d+|', $files);

        return count($files);

    }

    public function exists($partie)
    {
        foreach ($this->parties as $p) {
            if ($p['nom'] == $partie) {
                return true;
            }
        }

        return false;
    }

}
