<?php

namespace App\Services\NebData;

use App\Services\Utils;
use Illuminate\Support\Facades\Validator;

class Flotte
{
    public int $numero;
    public int $localisation;
    public int $vc, $vt;
    public int $mp, $populationsNonConverties, $populationsConverties, $robots;
    public int $proprietaire, $ancienProprietaire;
    public int $ordreExclusif;
    public int $cible;
    public bool $isEnPaix, $isTransporteBombe, $isCapturee, $isPiratee, $isCadeau, $isEmbuscade, $isDechagePc, $isExploration;
    public array $chemin;


    protected $validate = [
        'F' => ['regex:/^[0-9,]*$/', 'required'],
    ];

    public function fillFromNba($numFlotte, $rawNba)
    {
        $validator = Validator::make($rawNba, $this->validate);
        if ($validator->stopOnFirstFailure()->fails()) {
            $errorsStr = implode(' ', $validator->errors()->all());
            throw(new \Exception("Erreur dans la lecture du fichier .nba pour la flotte $numFlotte : $errorsStr"));
        };

        $this->numero = $numFlotte;

         list(
             $this->localisation,
             $this->vc,
             $this->vt,
             $this->mp,
             $this->populationsNonConverties,
             $this->populationsConverties,
             $this->robots,
             $this->proprietaire,
             $this->ancienProprietaire,
             $this->ordreExclusif,
             $this->cible,
             $this->isEnPaix,
             $this->isTransporteBombe,
             $this->isCapturee,
             $this->isPiratee,
             $this->isCadeau,
             $this->isEmbuscade,
             $this->isDechagePc,
             $this->isExploration,
            ) = Utils::commaStringToArrayOfInt($rawNba['F'], 19);

        $this->chemin = Utils::commaStringToArrayOfInt($rawNba['Chemin'] ?? '');
    }

    public function getVaisseaux() {
        return $this->vc + $this->vt;
    }

    public function getCargaison() {
        return $this->mp + $this->populationsNonConverties + $this->populationsConverties;
    }

}
