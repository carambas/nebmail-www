<?php

namespace App\Services\NebData;

use App\Services\Utils;

class Joueur
{
    public int $numero;
    public string $pseudo = '', $nom = '';
    public array $email = [];
    public int $md = 0;
    public int $equipe = 0;
    public int $classeId = 0;
    public int $tourJihad = 0, $numJouJihad = 0;
    public int $dep = 0, $att = 0, $def = 0, $rad = 0, $ali = 0, $car = 0, $depReste = 0, $attReste = 0, $defReste = 0, $radReste = 0, $aliReste = 0, $carReste = 0;
    public int $ca = 50, $cd = 100, $score = 0;
    public array $connuDeNom = [], $contacts = [], $allies = [], $chargeurs = [], $chargeursPop = [], $dechargeursPop = [], $pilleurs = [], $detailsScore = [];

    public string $equipeStr; // Liste des joueurs de l'équipe. Sert à déterminer le numéro d'équipe dans un 2ème temps.


    protected $validate = [
        'Coord' => ['regex:/^\d+,\d+$/'],
        'C' => ['regex:/^[0-9,]*$/'],
        'M' => ['regex:/^[0-9,]*$/', 'required'],
        'ME' => ['regex:/^[0-9,]*$/'],
        'Connu' => ['regex:/^[0-9,]*$/'],
        'AVuConnect' => ['regex:/^[0-9,]*$/'],
        'AVuCoord' => ['regex:/^[0-9,]*$/'],
        'StationMonde' => ['regex:/^[0-9,]*$/'],
        '1erPr' => ['string'],
        'Possession' => ['regex:/^[0-9,]*$/'],
    ];


    public function fillFromNba(int $numJou, array $rawNba, string $rawNbaEncoding = 'utf-8')
    {
        // TODO valider le contenu de rawNbaEncoding;

        $this->pseudo = Utils::strConvertIn($rawNba['Pseudo'], $rawNbaEncoding);
        $this->nom = Utils::strConvertIn($rawNba['Nom'] ?? '', $rawNbaEncoding);
        $this->email = explode(',', Utils::strConvertIn($rawNba['Email'] ?? '', $rawNbaEncoding));
        $this->md = (int) ($rawNba['Md'] ?? '');
        $this->equipeStr = $rawNba['Equipe'] ?? '';

        list(
            $this->classeId,
            $this->tourJihad,
            $this->numJouJihad,
            $this->dep,
            $this->att,
            $this->def,
            $this->rad,
            $this->ali,
            $this->car,
            $this->depReste,
            $this->attReste,
            $this->defReste,
            $this->radReste,
            $this->aliReste,
            $this->carReste,
            $this->ca,
            $this->cd,
            $this->score
        ) = Utils::commaStringToArrayOfInt($rawNba['J'] ?? '', 18);

        if ($this->ca === 0) {
            $this->ca = 50;
        }

        if ($this->cd === 0) {
            $this->cd = 100;
        }

        $this->connuDeNom = Utils::commaStringToArrayOfInt($rawNba['ConNom'] ?? '');
        $this->contacts = Utils::commaStringToArrayOfInt($rawNba['Contact'] ?? '');
        $this->allies = Utils::commaStringToArrayOfInt($rawNba['Allies'] ?? '');
        $this->chargeurs = Utils::commaStringToArrayOfInt($rawNba['Chargeurs'] ?? '');
        $this->chargeursPop = Utils::commaStringToArrayOfInt($rawNba['ChargeursPop'] ?? '');
        $this->dechargeursPop = Utils::commaStringToArrayOfInt($rawNba['DechargeursPop'] ?? '');
        $this->pilleurs = Utils::commaStringToArrayOfInt($rawNba['Pilleurs'] ?? '');
        $this->detailsScore = Utils::commaStringToArrayOfInt($rawNba['DetailScore'] ?? '');
    }
}
