<?php

namespace App\Services\NebData;

use App\Services\Utils;
use Illuminate\Support\Facades\Validator;

class Monde
{
    public int $numero;
    public string|null $nom;
    public int $duree = 0, $proprietaireConvertis = 0, $proprietaire = 0, $ancienProprietaire = 0, $premierProprietaire = 0;
    public int $industries = 0, $industriesActives = 0, $vi = 0, $actionVi = 0, $cibleVi = 0;
    public int $populations = 0, $convertis = 0, $robots = 0, $limitePopulation = 0, $vp = 0, $actionVp = 0, $cibleVp = 0, $populationsMortes = 0, $convertisMorts = 0, $robotsDetruits = 0;
    public int $capaciteMiniere = 0, $nbDechargementPc = 0, $nbPcDecharges = 0, $mp = 0, $populationsDisponibles = 0;
    public int $nbPillage = 0, $toursRecupPillage = 0;
    public bool $isCapture = false, $isCadeau = false, $isTrouNoir = false, $isBombe = false, $isPille = false, $isCorrompu = false;
    public int $x = 0, $y = 0;
    public int $nbExplo = 0, $nbExploCm = 0, $nbExploPop = 0, $nbExploLocalisation = 0, $potentielExploration = 0;
    public bool $isLoc=false, $isObs=false, $isVis=false;
    public array $connexions = [], $connexionsVues = [], $aStationne = [], $estVisible = [], $aVuCoord = [], $possessionHisto = [];

    protected $validate = [
        'Coord' => ['regex:/^\d+,\d+$/'],
        'C' => ['regex:/^[0-9,]*$/'],
        'M' => ['regex:/^[0-9,]*$/'],
        'ME' => ['regex:/^[0-9,]*$/'],
        'Explo' => ['regex:/^[0-9,]*$/'],
        'Connu' => ['regex:/^[0-9,]*$/'],
        'AVuConnect' => ['regex:/^[0-9,]*$/'],
        'AVuCoord' => ['regex:/^[0-9,]*$/'],
        'StationMonde' => ['regex:/^[0-9,]*$/'],
        '1erPr' => ['int'],
        'Possession' => ['regex:/^[0-9,]*$/'],
        'Nom' => ['string'],
    ];

    public function fillFromNba($numMonde, $rawNba, string $rawNbaEncoding = 'utf-8')
    {
        $validator = Validator::make($rawNba, $this->validate);
        if ($validator->stopOnFirstFailure()->fails()) {
            $errorsStr = implode(' ', $validator->errors()->all());
            throw(new \Exception("Erreur dans la lecture du fichier .nba pour le monde $numMonde : $errorsStr"));
        };

        $this->numero = $numMonde;

        list ($this->x, $this->y) = Utils::commaStringToArrayOfInt($rawNba['Coord'] ?? '0,0');
        $this->connexions = Utils::commaStringToArrayOfInt($rawNba['C'] ?? '');
        $this->nom = Utils::strConvertIn($rawNba['Nom'] ?? null, $rawNbaEncoding);

        // Monde
        list(
            $this->duree,
            $this->proprietaireConvertis,
            $this->capaciteMiniere,
            $this->nbDechargementPc,
            $this->proprietaire,
            $this->industries,
            $this->populations,
            $this->convertis,
            $this->robots,
            $this->limitePopulation,
            $this->mp,
            $this->vi,
            $this->vp,
            $this->nbPillage,
            $this->toursRecupPillage,
            $this->ancienProprietaire,
            $this->industriesActives,
            $this->populationsDisponibles,
            $this->isCapture,
            $this->isCadeau,
            $this->isTrouNoir,
            $this->isBombe,
            $this->isPille,
            $this->isCorrompu
            ) = Utils::commaStringToArrayOfInt($rawNba['M'] ?? '', 24);

        // Événements
        list(
            $this->actionVi,
            $this->actionVp,
            $this->cibleVi,
            $this->cibleVp,
            $this->populationsMortes,
            $this->convertisMorts,
            $this->robotsDetruits,
            $this->nbPcDecharges
        ) = Utils::commaStringToArrayOfInt($rawNba['ME'] ?? '', 8);

        // Explo
        list(
            $this->nbExplo,
            $this->nbExploCm,
            $this->nbExploPop,
            $this->nbExploLocalisation,
            $this->isLoc,
            $this->isObs,
            $this->isVis,
            $this->potentielExploration
        ) = Utils::commaStringToArrayOfInt($rawNba['Explo']??'',8);

        $this->estVisible = Utils::commaStringToArrayOfInt($rawNba['Connu'] ?? '');
        $this->connexionsVues = Utils::commaStringToArrayOfInt($rawNba['AVuConnect'] ?? '');
        $this->aVuCoord = Utils::commaStringToArrayOfInt($rawNba['AVuCoord'] ?? '');
        $this->aStationne = Utils::commaStringToArrayOfInt($rawNba['StationMonde'] ?? '');
        $this->premierProprietaire = $rawNba['1erPr'] ?? 0;
        $this->possessionHisto = Utils::commaStringToArrayOfInt($rawNba['Possession'] ?? '');
    }
}
