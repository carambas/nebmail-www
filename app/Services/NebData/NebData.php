<?php

namespace App\Services\NebData;

use Carbon\Carbon;
use App\Services\Utils;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class NebData
{
    private string $rawNbaEncoding = 'utf-8';
    public string $nomPartie;
    public int $numJoueur = 0, $nbMondes, $nbJoueurs, $nbFlottes;
    public int $longX, $longY;
    public array $equipes = [];
    public int $tour;
    public int|null $tourFinal;
    public string $nomArbitre, $emailArbitre, $passwordPartie;
    public string|null $dateDebut;
    public array $mondesDepartsSupplementaires;
    public bool $tourVerrouille, $piratageMultiClasse, $affichageScore, $bombesInterdites, $mevaPourTous, $diplomatieInterdite, $pillagesInterdits, $tirIndustriesInterdits, $emigrationSansHasard, $partieCruel;
    public array $joueurs = [];
    public array $mondes = [];
    public array $flottes = [];
    public array $tresors = [];

    protected $validate = [
        'NomPartie' => ['string', 'required', 'max:6'],
        'NbMondes' => ['int', 'required', 'max:1024'],
        'NbJou' => ['int', 'required', 'max:32'],
        'NbFlottes' => ['int', 'required', 'max:1024'],
        'Tour' => ['int', 'required'],
        'TourFinal' => ['int'],
        'DateDebut' => ['date_format:d/m/Y'],
        'NoJoueur' => ['int', 'max:32'],
        'TaillePlanX' => ['int', 'required', 'max:32'],
        'TaillePlanY' => ['int', 'required', 'max:32'],
        'Mds' => ['regex:/^[0-9,]*$/'],
        'Lock' => ['int'],
        'PiratageMultiClasse' => ['int'],
        'AffichageScore' => ['int', 'min:0', 'max:1'],
        'BombesInterdites' => ['int', 'min:0', 'max:1'],
        'MEVAPourTous' => ['int', 'min:0', 'max:1'],
        'DiploInterdite' => ['int', 'min:0', 'max:1'],
        'PillagesInterdits' => ['int', 'min:0', 'max:1'],
        'TirIndustries' => ['int', 'min:0', 'max:2'],
        'EmigrationSansHasard' => ['int', 'min:0', 'max:1'],
        'PartieCRUEL' => ['int', 'min:0', 'max:1'],
    ];

    public function __construct(
        public ?string $fileName = null,
        public ?string $fileContent = null
    ) {
        if ($this->fileName && !file_exists($this->fileName)) {
            $this->fileName = Storage::disk('data')->path($this->fileName);
        }

        $this->parseNba();
    }

    private function parseGeneral($rawGeneral)
    {
        $validator = Validator::make($rawGeneral, $this->validate);
        if ($validator->stopOnFirstFailure()->fails()) {
            $errorsStr = implode(' ', $validator->errors()->all());
            throw (new \Exception("Erreur de format du fichier " . basename($this->fileName) . ' : ' . $errorsStr));
        };

        $this->nomPartie = $rawGeneral['NomPartie'];
        $this->nbJoueurs = $rawGeneral['NbJou'];
        $this->nbMondes = $rawGeneral['NbMondes'];
        $this->nbFlottes = $rawGeneral['NbFlottes'];
        $this->longX = $rawGeneral['TaillePlanX'];
        $this->longY = $rawGeneral['TaillePlanY'];
        $this->tour = $rawGeneral['Tour'];
        $this->tourFinal = $rawGeneral['TourFinal'] ?? null;
        $this->emailArbitre = Utils::strConvertIn($rawGeneral['EmailArbitre'], $this->rawNbaEncoding);
        $this->passwordPartie = $rawGeneral['PasswordPartie'] ?? '';
        $this->dateDebut = $rawGeneral['DateDebut'] ?? null;
        $this->numJoueur = $rawGeneral['NoJoueur'] ?? 0;
        $this->nomArbitre = $rawGeneral['NomArbitre'];
        $this->mondesDepartsSupplementaires = Utils::commaStringToArrayOfInt($rawGeneral['Mds']);
        $this->tourVerrouille = $rawGeneral['Lock'] ?? 0;
        $this->piratageMultiClasse = $rawGeneral['PiratageMultiClasse'];
        $this->affichageScore = $rawGeneral['AffichageScore'];
        $this->bombesInterdites = $rawGeneral['BombesInterdites'];
        $this->mevaPourTous = $rawGeneral['MEVAPourTous'];
        $this->diplomatieInterdite = $rawGeneral['DiploInterdite'];
        $this->pillagesInterdits = $rawGeneral['PillagesInterdits'];
        $this->tirIndustriesInterdits = !$rawGeneral['TirIndustries'];
        $this->emigrationSansHasard = $rawGeneral['EmigrationSansHasard'];
        $this->partieCruel = $rawGeneral['PartieCRUEL'];
    }

    public function parseNba()
    {
        $raw_content = $this->fileContent ?? file_get_contents($this->fileName);
        $this->rawNbaEncoding = mb_detect_encoding($raw_content, ['UTF-8', 'ISO-8859-1'], false);

        $content = parse_ini_string($raw_content, true,  INI_SCANNER_RAW);

        $this->parseGeneral($content['GENERAL']);

        $joueurs = [];
        $equipes = collect([]);
        $numequipe = 1;

        foreach ($content as $key => $rawNba) {
            // Joueur
            if (preg_match('/J_(\d+)/', $key, $matches)) {
                $numJou = $matches[1];
                ($joueur = new Joueur)->fillFromNba($numJou, $rawNba, $this->rawNbaEncoding);

                // transformation equipeStr -> numéro d"equipe
                if (isset($joueur->equipeStr) && mb_strpos($joueur->equipeStr, ',') !== false) {
                    // On enregistre l'équipe et on génère son numéro si pas déjà connue
                    if (!$equipes->has($joueur->equipeStr)) {
                        $equipes->put($joueur->equipeStr, $numequipe++);
                    }
                }

                $joueurs[$numJou] = $joueur;
            }

            // Monde
            if (preg_match('/M_(\d+)/', $key, $matches)) {
                $numMonde = $matches[1];
                ($monde = new Monde)->fillFromNba($numMonde, $rawNba, $this->rawNbaEncoding);
                $this->mondes[$numMonde] = $monde;
            }

            // Flotte
            if (preg_match('/F_(\d+)/', $key, $matches)) {
                $numFlotte = $matches[1];
                ($flotte = new Flotte)->fillFromNba($numFlotte, $rawNba);
                $this->flottes[$numFlotte] = $flotte;
            }

            // Trésor
            if (preg_match('/T_(\d+)/', $key, $matches)) {
                $numTresor = $matches[1];
                ($tresor = new Tresor)->fillFromNba($numTresor, $rawNba, $this->rawNbaEncoding);
                $this->tresors[$numTresor] = $tresor;
            }
        }

        // On reparcourt la liste des équipes pour mise à jour des joueurs
        $equipes->each(function ($numequipe, $equipeStr) use (&$joueurs) {
            foreach (explode(',', $equipeStr) as $numjou) {
                if (isset($joueurs[$numjou])) {
                    $joueurs[$numjou]->equipe = $numequipe;
                }
            }
        });
        $this->joueurs = $joueurs;
    }

    public static function parseNbaGetJoueurs(string $fileName)
    {
        $raw_content = file_get_contents($fileName);
        $encoding = mb_detect_encoding($raw_content, ['UTF-8', 'ISO-8859-1'], false);

        $content = parse_ini_file($fileName, true,  INI_SCANNER_RAW);

        $nbjou = $content['GENERAL']['NbJou'];
        if (!isset($nbjou)) return null;

        $joueurs = [];
        $equipes = collect([]);
        $numequipe = 1;

        for ($noJou = 1; $noJou <= $nbjou; $noJou++) {

            if (!isset($content["J_$noJou"]['Pseudo']) || !isset($content["J_$noJou"]['Email'])) return null;

            // Recensement des équipes de joueurs
            $equipeStr = $content["J_$noJou"]['Equipe'];
            if (isset($equipeStr) && mb_strpos($equipeStr, ',') !== false) {
                // On enregistre l'équipe et on génère son numéro si pas déjà connue
                if (!$equipes->has($equipeStr)) {
                    $equipes->put($equipeStr, $numequipe++);
                }
            }

            $joueurs[$noJou] = [
                'pseudo' => mb_convert_encoding($content["J_$noJou"]['Pseudo'], config('nebmail.site_charset'), $encoding),
                'email' => explode(',', $content["J_$noJou"]['Email']),
            ];
        }

        // On reparcourt la liste des équipes pour mise à jour des joueurs
        $equipes->each(function ($numequipe, $equipeStr) use (&$joueurs) {
            foreach (explode(',', $equipeStr) as $numjou) {
                $joueurs[$numjou]['equipe'] = $numequipe;
            }
        });

        return $joueurs;
    }

    public static function parseNbaGetPartie(string $fileName)
    {
        $raw_content = file_get_contents($fileName);
        $encoding = mb_detect_encoding($raw_content, ['UTF-8', 'ISO-8859-1'], false);

        $content = parse_ini_file($fileName, true,  INI_SCANNER_RAW);

        return $content['GENERAL'];
    }

    public function isMd(int $num_monde): bool
    {
        return collect($this->joueurs)->pluck('md')->contains($num_monde);
    }

    public function countVaisseauxEtFlottes(): Collection
    {
        $vcmondes = collect($this->mondes)
            ->filter(fn ($monde) => $monde->vi + $monde->vp > 0)
            ->map(function ($monde) {
                return [
                    'localisation' => $monde->numero,
                    'num_jou' => $monde->proprietaire,
                    'vc' => $monde->vi + $monde->vp,
                    'vt' => 0,
                    'vaisseaux' => $monde->vi + $monde->vp,
                    'flottes' => 0,
                ];
            });

        $vais_flottes = collect($this->flottes)
            ->map(function ($flotte) {
                return [
                    'localisation' => $flotte->localisation,
                    'num_jou' => $flotte->proprietaire,
                    'vc' => $flotte->vc,
                    'vt' => $flotte->vt,
                    'vaisseaux' => $flotte->vc + $flotte->vt,
                    'flottes' => 1,
                ];
            });

        return $vcmondes
            ->concat($vais_flottes)
            ->groupBy(['localisation', 'num_jou'])
            ->map(function ($item, $key) {
                // On est au niveau monde
                return collect($item)->map(function ($item) {
                    // On est au niveau joueur
                    return $item->reduce(function ($carry, $item) {
                        $result = [
                            // 'localisation' => $item['localisation'],
                            // 'num_jou' => $item['num_jou'],
                            'vc' => ($carry['vc'] ?? 0) + $item['vc'],
                            'vt' => ($carry['vt'] ?? 0) + $item['vt'],
                            'vaisseaux' => ($carry['vaisseaux'] ?? 0) + $item['vaisseaux'],
                            'flottes' => ($carry['flottes'] ?? 0) + $item['flottes'],
                        ];

                        return $result;
                    });
                })->sortKeys();
            })->sortKeys();

        // return $vcmondes;
    }

    public function getPseudo(int $num_jou): string
    {
        return $num_jou > 0 ? $this->joueurs[$num_jou]->pseudo : 'neutre';
    }

    public function getCapaciteFlotte(int $num_fl): int
    {
        $flotte = $this->flottes[$num_fl];
        $car = $this->joueurs[$flotte->proprietaire]->car ?? 1;

        return $flotte->vc + $flotte->vt * $car;
    }
}
