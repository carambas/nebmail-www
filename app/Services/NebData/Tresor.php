<?php

namespace App\Services\NebData;

use App\Services\Utils;
use Illuminate\Support\Facades\Validator;

class Tresor
{
    public int $numero;
    public int $proprietaire;
    public int $localisation;
    public bool $sur_monde, $sur_flotte;
    public string $nom;

    protected $validate = [
        'T' => ['regex:/^[0-9]*,[FM],[0-9]+$/', 'required'],
        'Nom' => ['string', 'required'],
    ];

    public function fillFromNba($numTresor, $rawNba, string $rawNbaEncoding = 'utf-8')
    {
        $validator = Validator::make($rawNba, $this->validate);
        if ($validator->stopOnFirstFailure()->fails()) {
            $errorsStr = implode(' ', $validator->errors()->all());
            throw(new \Exception("Erreur dans la lecture du fichier .nba pour le tresor $numTresor : $errorsStr"));
        };

        $this->numero = $numTresor;


        list(
            $proprietaire,
            $flotte_ou_monde,
            $this->localisation,
        ) = Utils::commaStringToArray($rawNba['T'], 3);

        $this->proprietaire = (int) $proprietaire;
        $this->sur_monde = $flotte_ou_monde === 'M';
        $this->sur_flotte = $flotte_ou_monde === 'F';

        $this->nom = Utils::strConvertIn($rawNba['Nom'], $rawNbaEncoding);
    }
}
