<?php

namespace App\Services;

use App\Models\User;
use App\Services\Cr;
use Illuminate\Support\Str;
use App\Services\NebData\NebData;
use Illuminate\Support\Collection;

class OrdresService
{
    static public function getCountOrdresRendusEquipe(User $user): Collection
    {
        if (!$user->equipe)
            return collect();

        return $user->coequipiers()->withCount('ordre')->get();
    }

    static public function generateCartouche(User $user)
    {
        $tour = $user->partie->tour + 1;
        $ligne1 = "Ordres du joueur \"{$user->pseudoWithNumJou}\" pour le tour $tour";
        $ligne2 = "Ordres saisis sur le site nebmail";
        if (mb_strlen($ligne1) > (mb_strlen($ligne2))) {
            $ligne2 .= str_repeat(' ', mb_strlen($ligne1) - mb_strlen($ligne2));
        } else {
            $ligne1 .= str_repeat(' ', mb_strlen($ligne2) - mb_strlen($ligne1));
        }

        $ligne1 = "# $ligne1 #";
        $ligne2 = "# $ligne2 #";
        $ligne_bord = str_repeat('#',mb_strlen($ligne1));

        return ("$ligne_bord\n$ligne1\n$ligne2\n$ligne_bord\n\n");
    }

    static public function generateSquelette(User $user)
    {
        $crtxt = Cr::getCr(user: $user, type: 'txt');
        $crs = Cr::convertCrToArray($crtxt);

        $nbtFileName = CR::getCr(user: $user, type: 'nbt', file_output: true);
        $nebdata = new NebData($nbtFileName);
        $vais_fl = $nebdata->countVaisseauxEtFlottes();

        return $crs->map(function ($cr) use ($user, $nebdata, $vais_fl) {
            $num_monde = (int) $cr['numero'];
            return Str::of($cr['monde'])
                ->split('/\n/')
                ->prepend(Str::of("Monde $num_monde (@M_$num_monde@) "))
                ->map(fn ($ligne) => Str::of($ligne)->prepend('; ')) // On préfixe chaque ligne du CR par "; "
                ->push(self::squeletteOrdresMondes($user, $nebdata, $vais_fl[$num_monde] ?? collect([]), $num_monde))
                ->when(isset($vais_fl[$num_monde]), fn($c) => $c->push(self::squeletteStatsVaisseauxFlottes($nebdata, $vais_fl[$num_monde])))
                ->push("\n\n")
                ->join("\n");
        })
        ->push("; Autres ordres (@M_0@)\n\n")
        ->join("\n");

    }

    static private function squeletteOrdresMondes(User $user, NebData $nebdata, Collection $vais_fl, int $num_monde): string
    {
        $numjou = $user->numjou;
        $lignes = "";

        if ($nebdata->mondes[$num_monde]->proprietaire === $numjou) {
            // Constructions
            $lignes .= $nebdata->mondes[$num_monde]->industriesActives > 0 ? "\nM_$num_monde C {$nebdata->mondes[$num_monde]->industriesActives} " : '';
        }

        collect($nebdata->flottes)
            ->where('localisation', $num_monde)
            ->where('proprietaire', $numjou)
            ->each(function ($fl) use (&$lignes, $vais_fl, $numjou, $nebdata) {
                $lignes .= "\nF_{$fl->numero} -> ";

                // Transferts sur flottes vides
                if (($fl->getVaisseaux() === 0) && ($vais_fl[$numjou]['vaisseaux'] > 0)) {
                    $lignes .= "\nF_ T x VC F_{$fl->numero}";
                }

                // Déchargements
                $lignes .= $fl->mp > 0 ? "\nF_{$fl->numero} D x MP" : '';
                $lignes .= $fl->populationsNonConverties > 0 ? "\nF_{$fl->numero} D x N" : '';
                $lignes .= $fl->populationsConverties > 0 ? "\nF_{$fl->numero} D x C" : '';
                $lignes .= $fl->robots > 0 ? "\nF_{$fl->numero} D x R" : '';

                // Chargements
                if ($fl->getCargaison() < $nebdata->getCapaciteFlotte($fl->numero)) {
                    $lignes .= "\nF_{$fl->numero} C x MP";
                }
            });

        return $lignes;

    }

    static private function squeletteStatsVaisseauxFlottes(NebData $nebdata, Collection $vais_fl): string
    {
        if (! isset($vais_fl))
            return '';

        $lignes = "";

        // Récap statistiques VC VT et flottes
        foreach($vais_fl as $num_jou => $j) {

            $ligne = '# ' . $nebdata->getPseudo((int) $num_jou);
            for ($i = mb_strlen($ligne); $i < 40; $i++) { // Str::padRight ne gère pas les caractères multibyte
                $ligne .= ' ';
            }

            if ($j['vc'] > 0) {
                $ligne .= $j['vc'] . ' VC ';
            }

            if ($j['vt'] > 0) {
                $ligne .= $j['vt'] . ' VT ';
            }

            if ($j['flottes'] > 0) {
                $pluriel = $j['flottes'] > 1 ? 's' : '';
                $ligne .= "({$j['flottes']} flotte$pluriel)";
            }

            $lignes .= $ligne . "\n";
        }

        return $lignes;
    }
}
