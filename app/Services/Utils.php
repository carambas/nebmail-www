<?php


namespace App\Services;


use App\Models\Partie;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use App\Notifications\PasswordNotification;
use Illuminate\Http\Client\ConnectionException;

class Utils
{

    /**
     * Converti une chaine lue sur le disque dont le charset diffère du site
     * @param string|array $input
     * @return false|null[]|string|string[]|\string[][]|null
     */
    static function strConvertIn($input, $source_encoding = null)
    {
        return mb_convert_encoding($input ?? '', config('nebmail.site_charset'), $source_encoding ?? config('nebmail.files_encoding'));
    }

    /**
     * Converti une chaine écrite sur le disque dont le charset diffère du site
     * @param $str
     * @return false|string|string[]|null
     */
    static function strConvertOut($str)
    {
        return mb_convert_encoding($str, config('nebmail.files_encoding'), config('nebmail.site_charset'));
    }

    static function sendMailTo(array $to, string $subject, string $body, array $attach = null)
    {
        Log::debug(__METHOD__, ['subject' => $subject, 'body' => $body]);
        dispatch(function () use ($to, $body, $subject, $attach) {
            Mail::raw($body, function ($message) use ($to, $subject, $attach) {
                $message->to($to)->subject($subject);
                if (isset($attach)) {
                    $message->attach($attach);
                }
            });
        });
    }

    public static function genereMdp()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < 8; $i++) {
            $index = mt_rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    public static function genereMdpPartie(Partie $partie)
    {
        foreach ($partie->joueur()->where('password', null)->get() as $jou) {
            $jou->password_plain = Utils::genereMdp();
            $jou->password = Hash::make($jou->password_plain);
            $jou->api_token = Str::random(60);
            $jou->save();
            $jou->notify(new PasswordNotification);
        }
    }

    public static function postToDiscord(string $content, string $discord_url = null): void
    {
        $webhook = $discord_url ?? config('nebmail.discord_webhook_url');

        Log::debug(__METHOD__ . " Notification discord", ['content' => $content]);
        Log::debug(__METHOD__ . " Notification discord", ['url' => $discord_url]);

        try {
            Http::post($webhook, [
                'content' => $content,
                'username' => 'Nebmail'
            ]);
        } catch (ConnectionException $e) {
            Log::error(__METHOD__ . " Erreur retournée lors de l'appel à l'API Discord : {$e->getMessage()}");
        }
    }

    public static function commaStringToArrayOfInt(string $commaString = '', int $size = 0): array
    {
        $theArray = [];

        if ($commaString != '') {
            $theArray = array_map(function (string $value) {
                return (int) $value;
            }, explode(',', $commaString));
        }

        for ($i = count($theArray); $i < $size; $i++) {
            $theArray[] = 0;
        }

        return $theArray;
    }

    public static function commaStringToArray(string $commaString = '', int $size = 0): array
    {
        $theArray = [];

        if ($commaString != '') {
            $theArray = array_map(function (string $value) {
                return is_numeric($value) ? (int) $value : $value;
            }, explode(',', $commaString));
        }

        for ($i = count($theArray); $i < $size; $i++) {
            $theArray[] = '';
        }

        return $theArray;
    }
}
