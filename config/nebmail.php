<?php

return [
    'sm' => env('NEBMAIL_EMAIL', 'nebmail@ludimail.net'),
    'eval' => env('EVAL_CMD', '/home/nebmail/console/nebula_laz eval'),
    'cr' => env('CR_CMD', '/home/nebmail/console/nebula_laz cr'),
    'site_charset' => env('SITE_CHARSET', 'UTF-8'),
    'files_encoding' => env('FILES_ENCODING', 'ISO-8859-1'),
    'stat_nbmessages' => env('STAT_NBMESSAGES'),
    'stat_rum_url' => env('STAT_RUM_URL'),
    'cache_delay' => 3600,
    'mail_from_address' => env('MAIL_FROM_ADDRESS', 'olivier@gangloff.fr'),
    'mail_from_name' => env('MAIL_FROM_NAME', 'SM Nébula'),
    'mail_admin' => env('MAIL_ADMIN', 'olivier@gangloff.fr'),
    'slack_webhook_url' => env('SLACK_WEBHOOK_URL'),
    'discord_webhook_url' => env('DISCORD_WEBHOOK_URL'),
    'partie_message' => explode(',', env('PARTIE_MESSAGE')),

    // icône, route, route params, actif, à afficher si arbitre
    'menu' => [
        'Configuration et préférences' => ['ic:build', 'confpref', null, true, true],
        'Saisir/Modifier ses ordres' => ['bi:list-columns', 'ordres', null, true, false],
        'Consulter votre CR texte' => ['bi:card-list', 'crtexte', null, true, false],
        'Consulter le plan' => ['bxs:grid', 'plan', null, true, false],
        'Affichage Nebutil' => ['ic:outline-space-dashboard', 'nebutil', null, true, false],
        'Télécharger votre dernier CR (texte + NBT)' => ['ic:archive', 'cr', null, true, false],
    ],

];
