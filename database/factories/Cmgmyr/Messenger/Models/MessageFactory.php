<?php

namespace Database\Factories\Cmgmyr\Messenger\Models;

use App\Models\User;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $destinataires = User::all()->random(2);
        $proprietaire = $destinataires->random();

        return [
            'thread_id' => Thread::factory(),
            'user_id' => $proprietaire->id,
            'body' => $this->faker->sentence(),
        ];
    }
}
