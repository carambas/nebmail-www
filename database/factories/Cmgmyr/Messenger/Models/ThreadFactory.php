<?php

namespace Database\Factories\Cmgmyr\Messenger\Models;

use Illuminate\Database\Seeder;
use Cmgmyr\Messenger\Models\Thread;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ThreadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Thread::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // $broadcast = random_int(0, 1);
        // $anonymous = 0;
        // if ($broadcast === 1)
        //     $anonymous = random_int(0, 1);
        $anonymous = false;
        $broadcast = false;

        return [
            'subject' => $this->faker->sentence(5),
            'anonymous' => $anonymous,
            'broadcast' => $broadcast,
        ];
    }

    public function messageGeneral()
    {
        return $this->state(function (array $attributes) {
            return [
                'anonymous' => false,
                'broadcast' => true,
            ];
        });
    }

    public function rumeur()
    {
        return $this->state(function (array $attributes) {
            return [
                'anonymous' => true,
                'broadcast' => true,
            ];
        });
    }

}
