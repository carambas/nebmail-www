<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Ordre;
use App\Models\Partie;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrdreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ordre::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //'user_id' => 'TEST-' . (string) rand(1, optional(Partie::find('TEST'))->joueur->max('numjou')),
            'user_id' => User::factory(),
            'tour' => Partie::find('TEST')->tour + 1,
            //'tour' => rand(1, optional(Partie::find('TEST'))->tour + 1),
            'content' => 'F_' . rand(1, 200) . ' MM ' . rand(1, 200) . ' ' . rand(1, 200) . ' ' . rand(1, 200) . '\n A : 2\nM ' . rand(1, 200) . ' C : 2',
        ];
    }

    public function tourCourant()
    {
        return $this->state(function (array $attributes) {
            return [
                'tour' => Partie::find('TEST')->tour + 1,
            ];
        });
    }
}
