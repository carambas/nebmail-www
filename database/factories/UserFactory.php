<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Partie;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    static protected $increment = 0;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $partie_name = 'TEST';

        $numjou = optional(Partie::find('TEST'))->joueur->max('numjou') + 1 ?? 1;
        $numjou += self::$increment++;

        $email = [$this->faker->unique()->email()];
        if (rand(0, 1) == 1) {
            array_push($email, $this->faker->unique()->email());
        }

        return [
            'pseudo' => $this->faker->name(),
            'numjou' => $numjou,
            'partie_name' => $partie_name,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'api_token' => Str::random(60),
            'password_plain' => 'password',
            'email' => $email,
            'remember_token' => Str::random(10),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'codesm' => 0,
            'cc' => (bool) rand(0, 1),
            'rumeurs' => (bool) rand(0, 1),
            'msg_gen' => (bool) rand(0, 1),
        ];
    }


     /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (User $user) {
            //$increment = 0;
        })->afterCreating(function (User $user) {
            self::$increment = 0;
        });
    }

}
