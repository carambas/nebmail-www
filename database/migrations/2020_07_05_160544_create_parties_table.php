<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parties', function (Blueprint $table) {
            $table->string('name')->primary();
            $table->string('password');
            $table->integer('tour');
            $table->integer('tour_final')->nullable();
            $table->integer('codesm');
            $table->boolean('visible');
            $table->boolean('en_cours')->default(false)->comment('false si non débutée ou terminée');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parties');
    }
}
