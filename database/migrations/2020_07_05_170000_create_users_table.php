nebdata<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id')->virtualAs("CONCAT_WS('-', partie_name, numjou)")->index()->unique();
            $table->string('pseudo');
            $table->integer('numjou');
            $table->string('partie_name');
            $table->string('password');
            $table->string('password_plain');
            $table->string('email1');
            $table->string('email2')->nullable();
            $table->string('email')->virtualAs("CONCAT_WS(',', email1, email2)");
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->integer('codesm');
            $table->boolean('cc');
            $table->boolean('rumeurs');
            $table->boolean('msg_gen');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('partie_name')
                ->references('name')
                ->on('parties')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
