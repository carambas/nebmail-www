<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordres_histo', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->integer('tour');
            $table->mediumText('content')->default('');
            $table->timestamps();
        });

        DB::statement($this->dropView());
        DB::statement($this->createView());

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordres_histo');
        DB::statement($this->dropView());
    }

    private function dropView(): string
    {
        return <<<SQL
            DROP VIEW IF EXISTS `ordres`;
            SQL;
    }

    private function createView(): string
    {
        return <<<SQL
            CREATE VIEW `ordres` AS
            SELECT `ordres_histo`.* FROM `ordres_histo`
            INNER JOIN `users` on `users`.`id` = `ordres_histo`.`user_id`
            INNER JOIN `parties` on `users`.`partie_name` = `parties`.`name`
            WHERE `ordres_histo`.`tour` = `parties`.`tour` + 1
            SQL;
    }


}
