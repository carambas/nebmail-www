<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteOrdresView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement($this->dropView());

        Schema::table('ordres_histo', function (Blueprint $table) {
            $table->dropForeign (['user_id']);
        });

        Schema::rename('ordres_histo', 'ordres');

        Schema::table('ordres', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordres', function (Blueprint $table) {
            $table->dropForeign (['user_id']);
        });

        Schema::rename('ordres', 'ordres_histo');

        Schema::table('ordres_histo', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });


        DB::statement($this->createView());
    }

    private function dropView(): string
    {
        return <<<SQL
            DROP VIEW IF EXISTS `ordres`;
            SQL;
    }

    private function createView(): string
    {
        return <<<SQL
            CREATE VIEW `ordres` AS
            SELECT `ordres_histo`.* FROM `ordres_histo`
            INNER JOIN `users` on `users`.`id` = `ordres_histo`.`user_id`
            INNER JOIN `parties` on `users`.`partie_name` = `parties`.`name`
            WHERE `ordres_histo`.`tour` = `parties`.`tour` + 1
            SQL;
    }

}
