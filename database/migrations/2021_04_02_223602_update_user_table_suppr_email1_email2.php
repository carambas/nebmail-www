<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTableSupprEmail1Email2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('email');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->json('email')->default(new Expression('(JSON_ARRAY())'))->after('password_plain');
        });

        // DB::update("update users SET email_tmp = CONCAT_WS(',', email1, email2)");
        DB::update("update users SET email = JSON_ARRAY(email1)");
        DB::update("update users SET email = JSON_ARRAY_APPEND(email, '$', email2) WHERE email2 IS NOT NULL");


        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('email1');
            $table->dropColumn('email2');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('email1')->nullable()->after('password_plain');
            $table->string('email2')->nullable()->after('email1');
        });

        DB::update("update users SET email1 = JSON_UNQUOTE(JSON_EXTRACT(email, '$[0]'))");
        DB::update("update users SET email2 = JSON_UNQUOTE(JSON_EXTRACT(email, '$[1]'))");

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('email');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('email')->virtualAs("CONCAT_WS(',', email1, email2)")->after('password_plain');
        });
    }
}
