<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTableId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('id_new')->nullable()->after('id');
        });

        DB::update("update users SET id_new = id");

        Schema::table('users', function (Blueprint $table) {
            $table->string('id_new')->index()->unique()->change();
        });

        Schema::table('ordres', function (Blueprint $table) {
            $table->dropForeign (['user_id']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('id_new', 'id');
        });

        Schema::table('ordres', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordres', function (Blueprint $table) {
            $table->dropForeign (['user_id']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('id')->virtualAs("CONCAT_WS('-', partie_name, numjou)")->index()->unique()->first();
        });

        Schema::table('ordres', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }
}
