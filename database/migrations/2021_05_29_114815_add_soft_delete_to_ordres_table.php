<?php

use Carbon\Carbon;
use App\Models\Partie;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteToOrdresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordres', function (Blueprint $table) {
            $table->softDeletes();
        });

        Partie::all()->each(function ($partie) {
            DB::table('ordres')
                ->where('user_id', 'like', "{$partie->name}-%")
                ->where('tour', '<=', $partie->tour)
                ->update(['deleted_at' => Carbon::now()]);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordres', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
