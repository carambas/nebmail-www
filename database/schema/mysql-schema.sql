/*!999999\- enable the sandbox mode */ 
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `key` varchar(255) NOT NULL,
  `value` mediumtext NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `coordonnees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coordonnees` (
  `user_id` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `coordonnees_user_id_unique` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `extraits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extraits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `tour` int(11) NOT NULL,
  `monde_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `extraits_user_id_tour_monde_id_unique` (`user_id`,`tour`,`monde_id`),
  CONSTRAINT `extraits_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) DEFAULT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) NOT NULL,
  `payload` longtext NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `messenger_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messenger_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `thread_id` int(10) unsigned NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messenger_messages_thread_id_index` (`thread_id`),
  KEY `messenger_messages_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `messenger_participants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messenger_participants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `thread_id` int(10) unsigned NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `last_read` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ignore_non_repondu` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `messenger_participants_thread_id_index` (`thread_id`),
  KEY `messenger_participants_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `messenger_threads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messenger_threads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `anonymous` tinyint(1) NOT NULL DEFAULT 0,
  `broadcast` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ordres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordres` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `tour` int(11) NOT NULL,
  `brouillon` tinyint(1) NOT NULL DEFAULT 0,
  `content` mediumtext NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ordres_user_id_foreign` (`user_id`),
  CONSTRAINT `ordres_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `parties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parties` (
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tour` int(11) NOT NULL,
  `tour_final` int(11) DEFAULT NULL,
  `codesm` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `en_cours` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'false si non débutée ou terminée',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deadline` timestamp NULL DEFAULT NULL,
  `derniere_relance` timestamp NULL DEFAULT NULL,
  `diplomatie` tinyint(1) NOT NULL DEFAULT 1,
  `discord_url` varchar(255) DEFAULT NULL,
  `webhook_ordres_rendus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `relation_id` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `user_agent` text DEFAULT NULL,
  `payload` text NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` varchar(255) DEFAULT NULL,
  `pseudo` varchar(255) NOT NULL,
  `name` varchar(255) GENERATED ALWAYS AS (`pseudo`) VIRTUAL,
  `numjou` int(11) NOT NULL,
  `equipe` int(11) DEFAULT NULL,
  `partie_name` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `api_token` varchar(80) DEFAULT NULL,
  `password_plain` varchar(255) DEFAULT NULL,
  `email` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT json_array() CHECK (json_valid(`email`)),
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `codesm` int(11) NOT NULL,
  `cc` tinyint(1) NOT NULL,
  `rumeurs` tinyint(1) NOT NULL,
  `msg_gen` tinyint(1) NOT NULL,
  `relations_custom` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '{}' CHECK (json_valid(`relations_custom`)),
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `users_id_new_unique` (`id`),
  UNIQUE KEY `users_api_token_unique` (`api_token`),
  KEY `users_partie_name_foreign` (`partie_name`),
  CONSTRAINT `users_partie_name_foreign` FOREIGN KEY (`partie_name`) REFERENCES `parties` (`name`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

/*!999999\- enable the sandbox mode */ 
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (1,'2014_10_28_175635_create_threads_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (2,'2014_10_28_175710_create_messages_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (3,'2014_10_28_180224_create_participants_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (4,'2014_11_03_154831_add_soft_deletes_to_participants_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (5,'2014_12_04_124531_add_softdeletes_to_threads_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (6,'2017_03_30_152742_add_soft_deletes_to_messages_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (7,'2019_08_19_000000_create_failed_jobs_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (8,'2020_07_05_160544_create_parties_table',1);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (9,'2020_07_05_170000_create_users_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (10,'2020_07_07_000000_create_relations_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (11,'2020_08_03_181333_create_jobs_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (12,'2020_08_04_213822_create_ordres_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (13,'2020_08_30_145658_add_deadline_to_parties_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (14,'2020_09_01_173845_create_sessions_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (15,'2020_09_01_175117_create_cache_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (16,'2020_09_13_185213_job_batching',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (17,'2020_09_16_153853_update_for_messenger',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (18,'2020_09_28_213925_add_rumeurs_to_threads',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (19,'2020_10_24_175320_update_users_table_nullable_password',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (20,'2020_11_18_105320_add_diplomatie_to_parties_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (21,'2020_11_28_182539_add_discordurl_to_parties_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (22,'2021_03_07_004758_add_equipe_to_users_table',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (23,'2021_04_02_183639_delete_ordres_view',2);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (24,'2021_04_02_223602_update_user_table_suppr_email1_email2',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (25,'2021_04_03_225905_update_user_table_id',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (26,'2021_05_20_223440_add_brouillon_to_ordres_table',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (27,'2021_05_29_114815_add_soft_delete_to_ordres_table',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (28,'2021_07_12_103936_add_api_token_to_users_table',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (29,'2022_01_17_225529_add_webhook_notification_ordre_to_parties_table',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (30,'2022_02_05_152600_create_coordonnees_table',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (31,'2022_02_08_144923_create_extraits_table',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (32,'2022_02_23_174244_add_non_repondu_to_participants_table',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (33,'2022_03_30_204643_add_indexes_for_messenger',3);
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (34,'2022_09_20_150639_add_relation_custom_to_users_table',3);
