<?php

namespace Database\Seeders;

use App\Models\Coordonnees;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CoordonneesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coordonnees')->delete();

        Coordonnees::create([
            'user_id' => 'TEST-2',
            'value' => '18=3,11',
        ]);

    }
}
