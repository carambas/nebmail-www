<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PartieTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(OrdreTableSeeder::class);
        $this->call(RelationTableSeeder::class);
        $this->call(ExtraitsSeeder::class);
        $this->call(CoordonneesSeeder::class);
    }
}


