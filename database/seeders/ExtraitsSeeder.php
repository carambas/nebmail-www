<?php

namespace Database\Seeders;

use App\Models\Extrait;
use App\Services\NebData\Monde;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExtraitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('extraits')->delete();

        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 4,
            'content' => 'M_22 (24,43) "Joueurè 2(Toto le grand)"! I=1 [P=50(150)/8C:"Gehowa el grandé"]=1 MP=1(+3) Pi=2/4 [5,3]',
            'monde_id' => 22,
            'created_at' => '2022-04-04 09:00:00',
            'updated_at' => '2022-04-04 09:00:00',
        ]);
        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 3,
            'content' => 'M_22 (24,43) "Toto le grand:2" []** P=50(150) MP=(+3) Pi=2 PC=3 PE=600 E=7/8 [5,3]',
            'monde_id' => 22,
            'created_at' => '2022-04-04 10:00:00',
            'updated_at' => '2022-04-04 10:00:00',
        ]);
        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 2,
            'content' => 'M_22 (24,43) "Toto le grand:1" []=2 P=50(150) MP=1 [5,3]',
            'monde_id' => 22,
            'created_at' => '2022-04-04 11:00:00',
            'updated_at' => '2022-04-04 11:00:00',
        ]);
        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 1,
            'content' => 'M_22 (24,43) "Toto le grand"! [I=1/2]=2 [5,3]',
            'monde_id' => 22,
            'created_at' => '2022-04-04 12:00:00',
            'updated_at' => '2022-04-05 12:00:00',
        ]);
        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 1,
            'content' => 'M_4 (5,43) "Joueurè 2"! I=0/2 P=50(150) MP=1(+3) [4,5]',
            'monde_id' => 4,
            'created_at' => '2022-04-04 13:00:00',
            'updated_at' => '2022-04-04 13:00:00',
        ]);
        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 2,
            'content' => 'M_4 (5,43) "Joueurè 2"! P=50(150) MP=1(+3) [4,5]',
            'monde_id' => 4,
            'created_at' => '2022-04-04 14:00:00',
            'updated_at' => '2022-04-04 14:00:00',

        ]);
        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 3,
            'content' => 'M_4 (5,43) "Joueurè 2"! [I=1]*F_12 P=50(150) MP=1(+3) [4,5]',
            'monde_id' => 4,
            'created_at' => '2022-04-04 15:00:00',
            'updated_at' => '2022-04-04 15:00:00',
        ]);
        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 4,
            'content' => <<< MONDE
            "Grenier du monde"
            M_4 (5,43) "32"! I=1 P=50(150) MP=1(+3) [4,5]
                      T_100 <La Chose>
                  F_6 "Toto" [?]=2 du M_?
                      T_67 <Le trésor qui brille>
                 {F_100 "Toto" vers M_12}
            MONDE,
            'monde_id' => 4,
            'created_at' => '2022-04-04 16:00:00',
            'updated_at' => '2022-04-04 16:00:00',
        ]);

        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 4,
            'content' => <<< MONDE
            "Grenier du monde"
            M_5 (1,4) "Toto(Joueurè 2)"! I=1 P=50(150) MP=1(+3) [3,5]
            MONDE,
            'monde_id' => 5,
            'created_at' => '2022-04-04 17:00:00',
            'updated_at' => '2022-04-04 17:00:00',
        ]);

        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 3,
            'content' => <<< MONDE
                "Toto"
                M_16 (1,18) "Joueurè 2"!  I=1 P=50(150) MP=1(+3) [3,10]
                        T_100 <La Chose>
                    F_6 "Toto" [?]=2 du M_?
                        T_67 <Le trésor qui brille>
                    {F_100 "Toto" vers M_12}
                MONDE,
            'monde_id' => 16,
            'created_at' => '2022-04-03 17:00:00',
            'updated_at' => '2022-04-03 17:00:00',
        ]);

        Extrait::create([
            'user_id' => 'TEST-2',
            'tour' => 3,
            'content' => <<< MONDE
                M_13   (2,6,7,17,23,32,34   ,43) "Joueurè 2"! I=10 P=200(400) MP=10(+10) [6,6]
                MONDE,
            'monde_id' => 13,
            'created_at' => '2022-04-03 17:00:00',
            'updated_at' => '2022-04-03 17:00:00',

        ]);
    }
}
