<?php

namespace Database\Seeders;

use App\Models\Ordre;
use App\Models\Partie;
use App\Services\Utils;
use App\Models\User;
use Doctrine\DBAL\Schema\Index;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class OrdreTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ordres')->delete();

        // Seed des tours précédents : des ordres pour tous les joueurs
        foreach (Partie::withoutGlobalScopes()->get() as $partie) {
            $this->command->info('ordres de ' . $partie->name);

            for($tour = 1; $tour <= $partie->tour; $tour++) {
                $joueurs = $partie->joueurs_sans_arbitre->get();
                foreach($joueurs as $joueur) {
                    $ordreContent = $this->getOrdres($joueur->pseudoWithNumJou);
                    Ordre::updateOrCreate([
                        'user_id' => $joueur->id,
                        'tour' => $tour,
                        'content' => "# Ordres du tour $tour\n$ordreContent",
                        'deleted_at' => '2020-12-31 00:00:00'
                    ]);
                }
            }

        }

        // prochain tour : tout le monde n'a pas d'ordres rendus
        foreach (Partie::withoutGlobalScopes()->get() as $partie) {
            $tour = $partie->tour + 1;
            $this->command->info('ordres de ' . $partie->name);
            $files = Storage::disk('data')->files($partie->name);
            $files = preg_grep('|.*/\d+|', $files);
            foreach($files as $file) {
                $ordre = Utils::strConvertIn(Storage::disk('data')->get($file));
                $ordre = preg_replace("/^@Begin.*$/m", '', $ordre);
                $ordre = preg_replace("/^@End.*$/m", '', $ordre);

                $noJou = basename($file);

                $user = User::where([
                    'partie_name' => $partie->name,
                    'numjou' => $noJou
                ])->first();

                Ordre::Create([
                    'user_id' => $user->id,
                    'tour' => $tour,
                    'content' => "# Ordres du tour $tour\n$ordre"
                ]);
            }
        }


    }

    public function getOrdre(int $index = null): string
    {
        $ordres = [
            'M 15 C 3 VC F 12',
            'M 15 C 3 VC F 131',
            'F 12 M 12 M 23',
            'F131 MM 40 40 50',
            'F 5 C BOMBE',
            'F 5 L BOMBE',
            'F 135 * M'
        ];

        if (! isset($index)) {
            $index = rand(0, count($ordres) - 1);
        }

        return $ordres[$index];
    }

    public function getOrdres(string $pseudo):string
    {
        $ordres = collect([
            "############################################",
            "# Ordres du joueur $pseudo",
            "############################################",
            "",
        ]);
        for ($i = 0; $i < rand(3, 10); $i++) {
            $ordres->push($this->getOrdre());
        }

        return $ordres->join("\n");
    }
}
