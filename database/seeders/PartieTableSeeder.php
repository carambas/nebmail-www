<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Partie;
use App\Services\Files\Parties;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PartieTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('parties')->delete();

        $parties = new Parties();

        foreach ($parties->getParties() as $partie) {
            $this->command->info($partie['nom'] . ' ');
            $partieModel = Partie::create([
                'name' => $partie['nom'],
                'password' => $partie['password'],
                'tour' => $partie['tour'],
                'tour_final' => $partie['tour_final'],
                'codesm' => $partie['codesm'],
                'visible' => $partie['statut'] !== 'H',
                'en_cours' => $partie['statut'] === 'C',
            ]);

            if ($partie['nom'] === 'TEST') {
                $partieModel->derniere_relance = Carbon::now()->subDays(4)->setTime(hour: 20, minute: 0);
                $partieModel->deadline = Carbon::now()->addDays(3)->setTime(hour: 20, minute: 0);
                $partieModel->save();
            }

        }

        $this->command->info('parties table seeded!');
    }

}
