<?php

namespace Database\Seeders;

use App\Models\Partie;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RelationTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('relations')->delete();

        $reqArray = [];
        foreach (Partie::withoutGlobalScopes()->get() as $partie) {
            $this->command->info('relations de ' . $partie->name);
            $rel = (new \App\Services\Files\Joueurs($partie->name))->getRelations();

            $users = User::where('partie_name', $partie->name)->get();

            // $key = nojou
            // $var = array of nojou
            foreach ($rel as $key => $val) {
                $user_id = $users->where('numjou', $key)->first()->id;
                foreach($val as $noJouRel) {
                    $relation_id = $users->where('numjou', $noJouRel)->first()->id;

                    $reqArray[] = [
                        'user_id' => $user_id,
                        'relation_id' => $relation_id,
                    ];

                }
            }
        }
        DB::table('relations')->insert($reqArray);
    }
}
