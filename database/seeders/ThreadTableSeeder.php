<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Cmgmyr\Messenger\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use Illuminate\Support\Facades\Artisan;
use Cmgmyr\Messenger\Models\Participant;
use Seld\JsonLint\DuplicateKeyException;

class ThreadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messenger_threads')->delete();
        DB::table('messenger_participants')->delete();
        DB::table('messenger_messages')->delete();

        $destinataires = User::all()->random(2);
        $proprietaire = $destinataires->random();

        // Message::factory()
        //     ->for(
        //         Thread::factory()
        //         ->has(Participant::factory()->state(['user_id' => $destinataires[0]->id]))
        //         ->has(Participant::factory()->state(['user_id' => $destinataires[1]->id]))
        //     )
        //     //->for($destinataires->random())
        //     ->count(10)
        //     ->create();


        Thread::factory()
            ->count(5)
            ->hasParticipants(3)
            ->hasMessages(4, function (array $attributes, Thread $thread) {
                return ['user_id' => $thread->participants()->get()->random()->user_id];
            })->create();

        $this->deleteDuplicateParticipants();

        Artisan::call('cache:clear');
    }

    private function deleteDuplicateParticipants()
    {
        $duplicateRecords = DB::table('messenger_participants')
            ->select('thread_id', 'user_id', DB::raw('count(`user_id`) as occurences'), DB::raw('max(`id`) as id_max'))
            ->groupBy(['thread_id', 'user_id'])
            ->having('occurences', '>', 1)
            ->get();

        foreach($duplicateRecords as $duplicate) {
            Participant::where('id', $duplicate->id_max)->forceDelete();
        }

    }
}
