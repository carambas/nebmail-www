<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Partie;
use Illuminate\Support\Str;
use App\Services\Files\Joueurs;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        foreach (Partie::withoutGlobalScopes()->get() as $partie) {
            $this->command->info('joueurs de ' . $partie->name);
            $joueurs = new Joueurs($partie->name);
            for ($i = 0; $i <= $joueurs->getNbJou(); $i++) {
                $joueur = $joueurs->get($i);

                User::create([
                    'pseudo' => $joueur->get('pseudo'),
                    'numjou' => $joueur->get('numjou'),
                    'partie_name' => $partie->name,
                    'password' => Hash::make($joueur->get('password')),
                    'password_plain' => $joueur->get('password'),
                    'api_token' => $partie->name === 'TEST' ? 'api_token_' .  $joueur->get('numjou') : Str::random(60),
                    'email' => explode(',', $joueur->get('email')),
                    'codesm' => $joueur->get('codesm'),
                    'cc' => $joueur->get('cc'),
                    'rumeurs' => $joueur->get('rumeurs'),
                    'msg_gen' => $joueur->get('msgGen'),
                ]);
            }
        }

        $relations = [
            1 => ['couleur' => '#800000'],
            2 => ['couleur' => '#806080'],
            3 => [
                'couleur' => '#ADFF2F',
                'classe' => 2, // Marchand
                'confiance' => 4,
                'att' => 10,
                'def' => 5,
                'declAllie' => 0,
                'declChargeur' => 1,
                'declChargeurPop' => 1,
                'declDechargeurPop' => 0,
            ]
        ];

        // die(json_encode($relations));

        User::find('TEST-2')
            ->update([
                'relations_custom' => $relations
            ]);

        $this->command->info('users table seeded!');
    }
}
