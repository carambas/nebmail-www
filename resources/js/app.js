// eslint-disable-next-line camelcase
import { livewire_hot_reload } from 'virtual:livewire-hot-reload'
import './bootstrap'
import 'iconify-icon'
import Color from 'color'

window.Color = Color
livewire_hot_reload()
