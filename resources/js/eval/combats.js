const CAVT = 0.30
const CDVT = 0.70

export function calculeCAtt(niveauATT) {
    niveauATT = Math.min(niveauATT, 20)
    niveauATT = Math.max(niveauATT, 1)

    return Math.round(50 * Math.pow(1.1, niveauATT - 1))
}

export function calculeCDef(niveauDEF) {
    niveauDEF = Math.min(niveauDEF, 20)
    niveauDEF = Math.max(niveauDEF, 1)

    return Math.round(100 * Math.pow(0.9, niveauDEF - 1))
}

export class TirAttaque {
    /**
     *
     * @param {int} att
     * @param {bool} isEmbuscade
     * @param {int} vc
     * @param {int} vt
     */
    constructor(att, vc, vt, isEmbuscade) {
        this.att = att
        this.vc = vc
        this.vt = vt
        this.isEmbuscade = isEmbuscade
    }
}

export class TirDefense {
    /**
     *
     * @param {int} def
     * @param {int} car
     * @param {int} cargaison
     * @param {int} vc
     * @param {int} vt
     * @param {bool} isFuite
     */
    constructor(def, car, cargaison, vc, vt, isFuite) {
        this.def = def
        this.car = car
        this.cargaison = cargaison
        this.vc = vc
        this.vt = vt
        this.isFuite = isFuite
    }
}

/**
 *
 * @param {TirAttaque} attaque
 * @param {TirDefense} defense
 */
export function calculeDegatsTir(attaque, defense) {
    const touchés = Math.ceil(getNbVaisseauxTouches(attaque))
    const détruits = Math.ceil(touchés * getCDefFlotte(defense))

    const vcDétruits = Math.min(détruits, defense.vc)
    const vtDétruits = Math.min(détruits - vcDétruits, defense.vt)

    return {
        touchés,
        détruits,
        vcDétruits,
        vtDétruits,
    }
}

/**
 *
 * @param {TirDefense} defense
 */
function getCDefFlotte(defense) {
    let CD = 100
    const capaciteCargaison = defense.car * defense.vt + defense.vc
    if (capaciteCargaison > 0) {
        CD = (CDVT * defense.vt + (calculeCDef(defense.def) / 100) * defense.vc) *
            (1 + (defense.cargaison / capaciteCargaison)) / (defense.vt + defense.vc)
    }

    return defense.isFuite ? CD / 2 : CD
}

/**
 *
 * @param {TirAttaque} attaque
 * @returns
 */
function getNbVaisseauxTouches(attaque) {
    const touchés = CAVT * attaque.vt + (calculeCAtt(attaque.att) / 100) * attaque.vc
    return attaque.isEmbuscade ? 2 * touchés : touchés
}
