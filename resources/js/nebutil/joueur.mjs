/**
 * Classe utilitaire de fichier .nbt
 * @property {int} numero
 * @property {string} pseudo
 * @property {string} nom
 * @property {int} md
 * @property {int} equipe
 * @property {int} classeId
 * @property {int} tourJihad
 * @property {int} numJouJihad
 * @property {int} dep
 * @property {int} att
 * @property {int} def
 * @property {int} rad
 * @property {int} ali
 * @property {int} car
 * @property {int} depReste
 * @property {int} attReste
 * @property {int} attReste
 * @property {int} radReste
 * @property {int} aliReste
 * @property {int} carReste
 * @property {int} ca
 * @property {int} cd
 * @property {int} score
 * @property {int[]} connuDeNom
 * @property {int[]} contacts
 * @property {int[]} allies
 * @property {int[]} chargeurs
 * @property {int[]} chargeursPop
 * @property {int[]} dechargeursPop
 * @property {int[]} pilleurs
 * @property {int[]} detailsScore
 * @property {string} equipeStr - Liste des joueurs de l'équipe. Sert à déterminer le numéro d'équipe dans un 2ème temps.
 */
export class Joueur {
    constructor(joueur = null) {
        if (joueur) {
            for (const property in joueur) {
                this[property] = joueur[property]
            }
        } else {
            this.numero = 0
            this.pseudo = ''
            this.nom = ''
            this.email = []
            this.md = 0
            this.equipe = 0
            this.classeId = 0
            this.tourJihad = 0
            this.numJouJihad = 0
            this.dep = 0
            this.att = 0
            this.def = 0
            this.rad = 0
            this.ali = 0
            this.car = 0
            this.depReste = 0
            this.attReste = 0
            this.attReste = 0
            this.radReste = 0
            this.aliReste = 0
            this.carReste = 0
            this.ca = 50
            this.cd = 100
            this.score = 0
            this.connuDeNom = []
            this.contacts = []
            this.allies = []
            this.chargeurs = []
            this.chargeursPop = []
            this.dechargeursPop = []
            this.pilleurs = []
            this.detailsScore = []
            this.equipeStr = ''
        }
    }
}
