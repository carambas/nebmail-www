import { Joueur } from './joueur.mjs'

/**
 * Classe utilitaire de fichier .nbt
 * @property {Object} nbt - contenu d'un fichier .nbt
 */
export class Nbt {
    constructor(nbt) {
        this.nbt = nbt
    }

    getJoueurNumber(pseudoOuNumero) {
        let numero = pseudoOuNumero

        if (typeof pseudoOuNumero === 'string') {
            // Récupération du numéro basé sur le pseudo du joueur contenu dans le .nbt
            const joueur = Object.entries(this.nbt.joueurs).find(joueur => joueur[1].pseudo === pseudoOuNumero)
            numero = (joueur) ? joueur[0] : null

            if (!numero) {
                numero = this.addJoueur(pseudoOuNumero)
            }
        }

        return parseInt(numero)
    }

    addJoueur(pseudo) {
        const numJoueurs = Object.keys(this.nbt.joueurs).map(numero => parseInt(numero))

        // On cherche le premier numéro de joueur libre dans la liste des joueurs
        let numero = 0
        for (numero = 1; numero <= this.nbt.nbJoueurs + 1; numero++) {
            if (!numJoueurs.includes(numero)) {
                break
            }
        }

        // On crée le joueur à l'emplacement trouvé
        const joueur = new Joueur({ pseudo, numJou: numero })
        this.nbt.joueurs[numero] = joueur

        return numero
    }

    getNbt() {
        return this.nbt
    }

    /**
     * Si des numéros de joueurs sont de chaines de caractères, remplacement par un numéro de nouveau joueur ayant cette chaine en pseudo
     */
    sanitizeNumerosJoueurs() {
        // On remplace les pseudos connus par les numéros dans les champs "propriétaires" des mondes
        const properties = ['proprietaireConvertis', 'proprietaire', 'ancienProprietaire', 'premierProprietaire']
        for (const numMonde in this.nbt.mondes) {
            for (const property of properties) {
                this.nbt.mondes[numMonde][property] = this.getJoueurNumber(this.nbt.mondes[numMonde][property])
            }
        }
    }
}
