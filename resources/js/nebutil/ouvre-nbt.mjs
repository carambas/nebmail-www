import { notify } from '../notify.mjs'

class OuvreNbt {
    /**
     * constructeur
     * @param {Object} config - object de configuration
     * @param {string} config.encoding - encodage du contenu du nbt
     * @param {string} config.parseNbtUrl - URL d'appel de l'API parse-nbt
     * @param {string} config.nomPartie - Nom de la partie du joueur connecté
     * @param {number} config.noJoueur - Numéro du joueur connecté
     * @param {apiToken} config.nomPaapiToken - Token d'API pour le joueur connecté
     */
    constructor(config) {
        this.config = config

        const fileInput = document.querySelector('#file_input')

        if (fileInput) {
            document.querySelector('#file_input_button').addEventListener('click', () => fileInput.click())
            fileInput.addEventListener('change', this.loadNbt.bind(this))
        }
    }

    /** @param {Event} event */
    async loadNbt(event) {
        const input = event.target
        const [file] = input.files

        if (!file) return

        const nbtContent = await file.text()

        const nbtIni = this.parseINIString(nbtContent)

        if (nbtIni.GENERAL.NomPartie !== this.config.nomPartie) {
            notify('danger', 'Le fichier .nbt ne correspond pas à la partie ouverte')
            return
        }

        if (parseInt(nbtIni.GENERAL.NoJoueur) !== this.config.noJoueur) {
            notify('danger', `Le fichier .nbt n'est pas celui du joueur connecté : (fichier : ${nbtIni.GENERAL.NoJoueur}, connecté : ${this.config.noJoueur}`)
            return
        }

        try {
            const { nbt, crtxt } = await this.getCrFromNbt(file)

            window.dispatchEvent(
                new CustomEvent('stocke-tour', {
                    detail: {
                        nbt: nbt,
                        txt: crtxt,
                        tour: nbt.tour,
                        displayNotification: true
                    }
                }))
        } catch (e) {
            notify('danger', e.message)
        }
    }

    parseINIString(data) {
        const regex = {
            section: /^\s*\[\s*([^\]]*)\s*\]\s*$/,
            param: /^\s*([^=]+?)\s*=\s*(.*?)\s*$/,
            comment: /^\s*;.*$/
        }
        const value = {}
        const lines = data.split(/[\r\n]+/)
        let section = null
        lines.forEach(function (line) {
            if (regex.comment.test(line)) {
                return null
            } else if (regex.param.test(line)) {
                const match = line.match(regex.param)
                if (section) {
                    value[section][match[1]] = match[2]
                } else {
                    value[match[1]] = match[2]
                }
            } else if (regex.section.test(line)) {
                const match = line.match(regex.section)
                value[match[1]] = {}
                section = match[1]
            } else if (line.length === 0 && section) {
                section = null
            }
        })
        return value
    }

    /** @param {File} nbtFile */
    async getCrFromNbt(nbtFile) {
        const formData = new FormData()

        formData.append('api_token', this.config.apiToken)
        formData.append('stocke_nbt', 1)
        formData.append('nbt', nbtFile)

        const init = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
            },
            mode: 'same-origin',
            credentials: 'same-origin',
            cache: 'default',
            body: formData
        }

        const request = new Request(this.config.parseNbtUrl, init)

        const response = await fetch(request)

        if (!response.ok) {
            throw new Error('Une erreur a été rencontrée lors de l\'analyse du fichier .nbt. Assurez-vous qu\'il s\'agit bien d\'un fichier au format .nbt et vous appartenant')
        }

        const data = await response.json()

        if (typeof data === 'undefined') {
            throw new Error("Mauvais format de données dans l'historique des CR")
        }

        return data
    }
}

export { OuvreNbt }
window.OuvreNbt = OuvreNbt
