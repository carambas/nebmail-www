/**
 * Classe permettant de trouver l'ensemble des mondes se trouvant une distance inférieure ou égale d'un monde donné
 * @property {Object[]} mondes - Tableau d'objets de classe Monde correspondant aux mondes trouvés
 * */
export class TrouveMondesDistance {
    /**
     * @param {Object[]} mondes
     */
    constructor(mondes) {
        this.mondes = mondes
        this.mondesTrouves = []
    }

    /**
     * Retourne l'ensemble des numéros de monde distués à une distance inférieure ou égale à distance
     * @package
     * @param {integer} numMonde
     * @param {integer} distance
     */
    trouveMondesDistance(numMonde, distance) {
        if (distance < 0) {
            return
        }

        if (!this.mondesTrouves.includes(numMonde)) {
            this.mondesTrouves.push(numMonde)
        }

        for (const numMondeConnect of this.mondes[numMonde]?.connexions || []) {
            this.trouveMondesDistance(numMondeConnect, distance - 1)
        }
    }

    /**
     * Retourne l'ensemble des numéros de monde distués à une distance inférieure ou égale à distance
     * @public
     * @param {integer} numMonde
     * @param {integer} distance
     * @returns {int[]} numéros des mondes trouvés
     */
    getMondesDistance(numMonde, distance) {
        this.trouveMondesDistance(numMonde, distance)
        return this.mondesTrouves
    }
}
