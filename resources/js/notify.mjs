/**
 * Affiche une notification sur le site nebmail
 * @param {string} type
 * @param {string} message
 */
function notify(type, message) {
    window.dispatchEvent(
        new CustomEvent('notify', {
            detail: {
                typeMsg: type,
                msg: message,
            }
        }))
}

export { notify }
