'use strict'

import { OrdresParser } from './ordres-parser.mjs'

export function modeNebula(config, parserConfig) {
    const numberStart = /[\d]/
    const number = /^(\d+)/i
    const ordresParser = new OrdresParser()

    function tokenBase(stream, state) {
        // Erreur de syntaxe
        if (stream.sol() && !ordresParser.isOrdreSyntaxeOK(stream.string)) {
            let firstCommentChar = ';'
            const posComment1 = stream.string.search('#')
            const posComment2 = stream.string.search(';')

            if ((posComment1 >= 0) && (posComment2 >= 0)) {
                if (posComment1 < posComment2) {
                    firstCommentChar = '#'
                }
                stream.skipTo(firstCommentChar)
            } else {
                stream.skipTo(';') || stream.skipTo('#') || stream.skipToEnd()
            }
            return 'erreur'
        }

        // Ici on a affaire à une ligne vide ou avec syntaxe OK.
        const ch = stream.next()

        // Chaine de caractère
        if (ch === '"') {
            state.tokenize = tokenString(ch)
            return state.tokenize(stream, state)
        }

        // Nombre
        if (numberStart.test(ch)) {
            stream.backUp(1)
            if (stream.match(number)) return 'number'
            stream.next()
        }

        // Commentaire
        if ((ch === ';') || (ch === '#')) {
            stream.skipToEnd()
            return 'comment'
        }
    }

    function tokenString(quote) {
        return function (stream, state) {
            let next; let end = false
            while ((next = stream.next()) != null) {
                if (next === quote) {
                    end = true
                    break
                }
            }
            if (end) { state.tokenize = null }
            return 'string'
        }
    }

    // Interface
    return {
        startState: function () {
            return {
                tokenize: null,
            }
        },

        token: function (stream, state) {
            const style = (state.tokenize || tokenBase)(stream, state)
            return style
        },
    }
}
