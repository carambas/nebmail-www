'use strict'
import { OrdresParser } from './ordres-parser.mjs'

export function linterNebula(text, options) {
    const cm = window.CodeMirror
    const found = []
    const nebdata = options.nebdata
    const numJou = options.numJou
    const ordresParser = new OrdresParser()
    const ordres = text.split('\n')
    const mondesLigne = []
    const flottesLigne = []
    const flottesOrdresExc = []
    const mondesUp = []
    const mondesProdOK = []
    let upRechercheDep = 0
    let dep = 0
    nebdata.warnings = []

    function putWarningFlotteSansOrdreExcl(f) {
        const ligne = flottesLigne[f] ?? 0

        found.push({
            from: cm.Pos(ligne ?? 0, 0),
            to: cm.Pos(ligne ?? 0, 0),
            message: `F_${f} n'a pas reçu d'ordre exclusif`,
            severity: 'warning'
        })

        nebdata.warnings.push({
            type: 'F_ORDRE_EXCL',
            numero: f,
            ligne: parseInt(ligne)
        })
    }

    function putWarningMondeUpPasTousProduits(m, up) {
        let message = up > 0
            ? `M_${m} toutes les UP n'ont pas été utilisées`
            : `M_${m} pas assez d'UP pour l'ensemble des ordres de construction`

        const typeWarning = up > 0 ? 'warning' : 'error'
        const ligne = mondesLigne[m] ?? 0

        found.push({
            from: cm.Pos(ligne, 0),
            to: cm.Pos(ligne, 0),
            message,
            severity: typeWarning
        })

        nebdata.warnings.push({
            type: message = up > 0 ? 'M_UP_PAS_CONSOMMEES' : 'M_PAS_ASSEZ_UP',
            numero: m,
            ligne: parseInt(ligne)
        })
    }

    function findFlotteSansOrdreExclusif() {
        for (const f in nebdata.flottes) {
            if ((nebdata.flottes[f].proprietaire === numJou) && !(f in flottesOrdresExc)) {
                putWarningFlotteSansOrdreExcl(f)
            }
        }
    }

    function findMondeUpPasTousProduits() {
        for (const monde of Object.values(nebdata.mondes)) {
            if ((monde.proprietaire === numJou) && (mondesUp[monde.numero] !== 0) && (!mondesProdOK[monde.numero])) {
                putWarningMondeUpPasTousProduits(monde.numero, mondesUp[monde.numero])
            }
        }
    }

    // Détecte à quelle ligne dans l'éditeur on trouve la flotte du CR en commentaire
    function initFlottesLigne() {
        for (const l in ordres) {
            const flotteFound = ordres[l].replace(/^[^;#]*/, '') // On ne garde que les commentaires
                .replace(/^[;#]*/, '') // Caractère de début de commentaire supprimé
                .trim()
                .match(/^F_(\d+)/) // Il s'agit d'une flotte qui stationne sur un monde

            if (flotteFound) { // Test undefined pour le faire à la première occurrence
                if (typeof flottesLigne[flotteFound[1]] === 'undefined') {
                    flottesLigne[flotteFound[1]] = l
                }

                if (ordres[l].search(/@RIEN@/) >= 0) {
                    flottesOrdresExc[flotteFound[1]] = l
                }
            }
        }
    }

    function initMondesLigne() {
        upRechercheDep = 0

        for (const l in ordres) {
            const match = ordres[l].match(/@M_(\d+)@/)
            if (match) {
                const numMonde = match[1]
                mondesLigne[numMonde] = l

                if (nebdata.mondes[numMonde]?.proprietaire === numJou) {
                    mondesUp[numMonde] = nebdata.mondes[numMonde].industriesActives
                }

                if (ordres[l].search(/@PRODOK@/) >= 0) {
                    mondesProdOK[numMonde] = true
                }
            }

            // Syntaxe alternative pour compatibilité Nebutil
            const mondeFound = ordres[l].replace(/^[^;#]*/, '') // On ne garde que les commentaires
                .replace(/^[;#]*/, '') // Caractère de début de commentaire supprimé
                .trim()
                .match(/^M_(\d+).*@PRODOK@/) // Il s'agit d'un monde avec tag @PRODOK@

            if (mondeFound) {
                mondesProdOK[mondeFound[1]] = true
            }
        }
    }

    function decompteUsageUp(ordre) {
        const coutConstructionIndustrie = [4, 5, 5, 5, 5, 5, 5]

        if (ordre.isOrdreConstructionInd) {
            ordre.up_consommes *= coutConstructionIndustrie[nebdata.joueurs[numJou].classeId - 1]
        }
        mondesUp[ordre.monde] -= ordre.up_consommes

        if (ordre.ordreRechercheDep) {
            upRechercheDep += parseInt(ordre.up_consommes)
        }
    }

    function calculeDepApresEval() {
        const coutRechercheDepFromClasse = [10, 8, 9, 10, 10, 10, 6]
        const maxTechDep = 8

        const joueur = nebdata.joueurs[numJou]
        dep = joueur.dep
        const coutRechercheDep = coutRechercheDepFromClasse[joueur.classeId - 1]

        dep += Math.floor((upRechercheDep + joueur.depReste) / coutRechercheDep)
        if (dep > maxTechDep) {
            dep = maxTechDep
        }

        window.joueurTechnoDep = dep
    }

    initMondesLigne()

    // Détection des erreurs de syntaxes et ordres exclusifs des flottes
    for (const i in ordres) {
        const typeOrdre = ordresParser.getTypeOrdre(ordres[i])

        if (!typeOrdre.isSyntaxOk() && ordres[i].search(/@OK@/) === -1) {
            const ligne = cm.Pos(i, 0)
            found.push({
                from: ligne,
                to: ligne,
                message: 'Erreur de syntaxe',
                severity: 'error'
            })

            nebdata.warnings.push({
                type: 'ERREUR_SYNTAXE',
                numero: null,
                ligne: parseInt(i)
            })
        } else {
            // ordre exclusifs d'une flottes
            if (typeOrdre?.isOrdreExclusifFlotte) {
                flottesOrdresExc[typeOrdre.flotte] = true
            }

            // ordre de construction
            if (typeOrdre?.isOrdreConstruction || typeOrdre?.isOrdreConstructionInd) {
                decompteUsageUp(typeOrdre)
            }
        }
    }

    initFlottesLigne()
    findFlotteSansOrdreExclusif()
    findMondeUpPasTousProduits()

    calculeDepApresEval()

    return found
}
