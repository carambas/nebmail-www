import CodeMirror from 'codemirror'
import { modeNebula } from './codemirror-mode-nebula'
import { linterNebula } from './codemirror-nebula-lint'
import 'codemirror/addon/lint/lint.js'

window.CodeMirror = CodeMirror

CodeMirror.defineMode('nebula', modeNebula)
CodeMirror.registerHelper('lint', 'nebula', linterNebula)
