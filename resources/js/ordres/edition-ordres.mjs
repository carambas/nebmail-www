import { OrdresChemin } from './ordres-chemin.mjs'
import './ordres-contextmenu.mjs'
import { OrdresParser } from './ordres-parser.mjs'

let ordreCurrentLine
let nebdata

/**
 * Actions dans la fenêtre des ordres lorsqu'on tape Enter ou Tab : on regarde s'il n' s'agit pas
 * d'un pseudo ordre de déplacement du type F_f -> m (flèche)
 * @param {*} cm
 * @param {*} name
 * @returns
 */
function onKeyHandled(cm, name) {
    if (name !== 'Enter' && name !== 'Tab') {
        return
    }

    const line = name === 'Tab' ? cm.doc.getCursor().line : cm.doc.getCursor().line - 1
    const text = cm.doc.lineInfo(line).text

    const ordre = (new OrdresParser()).getTypeOrdre(text)

    // On a une flèche que si typeOrdre === 21 && sousTypeOrdre === 1
    if (ordre.typeOrdre !== 21 || ordre.sousTypeOrdre !== 1) {
        return
    }

    removeCmEventListeners()

    if (name === 'Enter') {
        cm.execCommand('deleteLine')
        cm.execCommand('goLineUp')
    }

    const ordresChemin = new OrdresChemin(nebdata)
    const numFlotte = ordre.flotte
    const from = nebdata.flottes[numFlotte].localisation
    const to = parseInt(ordre.chemin[0])

    const chemin = ordresChemin.trouvePlusCourtChemin(from, to, window.joueurTechnoDep ?? 3)

    if (!chemin.length) {
        return
    }

    const newText = `F_${numFlotte} MM ${chemin.join(' ')}`.padEnd(40) +
        `; F_${numFlotte} -> ${to} [dist=${chemin.length}]`

    cm.doc.replaceRange(newText, { line, ch: 0 }, { line, ch: text.length })

    ordreCurrentLine = null
    setCmEventListeners()
    forceEventLineChanged(cm)
}

function forceEventLineChanged(cm) {
    const pos = cm.doc.getCursor()
    cm.execCommand('goLineStart')
    ordreCurrentLine = null
    cm.doc.setCursor(pos)
}

function getNumMondeFromCursor(doc) {
    let mondeMatch
    let numLigne = doc.getCursor().line
    while (numLigne >= 0) {
        const ligne = doc.getLine(numLigne)
        mondeMatch = ligne.match(/@M_(\d+)@/)
        if (mondeMatch) {
            return mondeMatch[1]
        }
        numLigne--
    }

    return null
}

function setCmEventListeners() {
    window.myCodeMirrorOrdres.on('cursorActivity', cmEvent)
}

function removeCmEventListeners() {
    window.myCodeMirrorOrdres.off('cursorActivity', cmEvent)
}

function cmEvent(event) {
    if (ordreCurrentLine === event.doc.getCursor().line) {
        return
    }
    ordreCurrentLine = event.doc.getCursor().line

    const numMonde = getNumMondeFromCursor(event.doc)
    window.dispatchEvent(new CustomEvent('selection-monde', {
        detail: {
            monde: numMonde,
            from: 'ordres'
        }
    }))

    checkIfOrdreDeplacement(event.doc, numMonde)
}

function checkIfOrdreDeplacement(doc, numMonde) {
    // Trouver le contenu de la ligne courante
    const numLigne = doc.getCursor().line
    const ligne = doc.getLine(numLigne)
    const ordre = (new OrdresParser()).getTypeOrdre(ligne)

    if (!ordre?.isOrdreDeplacement) {
        window.dispatchEvent(new Event('effacer-chemin-flotte'))
        return
    }

    const origineChemin = nebdata.flottes[ordre.flotte].localisation
    const chemin = [origineChemin, ...ordre.chemin]

    window.dispatchEvent(new CustomEvent('afficher-chemin-flotte', {
        detail: {
            flotte: ordre.flotte,
            chemin
        }
    }))
}

function setCursorFromNumMonde(doc, numMonde) {
    const nblignes = doc.lineCount()
    let match = false
    let numLigne = 0
    let ligne

    do {
        ligne = doc.getLine(numLigne++)
        match = (ligne.search(`@M_${numMonde}@`) >= 0)
    } while ((numLigne < nblignes) && !match)

    if (!match) {
        return
    }

    numLigne = numLigne >= 2 ? numLigne - 2 : 0

    removeCmEventListeners()
    doc.setCursor(nblignes - 1, 0, {
        scroll: true
    })
    doc.setCursor(numLigne, 0, {
        scroll: true
    })
    setCmEventListeners()
}

function insereNumJou(event) {
    const doc = window.myCodeMirrorOrdres.doc
    const numJou = parseInt(event.detail, 10)
    const pseudo = nebdata.joueurs[numJou].pseudo

    // On récupère la ligne courante et l'emplacement du curseur
    const { line, ch } = doc.getCursor()
    const lineLength = doc.lineInfo(line).text.length

    // On remplace du curseur à la fin de ligne avec numéro de joueur et pseudo en commentaire
    doc.replaceRange(`${event.detail} ; ${pseudo}`, { line, ch }, { line, ch: lineLength })
}

function onBeforeUnload(event) {
    if (this.ordres_statut === 'changed') {
        event.returnValue = 'Plan non sauvegardé'
    }
}

function cmGutterClick(cm, line, gutter, event) {
    event.preventDefault()
    const warning = nebdata.warnings.find(element => element.ligne === line)

    if (!warning) {
        return false
    }

    const text = cm.doc.lineInfo(warning.ligne).text

    const replString = {
        F_ORDRE_EXCL: '@RIEN@',
        M_UP_PAS_CONSOMMEES: '@PRODOK@',
        M_PAS_ASSEZ_UP: '@PRODOK@',
        ERREUR_SYNTAXE: '@OK@'
    }[warning.type]

    cm.doc.replaceRange(' ; ' + replString, { line: warning.ligne, ch: text.length })
}

function onSelectionMonde(event) {
    if (['ordres', 'recherche'].includes(event.detail.from)) {
        return
    }

    const numMonde = event.detail.monde
    const doc = window.myCodeMirrorOrdres.doc

    setCursorFromNumMonde(doc, numMonde)
}

function onChange(event) {
    this.ordres_statut = event.doc.isClean() ? this.original_ordres_statut : 'changed'
}

export function alpineInit(self, livewireThis) {
    self.original_ordres_statut = self.ordres_statut

    const numJou = livewireThis.numJou

    window.addEventListener('dessine-plan', (e) => {
        nebdata = e.detail.nebdata
        self.crtxt = e.detail.crtxt

        window.OrdresContextMenu.Menu(nebdata)

        const myTextArea = document.getElementById('ordres')
        if (myTextArea != null) {
            const cm = window.CodeMirror.fromTextArea(myTextArea, {
                mode: 'nebula',
                dragDrop: false,
                autofocus: true,
                gutters: ['CodeMirror-lint-markers'],
                lint: { nebdata, numJou },
            })

            window.myCodeMirrorOrdres = cm

            cm.doc.setValue(livewireThis.ordres_txt)
            cm.doc.clearHistory()
            cm.doc.markClean()

            if (cm.doc.lineCount() < 8) {
                cm.setCursor(5, 0)
            }

            setCmEventListeners()
            cm.on('change', onChange.bind(self))
            cm.on('gutterClick', cmGutterClick.bind(self))
            cm.on('contextmenu', window.OrdresContextMenu.onContextMenu)
            cm.on('keyHandled', onKeyHandled.bind(self))
            document.addEventListener('insere-numjou', insereNumJou.bind(self))
            window.addEventListener('beforeunload', onBeforeUnload.bind(self))
            window.addEventListener('selection-monde', onSelectionMonde)
        }
    }, { once: true })
}

export function simul(self, livewireThis) {
    self.simul_eval = true
    document.getElementById('simul_button').blur()
    livewireThis.simul(window.myCodeMirrorOrdres.doc.getValue(), self.simul_eval)
}

export function sortieSimul(self) {
    self.simul_eval = false
    document.getElementById('sortie_simul_button').blur()

    window.dispatchEvent(
        new CustomEvent('dessine-plan', {
            detail: {
                nebdata,
                crtxt: self.crtxt,
                simul: false,
                ancientour: false
            }
        }))
}

export function envoi(self, livewireThis) {
    livewireThis.save(window.myCodeMirrorOrdres.doc.getValue())
    document.getElementById('envoi_button').blur()
    self.ordres_statut = 'sent'
    self.original_ordres_statut = self.ordres_statut
}

export function brouillon(self, livewireThis) {
    livewireThis.save(window.myCodeMirrorOrdres.doc.getValue(), true)
    document.getElementById('brouillon_button').blur()
    self.ordres_statut = 'draft'
    self.original_ordres_statut = self.ordres_statut
}
