/**
 * Trouve le chemin d'un point A à un point B
 */
export class OrdresChemin {
    constructor(data) {
        this.data = data
        this.chemins = []
    }

    exploreChemins(chemin, to, maxDist) {
        // chemin contient le chemin en cours d'élaboration (liste des mondes déjà traversés, etape = monde)
        const etape = chemin.at(-1)

        if (etape === to) {
            // Un chemin identifié, on le stocke
            this.chemins.push(chemin.splice(1))
            return
        }

        if (maxDist <= 0) {
            return
        }

        const connexions = this.data.mondes[etape]?.connexions ?? []

        connexions
            .filter(m => !chemin.includes(m))
            .filter(m => !this.data.mondes[m]?.isTrouNoir)
            .forEach(m => this.exploreChemins([...chemin, m], to, maxDist - 1))
    }

    sortCheminsByLength(a, b) {
        return a.length - b.length
    }

    trouvePlusCourtChemin(from, to, maxDist = 7) {
        this.exploreChemins([from], to, maxDist)
        this.chemins.sort(this.sortCheminsByLength)

        const chemin = this.chemins.length > 0 ? this.chemins[0] : []

        return chemin
    }
}
