import { ContextMenu } from '../contextmenujs-master/contextmenu.js'

export class OrdresContextMenu {
    static Menu(nebdata) {
        OrdresContextMenu.initItems(nebdata)
        OrdresContextMenu.menu = new ContextMenu(OrdresContextMenu.items)
    }

    static onContextMenu(instance, event) {
        OrdresContextMenu.menu.reload(OrdresContextMenu.items)
        event.preventDefault()
        OrdresContextMenu.menu.display(event)
    }

    static emit(eventName) {
        document.dispatchEvent(new Event(eventName))
    }

    static emitCustomEvent(eventName, distance) {
        const event = {
            detail: {
                numMonde: OrdresContextMenu.numMonde,
                element: OrdresContextMenu.element,
            }
        }

        if (typeof distance !== 'undefined') {
            event.detail.distance = distance
        }

        document.dispatchEvent(new CustomEvent(eventName, event))
    }

    static initItems(nebdata) {
        OrdresContextMenu.items = []

        for (const j in nebdata.joueurs) {
            OrdresContextMenu.items.push({
                text: nebdata.joueurs[j].pseudo,
                numero: j,
                icon: j,
                events: {
                    click: () => document.dispatchEvent(new CustomEvent('insere-numjou', { detail: j }))
                },
            })
        }
    }
}

window.OrdresContextMenu = OrdresContextMenu
