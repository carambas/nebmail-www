export class TypeOrdre {
    // typeOrdre
    // sousTypeOrdre
    // flotte
    // monde
    // erreur_syntaxe
    // ordre
    // isOrdreExclusifFlotte
    // isOrdreConstruction
    // isOrdreConstructionInd
    // isOrdreDeplacement
    // chemin
    // up_consommes

    constructor(ordresNumeros, typeOrdre, sousTypeOrdre) {
        this.ordre = ordresNumeros
        this.typeOrdre = parseInt(typeOrdre)
        this.sousTypeOrdre = parseInt(sousTypeOrdre)
        this.isOrdreExclusifFlotte = false
        this.isOrdreConstruction = false
        this.isOrdreConstructionInd = false
        this.isOrdreDeplacement = false
        this.isFlecheDeplacement = false
        this.ordreRechercheDep = false
        this.flotte = null
        this.monde = null
        this.erreur_syntaxe = null
        this.up_consommes = 0
        this.chemin = []

        this.detectOrdreExclusif()
        this.detectOrdreConstruction()
        this.detectOrdreDeplacement()
        this.detectFlecheDeplacement()
    }

    detectOrdreExclusif() {
        const ordresExclusifsFlottes = [
            '14.5', '14.6', '14.7', '14.8', '14.9', '14.10', '14.133',
            '14.11', '14.134', '14.135', '14.136', '20.1'
        ]

        if (ordresExclusifsFlottes.includes(String(this.typeOrdre) + '.' + String(this.sousTypeOrdre))) {
            this.isOrdreExclusifFlotte = true
        }

        const match = this.ordre.match(/^F[\s_]?(\d+)/)
        if (match) {
            this.flotte = parseInt(match[1])
        }
    }

    detectOrdreConstruction() {
        const ordresConstruction = [
            '3.1', '3.14', '3.2', '3.3', '3.4', '4.0', '3.5', '3.16', '3.17', '3.18', '3.19', '3.20',
            '3.21', '3.22', '3.23', '3.24', '3.8', '3.9', '3.10', '3.11', '3.13', '3.12', '5.1',
            '5.2', '5.3', '5.4'
        ]
        const ordreConstructionInd = ['6.0']

        const typeoch = String(this.typeOrdre) + '.' + String(this.sousTypeOrdre)

        if (ordresConstruction.includes(typeoch) || ordreConstructionInd.includes(typeoch)) {
            const match = this.ordre.match(/^M[\s_]?(\d+)[\s]?[CE][\s]?(\d+)/)
            if (match) {
                this.monde = match[1]
                this.up_consommes = match[2]
            }
        }

        if (ordresConstruction.includes(typeoch)) {
            this.isOrdreConstruction = true
        } else if (ordreConstructionInd.includes(typeoch)) {
            this.isOrdreConstructionInd = true
        }

        if (typeoch === '3.8') {
            this.ordreRechercheDep = true
        }
    }

    detectOrdreDeplacement() {
        this.isOrdreDeplacement = (this.typeOrdre === 14 && this.sousTypeOrdre === 11)

        if (!this.isOrdreDeplacement) return

        // rempli un tableau avec le chemin de la flotte
        this.chemin = [...this.ordre.matchAll(/\D(\d+)/g)]
            .map(match => parseInt(match[1]))
            .splice(1)
    }

    detectFlecheDeplacement() {
        this.isFlecheDeplacement = (this.typeOrdre === 21 && this.sousTypeOrdre === 1)

        if (!this.isFlecheDeplacement) return

        // rempli un tableau avec la destination du trajet demandé avec la flèche
        this.chemin = [...this.ordre.matchAll(/\D(\d+)/g)]
            .map(match => parseInt(match[1]))
            .splice(1)
    }

    isSyntaxOk() {
        return this.typeOrdre !== 21
    }
}

/**
 * Parser d'ordres de Nébula
 */
export class OrdresParser {
    // TabOrdresBrutSansEspaces;

    constructor() {
        this.TabOrdresBrutSansEspaces = []

        // Tableau des ordres...
        const TabOrdresBrut = {
            // Ordres spéciaux à exécuter avant l'éval [bots]
            'BOT d': '0.1',

            // Ordres de déclaration :
            'A : d': '1.1',
            'E : d': '1.2',
            'C : d': '1.3',
            'N : d': '1.4',
            'J : d': '1.5',
            'F d P': '1.7',
            'F d G': '1.8',
            'Z d': '1.6',
            Z: '1.6',
            'CP : d': '1.9',
            'NCP : d': '1.10',
            'DP : d': '1.11',
            'NDP : d': '1.12',
            'PI : d': '1.13',
            'NPI : d': '1.14',

            // Ordres de sondage :
            'VI M d S M d': '2.1',
            'VP M d S M d': '2.2',
            'F d S M d': '2.3',

            // Ordres de construction :
            'M d C d VC F d': '3.1',
            'M d C d VT F d': '3.14',
            'M d C d VI': '3.2',
            'M d C d VP': '3.3',
            'M d C d R': '3.4',
            'M d C d I': '6.0',
            'M d C d P': '4.0',
            'VI M d C d I': '7.0',
            'F d C BOMBE': '15.0',
            'M d T d R I': '6.1',
            'M d T d R F d': '3.15',
            'M d T d R VI': '3.6',
            'M d T d R VP': '3.7',

            // Ordres d'espionnage :
            'M d C d E d': '3.5',
            'M d C d E CM': '3.16',
            'M d C d E P': '3.17',
            'M d C d E CAR': '3.18',
            'M d C d E M': '3.19',
            'M d C d E F': '3.20',
            'M d C d E I': '3.21',
            'M d C d E V': '3.22',
            'M d C d E T': '3.23',
            'M d C d E IA': '3.24',

            // Ordres de recherche technologique :
            'M d C d DEP': '3.8',
            'M d C d ATT': '3.9',
            'M d C d DEF': '3.10',
            'M d C d RAD': '3.11',
            'M d C d CAR': '3.13',
            'M d C d ALI': '3.12',

            // Ordres de déchargement :
            'F d D d MP': '8.0',
            'F d D MP': '8.0',
            'F d D d PC': '9.0',
            'F d D PC': '9.0',
            'F d D d P': '5.8',
            'F d D P': '5.8',
            'F d D d C': '5.6',
            'F d D C': '5.6',
            'F d D d R': '5.7',
            'F d D R': '5.7',
            'F d D d N': '5.5',
            'F d D N': '5.5',
            'T d M': '10.0',

            // Ordres de transfert :
            'M d T d VI VP': '11.1',
            'M d T d VP VI': '11.2',
            'M d T d VI F d': '11.3',
            'M d T d VP F d': '11.4',
            'M d T d VI VT F d': '11.11',
            'M d T d VP VT F d': '11.12',
            'F d T d VC VI': '11.5',
            'F d T d VC VP': '11.6',
            'F d T d VC F d': '11.7',
            'F d T d VT F d': '11.8',
            'F d T d VC VT': '11.9',
            'F d T d VC VT F d': '11.10',

            // Ordres de chargement :
            'F d C d MP': '12.0',
            'F d C MP': '12.0',
            'F d C d P': '12.4',
            'F d C P': '12.4',
            'F d C d C': '12.2',
            'F d C C': '12.2',
            'F d C d R': '12.3',
            'F d C R': '12.3',
            'F d C d N': '12.1',
            'F d C N': '12.1',
            'T d F d': '13.0',

            // Ordres de migration :
            'M d E d P M d': '5.1',
            'M d E d C M d': '5.2',
            'M d E d N M d': '5.3',
            'M d E d R M d': '5.4',

            // Ordres de tir :
            'VP M d * C': '14.1',
            'VP M d * N': '14.2',
            'VI M d * F d': '14.3',
            'VP M d * F d': '14.4',
            'F d * I': '14.5',
            'F d * P': '14.6',
            'F d * M': '14.7',
            'F d * F d': '14.8',
            'VP M d ? N': '14.130',
            'VI M d ? F d': '14.131',
            'VP M d ? F d': '14.132',
            'F d ? I': '14.133',
            'F d ? P': '14.134',
            'F d ? M': '14.135',
            'F d ? F d': '14.136',

            // Ordres d'attaques spéciales :
            'F d L BOMBE': '14.9',
            'F d T d R': '14.10',
            'M d P': '18.0',
            'M d = s': '18.1',

            // Ordres de cadeaux :
            'F d C : d': '20.1',
            'M d C : d': '20.2',

            // Ordres d'exploration (MEVA)
            'F d E d VC P': '13.1',
            'F d E d VC CM': '13.2',

            // Ordres de douvement :
            'F d MM d': '14.11',
            'F d MM d d': '14.11',
            'F d MM d d d': '14.11',
            'F d MM d d d d': '14.11',
            'F d MM d d d d d': '14.11',
            'F d MM d d d d d d': '14.11',
            'F d MM d d d d d d d': '14.11',
            'F d M d': '14.11',
            'F d M d M d': '14.11',
            'F d M d M d M d': '14.11',
            'F d M d M d M d M d': '14.11',
            'F d M d M d M d M d M d': '14.11',
            'F d M d M d M d M d M d M d': '14.11',
            'F d M d M d M d M d M d M d M d': '14.11',

            // Flèche déplacement (pseudo ordre avec type 21 pour provoquer une erreur de syntaxe)
            'F d -> d': '21.1',
        }

        // ... auquel on supprime tous les espaces
        for (let key in TabOrdresBrut) {
            const value = TabOrdresBrut[key]
            key = key.replace(/\s*/g, '')
            this.TabOrdresBrutSansEspaces[key] = value
        }
    }

    /**
     *
     * @param {string} ordre
     * @returns {string}
     */
    supprimeCommentaires(ordre) {
        return ordre.replace(/[#;].*/g, '').trim()
    }

    /**
     *  Parse un ordre et indique si la syntaxe est connue ou non
     *
     * @param {string} str Ordre
     * @returns {boolean} Syntaxe connue ou non
     */
    isOrdreSyntaxeOK(ordre) {
        ordre = OrdresParser.getOrdreNormalise(ordre)
        const typeOrdre = this.getTypeOrdreFromOrdreNormalise(ordre).typeOrdre

        return (ordre === '') || (typeOrdre !== 21)
    }

    static getOrdreNormalise(ordre) {
        return ordre.toUpperCase()
            .replace(/".*"/g, 's') // Chaines => "s"
            .replace(/[#;].*/g, '') // Commentaires supprimés
            .replace(/\d+/g, 'd') // Nombres => "d"
            .replace(/[\s_]+/g, '') // Supprime tous les espaces
    }

    getTypeOrdreFromOrdreNormalise(ordreNormalise) {
        if (ordreNormalise === '') return { typeOrdre: 0, sousTypeOrdre: 0 }

        const to = this.TabOrdresBrutSansEspaces[ordreNormalise]?.split('.')
        const typeOrdre = to ? parseInt(to[0]) : 21
        const sousTypeOrdre = to ? parseInt(to[1]) : 0

        return { typeOrdre, sousTypeOrdre }
    }

    getTypeOrdre(ordre) {
        const ordresNumeros = ordre.toUpperCase()
            .replace(/".*"/g, 's') // Chaines => "s"
            .replace(/[#;].*/g, '') // Commentaires supprimés
            .replace(/[\s_]+/g, ' ') // Supprime tous les espaces

        ordre = ordresNumeros.replace(/\d+/g, 'd') // Nombres => "d"
            .replace(/[\s_]+/g, '') // Supprime tous les espaces

        const to = this.getTypeOrdreFromOrdreNormalise(ordre)
        const result = new TypeOrdre(ordresNumeros, to.typeOrdre, to.sousTypeOrdre)
        return result
    }

    /**
     *
     * @param {string} ordres
     * @param {integer} numMonde
     * @returns {array}
     */
    getOrdresMondeFromRawOrdres(ordres, numMonde) {
        if (!ordres) {
            return null
        }

        const lines = ordres.split('\n')

        if (!lines) {
            return null
        }

        const nbLignes = lines.length

        // Recherche début monde
        let match = false
        let numLigne = 0

        do {
            match = (lines[numLigne].search(`@M_${numMonde}@`) >= 0)
        } while ((++numLigne < nbLignes) && !match)

        if (!match || (numLigne === nbLignes)) {
            return null
        }

        const startLigne = numLigne - 1

        // Recherche monde suivant
        match = false
        do {
            match = lines[numLigne].match(/@M_(\d+)@/)
        } while ((++numLigne < nbLignes) && !match)

        return lines.slice(startLigne, numLigne - 1)
            .map((line) => {
                return {
                    ordre: this.supprimeCommentaires(line),
                    syntaxe: this.isOrdreSyntaxeOK(line)
                }
            })
            .filter((line) => line.ordre !== '')
    }
}
