import { ContextMenu } from '../contextmenujs-master/contextmenu.js'

export class PlanContextMenu {
    static Menu(numJoueur) {
        PlanContextMenu.initItems()

        document.querySelector('#svg_plan')
            .addEventListener('contextmenu', e => PlanContextMenu.onContextMenu(e, numJoueur))

        PlanContextMenu.menu = new ContextMenu([])
    }

    static onContextMenu(e, numJoueur) {
        const element = e.target.parentElement

        let menuItems = PlanContextMenu.items
        PlanContextMenu.numMonde = 0

        if (element.matches('[data-monde]')) {
            const numMonde = parseInt(element.getAttribute('data-monde'), 10)
            PlanContextMenu.numMonde = numMonde
            PlanContextMenu.element = element

            if (numJoueur <= 0) {
                menuItems = menuItems.filter(item => !item.joueur)
            }

            // Cercle monde de départ
            menuItems = element.querySelector('.cercle-md')
                ? menuItems.filter(item => !item.md)
                : menuItems.filter(item => !item.nonmd)

            // Affichage du numéro du monde dans le libellé du menuitem
            menuItems = menuItems.map(function (item) {
                if (item.text) {
                    item.text = item.text.replace(/M_\d+/, `M_${numMonde}`)
                }
                return item
            })

            // Affichage menu placement mondes
            if (!element.matches('.cnx-manquante')) {
                menuItems = menuItems.filter(item => !item.placemonde)
            }

            // Filtrage en cas de joueur neutre
            if (element.querySelector('.jou_0')) {
                menuItems = menuItems.filter(item => !item.non_neutre)
            }
        } else {
            menuItems = menuItems.filter(item => !item.monde)
        }

        PlanContextMenu.menu.reload(menuItems)
        PlanContextMenu.menu.display(e)
    }

    static emit(eventName) {
        document.dispatchEvent(new Event(eventName))
    }

    static emitCustomEvent(eventName, distance) {
        const event = {
            detail: {
                numMonde: PlanContextMenu.numMonde,
                element: PlanContextMenu.element,
            }
        }

        if (typeof distance !== 'undefined') {
            event.detail.distance = distance
        }

        document.dispatchEvent(new CustomEvent(eventName, event))
    }

    static initItems() {
        const distanceItems = []
        for (let d = 1; d <= 7; d++) {
            distanceItems.push({
                text: `distance ${d}`,
                events: {
                    click: () => PlanContextMenu.emitCustomEvent('show-mondes-distance', d)
                }
            })
        }

        PlanContextMenu.items = [
            {
                text: 'Trouver un monde (Ctrl+M)',
                icon: '<iconify-icon icon="ic:search" class="mr-1 md-24"></iconify-icon>',
                events: {
                    click: () => PlanContextMenu.emit('trouver-monde')
                }
            },
            {
                text: 'Trouver une flotte (Ctrl+L)',
                icon: '<iconify-icon icon="ic:search" class="mr-1 md-24"></iconify-icon>',
                events: {
                    click: () => PlanContextMenu.emit('trouver-flotte')
                }
            },
            {
                text: 'Trouver un trésor (Ctrl+R)',
                icon: '<iconify-icon icon="ic:search" class="mr-1 md-24"></iconify-icon>',
                events: {
                    click: () => PlanContextMenu.emit('trouver-tresor')
                }
            },
            {
                type: ContextMenu.DIVIDER,
                monde: true,
            },
            {
                text: 'Historique M_1',
                monde: true,
                icon: '<iconify-icon icon="ic:history" class="mr-1 md-24"></iconify-icon>',
                events: {
                    click: () => PlanContextMenu.emitCustomEvent('historique-cr')
                }
            },
            {
                text: 'Indiquer les mondes accessibles en...',
                monde: true,
                icon: '<iconify-icon icon="ic:send" class="mr-1 md-24"></iconify-icon>',
                sub: distanceItems,
            },
            {
                text: 'M_1 est un monde de départ',
                icon: '<iconify-icon icon="ic:outline-circle" class="text-danger mr-1 md-24"></iconify-icon>',
                monde: true,
                joueur: true,
                md: true,
                events: {
                    click: () => PlanContextMenu.emitCustomEvent('marque-md')
                }
            },
            {
                text: 'M_1 n\'est pas un monde de départ',
                icon: '<iconify-icon icon="ic:cancel" class="mr-1 md-24"></iconify-icon>',
                monde: true,
                joueur: true,
                nonmd: true,
                events: {
                    click: () => PlanContextMenu.emitCustomEvent('marque-md')
                }
            },
            {
                text: 'Changer la couleur du joueur',
                monde: true,
                non_neutre: true,
                icon: '<iconify-icon icon="ic:outline-color-lens" class="mr-1 md-24"></iconify-icon>',
                events: {
                    click: () => PlanContextMenu.emitCustomEvent('changer-couleur')
                }
            },
            {
                text: 'Placer sur le plan les mondes connectés',
                icon: '<iconify-icon icon="ic:outline-share" class="mr-1 md-24"></iconify-icon>',
                monde: true,
                placemonde: true,
                events: {
                    click: () => PlanContextMenu.emitCustomEvent('place-mondes')
                }
            },
            {
                text: 'Nettoyer l\'affichage',
                icon: '<iconify-icon icon="ic:outline-cleaning-services" class="mr-1 md-24"></iconify-icon>',
                events: {
                    click: () => PlanContextMenu.emit('nettoyer-affichage')
                }
            },
        ]
    }
}
