/**
 * Classe découpant un CR monde par monde
 * @property {string} cr - Le cr txt partie mondes
 * @property {string[]} crs - Tableau des cr de chaque monde
 * @property {string} enTete - En-tête du CR
 * @property {string} ordes - Récapitulatif des ordres
 */
export class Cr {
    constructor(cr) {
        this.crs = []

        if (!cr) {
            console.error('CR vide')
            return
        }

        let matches = cr.replace(/\r/sg, '')
        let crMondes = ''
        let enTete = ''
        let ordres = ''

        if (cr.includes('Ordres du joueur')) {
            matches = matches.match(/(?<enTete>.*?)(?<cr>^Md?[#_]?(\d+)\s.*?)?(?<ordres>^Ordres du joueur.*)/ms)
            crMondes = matches?.groups.cr ?? ''
            enTete = matches?.groups.enTete ?? ''
            ordres = matches?.groups.ordres ?? ''
        } else { // le CR de l'arbitre n'a pas d'en-têtes ni d'ordres
            matches = true
            crMondes = cr
        }

        // this.cr = ''
        if (matches) {
            this.enTete = enTete
            this.cr = crMondes
            this.ordres = ordres

            this.splitCrTxtInArray()
        } else {
            console.error('Pas de Cr trouvé')
            this.crs = []
        }
    }

    splitCrTxtInArray() {
        // regexp pour attraper un monde sur le CR :
        // (?:\".*?\"\n)? : peut éventuellement commencer par "..." dans le cas d'un nom de monde
        // puis ^(Md?[#_]?(\d+) : début d'un monde M_m M#m Md_m Md#m
        // Se termine par une ligne vide \n\n
        // const matches = this.cr.matchAll(/^((?:\".*?\"\n)?M[#d]?_(\d+) .*?)\n\n/smg)
        const matches = this.cr.matchAll(/((?:^".*?"\n)?^Md?[#_]?(\d+)\s.*?)\n\n/smg)

        this.crs = []

        for (const match of matches) {
            this.crs[match[2]] = match[1]
        }
    }

    /**
     * Reconstitue le CR txt complet
     * @returns {string}
     */
    getCrTxt() {
        return this.enTete + this.crPartieMondes() + '\n\n' + this.ordres
    }

    /**
     *
     * @returns Recontitue la partie "mondes" du CR
     */
    crPartieMondes() {
        return this.crs.filter(Boolean).join('\n\n')
    }

    crMondes() {
        return this.crs
    }

    getCrMonde(numero) {
        return this.crs[numero] ?? null
    }

    addCrMonde(crMonde) {
        const matches = crMonde.replace(/\r/sg, '').match(/((?:^".*?"\n)?^Md?[#_]?(\d+)\s.*)/sm)
        this.crs[matches[2]] = matches[1]
    }
}
