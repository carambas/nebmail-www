export class DessineCheminFlotte {
    constructor(plan) {
        this.plan = plan
        this.cheminDisplayed = false
        this.elementsDom = []
        window.addEventListener('afficher-chemin-flotte', this.afficheChemin.bind(this))
        window.addEventListener('effacer-chemin-flotte', this.supprimeChemin.bind(this))
    }

    afficheChemin(event) {
        const chemin = event.detail.chemin
        this.supprimeChemin()

        // On sélectionne tous les mondes du chemin
        for (let i = 0; i < chemin.length; i++) {
            const numMonde1 = chemin[i]
            const numMonde2 = chemin[i + 1] ?? 0

            // TODO ajouter le niveau de danger à traverser le monde
            this.ajouteElementMonde(numMonde1)
            this.ajouteElementConnexion(numMonde1, numMonde2)
            this.ajouteElementConnexion(numMonde2, numMonde1)
        }
        this.afficheCheminElementsDom()
        this.cheminDisplayed = true
    }

    ajouteElementMonde(numMonde) {
        this.elementsDom.push(document.querySelector(`[data-monde="${numMonde}"]`))
    }

    ajouteElementConnexion(numMonde1, numMonde2) {
        const element = document.querySelector(`#C${numMonde1}_${numMonde2}`)

        if (element) this.elementsDom.push(element)
    }

    afficheCheminElementsDom() {
        this.elementsDom.forEach(elt => elt.classList.add('chemin-flotte-success'))
    }

    supprimeChemin() {
        if (!this.cheminDisplayed) return

        this.elementsDom.forEach(elt => elt.classList.remove('chemin-flotte-success', 'chemin-flotte-warning', 'chemin-flotte-danger'))
        this.elementsDom = []
        this.cheminDisplayed = false
    }
}
