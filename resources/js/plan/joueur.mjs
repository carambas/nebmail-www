export class Joueur {
    constructor(joueur, numJou) {
        for (const property in joueur) {
            this[property] = joueur[property]
        }

        if (numJou !== undefined) {
            this.numJou = numJou
        }
    }

    getEquipeOrNumJou() {
        return (this.equipe) ? this.equipe : this.numJou
    }
}
