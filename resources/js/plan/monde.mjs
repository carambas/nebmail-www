export class Monde {
    constructor(monde, joueur, cr) {
        // TODO gérer le cas d'un monde null
        for (const property in monde) {
            this[property] = monde[property]
        }
        this.joueur = joueur
        this.cr = cr
        this.nbflottes = 0
        this.nbvcflottes = 0
        this.nbflottesA = 0
        this.nbvcflottesA = 0
        this.nbflottesE = 0
        this.nbvcflottesE = 0
        this.allie = false
    }

    getXBitmap(tailleMonde, x = null) {
        x = x ?? this.x
        return (2 * x - 0.5) * tailleMonde
    }

    getYBitmap(tailleMonde, y = null) {
        y = y ?? this.y
        return (2 * y - 0.5) * tailleMonde
    }

    getLargeur(tailleMonde) {
        return tailleMonde * 1.4
    }
}
