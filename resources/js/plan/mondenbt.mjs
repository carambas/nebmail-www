/**
 * Objet contenant un monde tel que lu dans un fichier nbt
 * @property {int} numero - numéro du monde
 * @property {string} nom
 * @property {int} duree
 * @property {int|string} proprietaireConvertis
 * @property {int|string} proprietaire
 * @property {int|string} ancienProprietaire
 * @property {int|string} premierProprietaire
 * @property {int} industries
 * @property {int} industriesActives
 * @property {int} vi
 * @property {int} actionVi
 * @property {int} cibleVi
 * @property {int} populations
 * @property {int} convertis
 * @property {int} robots
 * @property {int} limitePopulation
 * @property {int} vp
 * @property {int} actionVp
 * @property {int} cibleVp
 * @property {int} populationsMortes
 * @property {int} convertisMorts
 * @property {int} robotsDetruits
 * @property {int} capaciteMiniere
 * @property {int} nbDechargementPc
 * @property {int} nbPcDecharges
 * @property {int} mp
 * @property {int} populationsDisponibles
 * @property {int} nbPillage
 * @property {int} toursRecupPillage
 * @property {int} isCapture
 * @property {int} isCadeau
 * @property {int} isTrouNoir
 * @property {int} isBombe
 * @property {int} isPille
 * @property {int} isCorrompu
 * @property {int} x
 * @property {int} y
 * @property {int} nbExplo
 * @property {int} nbExploCm
 * @property {int} nbExploPop
 * @property {int} nbExploLocalisation
 * @property {int} potentielExploration
 * @property {int} isLoc
 * @property {int} isObs
 * @property {int} isVis
 * @property {int[]} connexions
 * @property {bool[]} connexionsVues
 * @property {bool[]} aStationne
 * @property {bool[]} estVisible
 * @property {bool[]} aVuCoord
 * @property {int[]} possessionHisto
 * @property {bool} tourPasse
 */
export class MondeNbt {
    constructor() {
        this.numero = 0
        this.tourPasse = false
        this.tour = null
        this.nom = null
        this.duree = 0
        this.proprietaireConvertis = 0
        this.proprietaire = 0
        this.ancienProprietaire = 0
        this.premierProprietaire = 0
        this.industries = 0
        this.industriesActives = 0
        this.vi = 0
        this.actionVi = 0
        this.cibleVi = 0
        this.populations = 0
        this.convertis = 0
        this.robots = 0
        this.limitePopulation = 0
        this.vp = 0
        this.actionVp = 0
        this.cibleVp = 0
        this.populationsMortes = 0
        this.convertisMorts = 0
        this.robotsDetruits = 0
        this.capaciteMiniere = 0
        this.nbDechargementPc = 0
        this.nbPcDecharges = 0
        this.mp = 0
        this.populationsDisponibles = 0
        this.nbPillage = 0
        this.toursRecupPillage = 0
        this.isCapture = false
        this.isCadeau = false
        this.isTrouNoir = false
        this.isBombe = false
        this.isPille = false
        this.isCorrompu = false
        this.x = 0
        this.y = 0
        this.nbExplo = 0
        this.nbExploCm = 0
        this.nbExploPop = 0
        this.nbExploLocalisation = 0
        this.potentielExploration = 0
        this.isLoc = false
        this.isObs = false
        this.isVis = false
        this.connexions = []
        this.connexionsVues = []
        this.aStationne = []
        this.estVisible = []
        this.aVuCoord = []
        this.possessionHisto = []
    }
}
