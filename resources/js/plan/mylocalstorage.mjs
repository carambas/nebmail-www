import localforage from 'localforage'

export class MyLocalStorage {
    // partieName
    // numeroJoueur

    constructor(partieName, numeroJoueur) {
        this.partieName = partieName
        this.numeroJoueur = numeroJoueur
    }

    async clean() {
        await localforage.clear()
    }

    getStorageKey(key) {
        return `${this.partieName}.j${this.numeroJoueur}.${key}`
    }

    async getItem(keyName) {
        return await localforage.getItem(this.getStorageKey(keyName))
    }

    async setItem(keyName, value) {
        await localforage.setItem(this.getStorageKey(keyName), value)
    }

    async removeItem(keyName, value) {
        await localforage.removeItem(this.getStorageKey(keyName))
    }

    /**
     * get item with localStorage API
     * !!! N'est pas lu au même endroit que getItem
     * @param keyName string
     * @returns object
     */
    getItemSync(keyName) {
        const value = localStorage.getItem(this.getStorageKey(keyName))
        return value ? JSON.parse(value) : null
    }

    /**
     * set item with localStorage API
     * !!! N'est pas écrit au même endroit que setItem
     * @param keyName string
     * @param value object
     */
    setItemSync(keyName, value) {
        localStorage.setItem(this.getStorageKey(keyName), JSON.stringify(value))
    }
}
