import { MondeNbt } from './mondenbt.mjs'

/**
 * Classe parsant un extrait de CR et retournant un objet monde
 * @property {string} txt - Le cr txt du monde
 * @property {MondeNbt} monde - Le cr txt du monde
 * @property {bool} erreur - Une erreur a été rencontrée lors du parsing
 */
export class ParseExtrait {
    /**
     * Constructor
     * @param {object} extrait
     */
    constructor(extrait) {
        this.txt = extrait.content
        this.monde = new MondeNbt()
        this.monde.tour = extrait.tour
        this.erreur = false
        try {
            this.parse()
        } catch (err) {
            this.erreur = true
            console.log('Une erreur a été rencontrée lors du parsing de l\'extrait :')
            console.log('%c' + this.txt, 'color: red')
            console.info(err)
        }
    }

    parse() {
        // On ne garde que la partie description du monde sans trésors et flottes
        const lignes = this.txt
            .replace(/^\s*\n.*/msg, '') // On supprime ce qui suit une ligne blanche
            .split('\n')

        const txt = lignes.reduce((acc, ligne) => {
            if (this.ligneIsNomMonde(ligne)) return acc
            if (!ligne.match(/^(\s+T|\s+[{]?F)/)) acc.push(ligne) // On ne garde tout ce qui ne correspond pas à un trésor, une flotte ou une trace
            return acc
        }, [])
            .join(' ') // On remet tout sur une seule ligne
            .replace(/\s+/g, ' ') // On supprime les espaces en trop

        let reste = txt
        reste = this.parseNomNumeroConnexionsEtProprio(reste)
        reste = this.parseIndustries(reste)
        reste = this.parsePopulation(reste)
        reste = this.parseMp(reste)
        reste = this.ParsePe(reste)
        reste = this.ParseExplo(reste)
        reste = this.ParsePillage(reste)
        reste = this.ParsePc(reste)
        this.ParseCoord(reste)
    }

    ligneIsNomMonde(ligne) {
        const matches = ligne.match(/^"(.*?)"/)
        if (matches) {
            this.monde.nom = matches[1]
            return true
        }

        return false
    }

    parseNomNumeroConnexionsEtProprio(txt) {
        // Numéro, connexions, bloc proptiétaire
        const matches = txt.match(/(?<nom>^".*?")?^Md?[#_]?(?<numero>\d+)\s+(?<connexions>\(.*?\))\s(?<proprietaire>".*?"\S*)?/m)
        this.monde.numero = parseInt(matches.groups.numero)
        this.parseConnexions(matches.groups.connexions)
        this.parseProprietaire(matches.groups.proprietaire)

        return txt.replace(matches[0], '')
    }

    parseConnexions(str) {
        this.monde.connexions = str.replace(/[()\s]*/g, '')
            .split(',')
            .map(cnx => parseInt(cnx))
    }

    parseProprietaire(str) {
        const matches = str?.match(/"(?<proprietaire>[^()"]*?)?(\((?<ancienProprietaire>[^()"]+)\))?(:(?<duree>\d+))?(:(?<dureeSupExplo>\d+))?"(?<flags>\S*)/)
        this.monde.proprietaire = matches?.groups.proprietaire ?? 0
        this.monde.ancienProprietaire = matches?.groups.ancienProprietaire ?? 0
        this.monde.duree = parseInt(matches?.groups.duree ?? 0)
        if (matches?.groups.dureeSupExplo) {
            this.monde.nbExploCm = (parseInt(matches.groups.dureeSupExplo) - this.monde.duree) / 3
        }

        if (matches?.groups.flags) {
            if (matches.groups.flags.includes('!')) this.monde.isCapture = true
            if (matches.groups.flags.includes('C')) this.monde.isCadeau = true
            if (matches.groups.flags.includes('P')) this.monde.isPille = true
        }
    }

    parseIndustries(txt) {
        let matches = txt.match(/\s(\[(I=(?<ind>[\d/]+))?\](=(?<vi>\d+))?(\S*))/)
        if (!matches) {
            matches = txt.match(/(I=(?<ind>[\d/]+))/)
        }

        if (matches) { // Parsing du bloc industries
            this.monde.vi = matches.groups.vi ?? 0
            const blocInd = matches.groups.ind ?? '0'
            const [indActives, indTotales] = blocInd.split('/')
            this.monde.industriesActives = parseInt(indActives)
            this.monde.industries = parseInt(indTotales ?? indActives)

            return txt.replace(matches[0], '')
        }

        // Pas de bloc industries
        return txt
    }

    parsePopulation(txt) {
        const matchesPseudo = txt.match(/"(.*?)"/)
        let pseudoProprioConvertis = null
        if (matchesPseudo) {
            pseudoProprioConvertis = matchesPseudo[1]
            txt = txt.replace(`"${pseudoProprioConvertis}"`, '%s%')
            this.monde.proprietaireConvertis = pseudoProprioConvertis
        }
        // const matches = txt.match(/((\[?P=.*?))(?<reste>\s.*)/)
        const matches = txt.match(/\s?((\[?P=\S+))/)
        let blocPop = matches[1]
        const reste = txt.replace(matches[0], '')

        // VP
        const matchesVp = blocPop.match(/\[(?<blocPop>.*)\]=(?<vp>(\d+)?)(?<flag>.*)/)
        if (matchesVp) {
            this.monde.vp = parseInt(matchesVp.groups.vp)
            blocPop = matchesVp.groups.blocPop
        }

        // Populations
        const matchesPop = blocPop.match(/P=(?<pop>\d+)(?<type>C|R)?(?<morts>.*?)?(\((?<max>\d+)\))(\/(?<convertis>\d+)C)?/)
        switch (matchesPop.groups.type) {
            case 'R':
                this.monde.robots = parseInt(matchesPop.groups.pop)
                break
            case 'C':
                this.monde.convertis = parseInt(matchesPop.groups.pop)
                this.monde.populations = parseInt(matchesPop.groups.pop)
                this.monde.proprietaireConvertis = this.monde.proprietaire
                break
            default:
                this.monde.populations = parseInt(matchesPop.groups.pop) ?? 0
        }

        if (matchesPop.groups.convertis) {
            this.monde.convertis = parseInt(matchesPop.groups.convertis)
            if (!pseudoProprioConvertis) {
                this.monde.proprietaireConvertis = this.monde.proprietaire
            }
        }

        return reste
    }

    parseMp(txt) {
        const matches = txt.match(/\sMP=(?<mp>\d+)?(\(\+(?<capaciteMiniere>\d+)\))?/)

        if (matches) {
            this.monde.mp = parseInt(matches.groups.mp ?? 0)
            this.monde.capaciteMiniere = parseInt(matches.groups.capaciteMiniere ?? 0)
            txt = txt.replace(matches[0], '')
        }

        return txt
    }

    ParsePe(txt) {
        const matches = txt.match(/\sPE=(?<potentielExploration>\d+)/)

        if (matches) {
            this.monde.potentielExploration = parseInt(matches.groups.potentielExploration)
            txt = txt.replace(matches[0], '')
        }

        return txt
    }

    ParseExplo(txt) {
        const matches = txt.match(/\sE=(?<nbExplo>\d+)\S*/)

        if (matches) {
            this.monde.nbExplo = parseInt(matches.groups.nbExplo)
            txt = txt.replace(matches[0], '')
        }

        return txt
    }

    ParsePillage(txt) {
        const matches = txt.match(/\sPi=(?<nbPillage>\d+)(\/(?<toursRecupPillage>\d+))?/)

        if (matches) {
            this.monde.nbPillage = parseInt(matches.groups.nbPillage ?? 0)
            this.monde.toursRecupPillage = parseInt(matches.groups.toursRecupPillage ?? 0)
            txt = txt.replace(matches[0], '')
        }

        return txt
    }

    ParsePc(txt) {
        const matches = txt.match(/\sPC=(?<nbDechargementPc>\d+)/)

        if (matches) {
            this.monde.nbDechargementPc = parseInt(matches.groups.nbDechargementPc)
            txt = txt.replace(matches[0], '')
        }

        return txt
    }

    ParseCoord(txt) {
        const matches = txt.match(/\s\[(?<x>\d+),(?<y>\d+)\]/)

        if (matches) {
            this.monde.x = parseInt(matches.groups.x ?? 0)
            this.monde.y = parseInt(matches.groups.y ?? 0)
            txt = txt.replace(matches[0], '')
        }

        return txt
    }
}
