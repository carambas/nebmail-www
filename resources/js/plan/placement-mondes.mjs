import { PlanModulo } from './planmodulo.mjs'
import { Monde } from './monde.mjs'

export class PlacementMondes {
    constructor(plan, plandrag) {
        this.plan = plan
        this.plandrag = plandrag
        document.addEventListener('lock-plan', this.onLockPlan.bind(this))
        document.addEventListener('unlock-plan', this.onUnLockPlan.bind(this))
        document.addEventListener('place-mondes', this.onPlaceMondes.bind(this))
    }

    getMondesAvecVoisinsNonPlaces() {
        const result = []
        for (const monde of Object.values(this.plan.mondes)) {
            for (const m of monde.connexions) {
                if ((!this.plan.mondes[m]?.x || !this.plan.mondes[m]?.y) && !result.includes(monde.numero)) {
                    result.push(monde.numero)
                }
            }
        }

        return result
    }

    getMondesVoisinsNonPlaces(numMonde) {
        return this.plan.mondes[numMonde].connexions.filter(num => !this.plan.mondes[num]?.x || !this.plan.mondes[num]?.y)
    }

    /**
     * Ajoute la classe cnx-manquante au group svg des mondes passés en paramètre
     * @param {int[]} numMondes
     */
    setCnxManquanteClass() {
        const numMondes = this.getMondesAvecVoisinsNonPlaces()

        this.unsetCnxManquanteClass()

        for (const m of numMondes) {
            document.querySelector(`[data-monde="${m}"]`)?.classList.add('cnx-manquante')
        }
    }

    unsetCnxManquanteClass() {
        document.querySelectorAll('.cnx-manquante').forEach(groupElt => { groupElt.classList.remove('cnx-manquante') })
    }

    onLockPlan() {
        this.unsetCnxManquanteClass()
    }

    onUnLockPlan() {
        this.setCnxManquanteClass()
    }

    /**
     * Trouve un emplacement avec connexions bien orientées et situé à une distance max de 3
     * @param {*} coordDepart
     * @returns
     */
    trouveEmplacementIdealDistanceTrois(coordDepart) {
        const verifPlan = this.plan.verifPlan

        // On cherche un monde à une distance de 3 max permettant une connexion bien orientée
        for (let distance = 1; distance <= 3; distance++) {
            for (let dx = -1; dx <= 1; dx++) {
                for (let dy = -1; dy <= 1; dy++) {
                    if (dx === 0 && dy === 0) continue

                    const coord = PlanModulo.getModuloCoord({
                        x: coordDepart.x + dx * distance,
                        y: coordDepart.y + dy * distance
                    })

                    if (!verifPlan.emplacementOccupe(coord.x, coord.y)) {
                        return coord
                    }
                }
            }
        }

        return null
    }

    trouveEmplacementQuelconque(coordDepart, connexionAcceptee = false) {
        const verifPlan = this.plan.verifPlan
        const coordLibres = []

        console.log('trouveEmplacementQuelconque (dernier recours)', coordDepart, connexionAcceptee)

        // On classe les emplacements libres du plan par ordre décroissant
        for (let dx = 0; dx < this.plan.longX; dx++) {
            for (let dy = 0; dy < this.plan.longY; dy++) {
                if (dx === 0 && dy === 0) continue

                const coord = PlanModulo.getModuloCoord({
                    x: coordDepart.x + dx,
                    y: coordDepart.y + dy
                })

                if (!verifPlan.emplacementOccupe(coord.x, coord.y)) {
                    const vecteur = PlanModulo.getDistanceModulo({ x: dx, y: dy })
                    const distance = Math.max(Math.abs(vecteur.x), Math.abs(vecteur.y))

                    coordLibres.push({
                        coord,
                        distance
                    })
                }
            }
        }
        coordLibres.sort((a, b) => a.distance - b.distance)

        return coordLibres.length > 0
            ? coordLibres[0].coord
            : this.trouveEmplacementQuelconque(coordDepart, true)
    }

    trouveEmplacementLibre(coordDepart) {
        return this.trouveEmplacementIdealDistanceTrois(coordDepart) ??
            this.trouveEmplacementQuelconque(coordDepart)
    }

    placeMondeCoord(numMonde, coord, numMondesConnectes) {
        let monde = this.plan.mondes[numMonde]

        if (!monde) {
            monde = new Monde(null, 0, null)
            monde.numero = numMonde
            monde.connexions = []
            this.plan.mondes[numMonde] = monde
        }

        monde.x = coord.x
        monde.y = coord.y
        monde.oldX = 0
        monde.oldY = 0
        monde.coordChanged = true
        monde.nouveau = true
        this.plan.verifPlan.addMonde(monde, numMondesConnectes.map(num => this.plan.mondes[num]))
    }

    placeMonde(numMonde, numMondeFrom) {
        const mondeFrom = this.plan.mondes[numMondeFrom]
        const numMondesConnectes = this.plan.getMondeConnexions(numMonde)

        const coord = this.trouveEmplacementLibre({ x: mondeFrom.x, y: mondeFrom.y })

        this.placeMondeCoord(numMonde, coord, numMondesConnectes)

        // Si les mondes ayant une connexion avec le monde placé ne sont pas présents dans
        // this.numMondesVoisinsARedessiner, on les y ajoute
        numMondesConnectes
            .filter(num => !this.numMondesVoisinsARedessiner.includes(num))
            .forEach(num => this.numMondesVoisinsARedessiner.push(num))
    }

    onPlaceMondes(e) {
        const numMonde = e.detail.numMonde
        this.numMondesVoisinsARedessiner = []

        // Trouver l'emplacement disponible le plus proche
        const mondesVoisinsNonPlaces = this.getMondesVoisinsNonPlaces(numMonde)
        for (const m of mondesVoisinsNonPlaces) {
            this.placeMonde(m, numMonde)
        }

        // On affiche le monde sur le plan
        document.dispatchEvent(new CustomEvent('monde-position-changed', {
            detail: {
                mondes: [...this.numMondesVoisinsARedessiner, ...mondesVoisinsNonPlaces],
            }
        }))

        this.setCnxManquanteClass()
    }
}
