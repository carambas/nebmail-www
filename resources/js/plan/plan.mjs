import { Joueur } from './joueur.mjs'
import { Monde } from './monde.mjs'
import { Cr } from './cr.mjs'
import { SvgTools } from './svgtools.mjs'
import { PlanMonde } from './planmonde.mjs'
import { PlanBtn } from './planbtn.mjs'
import { PlanDrag } from './plandrag.mjs'
import { PlanMap } from './planmap.mjs'
import { OrdresParser } from '../ordres/ordres-parser.mjs'
import { PlanHisto } from './planhisto.mjs'
import { MyLocalStorage } from './mylocalstorage.mjs'
import { PlanContextMenu } from './contextmenu.mjs'
import { TrouveMondesDistance } from '../nebutil/trouvemondedistance.mjs'
import { PlanSaveCoord } from './plansavecoord.mjs'
import { VerifPlan } from './verif-plan.mjs'
import { PlanModulo } from './planmodulo.mjs'
import { DessineCheminFlotte } from './dessine-chemin-flotte.mjs'
import { Tooltip } from '../tooltip-cr/tooltip.mjs'
import * as Combats from '../eval/combats.js'

/**
 * Classe englobant l'affichage du plan
 * @property {HTMLElement} svg - Elément SVG dans lequel le plan sera affiché
 * @property {HTMLElement} svg_g - Groupe SVG à l'intérieur de svg
 * @property {Monde[]} mondes - Tableau des mondes
 * @property {Joueur[]} joueurs - Tableau des mondes
 * @property {Object} decalage - Décalage du plan
 * @property {PlanHisto} planHisto - Historique du plan et CR
 * @property {array} customCoord - Coordonnées personnalisées par le joueur prenant le pas sur les coordonnées indiquées dans nebdata
 * @property {int} taille_monde - Taille du rectangle d'un monde
 * @property {Cr} cr - Classe d'accès au CR texte du tour
 *  */
class Plan {
    constructor(svg, nomPartie, userId, numJoueur, tour, crApiUrl, apiToken, customCoord) {
        // inits
        window.plan = this
        this.nomPartie = nomPartie
        this.userId = userId
        this.numJoueur = numJoueur
        this.tour = tour
        this.customCoord = customCoord
        this.svg = svg
        this.svg_g = document.querySelector('#svg_plan_group')
        this.taille_monde = 30
        this.largeur = 1.4 * this.taille_monde
        this.decalage = { x: 0, y: 0 }
        this.drawed = false
        this.simul = false
        this.mode = 1
        this.zoom = 10
        this.show_pillage = true
        this.show_explo = true
        this.planHisto = null
        this.selected_monde = 0
        this.mondesADeplacer = []
        this.fleches = {
            ArrowRight: { x: -1, y: 0 },
            ArrowLeft: { x: 1, y: 0 },
            ArrowDown: { x: 0, y: -1 },
            ArrowUp: { x: 0, y: 1 }
        }

        this.planHisto = new PlanHisto({
            plan: this,
            nomPartie,
            tour,
            userId,
            numJoueur,
            crApiUrl,
            apiToken,
            planButtons: true,
        })

        this.myStorage = new MyLocalStorage(nomPartie, numJoueur)

        this.disMonde = new PlanMonde(this.taille_monde)
        this.disMonde.addDefs(svg)
        this.drag = new PlanDrag(this)
        this.planSaveCoord = new PlanSaveCoord(this)
        this.verifPlan = new VerifPlan(this)
        this.dessineCheminFlotte = new DessineCheminFlotte(this)

        this.loadCrDernierTour()
        this.initEventListeners()
    }

    initEventListeners() {
        this.svg.addEventListener('click', this.onClick.bind(this))
        this.svg.addEventListener('auxclick', this.onAuxClick.bind(this))
        this.svg.addEventListener('mousedown', this.onAuxClick.bind(this))
        this.svg.addEventListener('blur', event => this.onBlur(event))
    }

    async loadCrDernierTour() {
        const crTxtNbt = await this.planHisto.getLastCrFromApi()
        const cr = crTxtNbt.cr
        this.nebdata = crTxtNbt.nbt

        this.drawMapGroup()
        this.disMap = new PlanMap()

        this.drawConnexionsGroup()
        this.drawMondesGroup()
        this.updateData(this.nebdata, cr)
        this.setSvgSize()
        this.readDecalage(true)

        this.ordres_parser = new OrdresParser()
        this.planHisto.getHistoryCr()
            .catch(error => console.error(error))

        window.addEventListener('resize', this.setSvgSize.bind(this))
        window.addEventListener('dessine-plan', this.onDessinePlan.bind(this))
        document.addEventListener('data_histo_available', this.onDataHisto.bind(this))

        window.addEventListener('selection-monde', event => this.onSelectionMonde(event))
        document.addEventListener('keydown', event => this.onKeyDown(event))
        document.addEventListener('trouver-monde', event => this.onTrouverMonde(event))
        document.addEventListener('trouver-flotte', event => this.onTrouverFLotte(event))
        document.addEventListener('trouver-tresor', event => this.onTrouverTresor(event))
        document.addEventListener('historique-cr', event => this.onHistoriqueCr(event))
        document.addEventListener('marque-md', event => this.onMarqueMd(event))
        document.addEventListener('show-mondes-distance', event => this.onShowMondesDistance(event))
        document.addEventListener('nettoyer-affichage', event => this.onNettoyerAffichage(event))
        document.addEventListener('bouge-mondes-offset', event => this.onBougeMondesOffset(event))
        document.addEventListener('PlanMode', this.menuMode.bind(this))
        document.addEventListener('PlanZoom', this.menuZoom.bind(this))
        document.addEventListener('PlanObs', this.menuObs.bind(this))
        document.addEventListener('PlanPil', this.menuPil.bind(this))
        document.addEventListener('PlanResize', this.setSvgSize.bind(this))
        document.addEventListener('coordonnes-updated', this.onCoordonneesUpdated.bind(this))
        document.addEventListener('empty-local-storage', this.onEmptyLocalStorage.bind(this))
        document.addEventListener('monde-position-changed', this.onMondePositionChanged.bind(this))

        PlanContextMenu.Menu(this.numJoueur)
    }

    buildButton(btn, ename, values) {
        return new PlanBtn(btn, ename, values)
    }

    isDecalageChanged(decalage) {
        return !this.decalage ? true : (decalage.x !== this.decalage.x) || (decalage.y !== this.decalage.y)
    }

    readDecalage(drawIfChanged = false) {
        const str = this.myStorage.getItemSync('plan.decalage')

        // On réinitialise le décalage sans réafficher le plan
        this.decalePlan({ x: -this.decalage.x, y: -this.decalage.y }, false)

        // Maintenant on applique le décalage lu, sinon zéro
        const decalage = str ? PlanModulo.getModuloDecalage(str) : { x: 0, y: 0 }

        this.decalePlan(decalage, drawIfChanged)
    }

    readPref() {
        const prefs = this.myStorage.getItemSync('plan.prefs')

        if (prefs) {
            this.mode = prefs.mode
            this.show_explo = prefs.obs
            this.show_pillage = prefs.pil
            this.zoom = prefs.zoom

            document.dispatchEvent(new CustomEvent('PlanMode', { detail: { mode: this.mode } }))
            document.dispatchEvent(new CustomEvent('PlanObs', { detail: { mode: this.show_explo ? 1 : 0 } }))
            document.dispatchEvent(new CustomEvent('PlanPil', { detail: { mode: this.show_pillage ? 1 : 0 } }))
        }
    }

    storedecalage() {
        this.myStorage.setItemSync('plan.decalage', this.decalage)
    }

    storePrefs() {
        this.myStorage.setItemSync('plan.prefs', { mode: this.mode, obs: this.show_explo, pil: this.show_pillage, zoom: this.zoom })
    }

    copyCustomCoord(numMonde) {
        this.mondes[numMonde].x = this.customCoord[numMonde][0]
        this.mondes[numMonde].y = this.customCoord[numMonde][1]
        this.decaleCoordMonde(numMonde, this.decalage)
    }

    updateCustomCoord() {
        for (const numMonde in this.mondes) {
            if (this.customCoord[numMonde]) {
                this.copyCustomCoord(numMonde)
            }

            // On regarde pour ajouter les mondes connectés si on a les coordonnées et pas de cr
            for (const numMondeCnx of this.mondes[numMonde].connexions) {
                if (this.customCoord[numMondeCnx] && !this.mondes[numMondeCnx]) {
                    this.mondes[numMondeCnx] = new Monde(null, 0, null)
                    this.mondes[numMondeCnx].numero = numMondeCnx
                    this.mondes[numMondeCnx].connexions = []
                    this.copyCustomCoord(numMondeCnx)
                }
            }
        }
    }

    /**
     *
     * @param {*} nebdata
     * @param {Cr} cr
     * @param {boolean} simul
     */
    async updateData(nebdata, cr, simul) {
        this.drawed = false
        this.decalage = { x: 0, y: 0 }
        PlanModulo.longX = nebdata.longX
        PlanModulo.longY = nebdata.longY

        this.cr = new Cr(cr)

        this.simul = simul
        // On crée les properties à partir de l'objet json nebdata
        for (const property in nebdata) {
            if (!['joueurs', 'mondes'].includes(property)) {
                this[property] = nebdata[property]
            }
        }

        this.joueurs = []
        for (const numJou in nebdata.joueurs) {
            this.joueurs[numJou] = new Joueur(nebdata.joueurs[numJou], parseInt(numJou))
        }

        this.mondes = []
        for (const numMonde in nebdata.mondes) {
            const joueurProprietaire = nebdata.mondes[numMonde].proprietaire > 0
                ? this.joueurs[nebdata.mondes[numMonde].proprietaire]
                : null
            this.mondes[numMonde] = new Monde(nebdata.mondes[numMonde], joueurProprietaire, this.cr.getCrMonde(numMonde))

            if (!nebdata.numJoueur || (nebdata.mondes[numMonde].proprietaire === nebdata.numJoueur || this.joueurs[nebdata.numJoueur].allies.indexOf(nebdata.mondes[numMonde].proprietaire) > -1)) {
                this.mondes[numMonde].allie = true
            }
        }

        this.updateCustomCoord()

        for (const numFlotte in nebdata.flottes) {
            const flotte = nebdata.flottes[numFlotte]
            if (flotte.localisation) {
                if (!nebdata.numJoueur) {
                    if (this.mondes[flotte.localisation].proprietaire === flotte.proprietaire) {
                        this.mondes[flotte.localisation].nbflottes++
                        this.mondes[flotte.localisation].nbvcflottes += flotte.vc + flotte.vt
                    } else if (this.mondes[flotte.localisation].proprietaire &&
                        flotte.proprietaire && this.joueurs[flotte.proprietaire].allies.indexOf(this.mondes[flotte.localisation].proprietaire) > -1) {
                        this.mondes[flotte.localisation].nbflottesA++
                        this.mondes[flotte.localisation].nbvcflottesA += flotte.vc + flotte.vt
                    } else {
                        this.mondes[flotte.localisation].nbflottesE++
                        this.mondes[flotte.localisation].nbvcflottesE += flotte.vc + flotte.vt
                    }
                } else if (flotte.proprietaire === nebdata.numJoueur) {
                    this.mondes[flotte.localisation].nbflottes++
                    this.mondes[flotte.localisation].nbvcflottes += flotte.vc + flotte.vt
                } else if (this.joueurs[nebdata.numJoueur].allies.indexOf(flotte.proprietaire) > -1) {
                    this.mondes[flotte.localisation].nbflottesA++
                    this.mondes[flotte.localisation].nbvcflottesA += flotte.vc + flotte.vt
                } else {
                    this.mondes[flotte.localisation].nbflottesE++
                    this.mondes[flotte.localisation].nbvcflottesE += flotte.vc + flotte.vt
                }
            }
        }
        this.readPref()
        this.readDecalage(true)

        document.dispatchEvent(new Event('plan-data-set'))
    }

    setSvgSize() {
        let div = document.getElementById('nebutil-plan')
        // si #nebutil-plan absent se rabattre sur plan-redim
        if (!div) { div = document.getElementById('plan-redim') }
        const rct = div.getBoundingClientRect()

        const width = rct.width - 18
        const height = rct.height - 10
        const zwidth = width * this.zoom / 10
        const zheight = height * this.zoom / 10
        this.svg.setAttribute('width', width)
        this.svg.setAttribute('height', rct.height)
        this.svg.setAttribute('viewBox', '0 0 ' + zwidth + ' ' + zheight)

        this.nb_mondes_x = Math.floor(zwidth / this.taille_monde / 2)
        this.nb_mondes_y = Math.floor(zheight / this.taille_monde / 2)
    }

    clear() {
        this.drawed = false

        while (this.g_map.firstChild) {
            this.g_map.removeChild(this.g_map.firstChild)
        }
        while (this.g_mondes.firstChild) {
            this.g_mondes.removeChild(this.g_mondes.firstChild)
        }
        this.clearConnexions()
    }

    clearConnexions() {
        while (this.gConnexions.firstChild) {
            this.gConnexions.removeChild(this.gConnexions.firstChild)
        }
    }

    draw(mondesList = null, redraw = false) {
        if (!this.drawed) {
            this.clear()
        }

        if (!mondesList) {
            mondesList = this.mondes.map((m) => m.numero)
        }

        for (const numMonde of Object.values(mondesList)) {
            this.drawConnexions(this.mondes[numMonde], redraw)
        }

        for (const numMonde of Object.values(mondesList)) {
            if (this.mondes[numMonde]) {
                this.drawMonde(this.mondes[numMonde])
            }
        }

        this.verifPlan.setStackedClass()
        this.verifPlan.setConnexionDeTraversClass(this.drag.lock)

        if (this.selected_monde > 0) {
            this.disMonde.drawMondeSelect(this.mondes[this.selected_monde])
        }

        this.drawed = true
    }

    removeMondes(mondes) {
        const numeros = mondes.map(monde => monde.numero)

        // On cherche les mondes connectés aux mondes à supprimer
        const mondesARedessiner = this.mondes.filter(monde => monde.connexions.some(cnx => numeros.includes(cnx)) && !numeros.includes(monde.numero))

        for (const monde of Object.values(mondes)) {
            // On supprime les connexions arrivant sur le monde s'il ne s'agit pas d'un monde à supprimer
            mondesARedessiner
                .filter(m => m.connexions.some(cnx => cnx === monde.numero))
                .forEach(m => document.getElementById(`C${m.numero}_${monde.numero}`)?.remove())

            // On supprime le monde et toutes ses connexions
            document.getElementById(`CM_${monde.numero}`)?.remove()
            document.querySelector('[data-monde="' + monde.numero + '"]')?.remove()
        }
    }

    connexionSensUnique(md1, md2) {
        for (const m in this.mondes[md2].connexions) {
            if (m === md1) {
                return true
            }
        }
        return false
    }

    displaySelectMonde(numMonde) {
        if (!numMonde) {
            return
        }

        this.selected_monde = numMonde
        const rect = this.rect_monde_select
        const monde = this.mondes[numMonde]
        const x = monde.getXBitmap(this.taille_monde) - 2 * this.taille_monde / 2
        const y = monde.getYBitmap(this.taille_monde) - 2 * this.taille_monde / 2
        rect.setAttribute('x', x)
        rect.setAttribute('y', y)
        rect.setAttribute('visibility', 'visible')
    }

    drawMapGroup() {
        const g = SvgTools.add(this.svg_g, 'g')
        g.setAttribute('stroke', 'white')
        g.setAttribute('stroke-width', 1)
        this.g_map = g
    }

    drawConnexionsGroup() {
        const g = SvgTools.add(this.svg_g, 'g')
        g.setAttribute('stroke', 'white')
        g.setAttribute('stroke-width', 1)
        this.gConnexions = g
    }

    drawMondesGroup() {
        const g = SvgTools.add(this.svg_g, 'g')
        g.setAttribute('stroke', 'white')
        g.setAttribute('stroke-width', 1)
        this.g_mondes = g
    }

    menuMode(evt) {
        this.mode = evt.detail.mode
        this.clear()
        this.draw()
        this.storePrefs()
    }

    menuZoom(evt) {
        if (evt.detail.mode === 'in') {
            if (this.zoom > 3) {
                this.zoom--
                this.setSvgSize()
            }
        } else {
            if (this.zoom < 30) {
                this.zoom++
                this.setSvgSize()
            }
        }
        this.storePrefs()
    }

    menuObs(evt) {
        this.show_explo = evt.detail.mode > 0
        this.clear()
        this.draw()
        this.storePrefs()
    }

    menuPil(evt) {
        this.show_pillage = evt.detail.mode > 0
        this.clear()
        this.draw()
        this.storePrefs()
    }

    drawConnexions(monde, redraw = false) {
        // TODO sens unique
        // if this.connexionSensUnique(monde1, monde2)

        if (!monde || monde.x * monde.y === 0) {
            return
        }

        // On crée le groupe de connexion du monde
        const groupId = `CM_${monde.numero}`
        const gConnexionsMonde = document.getElementById(groupId) ?? SvgTools.add(this.gConnexions, 'g', groupId)

        this.disMonde.setPositionConnexions(monde, gConnexionsMonde)

        if (!this.drawed || redraw) {
            monde.connexions.forEach(mdConnecte => {
                if (!this.mondes[mdConnecte]?.x || !this.mondes[mdConnecte]?.y) {
                    return
                }

                const lineId = `C${monde.numero}_${mdConnecte}`
                const line = document.getElementById(lineId) ?? SvgTools.add(gConnexionsMonde, 'line', lineId)

                this.disMonde.drawPositionConnexion(monde, this.mondes[mdConnecte], line)
            })
        }
    }

    isMondeDepartFromNebdata(md) {
        // Lecture infos nebdata
        for (const j in this.joueurs) {
            if (this.joueurs[j].md === md) {
                return true
            }
        }
    }

    isMondeDepart(md) {
        if (this.isMondeDepartFromNebdata(md)) {
            return true
        }

        return this.myStorage.getItemSync('plan.mds')?.includes(md) ?? false
    }

    drawUpdatedStyle(numerosMondes) {
        for (const m of numerosMondes) {
            const mondeElt = document.querySelector(`[data-monde="${m}"] .rectangle-monde`)
            if (!mondeElt) continue

            if (this.mondes[m]?.coordChanged) {
                mondeElt.classList.add('modified')
            } else {
                mondeElt.classList.remove('modified')
            }
        }
    }

    drawMonde(monde) {
        if (monde.x * monde.y === 0) {
            return
        }

        let grm = document.querySelector('[data-monde="' + monde.numero + '"]')
        if (!grm || !this.drawed) {
            const largeur = monde.getLargeur(this.taille_monde)
            grm = SvgTools.add(this.g_mondes, 'g')
            grm.setAttribute('data-monde', monde.numero)

            if (!monde?.cr) {
                grm.classList.add('nocr')
            }

            if (monde?.isTrouNoir) {
                grm.classList.add('trou-noir')
            }
            monde.isMondeDepart = this.isMondeDepart(monde.numero)

            monde.joueur_couleur = this.joueurs[monde.proprietaire]?.getEquipeOrNumJou() ?? 0

            switch (this.mode) {
                case 1:
                    this.disMonde.drawMondeExtended(monde, grm, largeur, this.taille_monde)
                    break
                default:
                    this.disMonde.drawMondeSimple(monde, grm, largeur, this.taille_monde)
            }

            if (this.show_pillage) {
                this.disMonde.drawPillage(monde, grm, largeur)
            }

            if (this.show_explo) {
                this.disMonde.drawExplo(monde, grm, largeur)
            }
        }
        this.disMonde.setPositionMonde(monde, grm)
    }

    async onClick(e) {
        if (this.drag.dragging) {
            this.drag.dragging = false
            return
        }

        const element = e.target.parentElement

        // On a cliqué sur un monde
        if (element.matches('[data-monde]')) {
            const numMonde = element.getAttribute('data-monde')

            if (this.mondes[numMonde].cr || this.planHisto?.mondeVisibleTour[numMonde]) {
                const crtxt = this.mondes[numMonde].cr ?? await this.planHisto.getLastCr(numMonde)
                const tourPrec = (this.mondes[numMonde]?.cr === null)
                    ? this.planHisto.mondeVisibleTour[numMonde]
                    : false
                const tour = tourPrec ? this.planHisto.mondeVisibleTour[numMonde] : null

                document.dispatchEvent(new CustomEvent('tooltip-show', {
                    detail: {
                        domRef: element.firstChild,
                        numMonde,
                        tourPrec,
                        nomMonde: this.mondes[numMonde].nom,
                        historique: false,
                        crtxt: [{ cr: crtxt, tour }],
                        ordres: this.ordres_parser.getOrdresMondeFromRawOrdres(
                            window.myCodeMirrorOrdres?.doc.getValue(), numMonde)
                    }
                }))

                return
            }
        }

        document.dispatchEvent(new Event('tooltip-hide'))
    }

    async onHistoriqueCr(e) {
        const numMonde = e.detail.numMonde
        const historiqueCr = await this.planHisto.getHistoCr(numMonde)
        const element = e.detail.element

        document.dispatchEvent(new CustomEvent('tooltip-show', {
            detail: {
                domRef: element.firstChild,
                numMonde,
                tourPrec: false,
                nomMonde: '',
                historique: true,
                crtxt: historiqueCr,
                ordres: null
            }
        }))
    }

    async onMarqueMd(e) {
        const numMonde = parseInt(e.detail.numMonde, 10)

        if (this.isMondeDepartFromNebdata(numMonde)) {
            return
        }

        const mds = this.myStorage.getItemSync('plan.mds') ?? []

        // On enlève numMonde si présent sinon on l'ajoute
        const index = mds.indexOf(numMonde)
        if (index !== -1) {
            mds.splice(index, 1)
            this.disMonde.removeMdCircle(numMonde)
        } else {
            mds.push(numMonde)
            this.disMonde.addMdCircle(numMonde)
        }

        this.myStorage.setItemSync('plan.mds', mds)
    }

    onShowMondesDistance(e) {
        const numMonde = parseInt(e.detail.numMonde, 10)
        const distance = parseInt(e.detail.distance, 10)

        const mondes = (new TrouveMondesDistance(this.mondes)).getMondesDistance(numMonde, distance)
        // const mondes = Services.trouveMondesDistance(this.mondes, numMonde, distance)

        this.disMonde.addSelectArea(mondes)
    }

    onAuxClick(e) {
        if (e.which !== 2 || e.buttons !== 4) {
            return
        }

        const element = e.target.parentElement

        // On a cliqué sur un monde
        if (element.matches('[data-monde]')) {
            e.preventDefault()
            const numMonde = element.getAttribute('data-monde')
            if (this.mondes[numMonde].cr) {
                window.dispatchEvent(new CustomEvent('selection-monde', {
                    detail: {
                        monde: element.getAttribute('data-monde'),
                        from: 'plan'
                    }
                }))
            }
        }
    }

    onBlur() {
        document.dispatchEvent(new Event('tooltip-hide'))
    }

    onKeyDown(e) {
        document.dispatchEvent(new Event('tooltip-hide'))
        if (e.ctrlKey) {
            if (e.key === 'l' || e.key === 'L') {
                e.preventDefault()
                document.dispatchEvent(new Event('trouver-flotte'))
            } else if (e.key === 'm' || e.key === 'M') {
                document.dispatchEvent(new Event('trouver-monde'))
            } else if (e.key === 'r' || e.key === 'R') {
                e.preventDefault()
                document.dispatchEvent(new Event('trouver-tresor'))
            } else if (e.key === '>' || e.key === '+') {
                document.dispatchEvent(new CustomEvent('PlanZoom', { detail: { mode: 'in' } }))
            } else if (e.key === '<' || e.key === '-') {
                document.dispatchEvent(new CustomEvent('PlanZoom', { detail: { mode: 'out' } }))
            } else if (Object.keys(this.fleches).includes(e.code)) {
                // Pas de défilement du plan si Ctrl+flèche dans un textarea
                if (e.srcElement.tagName === 'TEXTAREA') {
                    return
                }
                const direction = this.fleches[e.code]
                e.preventDefault()
                this.decalePlan(direction, true)
            }
        }
    }

    onTrouverMonde(e) {
        const numMonde = window.prompt('Trouver le monde')
        if (numMonde > 0) {
            window.dispatchEvent(new CustomEvent('selection-monde', {
                detail: {
                    monde: numMonde,
                    from: 'recherche'
                }
            }))
        }
    }

    onTrouverFLotte(e) {
        const numFlotte = window.prompt('Trouver la flotte')
        if (numFlotte > 0) {
            this.disMonde.hideSelectMonde()
            const flotte = this.flottes[numFlotte]
            if (flotte) {
                window.dispatchEvent(new CustomEvent('selection-monde', {
                    detail: {
                        monde: flotte.localisation,
                        from: 'recherche'
                    }
                }))
            } else {
                window.alert('Flotte introuvable')
            }
        }
    }

    onTrouverTresor(e) {
        const numMonde = window.prompt('Trouver le trésor')
        if (numMonde > 0) {
            this.disMonde.hideSelectMonde()
            const tresor = this.tresors[numMonde]
            const localisation = tresor?.sur_monde ? tresor?.localisation : this.flottes[tresor?.localisation]?.localisation
            if (localisation) {
                window.dispatchEvent(new CustomEvent('selection-monde', {
                    detail: {
                        monde: localisation,
                        from: 'recherche'
                    }
                }))
            } else {
                window.alert('Trésor introuvable')
            }
        }
    }

    onSelectionMonde(e) {
        this.doSelectMonde(e.detail.monde)
    }

    doSelectMonde(numMonde) {
        const monde = this.mondes[numMonde]

        if (monde) {
            this.selected_monde = numMonde
            if (monde.x > this.nb_mondes_x || monde.y > this.nb_mondes_y) {
                const dec = {
                    x: Math.ceil(this.nb_mondes_x / 2) - monde.x,
                    y: Math.ceil(this.nb_mondes_y / 2) - monde.y
                }
                this.decalePlan(dec, true)
            } else {
                this.disMonde.drawMondeSelect(monde)
            }
        } else {
            this.selected_monde = 0
            this.disMonde.hideSelectMonde()
        }
    }

    decaleCoordMonde(numMonde, decalage) {
        if (this.mondes[numMonde].x * this.mondes[numMonde].y !== 0) {
            this.mondes[numMonde].x = (this.mondes[numMonde].x + decalage.x + this.longX - 1) % this.longX + 1
            this.mondes[numMonde].y = (this.mondes[numMonde].y + decalage.y + this.longY - 1) % this.longY + 1

            if (typeof this.mondes[numMonde].oldX !== 'undefined') {
                this.mondes[numMonde].oldX = (this.mondes[numMonde].oldX + decalage.x + this.longX - 1) % this.longX + 1
            }

            if (typeof this.mondes[numMonde].oldY !== 'undefined') {
                this.mondes[numMonde].oldY = (this.mondes[numMonde].oldY + decalage.y + this.longY - 1) % this.longY + 1
            }
        }
    }

    decalePlan(decalage, redraw = false, mondesList = null) {
        if (!mondesList) {
            mondesList = this.mondes.map((m) => m.numero)
        }

        for (const numMonde of Object.values(mondesList)) {
            this.decaleCoordMonde(numMonde, decalage)
        }

        if (redraw) {
            this.draw(mondesList)
        }

        this.decalage.x += decalage.x
        this.decalage.y += decalage.y

        this.decalage = PlanModulo.getModuloDecalage(this.decalage)

        this.storedecalage()
    }

    onDessinePlan(e) {
        const divplan = document.querySelector('#nebutil-plan')

        if (!divplan) return

        if (e.detail.simul) {
            divplan.classList.add('plan-simul')
        } else {
            divplan.classList.remove('plan-simul')
        }

        if (e.detail.ancientour) {
            divplan.classList.add('plan-tour-prec')
        } else {
            divplan.classList.remove('plan-tour-prec')
        }

        this.selected_monde = 0

        this.updateData(e.detail.nebdata, e.detail.crtxt, e.detail.simul)
    }

    async onDataHisto(e) {
        // this.dernierTourStocke = e.detail.histo.dernierTourStocke
        this.draw()
    }

    onBougeMondesOffset(e) {
        const offsetTotal = e.detail.dragOffsetTotal
        const offsetDelta = e.detail.dragOffsetDelta

        for (const monde of Object.values(this.mondes)) {
            if (monde.x === 0 || monde.y === 0) {
                break
            }

            let deltaX = 0
            let deltaY = 0

            if (typeof monde.translate === 'undefined') {
                monde.translate = { x: 0, y: 0 }
            }

            // On ne regarde que le signe d'offsetDelta qui indique le sens de scrolling
            if ((offsetDelta.x > 0) && (monde.x + monde.translate.x + offsetTotal.x > this.longX + 0.8) && (monde.translate.x !== -this.longX)) {
                deltaX = -this.longX
            }

            if ((offsetDelta.x < 0) && (monde.x + monde.translate.x + offsetTotal.x < 1.2) && (monde.translate.x !== this.longX)) {
                deltaX = this.longX
            }

            if ((offsetDelta.y > 0) && (monde.y + monde.translate.y + offsetTotal.y > this.longY + 0.8) && (monde.translate.y !== -this.longY)) {
                deltaY = -this.longY
            }

            if ((offsetDelta.y < 0) && (monde.y + monde.translate.y + offsetTotal.y < 1.2) && (monde.translate.y !== this.longY)) {
                deltaY = this.longY
            }

            if ((deltaX !== 0) || (deltaY !== 0)) {
                this.mondesADeplacer.push(monde)
                monde.translate.x += deltaX
                monde.translate.y += deltaY
            }
        }

        setTimeout(this.translateMondesChunk(), 2)
    }

    translateMondesChunk() {
        let translateRestants = 5

        while (this.mondesADeplacer.length > 0 && translateRestants > 0) {
            const monde = this.mondesADeplacer.shift()

            this.disMonde.setPositionMonde(monde, null)
            this.disMonde.setPositionConnexions(monde, null)
            translateRestants--
        }

        if (this.mondesADeplacer.length > 0) {
            setTimeout(this.translateMondesChunk(), 2)
        }
    }

    onNettoyerAffichage() {
        this.disMonde.clearSelectArea()
        this.disMonde.hideSelectMonde()
        this.selected_monde = 0
        this.draw()
    }

    onCoordonneesUpdated(e) {
        this.customCoord = e.detail.coord
        this.updateCustomCoord()
        this.clear()
        this.draw()
    }

    async onEmptyLocalStorage() {
        await this.myStorage.clean()
        await this.planHisto.getCrsFromApi()
        await this.planHisto.refreshCrEtExtraits(this.tour)
        alert('Stockage local vidé. Données rechargées sur le serveur.')
    }

    /**
     * Récupère les connexions d'un monde en les complétant par les mondes qui ont une connexion vers lui
     * Permet de faire bouger les connexions lors d'un drag même en cas de connexion à sens unique
     * @param {int} numero
     */
    getMondeConnexions(numero) {
        // const connexions = [...this.mondes[numero].connexions]
        const connexions = this.mondes[numero]?.connexions ?? []

        this.mondes.forEach(monde => monde.connexions.forEach(m => {
            if (m === numero && !connexions.includes(monde.numero)) {
                connexions.push(monde.numero)
            }
        }))

        return connexions
    }

    onMondePositionChanged(e) {
        this.draw(e.detail.mondes, true)
        this.drawUpdatedStyle(e.detail.mondes)
    }
}

export { Plan }
window.Plan = Plan
window.Combats = Combats
window.Tooltip = Tooltip
