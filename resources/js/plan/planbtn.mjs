export class PlanBtn {
    // btn
    // eventListenerName
    // values
    // value

    constructor(btn, eventListenerName, values) {
        this.btn = btn
        this.eventListenerName = eventListenerName
        this.values = values
        this.value = 1

        document.addEventListener(eventListenerName, function (e) {
            this.value = e.detail.mode
            this.setBtn()
        }.bind(this))

        this.btn.addEventListener('click', this.clickMe.bind(this))
    }

    clickMe() {
        this.value = 1 - this.value
        document.dispatchEvent(new CustomEvent(this.eventListenerName, { detail: { mode: this.value } }))
        this.setBtn()
    }

    setBtn() {
        // this.btn.innerHTML = this.values[this.value]
        this.btn.setAttribute('icon', this.values[this.value])
    }
}
