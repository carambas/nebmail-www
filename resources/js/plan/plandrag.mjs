import { notify } from '../notify.mjs'
import { PlacementMondes } from './placement-mondes.mjs'
import { PlanModulo } from './planmodulo.mjs'

const MIN_OFFSET = 5

/**
 *
 * @property {Plan} plan - plan
 * @property {bool} dragging - est en cours de drag
 * @property {bool} lock - le plan est verrouillé / déverrouillé
 * @property {int} moveMonde - numéro du monde en train d'être déplace
 */
export class PlanDrag {
    /**
     *
     * @param {Plan} plan
     */
    constructor(plan) {
        this.plan = plan
        this.lock = true
        this.moveMonde = 0
        this.groupMondeMove = null
        this.groupMondeConnexionsMove = null
        this.mondeConnexionsMove1 = []
        this.mondeConnexionsMove2 = []

        this.dragOffset = { x: 0, y: 0 }
        this.previousDragOffset = { x: 0, y: 0 }
        this.dragging = false

        this.ondragstart = this.dragStart.bind(this)
        this.ondragstop = this.dragStop.bind(this)
        this.ondragmove = this.dragMove.bind(this)
        this.plan.svg.addEventListener('mousedown', this.ondragstart)
        this.g = document.querySelector('#svg_plan_group')

        this.lockButton = document.querySelector('#lock-button')
        this.lockButton.addEventListener('click', this.onLockClick.bind(this))

        this.placementMondes = new PlacementMondes(this.plan, this)
    }

    onLockClick(e) {
        if (!this.lock && this.plan.planSaveCoord.saveButtonEnabled) {
            if (!confirm('Les modifications apportées au plan vont être annulées. Continuer ?')) {
                return
            }
        }

        this.lock = !this.lock

        if (this.lock) {
            e.target.setAttribute('icon', 'ic:outline-lock')
            e.target.setAttribute('title', 'Empêche de modifier l\'emplacement des mondes')
            e.target.classList.remove('text-danger')
            notify('success', 'Le plan est verrouillé. Aucune modification possible.')
            this.plan.planSaveCoord.restoreOldCoordonnees()
            document.dispatchEvent(new Event('disable-save-button'))
            document.dispatchEvent(new Event('lock-plan'))
        } else {
            e.target.setAttribute('icon', 'ic:outline-lock-open')
            e.target.setAttribute('title', 'Permet de modifier l\'emplacement des mondes')
            e.target.classList.add('text-danger')
            notify('warning', 'Le plan déverrouillé. Il est possible de modifier l\'emplacement des mondes.')
            document.dispatchEvent(new Event('unlock-plan'))
        }
    }

    dragStart(e) {
        if (e.buttons === 1 || e.buttons === 4) {
            this.dragging = false
            e.preventDefault()

            this.startDrag = { x: e.clientX, y: e.clientY }
            this.previousDragOffset = { x: 0, y: 0 }

            if (!this.lock) {
                // On regarde si on a cliqué dans un monde
                const groupMonde = e.target.closest('g[data-monde]')
                if (groupMonde) {
                    this.groupMondeMove = groupMonde
                    this.dragStartMonde()
                }
            }

            window.addEventListener('mouseup', this.ondragstop)
            window.addEventListener('mousemove', this.ondragmove)
        }
    }

    dragStartMonde() {
        this.moveMonde = parseInt(this.groupMondeMove.getAttribute('data-monde'))
        if (!this.plan.mondes[this.moveMonde].oldX && !this.plan.mondes[this.moveMonde].oldY) {
            this.plan.mondes[this.moveMonde].oldX = this.plan.mondes[this.moveMonde].x
            this.plan.mondes[this.moveMonde].oldY = this.plan.mondes[this.moveMonde].y
        }

        this.initialMondePosition = this.groupMondeMove.getAttribute('transform')
        const matches = this.initialMondePosition.match(/translate\((\S+) (\S+)\)/)
        this.offsetMouseMonde = {
            x: this.startDrag.x - parseFloat(matches[1]) * 10 / this.plan.zoom,
            y: this.startDrag.y - parseFloat(matches[2]) * 10 / this.plan.zoom
        }

        this.groupMondeConnexionsMove = this.plan.svg.querySelector(`#CM_${this.moveMonde}`)
        for (const numMondeCnx of this.plan.getMondeConnexions(this.moveMonde)) {
            const line1 = this.plan.svg.querySelector(`#C${this.moveMonde}_${numMondeCnx}`)
            if (line1) {
                this.mondeConnexionsMove1.push({
                    xInit: parseInt(line1.getAttribute('x1')),
                    yInit: parseInt(line1.getAttribute('y1')),
                    element: line1,
                })
            }

            const line2 = this.plan.svg.querySelector(`#C${numMondeCnx}_${this.moveMonde}`)
            if (line2) {
                this.mondeConnexionsMove2.push({
                    xInit: parseInt(line2.getAttribute('x2')),
                    yInit: parseInt(line2.getAttribute('y2')),
                    element: line2,
                })
            }
            // this.plan.svg.querySelector(`#C${numMondeCnx}_${this.moveMonde}`)
        }
    }

    dragStop(e) {
        this.plan.mondesADeplacer = []
        window.removeEventListener('mouseup', this.ondragstop)
        window.removeEventListener('mousemove', this.ondragmove)

        this.plan.mondes.forEach(function (monde) {
            monde.translate = undefined
        })
        this.g.setAttribute('transform', 'translate(0 0)')

        let dec = { x: 0, y: 0 }

        dec = this.getOffsetCoordPlan()

        if (this.groupMondeMove) {
            this.dragStopMonde(e)
        }

        if (dec.x !== 0 || dec.y !== 0) {
            this.previousDragOffset = { x: 0, y: 0 }
            this.dragOffset = { x: 0, y: 0 }

            if (!this.groupMondeMove) {
                this.plan.decalePlan(dec, true)
            }

            this.startDrag = { x: e.clientX, y: e.clientY }
            this.plan.tooltip?.hide()
        }

        this.moveMonde = 0
        this.groupMondeMove = null
        this.groupMondeConnexionsMove = null
        this.mondeConnexionsMove1 = []
        this.mondeConnexionsMove2 = []
    }

    dragStopMonde(e) {
        // On a besoin de savoir le delta entre début de drag du monde et fin du drag
        const deltaCoordMouse = { x: e.clientX - this.startDrag.x, y: e.clientY - this.startDrag.y }

        // On le converti en coordonnées
        const deltaCoordPlan = this.roundCoord(this.getCoordPlan(deltaCoordMouse))

        // On les ajoute aux coordonnées du monde
        let coord = {
            x: this.plan.mondes[this.moveMonde].x + deltaCoordPlan.x,
            y: this.plan.mondes[this.moveMonde].y + deltaCoordPlan.y
        }

        // On fait un coup de modulo pour que le monde reste aux bornes du plan
        coord = PlanModulo.getModuloDecalage(coord)

        // On note que les coordonnées ont été altérées
        if (!this.plan.mondes[this.moveMonde].coordChanged &&
            (Math.abs(coord.x - this.plan.mondes[this.moveMonde].oldX) > 0.1 ||
            Math.abs(coord.y - this.plan.mondes[this.moveMonde].oldY) > 0.1)) {
            this.plan.mondes[this.moveMonde].coordChanged = true
        } else {
            // Coordonnées revenues à leur valeur d'origine => il n'y a plus de modification
            if (Math.abs(coord.x - this.plan.mondes[this.moveMonde].oldX) < 0.1 &&
                Math.abs(coord.y - this.plan.mondes[this.moveMonde].oldY) < 0.1) {
                this.plan.mondes[this.moveMonde].coordChanged = false
            }
        }

        this.plan.mondes[this.moveMonde].x = coord.x
        this.plan.mondes[this.moveMonde].y = coord.y

        // On redessine le monde et ses connexions
        document.dispatchEvent(new CustomEvent('monde-position-changed', {
            detail: {
                mondes: [this.moveMonde, ...this.plan.getMondeConnexions(this.moveMonde)],
            }
        }))
    }

    dragMove(e) {
        e.preventDefault()

        // On ne commencer le drag qu'en cas de déplacement d'au moins 5 pixels pour ne pas perturber le clic sur un monde
        if (!this.dragging && Math.abs(e.clientX - this.startDrag.x) < MIN_OFFSET &&
            Math.abs(e.clientY - this.startDrag.y) < MIN_OFFSET) {
            return
        }

        let newDragging = false
        if (!this.dragging) {
            this.plan.tooltip?.hide()
            this.dragging = true
            newDragging = true
        }

        this.dragOffset = { x: e.clientX - this.startDrag.x, y: e.clientY - this.startDrag.y }

        if (this.groupMondeMove) {
            this.dragMoveMonde(e, newDragging)
            return
        }

        this.g.setAttribute('transform', `translate(${this.dragOffset.x * this.plan.zoom / 10} ${this.dragOffset.y * this.plan.zoom / 10})`)

        const offsetDelta = this.getCoordPlan({
            x: this.dragOffset.x - this.previousDragOffset.x,
            y: this.dragOffset.y - this.previousDragOffset.y
        })

        const offsetTotal = this.getCoordPlan({
            x: this.dragOffset.x,
            y: this.dragOffset.y
        })

        if (Math.abs(offsetDelta.x) >= 0.5 || Math.abs(offsetDelta.y) >= 0.5) {
            document.dispatchEvent(new CustomEvent('bouge-mondes-offset', {
                detail: {
                    dragOffsetTotal: offsetTotal,
                    dragOffsetDelta: offsetDelta,
                }
            }))

            this.previousDragOffset = this.dragOffset
        }
    }

    dragMoveMonde(e, newDragging) {
        if (newDragging) {
            // Déplace l'élément DOM groupe de monde en fin de liste des mondes pour
            // apparaitre au-dessus des autres mondes lors du drag
            this.groupMondeMove.parentNode.insertBefore(this.groupMondeMove, null)
        }

        const x = (e.clientX - this.offsetMouseMonde.x) * this.plan.zoom / 10
        const y = (e.clientY - this.offsetMouseMonde.y) * this.plan.zoom / 10
        this.groupMondeMove.setAttribute('transform', `translate(${x} ${y})`)

        const deltaX = (e.clientX - this.startDrag.x) * this.plan.zoom / 10
        const deltaY = (e.clientY - this.startDrag.y) * this.plan.zoom / 10
        this.moveConnexionsMonde1(deltaX, deltaY)
        this.moveConnexionsMonde2(deltaX, deltaY)
    }

    moveConnexionsMonde1(x, y) {
        this.mondeConnexionsMove1.forEach(elt => {
            elt.element.setAttribute('x1', x + elt.xInit)
            elt.element.setAttribute('y1', y + elt.yInit)
        })
    }

    moveConnexionsMonde2(x, y) {
        this.mondeConnexionsMove2.forEach(elt => {
            elt.element.setAttribute('x2', x + elt.xInit)
            elt.element.setAttribute('y2', y + elt.yInit)
        })
    }

    getOffsetCoordPlan() {
        return this.getCoordPlan(this.dragOffset)
    }

    getOffsetCoordMonde() {
        return this.getCoordPlan(this.dragOffset)
    }

    getCoordPlan(client) {
        return {
            x: client.x / (this.plan.taille_monde * 20 / this.plan.zoom),
            y: client.y / (this.plan.taille_monde * 20 / this.plan.zoom)
        }
    }

    roundCoord(client) {
        return {
            x: Math.round(client.x),
            y: Math.round(client.y),
        }
    }
}
