import { Cr } from './cr.mjs'
import { MyLocalStorage } from './mylocalstorage.mjs'
import { notify } from '../notify.mjs'
import { ParseExtrait } from './parseextrait.mjs'
import { Nbt } from '../nebutil/nbt.mjs'
import { MondeNbt } from './mondenbt.mjs'

/**
 * Classe gérant récupérant et stockant les CR des tours courant et précédents
 * @property {string} apiToken - token pour l'API cr
 * @property {string} userId - Id du joueur
 * @property {string} crApiUrl - URL de l'API cr
 * @property {int} tour - Tour courant
 * @property {MyLocalStorage} myStorage - instance de l'object MyLocalStorage utilisée pour stocker les données dans IndexedDB
 * @property {bool} histoDisponible - historique disponible après avoir été récupérée par API
 * @property {int[]} mondeVisibleTour - Pour chaque monde indique le numéro du dernier tour pour lequel il est visible
 * @property {int} dernierTourStocke - Numéro du dernier tour pour lequel le CR est stocké
 * @property {bool} planButtons - Indique s'il faut écouter les événement
 * @property {Object[]} extraits - Tableau d'extraits de CR donne le CR d'un monde pour un tour donné (extrait = tour, monde, content)
 * @property {Cr} cr - Classe d'accès au CR texte du tour courant
 * @property {Object} nbt - Fichier nbt du tour courant
 * @property {Object[]} erreursExtraitCr - Présence d'une erreur dans le parsing d'un extraitde CR
 * @property {string} lastExtraitAt - Date du dernier extrait stocké en cache
 * @
 */
export class PlanHisto {
    constructor({ plan, nomPartie, tour, userId, numJoueur, crApiUrl, apiToken, planButtons } = {}) {
        this.plan = plan
        this.apiToken = apiToken
        this.userId = userId
        this.crApiUrl = crApiUrl
        this.tour = tour
        this.myStorage = new MyLocalStorage(nomPartie, numJoueur)
        this.histoDisponible = false
        this.mondeVisibleTour = null
        this.dernierTourStocke = -1
        this.planButtons = planButtons
        this.extraits = []
        this.erreursExtraitCr = []

        if (this.planButtons) {
            document.addEventListener('change_tour', this.onChangeTour.bind(this))
            window.addEventListener('dessine-plan', this.onDessinePlan.bind(this))
            window.addEventListener('stocke-tour', this.onStockeTour.bind(this))
        }
        window.addEventListener('nouveaux-extraits-cr', this.onNouveauxExtraitsCr.bind(this))
        window.addEventListener('supprimer-extrait-cr', this.onSupprimeExtraitCr.bind(this))
    }

    addButtons() {
        // Seulement pour intercation avec svg du plan
        if (!this.planButtons) {
            return
        }

        const buttonDown = document.querySelector('#tour-down-button')
        buttonDown.classList.remove('d-none')

        const buttonUp = document.querySelector('#tour-up-button')
        buttonUp.classList.remove('d-none')

        buttonDown.addEventListener('click', this.onButtonClick)
        buttonUp.addEventListener('click', this.onButtonClick)

        this.buttonDown = buttonDown
        this.buttonUp = buttonUp
    }

    async readLastExtraitAt() {
        this.lastExtraitAt = await this.myStorage.getItem('last_extrait_at') ?? '1970-01-01 00:00:00'
    }

    /**
     * Appel api GET /api/cr
     * @param toursManquants array of integer : les tours à récupérer
     */
    async getCrsFromApi({ dernierTour } = { dernierTour: false }) {
        let toursManquants
        if (dernierTour) {
            toursManquants = [this.tour]
        } else {
            this.dernierTourStocke = await this.getNumeroDernierTourStocke()
            console.log('Dernier tour stocké ' + this.dernierTourStocke)
            toursManquants = []
            for (let t = this.dernierTourStocke + 1; t <= this.tour; t++) {
                toursManquants.push(t)
            }
        }

        await this.readLastExtraitAt()
        const needCr = toursManquants.length !== 0

        const init = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'X-HTTP-Method-Override': 'GET',
            },
            mode: 'same-origin',
            credentials: 'same-origin',
            cache: 'default',
            body: JSON.stringify({
                api_token: this.apiToken,
                data: {
                    user: this.userId,
                    tours: toursManquants,
                    txt: needCr,
                    nbt: needCr,
                    extraits: true,
                    extraitsAfter: this.lastExtraitAt,
                }
            })
        }

        const request = new Request(this.crApiUrl, init)

        const response = await fetch(request)

        if (!response.ok) {
            throw new Error('Une erreur a été rencontrée lors de la récupération des CR des tours passés')
        }

        const data = (await response.json()).data

        if (typeof data === 'undefined') {
            throw new Error("Mauvais format de données dans l'historique des CR")
        }

        await this.storeApiResponse(data, dernierTour)
    }

    async storeApiResponse(data, dernierTour) {
        const promises = []

        if (data.cr) {
            data.cr.forEach((elt) => {
                if (elt.tour === this.tour) {
                    this.cr = new Cr(elt.txt)
                }
                promises.push(this.myStorage.setItem(`cr.tour.${elt.tour}.txt`, elt.txt.replace(/\r/g, '')))
                promises.push(this.myStorage.setItem(`cr.tour.${elt.tour}.nbt`, elt.nbt))

                if (elt.tour > this.dernierTourStocke) {
                    this.dernierTourStocke = elt.tour
                }
            })
            if (data.cr.length > 0) {
                console.log('Dernier tour stocké ' + this.dernierTourStocke)
            }
        }
        if (data.extraits) {
            promises.push(this.storeNewExtraits(data.extraits, data.lastExtraitAt))
        }

        if (!dernierTour) {
            promises.push(this.myStorage.setItem('cr.dernier_tour_stocke', this.tour))
        }

        await Promise.all(promises)
    }

    async storeNewExtraits(extraits, lastExtraitAt) {
        const promises = []

        // On insère data.extraits dans this.extraits
        extraits.forEach((elt) => {
            elt.content = elt.content.replace(/\r/g, '')
            if (this.tour === elt.tour) {
                this.cr.addCrMonde(elt.content)
            }

            const extraitIndex = this.extraits.findIndex(extrait => extrait.tour === elt.tour && extrait.numero === elt.numero)
            if (extraitIndex !== -1) {
                // Si on a déjà un extrait pour ce monde et ce tour, on le met à jour
                this.extraits[extraitIndex] = elt
            } else {
                // Sinon on l'insère à la fin
                this.extraits.push(elt)
            }
        })
        this.lastExtraitAt = lastExtraitAt

        // On stoche this.extraits dans le storage
        promises.push(this.myStorage.setItem('extraits', this.extraits))
        promises.push(this.myStorage.setItem('last_extrait_at', this.lastExtraitAt))

        await Promise.all(promises)
    }

    async deleteExtrait(tour, numero) {
        const index = this.extraits.findIndex(extrait => extrait.tour === tour && extrait.numero === numero)
        this.extraits.splice(index, 1)
        await this.myStorage.setItem('extraits', this.extraits)
    }

    async onStockeTour(e) {
        const storage = []
        const { tour, nbt, txt } = e.detail
        storage.push(this.myStorage.setItem(`cr.tour.${tour}.txt`, txt.replace(/\r/g, '')))
        storage.push(this.myStorage.setItem(`cr.tour.${tour}.nbt`, nbt))
        await Promise.all(storage)

        if (this.tour === tour) {
            this.cr = new Cr(txt)
        }

        await this.mergeExtraitsIntoCr()

        if (parseInt(this.tour) === tour) {
            window.dispatchEvent(
                new CustomEvent('dessine-plan', {
                    detail: {
                        nebdata: nbt,
                        crtxt: txt,
                        simul: false,
                        ancientour: this.dernierTourStocke !== tour,
                        extraits: this.extraits,
                    }
                }))
        }

        if (e.detail.displayNotification) {
            notify('success', `Intégration du fichier .nbt du tour ${tour} effectuée avec succès`)
        }
    }

    async getLastCrFromApi() {
        let txt = await this.myStorage.getItem(`cr.tour.${this.tour}.txt`)
        let nbt = await this.myStorage.getItem(`cr.tour.${this.tour}.nbt`)

        if (!txt || !nbt) {
            await this.getCrsFromApi({ dernierTour: true })
            txt = await this.myStorage.getItem(`cr.tour.${this.tour}.txt`)
            nbt = await this.myStorage.getItem(`cr.tour.${this.tour}.nbt`)
        }

        this.cr = new Cr(txt)
        this.nbt = nbt

        await this.mergeExtraitsIntoCr()

        window.dispatchEvent(
            new CustomEvent('dessine-plan', {
                detail: {
                    nebdata: nbt,
                    crtxt: this.cr.getCrTxt(),
                    simul: false,
                    ancientour: false,
                    extraits: this.extraits,
                }
            }))

        return {
            cr: this.cr.getCrTxt(),
            nbt,
        }
    }

    async mergeCrToursPrecs(nbt) {
        if (!nbt) {
            nbt = this.nbt
        }

        for (let tour = this.tour - 1; tour >= 0; tour--) {
            const nbtPrec = await this.myStorage.getItem(`cr.tour.${tour}.nbt`)

            for (const m of Object.keys(nbtPrec?.mondes ?? [])) {
                if (nbtPrec.mondes[m].isVis && !nbt.mondes[m]?.isVis && !nbt.mondes[m]?.tourPasse) {
                    nbt.mondes[m] = nbtPrec.mondes[m]
                    nbt.mondes[m].tourPasse = true
                    nbt.mondes[m].tour = tour
                }
            }
        }
    }

    /**
     *
     * @param {array} c1
     * @param {array} c2
     */
    mergeConnexions(c1, c2) {
        const connexions = c1.concat(c2).sort()
        return [...new Set(connexions)]
    }

    async mergeExtraitsIntoCr(nbt = null, cr = null) {
        if (!nbt) {
            nbt = this.nbt
        }

        if (!cr) {
            cr = this.cr
        }

        await this.mergeCrToursPrecs(nbt)

        this.extraits = await this.myStorage.getItem('extraits')

        // On trie les extraits par tour croissant
        this.extraits = this.extraits.sort((a, b) => a.tour - b.tour)

        this.erreursExtraitCr = []
        this.removeParsingErrors()

        for (const extrait of this.extraits ?? []) {
            const parsedExtrait = new ParseExtrait(extrait)
            if (!parsedExtrait.erreur) {
                if (extrait.tour < nbt.tour) {
                    // Tour précédent : on ne note que les coordonnées du monde pour le tour courant

                    if (!nbt.mondes[parsedExtrait.monde.numero]) {
                        // Le monde n'existe pas dans le cr nbt => on le crée
                        nbt.mondes[parsedExtrait.monde.numero] = new MondeNbt()
                        nbt.mondes[parsedExtrait.monde.numero].numero = extrait.numero
                    }

                    parsedExtrait.monde.x = nbt.mondes[parsedExtrait.monde.numero].x > 0 ? nbt.mondes[parsedExtrait.monde.numero].x : parsedExtrait.monde.x
                    parsedExtrait.monde.y = nbt.mondes[parsedExtrait.monde.numero].y > 0 ? nbt.mondes[parsedExtrait.monde.numero].y : parsedExtrait.monde.y
                    // parsedExtrait.monde.connexions = nbt.mondes[parsedExtrait.monde.numero].connexions.length > 0 ? nbt.mondes[parsedExtrait.monde.numero].connexions : parsedExtrait.monde.connexions
                    parsedExtrait.monde.connexions = this.mergeConnexions(parsedExtrait.monde.connexions, nbt.mondes[parsedExtrait.monde.numero].connexions)

                    // Si le monde n'est pas visible au tour courant, on prend les informations de l'extrait
                    if (!nbt.mondes[parsedExtrait.monde.numero].isVis) {
                        if (!nbt.mondes[parsedExtrait.monde.numero].nom) {
                            nbt.mondes[parsedExtrait.monde.numero].nom = parsedExtrait.monde.nom ?? ''
                        }

                        parsedExtrait.monde.tourPasse = true
                        nbt.mondes[parsedExtrait.monde.numero] = parsedExtrait.monde
                    }
                } else if (extrait.tour === nbt.tour) {
                    // Nbt
                    nbt.mondes[parsedExtrait.monde.numero] = parsedExtrait.monde
                    parsedExtrait.monde.tourPasse = false

                    // Txt
                    cr.addCrMonde(extrait.content)
                }
            } else {
                extrait.erreur = true
                this.erreursExtraitCr.push(extrait)
            }
        }

        const nbtObject = new Nbt(this.nbt)
        nbtObject.sanitizeNumerosJoueurs()
        this.nbt = nbtObject.getNbt()
        this.displayParsingErrors()
    }

    /**
     * Ajoute les extraits du tour dans le tableau crs
     * @param {int} tour
     * @param {string[]} crs
     */
    addExtraitsToCrsTxt(tour, crs) {
        for (const extrait of this.extraits ?? []) {
            if (extrait.tour === tour && !crs[extrait.numero]) {
                crs[extrait.numero] = extrait.content
            }
        }
    }

    removeParsingErrors() {
        const warningElt = document.querySelector('#parsing-cr-warning')
        warningElt.classList.add('d-none')
        warningElt.setAttribute('title', '')
    }

    displayParsingErrors() {
        if (this.erreursExtraitCr.length > 0) {
            const warningElt = document.querySelector('#parsing-cr-warning')
            warningElt.setAttribute('title', 'Erreurs dans le parsing des cr :\n' + this.erreursExtraitCr.map((extrait) => 'tour ' + extrait.tour + '\n' + extrait.content + '\n').join('\n'))
            warningElt.classList.remove('d-none')
        }
    }

    /**
     * Récupère les CR des tours précédents soit dans le stockage local, soit en appelant l'API GET /api/cr
     * rempli le tableau mondeVisibleTour qui permet de savoir pour chaque monde quel est le tour le plus récent avec un cr disponible
     * envoie l'event 'data_histo_available' pour signaler la disponibilité des CR
     */
    async getHistoryCr() {
        await this.getCrsFromApi()

        this.dernierTourStocke = await this.getNumeroDernierTourStocke()

        // On va ici faire la recherche des CR des tours passés qui ne sont pas vus au tour courant
        const mondeVisibleTour = []
        for (let tour = 0; tour <= this.dernierTourStocke; tour++) {
            const crtxt = await this.myStorage.getItem(`cr.tour.${tour}.txt`)
            for (const m of Object.keys((new Cr(crtxt)).crMondes())) {
                mondeVisibleTour[m] = tour
            }
        }

        // Ajout extraits
        for (const extrait of this.extraits ?? []) {
            const numero = extrait.numero
            if (!mondeVisibleTour[numero] || mondeVisibleTour[numero] < extrait.tour) {
                mondeVisibleTour[numero] = extrait.tour
            }
        }

        this.mondeVisibleTour = mondeVisibleTour
        this.refreshCrEtExtraits(this.tour)

        if (this.planButtons) {
            document.dispatchEvent(
                new CustomEvent('data_histo_available', {
                    detail: {
                        histo: this,
                    }
                }))

            this.addButtons()
            this.histoDisponible = true
        }
    }

    async getNumeroDernierTourStocke() {
        let result = parseInt(await this.myStorage.getItem('cr.dernier_tour_stocke'), 10)

        if (isNaN(result)) {
            result = -1
        }

        return result
    }

    async getLastCr(numMonde) {
        const tour = this.mondeVisibleTour[numMonde]
        const crtxt = await this.myStorage.getItem(`cr.tour.${tour}.txt`)
        const crs = (new Cr(crtxt)).crMondes()
        this.addExtraitsToCrsTxt(tour, crs)

        return crs[numMonde]
    }

    /**
     * Renvoie l'historique des CR d'un monde sous forme d'une chaîne en se basant sur le stockage local
     * @param {int} numMonde
     * @returns array
     */
    async getHistoCr(numMonde) {
        const histoCr = []
        for (let tour = this.tour; tour >= 0; tour--) {
            const crtxt = await this.myStorage.getItem(`cr.tour.${tour}.txt`)
            const crs = (new Cr(crtxt)).crMondes()
            this.addExtraitsToCrsTxt(tour, crs)

            if (crs[numMonde]) {
                histoCr.push({
                    cr: crs[numMonde],
                    tour
                })
            }
        }
        return histoCr
    }

    async onChangeTour(e) {
        let tour = e.detail.tour

        if (tour === 'up') {
            tour = this.tour >= this.dernierTourStocke ? this.dernierTourStocke : this.tour + 1
        } else if (tour === 'down') {
            tour = this.tour <= 0 ? 0 : this.tour - 1
        }

        if (tour > this.dernierTourStocke) {
            console.log('Tour inexistant dans le stockage local')
            console.log('this.dernierTourStocke = ' + this.dernierTourStocke)
            console.log('this.tour = ' + this.tour)
            return
        }

        if (tour === this.tour) {
            console.log("L'affichage est déjà au tour " + tour)
            return
        }

        this.refreshCrEtExtraits(tour)

        this.tour = tour
    }

    async refreshCrEtExtraits(tour) {
        this.nbt = await this.myStorage.getItem(`cr.tour.${tour}.nbt`)
        this.cr = new Cr(await this.myStorage.getItem(`cr.tour.${tour}.txt`))
        await this.mergeExtraitsIntoCr()

        window.dispatchEvent(
            new CustomEvent('dessine-plan', {
                detail: {
                    nebdata: this.nbt,
                    crtxt: this.cr.getCrTxt(),
                    simul: false,
                    ancientour: this.nbt.tour < this.dernierTourStocke,
                    extraits: this.extraits,
                }
            }))
    }

    async onNouveauxExtraitsCr(e) {
        await this.storeNewExtraits(e.detail.extraits, e.detail.lastExtraitAt)
        await this.refreshCrEtExtraits(this.tour)
        notify('success', 'Nouveaux extraits de CR intégrés')
    }

    async onSupprimeExtraitCr(e) {
        await this.deleteExtrait(e.detail.tour, e.detail.numero)
        await this.refreshCrEtExtraits(this.tour)
        notify('success', 'Extrait de CR supprimé')
    }

    onButtonClick(e) {
        if (e.target.classList.contains('menu_btn_disabled')) {
            return
        }

        document.dispatchEvent(
            new CustomEvent('change_tour', {
                detail: {
                    tour: e.target.getAttribute('direction')
                }
            }))
    }

    async onDessinePlan(e) {
        if (!this.buttonUp || !this.buttonDown) {
            return
        }

        this.tour = e.detail.nebdata.tour

        if (e.detail.nebdata.tour === this.dernierTourStocke || e.detail.simul) {
            this.buttonUp.classList.add('menu_btn_disabled')
        } else if (e.detail.nebdata.tour < this.dernierTourStocke) {
            this.buttonUp.classList.remove('menu_btn_disabled')
        }

        if (e.detail.nebdata.tour === 0 || e.detail.simul) {
            this.buttonDown.classList.add('menu_btn_disabled')
        } else if (e.detail.nebdata.tour > 0) {
            this.buttonDown.classList.remove('menu_btn_disabled')
        }
    }
}
