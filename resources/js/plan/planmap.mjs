import { SvgTools } from './svgtools.mjs'

export class PlanMap {
    // g

    constructor(g) {
        this.g = g
    }

    drawConnexion(sx, sy, tx, ty) {
        SvgTools.addLine(this.g, sx, sy, tx, ty)
    }
}
