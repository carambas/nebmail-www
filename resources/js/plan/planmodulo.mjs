/**
 * Classe exposant des fonctions statiques gérant les coordonnées dans le cadre d'un plan torique
 * @property {int} longX - largueur du plan (static)
 * @property {int} longY - hauteur du plan (static)
 */
export class PlanModulo {
    /**
     * Renvoie des coordonnées entre 0 et this.longX-1 et this.longY-1
     * @param {*} decalage
     * @returns
     */
    static getModuloDecalage(decalage) {
        decalage.x = decalage.x < 0 ? decalage.x % this.longX + this.longX : decalage.x % this.longX
        decalage.y = decalage.y < 0 ? decalage.y % this.longY + this.longY : decalage.y % this.longY

        return decalage
    }

    /**
     * Renvoie des coordonnées entre 1 et this.longX/this.longY
     * @param {*} coord
     * @returns
     */
    static getModuloCoord(coord, limiteInfX = 1, limiteInfY = 1) {
        coord.x = coord.x < limiteInfX ? (coord.x - limiteInfX) % this.longX + this.longX + limiteInfX : (coord.x - limiteInfX) % this.longX + limiteInfX
        coord.y = coord.y < limiteInfY ? (coord.y - limiteInfY) % this.longY + this.longY + limiteInfY : (coord.y - limiteInfY) % this.longY + limiteInfY

        return coord
    }

    /**
     *
     * @param {*} coord
     * @returns
     */
    static getDistanceModulo(coord) {
        if (Math.abs(coord.x) > this.longX / 2) {
            coord.x += -Math.sign(coord.x) * this.longX
        }

        if (Math.abs(coord.y) > this.longY / 2) {
            coord.y += -Math.sign(coord.y) * this.longY
        }

        return coord
    }
}
