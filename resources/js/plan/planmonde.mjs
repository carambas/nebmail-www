import { SvgTools } from './svgtools.mjs'
import { PlanModulo } from './planmodulo.mjs'

export class PlanMonde {
    constructor(msize) {
        this.msize = msize
        this.largeur = 1.4 * msize
        this.rayon_cercle_md = msize * 1.4 / 2 * Math.sqrt(2) - 0.5

        this.c = {
            vp: { x: -1, y: this.largeur / 3 },
            vi: { x: this.largeur + 1, y: this.largeur / 3 },
            vc: { x: -1, y: this.largeur },
            fl: { x: this.largeur / 3 - 6, y: this.largeur },
            vce: { x: this.largeur, y: this.largeur },
            fle: { x: this.largeur * 2 / 3 + 4, y: this.largeur },
            pil: { x: this.largeur, y: 0.5 },
            obs: { x: 0, y: 0 },
            pe: { x: this.largeur / 3, y: 0 }
        }
    }

    addDefs(svg) {
        const defs = SvgTools.add(svg, 'defs')
        const svivp = SvgTools.addSymbol(defs, 's_vivp', 12, 12)
        SvgTools.addCircle(svivp, 6, 6, 6, null, 'cvp')
        const svivpe = SvgTools.addSymbol(defs, 's_vivpe', 12, 12)
        SvgTools.addCircle(svivpe, 6, 6, 6, null, 'cvpe')
        const sfl = SvgTools.addSymbol(defs, 's_fl', 2 + this.c.fl.x - this.c.vc.x + 11, 14)
        const sflp = SvgTools.add(sfl, 'path')
        sflp.setAttribute('d', 'M ' + (2 + this.c.fl.x - this.c.vc.x) + ' 0l 7 0 4 6 -4 6 -7 0 -4 -6z')
        sflp.classList.add('flottes')
        SvgTools.addCircle(sfl, 6, 6, 6, null, 'vcflottes')
        const sfla = SvgTools.addSymbol(defs, 's_fla', 2 + this.c.fl.x - this.c.vc.x + 11, 14)
        const sflap = SvgTools.add(sfla, 'path')
        sflap.setAttribute('d', 'M ' + (2 + this.c.fl.x - this.c.vc.x) + ' 0l 7 0 4 6 -4 6 -7 0 -4 -6z')
        sflap.classList.add('flottesA')
        SvgTools.addCircle(sfla, 6, 6, 6, null, 'vcflottesA')
        const sfle = SvgTools.addSymbol(defs, 's_fle', this.c.vce.x - this.c.fle.x + 16, 14)
        const sflep = SvgTools.add(sfle, 'path')
        sflep.setAttribute('d', 'M 6 0l 7 0 4 6 -4 6 -7 0 -4 -6z')
        sflep.classList.add('flottesE')
        SvgTools.addCircle(sfle, this.c.vce.x - this.c.fle.x + 10, 6, 6, null, 'vcflottesE')
        const spil = SvgTools.addSymbol(defs, 's_pil', 14, 14)
        const spilp = SvgTools.add(spil, 'path')
        spilp.setAttribute('d', 'M 0 7 l7 -7 7 7 -7 7z')
        spilp.classList.add('pillage')
        const spe = SvgTools.addSymbol(defs, 's_pe', 18, 10)
        const spep = SvgTools.add(spe, 'path')
        spep.setAttribute('d', 'M 0 5 l 3 -5 12 0 3 5 -3 5 -12 0z')
        spep.classList.add('potexplo')
    }

    drawMondeSimple(monde, grm) {
        SvgTools.addRect(grm, 0, 0, this.largeur, this.largeur, 'jou_' + monde.proprietaire)
        const text = SvgTools.addText(grm, monde.numero, 1.4 * this.msize / 2, 1.4 * this.msize / 2 + 2, 'num-monde', 'jou_' + monde.proprietaire)

        text.setAttribute('id', 'text-M_' + monde.numero)

        if (monde.isMondeDepart) {
            this.drawMondeDepart(grm, monde.numero, this.rayon_cercle_md)
        }
    }

    drawMondeExtended(monde, grm) {
        const dect = 3

        // Rectangle de fond du monde
        SvgTools.addRect(grm, 0, 0, this.largeur, this.largeur, 'fond-rectangle-monde')

        // Rectangle de contour du numéro du monde
        SvgTools.addRect(grm, 0, this.largeur * 2 / 3, this.largeur, this.largeur / 3, 'jou_' + monde.joueur_couleur, 'numero-monde')

        // Rectangle de contour du monde (doit être placé au-dessus, donc après, contour du numéro du monde)
        SvgTools.addRect(grm, 0, 0, this.largeur, this.largeur, 'rectangle-monde')

        const text = SvgTools.addText(grm, monde.numero, 1.4 * this.msize / 2, this.largeur / 3 + 1.4 * this.msize / 2 + 1,
            'num-extended', 'jou_' + monde.joueur_couleur)
        text.setAttribute('id', 'text-M_' + monde.numero)

        if (monde.isMondeDepart) {
            this.drawMondeDepart(grm, monde.numero, this.rayon_cercle_md)
        }

        if (monde.mp > 0) {
            SvgTools.addText(grm, monde.mp, dect, 5, 'mp')
        }

        if (monde.capaciteMiniere > 0) {
            SvgTools.addText(grm, '+' + monde.capaciteMiniere, this.largeur - dect, 5, 'mpa')
        }

        if (monde.populations > 0) {
            SvgTools.addText(grm, monde.populations, dect, this.largeur / 2, 'pop')
        }

        if (monde.industries > 0) {
            SvgTools.addText(grm,
                monde.industriesActives < monde.industries
                    ? '' + monde.industriesActives + '/' + monde.industries
                    : monde.industries,
                this.largeur - dect, this.largeur / 2, 'ind')
        }
        if (monde.vp > 0) {
            SvgTools.addUse(grm, monde.allie ? '#s_vivp' : '#s_vivpe', this.c.vp.x - 6, this.c.vp.y - 6.5)
            SvgTools.addText(grm, monde.vp, this.c.vp.x, this.c.vp.y, monde.allie ? 'tvp' : 'tvpe')
        }

        if (monde.vi > 0) {
            SvgTools.addUse(grm, monde.allie ? '#s_vivp' : '#s_vivpe', this.c.vi.x - 6, this.c.vi.y - 6.5)
            SvgTools.addText(grm, monde.vi, this.c.vi.x, this.c.vi.y, monde.allie ? 'tvp' : 'tvpe')
        }

        if (monde.nbflottes > 0 || monde.nbflottesA > 0) {
            SvgTools.addUse(grm, monde.nbflottes > 0 ? '#s_fl' : '#s_fla', this.c.vc.x - 6, this.c.vc.y - 6.5)
            SvgTools.addText(grm, monde.nbflottes + monde.nbflottesA, this.c.fl.x, this.c.fl.y, 'tvp')
            SvgTools.addText(grm, monde.nbvcflottes + monde.nbvcflottesA, this.c.vc.x, this.c.vc.y, 'tvp')
        }

        if (monde.nbflottesE > 0) {
            SvgTools.addUse(grm, '#s_fle', this.c.fle.x - 10, this.c.fle.y - 6.5)
            SvgTools.addText(grm, monde.nbflottesE, this.c.fle.x, this.c.fle.y, 'tvpe')
            SvgTools.addText(grm, monde.nbvcflottesE, this.c.vce.x, this.c.vce.y, 'tvpe')
        }
    }

    drawPillage(monde, grm) {
        if (monde.nbPillage > 0) {
            SvgTools.addUse(grm, '#s_pil', this.c.pil.x - 7, this.c.pil.y - 7.5)
            SvgTools.addText(grm,
                '' + monde.nbPillage + (monde.toursRecupPillage > 0 ? '/' + monde.toursRecupPillage : ''),
                this.c.pil.x, this.c.pil.y, 'tpillage')
        }
    }

    drawExplo(monde, grm) {
        if (monde.isObs || monde.isLoc) {
            SvgTools.addUse(grm, monde.isObs ? '#visibility_observe' : '#visibility_sur_place', -5, -8)
        }

        if (monde.potentielExploration > 0) {
            SvgTools.addUse(grm, '#s_pe', this.c.pe.x - 9, this.c.pil.y - 6)
            SvgTools.addText(grm, monde.potentielExploration, this.c.pe.x, this.c.pe.y, 'tpotexplo')
        }
    }

    async setPositionMonde(monde, group = null) {
        if (monde.x === 0 || monde.y === 0) {
            return
        }

        if (!group) {
            group = document.querySelector('[data-monde="' + monde.numero + '"]')
        }

        let x = monde.getXBitmap(this.msize) - 1.4 * this.msize / 2
        let y = monde.getYBitmap(this.msize) - 1.4 * this.msize / 2

        if (monde.translate) {
            x += (monde.translate.x * 2) * this.msize
            y += (monde.translate.y * 2) * this.msize
        }

        group.setAttribute('transform', `translate(${x} ${y})`)
    }

    /**
     * Dessine une connexion dans le référentiel du groupe des connexions de monde1
     * @param monde1
     * @param monde2
     * @param line element line de la connexion
     */
    drawPositionConnexion(monde1, monde2, line) {
        if (!monde1 || !monde2) {
            return
        }

        if (monde2.x * monde2.y * monde1.x * monde1.y === 0) {
            return
        }

        const destCoord = PlanModulo.getDistanceModulo({
            x: monde2.x - monde1.x,
            y: monde2.y - monde1.y,
        })

        line.setAttribute('x1', 0)
        line.setAttribute('y1', 0)
        line.setAttribute('x2', this.msize * 2 * destCoord.x)
        line.setAttribute('y2', this.msize * 2 * destCoord.y)
    }

    /**
     * Crée le groupe contenant toutes les connexions partant du monde
     * @param monde
     * @param group element group svg
     */
    async setPositionConnexions(monde, group = null) {
        if (!group) {
            group = document.querySelector(`#CM_${monde.numero}`)
        }

        let x = monde.getXBitmap(this.msize)
        let y = monde.getYBitmap(this.msize)

        if (monde.translate) {
            x += (monde.translate.x * 2) * this.msize
            y += (monde.translate.y * 2) * this.msize
        }

        group.setAttribute('transform', `translate(${x} ${y})`)
    }

    drawMondeSelect(monde) {
        this.hideSelectMonde()

        const parent = document.querySelector(`[data-monde="${monde.numero}"]`)

        const x = 0
        const y = 0
        SvgTools.addRect(parent, x - 6, y - 6, this.largeur + 12, this.largeur + 12, 'select_monde')
    }

    hideSelectMonde() {
        const prev = document.getElementsByClassName('select_monde')
        while (prev.length > 0) {
            prev[0].parentNode.removeChild(prev[0])
        }
    }

    removeMdCircle(numMonde) {
        if (!numMonde) {
            return
        }

        document.getElementById(this.getCercleMdId(numMonde)).remove()
    }

    addMdCircle(numMonde) {
        let rectNumMonde = document.querySelector(`[data-monde="${numMonde}"] rect:nth-child(2)`)

        // Si on ne etouve rien c'est qu'on est en mode simple => on se rabat sur le rectangle principal du monde
        if (!rectNumMonde) {
            rectNumMonde = document.querySelector(`[data-monde="${numMonde}"] rect`)
        }

        const centre = this.msize * (0.5 + 0.2)
        SvgTools.addCircleAfter(rectNumMonde, centre, centre, this.rayon_cercle_md, this.getCercleMdId(numMonde), 'cercle-md')
    }

    getCercleMdId(numMonde) {
        return `cercle-M_${numMonde}`
    }

    drawMondeDepart(parent, numMonde) {
        if (!numMonde) {
            return
        }

        let circle = document.querySelector(`#cercle-M_${numMonde}`)

        if (circle) {
            return
        }

        const centre = this.msize * (0.5 + 0.2)

        circle = SvgTools.addCircle(parent, centre, centre, this.rayon_cercle_md, this.getCercleMdId(numMonde), 'cercle-md')
    }

    clearSelectArea() {
        document.querySelectorAll('.select_monde_area').forEach(element => element.remove())
    }

    addSelectArea(mondes) {
        this.clearSelectArea()
        for (const numMonde of mondes) {
            const parent = document.querySelector(`[data-monde="${numMonde}"]`)
            const left = Math.round(-this.msize * 0.3)
            const width = this.msize * 2
            SvgTools.addRect(parent, left, left, width, width, 'select_monde_area')
        }
    }
}
