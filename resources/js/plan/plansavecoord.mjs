import { notify } from '../notify.mjs'
import { PlanModulo } from './planmodulo.mjs'

export class PlanSaveCoord {
    constructor(plan) {
        this.plan = plan
        this.saveButtonEnabled = false
        this.saveButton = document.querySelector('#save-button')
        this.saveButton.addEventListener('click', this.onSaveClick.bind(this))
        document.addEventListener('disable-save-button', this.onDisableSaveButton.bind(this))
        document.addEventListener('enable-save-button', this.onEnableSaveButton.bind(this))
        document.addEventListener('monde-position-changed', this.onMondePositionChanged.bind(this))
        window.addEventListener('beforeunload', this.onBeforeUnload.bind(this))
    }

    getUpdatedCoordonnees() {
        return this.plan.mondes
            .filter(monde => monde.coordChanged && (monde.x !== monde.oldX || monde.y !== monde.oldY))
            .map(monde => {
                const { x, y } = PlanModulo.getModuloCoord({
                    x: Math.round(monde.x - this.plan.decalage.x),
                    y: Math.round(monde.y - this.plan.decalage.y),
                })

                return {
                    numero: monde.numero,
                    x,
                    y,
                }
            })
    }

    cleanUpdatedCoordonnees() {
        this.plan.mondes
            .filter(monde => monde.coordChanged)
            .forEach(monde => {
                delete monde.coordChanged
                delete monde.oldX
                delete monde.oldY
                this.plan.drawUpdatedStyle([monde.numero])
            })
    }

    restoreOldCoordonnees() {
        // On supprime les nouveaux mondes mondes placés
        const mondesToDelete = this.plan.mondes.filter(monde => monde.nouveau)

        this.plan.removeMondes(mondesToDelete)
        mondesToDelete.forEach(monde => delete this.plan.mondes[monde.numero])

        // On regarde dans les mondes qui restent ceux qu'il faut replacer
        let mondesToRedraw = []
        this.plan.mondes
            .filter(monde => monde.coordChanged)
            .forEach(monde => {
                delete monde.coordChanged
                monde.x = monde.oldX
                monde.y = monde.oldY
                mondesToRedraw.push(monde.numero, ...this.plan.getMondeConnexions(monde.numero))
                this.plan.drawUpdatedStyle([monde.numero])
            })

        if (mondesToRedraw.length) {
            mondesToRedraw = [...new Set(mondesToRedraw)]
            this.plan.draw(mondesToRedraw, true)
            notify('success', 'Les mondes déplacés ont été remis à leur place')
        }
    }

    onSaveClick(e) {
        const updatedCoordonnees = this.getUpdatedCoordonnees()

        if (updatedCoordonnees.length) {
            this.cleanUpdatedCoordonnees()
            window.livewire.emit('storeCoordonneesFromPlan', updatedCoordonnees)
        }
    }

    onDisableSaveButton() {
        this.saveButton.classList.remove('menu_btn')
        this.saveButton.classList.add('menu_btn_disabled')
        this.cleanUpdatedCoordonnees()
        this.saveButtonEnabled = false
    }

    onEnableSaveButton() {
        this.saveButton.classList.remove('menu_btn_disabled')
        this.saveButton.classList.add('menu_btn')
        this.saveButtonEnabled = true
    }

    onMondePositionChanged() {
        if (this.getUpdatedCoordonnees().length) {
            document.dispatchEvent(new Event('enable-save-button'))
            return
        }

        document.dispatchEvent(new Event('disable-save-button'))
    }

    onBeforeUnload(e) {
        if (this.saveButtonEnabled) {
            e.returnValue = 'Plan non sauvegardé'
        }
    }
}
