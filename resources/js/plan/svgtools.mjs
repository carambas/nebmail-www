export class SvgTools {
    static add(parent, tagname, id = null) {
        const elem = document.createElementNS('http://www.w3.org/2000/svg', tagname)

        if (id) {
            elem.setAttribute('id', id)
        }

        if (parent) {
            parent.appendChild(elem)
        }

        return elem
    }

    static addSymbol(parent, id, w, h) {
        const s = SvgTools.add(parent, 'symbol')
        s.setAttribute('id', id)
        s.setAttribute('width', w)
        s.setAttribute('height', h)
        return s
    }

    static addUse(parent, id, x, y) {
        const use = SvgTools.add(parent, 'use')
        use.setAttribute('href', id)
        if (x !== 0 && y !== 0) {
            use.setAttribute('transform', 'translate(' + x + ',' + y + ')')
        }
        return use
    }

    static addText(parent, text, x, y, ...classes) {
        const txt = SvgTools.add(parent, 'text')
        txt.setAttribute('x', x)
        txt.setAttribute('y', y)

        if (classes) {
            for (const c of classes) {
                txt.classList.add(c)
            }
        }

        txt.innerHTML = text

        return txt
    }

    static addCircle(parent, x, y, r, id, ...classes) {
        const circle = SvgTools.add(parent, 'circle')
        SvgTools.setCircleAttributes(circle, x, y, r, id, ...classes)

        return circle
    }

    static addCircleAfter(prevElement, x, y, r, id, ...classes) {
        const circle = SvgTools.add(null, 'circle')
        SvgTools.insertAfter(circle, prevElement)
        SvgTools.setCircleAttributes(circle, x, y, r, id, ...classes)

        return circle
    }

    static setCircleAttributes(circle, x, y, r, id, ...classes) {
        if (id) {
            circle.setAttribute('id', id)
        }

        circle.setAttribute('cx', x)
        circle.setAttribute('cy', y)
        circle.setAttribute('r', r)

        if (classes) {
            for (const c of classes) {
                circle.classList.add(c)
            }
        }
    }

    static addRect(parent, x, y, w, h, ...classes) {
        const rect = SvgTools.add(parent, 'rect')
        rect.setAttribute('x', x)
        rect.setAttribute('y', y)
        rect.setAttribute('width', w)
        rect.setAttribute('height', h)

        if (classes) {
            for (const c of classes) {
                rect.classList.add(c)
            }
        }

        return rect
    }

    static addLine(parent, x1, y1, x2, y2, ...classes) {
        const line = SvgTools.add(parent, 'line')
        line.setAttribute('x1', x1)
        line.setAttribute('y1', y1)
        line.setAttribute('x2', x2)
        line.setAttribute('y2', y2)

        if (classes) {
            for (const c of classes) {
                line.classList.add(c)
            }
        }

        return line
    }

    static insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling)
    }
}
