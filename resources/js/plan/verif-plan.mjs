import { PlanModulo } from './planmodulo.mjs'

/**
 * Détecte les mondes empilés et leur donne la classe "stacked" au niveau du group svg
 */
export class VerifPlan {
    constructor(plan) {
        this.plan = plan
        PlanModulo.longX = plan.longX
        PlanModulo.longY = plan.longY
        this.fillMondesByCoord()
        document.addEventListener('unlock-plan', this.onUnlockPlan.bind(this))
        document.addEventListener('lock-plan', this.onLockPlan.bind(this))
    }

    fillMondesByCoord(connexions = true) {
        this.mondesByCoord = []
        const mondes = this.plan.mondes

        for (const numMonde in this.plan.mondes) {
            const monde = this.plan.mondes[numMonde]
            if (monde.x > 0 && monde.y > 0) {
                this.addMonde(monde)

                if (connexions) {
                    for (const numMondeCnx of monde.connexions) {
                        if (!this.checkConnexionMalOrientee(monde.x, monde.y, mondes[numMondeCnx]?.x, mondes[numMondeCnx]?.y)) {
                            this.fillConnexionsByCoord(monde.x, monde.y, mondes[numMondeCnx]?.x, mondes[numMondeCnx]?.y)
                        }
                    }
                }
            }
        }
    }

    addMonde(monde, mondesFrom = []) {
        if (!this.mondesByCoord[`${monde.x},${monde.y}`]) {
            this.mondesByCoord[`${monde.x},${monde.y}`] = []
        }
        this.mondesByCoord[`${monde.x},${monde.y}`].push(monde.numero)

        for (const mondeFrom of mondesFrom) {
            this.fillConnexionsByCoord(monde.x, monde.y, mondeFrom.x, mondeFrom.y)
        }
    }

    /**
     * Détecte les mondes empilés et donne la propriété stacked à true
     */
    checkStackedMondes() {
        if (this.mondesByCoord.length === 0) {
            this.fillMondesByCoord()
        }

        this.plan.mondes.forEach(monde => delete monde.stacked)

        Object.values(this.mondesByCoord)
            .filter(item => item.length > 1) // On ne conserve que les valeurs des cases où il y a plus d'un élément (monde, connexion)
            .flat() // On met en vrac les numéros des mondes et "C" pour les connexions
            .filter(Number.isInteger) // On ne garde que les numéros de mondes
            .forEach(numMonde => {
                this.plan.mondes[numMonde].stacked = true
            })
    }

    /**
     * Ajoute la classe stacked à l'objet group svg des mondes ayant la propriété stacked
     */
    setStackedClass() {
        document.querySelectorAll('.stacked').forEach(groupElt => { groupElt.classList.remove('stacked') })
        this.checkStackedMondes()

        this.plan.mondes
            .filter(monde => monde.stacked)
            .forEach(monde => {
                document.querySelector(`[data-monde="${monde.numero}"]`)?.classList.add('stacked')
            })
    }

    emplacementOccupeByMonde(monde, connexionAcceptee = false) {
        return this.emplacementOccupe(monde.x, monde.y, connexionAcceptee)
    }

    emplacementOccupe(x, y, connexionAcceptee = false) {
        const emplacement = Object.keys(this.mondesByCoord).filter(coordStr => {
            const matches = coordStr.match(/([0-9.]+),([0-9.]+)/)
            const indexX = parseFloat(matches[1])
            const indexY = parseFloat(matches[2])
            return Math.abs(indexX - x) < 0.1 && Math.abs(indexY - y) < 0.1
        })

        // console.log(emplacement)

        return connexionAcceptee
            ? emplacement.filter(Number.isInteger).length > 0
            : emplacement.length > 0
    }

    checkConnexionMalOrientee(x1, y1, x2, y2) {
        if (!x2 || !y2) {
            return false
        }

        const decalage = PlanModulo.getDistanceModulo({ x: x2 - x1, y: y2 - y1 })

        // Connexion verticale ou horizontale
        if (Math.abs(decalage.x) < 0.01 || Math.abs(decalage.y) < 0.01) {
            return false
        }

        // Si on a la même longueur pour x et y alors on a un angle multiple de 45°
        return !(Math.abs(Math.abs(decalage.x / decalage.y) - 1) < 0.01)
    }

    fillConnexionsByCoord(x1, y1, x2, y2) {
        if (!x2 || !y2) {
            return
        }

        const decalage = PlanModulo.getDistanceModulo({ x: x2 - x1, y: y2 - y1 })
        const tailleConnexion = Math.max(Math.abs(decalage.x), Math.abs(decalage.y))
        const direction = { x: Math.round(decalage.x / tailleConnexion), y: Math.round(decalage.y / tailleConnexion) }

        for (let etape = 1; etape < tailleConnexion; etape++) {
            const coord = PlanModulo.getModuloCoord({ x: x1 + direction.x * etape, y: y1 + direction.y * etape })
            if (!this.mondesByCoord[`${coord.x},${coord.y}`]) {
                this.mondesByCoord[`${coord.x},${coord.y}`] = []
            }

            if (!this.mondesByCoord[`${coord.x},${coord.y}`].includes('C')) {
                this.mondesByCoord[`${coord.x},${coord.y}`].push('C')
            }
        }
    }

    /**
     * Trouve les mondes que traverse une connexion
     * Ne fonctionne que si la connexion a un angle multiple de 45°
     */
    checkMondesTraversesParConnexion(x1, y1, x2, y2) {
        const decalage = PlanModulo.getDistanceModulo({ x: x2 - x1, y: y2 - y1 })
        const tailleConnexion = Math.max(Math.abs(decalage.x), Math.abs(decalage.y))
        // const tailleConnexion = Math.max(decalage.x, decalage.y)
        const direction = { x: Math.round(decalage.x / tailleConnexion), y: Math.round(decalage.y / tailleConnexion) }
        const result = []

        for (let etape = 1; etape < tailleConnexion; etape++) {
            const coord = PlanModulo.getModuloCoord({ x: x1 + direction.x * etape, y: y1 + direction.y * etape })
            if (this.mondesByCoord[`${coord.x},${coord.y}`]) {
                result.push(...this.mondesByCoord[`${coord.x},${coord.y}`])
            }
        }

        return result.filter(Number.isInteger)
    }

    setConnexionMalOrienteeClass(numMonde1, numMonde2) {
        document.querySelector(`#C${numMonde1}_${numMonde2}`)?.classList.add('bad-orientation')
        document.querySelector(`#C${numMonde2}_${numMonde1}`)?.classList.add('bad-orientation')
    }

    setConnexionTraverseMondes(numMondeDepart, numMondeArrivee, mondesTraverses) {
        document.querySelector(`#C${numMondeDepart}_${numMondeArrivee}`)?.classList.add('connexion-traverse-monde')
        document.querySelector(`#C${numMondeArrivee}_${numMondeDepart}`)?.classList.add('connexion-traverse-monde')
        for (const numMonde of mondesTraverses) {
            document.querySelector(`[data-monde="${numMonde}"]`)?.classList.add('stacked')
        }
    }

    unsetConnexionDeTraversClass() {
        document.querySelectorAll('.bad-orientation').forEach(groupElt => { groupElt.classList.remove('bad-orientation') })
    }

    setConnexionDeTraversClass(lock = true) {
        if (this.mondesByCoord.length === 0) {
            this.fillMondesByCoord()
        }

        this.unsetConnexionDeTraversClass()
        document.querySelectorAll('.connexion-traverse-monde').forEach(groupElt => { groupElt.classList.remove('connexion-traverse-monde') })

        // On identifie les connexions qui ne font pas un angle multiple de 45° => class "connexion-de-travers"
        for (const monde of Object.values(this.plan.mondes)) {
            const x1 = monde.x
            const y1 = monde.y

            if (x1 === 0 || y1 === 0) {
                continue
            }

            // const connexions =
            for (const numMonde of monde.connexions) {
                const monde2 = this.plan.mondes[numMonde]
                const x2 = monde2?.x
                const y2 = monde2?.y

                if (!monde2 || x2 === 0 || y2 === 0) {
                    continue
                }

                if (this.checkConnexionMalOrientee(x1, y1, x2, y2)) {
                    // On a une connexion non multiple de 45°
                    if (!lock) {
                        this.setConnexionMalOrienteeClass(monde.numero, numMonde)
                    }
                } else {
                    // On a une connexion multiple de 45°. Il est alors pertinent de vérifier s'il y a des mondes traversés
                    // On ne le fait que lorsque le plan est déverrouillé
                    const mondesTraverses = this.checkMondesTraversesParConnexion(x1, y1, x2, y2)
                    if (mondesTraverses.length > 0) {
                        this.setConnexionTraverseMondes(monde.numero, numMonde, mondesTraverses)
                    }
                }
            }
        }
    }

    onLockPlan(e) {
        this.unsetConnexionDeTraversClass()
    }

    onUnlockPlan(e) {
        this.setConnexionDeTraversClass(false)
    }
}
