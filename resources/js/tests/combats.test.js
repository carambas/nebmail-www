import * as Combats from '../eval/combats.js'

describe('CATT', () => {
    it('Catt expected values should be 55', () => {
        expect(Combats.calculeCAtt(2)).toBe(55)
        expect(Combats.calculeCAtt(10)).toBe(118)
        expect(Combats.calculeCAtt(7)).toBe(89)
        expect(Combats.calculeCAtt(100)).toBe(306)
        expect(Combats.calculeCAtt(-5)).toBe(50)
    })

    it('Cdef expected values', () => {
        expect(Combats.calculeCDef(1)).toBe(100)
        expect(Combats.calculeCDef(-10)).toBe(Combats.calculeCDef(1))
        expect(Combats.calculeCDef(100)).toBe(Combats.calculeCDef(20))
        expect(Combats.calculeCDef(6)).toBe(59)
        expect(Combats.calculeCDef(12)).toBe(31)
    })

    it('Test attaque 1', () => {
        expect(
            Combats.calculeDegatsTir(
                {
                    att: 5,
                    vc: 10,
                    vt: 10,
                    isEmbuscade: false,
                },
                {
                    def: 5,
                    car: 2,
                    vc: 10,
                    vt: 10,
                    cargaison: 5,
                    isFuite: false,
                }
            )
        ).toEqual({
            touchés: 11,
            détruits: 9,
            vcDétruits: 9,
            vtDétruits: 0,
        })
    })

    it('Test attaque embuscade', () => {
        expect(
            Combats.calculeDegatsTir(
                {
                    att: 12,
                    vc: 30,
                    vt: 0,
                    isEmbuscade: true,
                },
                {
                    def: 1,
                    car: 1,
                    vc: 100,
                    vt: 0,
                    cargaison: 0,
                    isFuite: false,
                }
            )
        ).toEqual({
            touchés: 86,
            détruits: 86,
            vcDétruits: 86,
            vtDétruits: 0,
        })
    })
    it('Test attaque fuite', () => {
        expect(
            Combats.calculeDegatsTir(
                {
                    att: 12,
                    vc: 30,
                    vt: 0,
                    isEmbuscade: false,
                },
                {
                    def: 1,
                    car: 1,
                    vc: 100,
                    vt: 0,
                    cargaison: 0,
                    isFuite: true,
                }
            )
        ).toEqual({
            touchés: 43,
            détruits: 22,
            vcDétruits: 22,
            vtDétruits: 0,
        })
    })

    it('attaque attaque cargaison', () => {
        expect(
            Combats.calculeDegatsTir(
                {
                    att: 8,
                    vc: 30,
                    vt: 0,
                    isEmbuscade: false,
                },
                {
                    def: 1,
                    car: 5,
                    vc: 20,
                    vt: 20,
                    cargaison: 50,
                    isFuite: false,
                }
            )
        ).toEqual({
            touchés: 30,
            détruits: 37,
            vcDétruits: 20,
            vtDétruits: 17,
        })
    })
})
