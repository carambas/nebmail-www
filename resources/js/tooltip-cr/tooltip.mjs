import { createPopper } from '@popperjs/core'
import maxSize from 'popper-max-size-modifier'
import { notify } from '../notify.mjs'

const applyMaxSize = {
    name: 'applyMaxSize',
    enabled: true,
    phase: 'beforeWrite',
    requires: ['maxSize'],
    fn({ state }) {
        // The `maxSize` modifier provides this data
        const { height } = state.modifiersData.maxSize
        state.styles.popper.maxHeight = `${height}px`
    }
}

export class Tooltip {
    // tooltip
    // tooltipContent
    // popper
    // visible

    constructor() {
        const svg = document.getElementById('svg_plan')
        this.visible = false
        this.tooltip = document.getElementById('tooltip')
        // this.tooltipContent = new TooltipContent()
        this.numMonde = 0

        this.popper = createPopper(svg, this.tooltip, {
            placement: 'auto',
        })

        this.tooltip.addEventListener('click', event => this.onClick(event))
        this.tooltip.addEventListener('keydown', event => this.onKeyDown(event))
        // document.addEventListener('tooltip-show', event => this.show(event))
        // document.addEventListener('tooltip-hide', event => this.hide(event))
    }

    /**
     * Affiche le tooltip en indiquant l'élélent auquel il s'applique et son contenu
     *
     * @constructor
     * @this {Tooltip}
     * @param {HTMLElement} referenceDom : élément DOM lié au tooltip
     * @param {TooltipContent} content
     * @returns
     */
    show(content) {
        // Si la popup est déjà affichée et inchangée alors on la ferme
        if (this.tooltip.hasAttribute('data-show') &&
            (this.popper.state.elements.reference === content.domRef)) {
            this.hide()
            return
        }

        this.crsOriginaux = JSON.parse(JSON.stringify(content.crtxt)) // Deep copy sans référence
        this.removeNomMondes(content)

        this.tooltip.setAttribute('data-show', '')
        this.popper.state.elements.reference = content.domRef
        this.popper.setOptions({
            modifiers: [
                { name: 'eventListeners', enabled: true },
                maxSize,
                applyMaxSize,
            ],
        })

        this.visible = true

        this.popper.update()
    }

    hide() {
        this.tooltip.removeAttribute('data-show')

        this.popper.setOptions({
            modifiers: [{ name: 'eventListeners', enabled: false }],
        })

        this.visible = false
    }

    onCopyClick(index, e) {
        const tour = this.crsOriginaux[index].tour
        let cr = typeof tour === 'number' ? `### Information datant du tour ${tour}\n` : ''
        cr += this.crsOriginaux[index].cr

        navigator.clipboard.writeText(cr)
        notify('success', 'L\'extrait de CR a été copié dans le presse-papier')
        e.stopPropagation()
        e.preventDefault()
    }

    onCopyAllClick(e) {
        const cr = this.crsOriginaux.map(crTour => {
            let result = typeof crTour.tour === 'number' ? `### Information datant du tour ${crTour.tour}\n` : ''
            result += crTour.cr
            return result
        }).join('\n\n')

        navigator.clipboard.writeText(cr)
        notify('success', 'Les extraits de CR ont été copiés dans le presse-papier')
        e.stopPropagation()
        e.preventDefault()
    }

    onClick(e) {
        this.hide()
    }

    onKeyDown(e) {
        if (e.key === 'Escape') return

        e.stopPropagation()
    }

    isVisible() {
        return this.visible
    }

    getContent() {
        return this.content.innerText
    }

    flecheClick(numMonde) {
        window.dispatchEvent(new CustomEvent('selection-monde', {
            detail: {
                monde: numMonde,
                from: 'plan-tooltip'
            }
        }))
    }

    removeNomMondes(content) {
        content.crtxt.forEach(crTour => {
            if (content.nomMonde) crTour.cr = crTour.cr.replace(/^".*?"\n/s, '')
        })
    }
}
