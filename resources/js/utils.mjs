export function htmlEncode(_s) {
    txt.data = _s
    return spn.innerHTML
}

/**
 * Groupby : regroupe un tableau d'obejets selon une proptiété
 * @param {Object[]} tableauObjets
 * @param {string} propriete
 * @returns
 */
export function groupBy(tableauObjets, propriete) {
    return tableauObjets.reduce(function (acc, obj) {
        const cle = obj[propriete]
        if (!acc[cle]) {
            acc[cle] = []
        }
        acc[cle].push(obj)
        return acc
    }, {})
}

const spn = document.createElement('span')
const txt = document.createTextNode('')
spn.appendChild(txt)
