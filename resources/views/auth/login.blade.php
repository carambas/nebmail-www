<x-layouts.app>
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Connexion</div>

                @error('status')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="partie" class="col-md-4 col-form-label text-md-right">Partie</label>

                            <div class="col-md-6">
                                <select id="partie" class="form-control @error('partie') is-invalid @enderror" name="partie_name"  required autocomplete="Partie" autofocus>
                                @foreach($parties as $partie)
                                        <option @selected(in_array($partie, [old('partie_name'), Cookie::get('partie_name')]))>{{ $partie }}</option>
                                @endforeach
                                </select>

                                @error('partie')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="numjou" class="col-md-4 col-form-label text-md-right">Numéro de joueur</label>

                            <div class="col-md-6">
                                <input id="numjou" type="number" class="form-control @error('numjou') is-invalid @enderror" name="numjou" value="{{ old('numjou') }}" required autocomplete="Joueur" autofocus>

                                @error('numjou')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        Se souvenir de moi
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Connexion
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</x-layouts.app>
