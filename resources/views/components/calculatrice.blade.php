<x-modal
id="calculatrice-modal"
title="Calculatrice de combat"
:centered="true"
:scrollable="false"
size="l"
>
    <x-slot name="body">
        <div class="row">
            <div class="col-sm-4">
                <div>Attaquant</div>
                <div class="form-group row">
                    <label for="attaquant-att" class="col-sm-6 col-form-label">ATT</label>
                    <div class="col-sm-6">
                        <input id="attaquant-att" type="text" class="form-control" value="1">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="attaquant-vc" class="col-sm-6 col-form-label">VC</label>
                    <div class="col-sm-6">
                        <input id="attaquant-vc" type="text" class="form-control" value="1">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="attaquant-vt" class="col-sm-6 col-form-label">VT</label>
                    <div class="col-sm-6">
                        <input id="attaquant-vt" type="text" class="form-control" value="0">
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-2">Défenseur</div>
                    <div class="col-sm-3 offset-sm-3">Détruits</div>
                    <div class="col-sm-3">Restant</div>
                </div>
                <div class="form-group row">
                    <label for="defenseur-def" class="col-sm-2 col-form-label">DEF</label>
                    <div class="col-sm-3">
                        <input id="defenseur-def" type="text" class="form-control" value="1">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="defenseur-vc" class="col-sm-2 col-form-label">VC</label>
                    <div class="col-sm-3">
                        <input id="defenseur-vc" type="text" class="form-control" value="1">
                    </div>
                    <div class="col-sm-3">
                        <input id="defenseur-vc-detruits" type="text" class="form-control text-muted" disabled>
                    </div>
                    <div class="col-sm-3">
                        <input id="defenseur-vc-restants" type="text" class="form-control text-muted" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="defenseur-vt" class="col-sm-2 col-form-label">VT</label>
                    <div class="col-sm-3">
                        <input id="defenseur-vt" type="text" class="form-control" value="0">
                    </div>
                    <div class="col-sm-3">
                        <input id="defenseur-vt-detruits" type="text" class="form-control text-muted" disabled>
                    </div>
                    <div class="col-sm-3">
                        <input id="defenseur-vt-restants" type="text" class="form-control text-muted" disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="defenseur-car" class="col-sm-2 col-form-label">CAR</label>
                    <div class="col-sm-3">
                        <input id="defenseur-car" type="text" class="form-control" value="1">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="defenseur-cargaison" class="col-sm-2 col-form-label">Cargais.</label>
                    <div class="col-sm-3">
                        <input id="defenseur-cargaison" type="text" class="form-control" value="0">
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="attaquant-embuscade">
                    <label class="form-check-label" for="attaquant-embuscade">
                        Embuscade
                    </label>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="defenseur-fuite">
                    <label class="form-check-label" for="defenseur-fuite">
                        Fuite
                    </label>
                </div>
            </div>
        </div>
        <div lass="row">
            <textarea class="form-control mt-3 d-none" id="calculatrice-text" rows="4"></textarea>
        </div>
    </x-slot>
    <x-slot name="footer">
        <button type="button" class="btn btn-success" id="calcule-button">Calcul</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">Annuler</button>
    </x-slot>
</x-modal>

@push('page-js-script')
<script type="module">
    document.querySelector('#calcule-button')
        .addEventListener('click', onCalcul)

    document.querySelector('#calculatrice-modal')
        .addEventListener('keypress', formKeyPressed)

    function formKeyPressed(event) {
        if (event.key === 'Enter') {
            event.preventDefault()
            onCalcul()
        }
    }

    function onCalcul() {
        const attaque = readAttaque()
        const defense = readDefense()

        const degats = Combats.calculeDegatsTir(attaque, defense)

        setDefenseValue(defense, degats)
        rempliTexte(degats)
    }

    function readAttaque() {
        return new Combats.TirAttaque(
            getInputValue(document.querySelector('#attaquant-att'), 1, 1),
            getInputValue(document.querySelector('#attaquant-vc'), 0, 1),
            getInputValue(document.querySelector('#attaquant-vt'), 0, 0),
            document.querySelector('#attaquant-embuscade').checked ?? false
        )
    }

    function readDefense() {
        return new Combats.TirDefense(
            getInputValue(document.querySelector('#defenseur-def'), 1, 1),
            getInputValue(document.querySelector('#defenseur-car'), 1, 1),
            getInputValue(document.querySelector('#defenseur-cargaison'), 0, 0),
            getInputValue(document.querySelector('#defenseur-vc'), 0, 1),
            getInputValue(document.querySelector('#defenseur-vt'), 0, 0),
            document.querySelector('#defenseur-fuite').checked ?? false
        )
    }

    function setDefenseValue(defense, degats) {
        document.querySelector('#defenseur-vc-detruits').value = degats.vcDétruits
        document.querySelector('#defenseur-vc-restants').value = defense.vc - degats.vcDétruits
        document.querySelector('#defenseur-vt-detruits').value = degats.vtDétruits
        document.querySelector('#defenseur-vt-restants').value = defense.vt - degats.vtDétruits
    }

    function rempliTexte(degats) {
        const texte =
            `Vaisseaux touchés : ${degats.touchés}\n` +
            `Vaisseaux détruits : ${degats.détruits}\n` +
            `VC détruits : ${degats.vcDétruits}\n` +
            `VT détruits : ${degats.vtDétruits}`

        const text = document.querySelector('#calculatrice-text')
        text.classList.remove('d-none')
        text.value = texte
    }

    function getInputValue(input, min, defaultValue) {
        let value = input.value
        if (!value) value = defaultValue
        value = Math.max(Number(value), min)

        input.value = value
        return value
    }
</script>
@endpush
