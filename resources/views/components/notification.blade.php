<div>
    <div
        x-data="noticesHandler"
        aria-live="polite"
        aria-atomic="true"
        style="position: absolute; right:0.5rem; z-index: 100;"
        @notify.window="add($event.detail)">

            <template x-for="notice of notices" :key="notice.id">
                <div x-show.transition="visible.includes(notice)"
                    class="toast show" role="alert"
                    aria-live="assertive"
                    aria-atomic="true"
                >
                    <div class="toast-header text-white bg-success"
                        :class="{
                            'bg-success': notice.typeMsg === 'success',
                            'bg-warning': notice.typeMsg === 'warning',
                            'bg-danger': notice.typeMsg === 'danger',
                        }">
                        <strong class="mr-auto">Notification</strong>
                    </div>
                    <div class="toast-body" x-text="notice.msg">
                    </div>
                </div>
            </template>

            @if (Session::has('error_message') || Session::has('error'))
            <div
                class="toast show" role="alert"
                aria-live="assertive"
                aria-atomic="true"
                data-delay="5000"
            >
                <div class="toast-header text-white bg-danger">
                    <strong class="mr-auto">Erreur</strong>
                </div>
                <div class="toast-body">
                    {{ Session::get('error_message') }}
                    {{ Session::get('error') }}
                </div>
            </div>
            @endif

            @if (Session::has('status') || Session::has('mail'))
            <div
                class="toast show" role="alert"
                aria-live="assertive"
                aria-atomic="true"
                data-delay="5000"
            >
                <div class="toast-header text-white bg-success">
                    <strong class="mr-auto">Notification</strong>
                </div>
                <div class="toast-body">
                    {{ Session::get('status') }}
                    {{ Session::get('mail') }}
                </div>
            </div>
            @endif
    </div>

</div>


@once
    @push('page-js-script')
        <script type="module">
            $('.toast').toast('show')

            document.addEventListener('alpine:init', () => {
                Alpine.data('noticesHandler', () => ({
                    notices: [],
                    visible: [],
                    add(notice) {
                        notice.id = Date.now()
                        while (this.notices.findIndex(n => n.id === notice.id) > -1) {
                            notice.id++
                        }
                        this.notices.push(notice)
                        this.fire(notice.id)
                    },
                    fire(id) {
                        this.visible.push(this.notices.find(notice => notice.id === id))
                        const timeShown = 2500 * this.visible.length
                        setTimeout(() => {
                            this.remove(id)
                        }, timeShown)
                    },
                    remove(id) {
                        // const index = this.visible.findIndex(notice => notice.id === id)
                        // this.visible.splice(index, 1)

                        if (this.visible.length) {
                            this.visible.splice(0, 1)
                        }
                    },
                }))
            })
        </script>
    @endpush
@endonce
