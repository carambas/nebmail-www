<div id="tooltip" :class="{'tourprec': Number.isInteger(tourPrec)}" role="tooltip" x-data="tooltip">
    <div class="tooltip-content">
        <div id="tooltip_content_cr">
            <template x-for="(crTour, index) in crtxt">
                <div class="tooltip-cr-element">
                    <template x-if="Number.isInteger(tourPrec)">
                        <pre><code><span id="tooltip_content_info_tour_precedent">### Information datant du tour <span x-text="tourPrec"></span></span></code></pre>
                    </template>
                    <template x-if="nomMonde">
                        <pre><code><span id="tooltip_content_nom_monde" x-text="nomMonde"></span></code></pre>
                    </template>
                    <div class="pr-4">
                        <template x-if="typeof crTour.tour === 'number' && !Number.isInteger(tourPrec)" >
                            <pre><code class="cr" x-text="`### Information datant du tour ${crTour.tour}\n`"></code></pre>
                        </template>
                        <pre><code class="cr" x-text="crTour.cr"></code></pre>
                    </div>
                    <div class="tooltip-cr-overlay">
                        <div>
                            <div>
                                <iconify-icon icon="ic:outline-content-copy" class="md-16 copy-tour" title="Copier le CR du tour" @click="tooltip.onCopyClick(index, $event)"></iconify-icon>
                            </div>
                            <template x-if="typeof crTour.tour === 'number'">
                                <div>
                                    <iconify-icon icon="ic:outline-file-copy" class="md-16 copy-all" title="Copier tous les CR" @click="tooltip.onCopyAllClick($event)"></iconify-icon>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </template>
        </div>
        <template x-if="ordres.length > 0">
            <div>
                <div id="tooltip_separateur" class="flex-row d-flex">
                    <span class="w-100 tooltip-line"></span>
                    <iconify-icon id="tooltip_fleche" icon="ic:arrow-forward" class="md-12 tooltip-fleche" @click="tooltip.flecheClick(numMonde)"></iconify-icon>
                </div>
                <div id="tooltip_content_ordres">
                    <template x-for="ordre in ordres">
                        <pre><code><span id="tooltip_content_cr" :class="{ 'syntaxe-ko': !ordre.syntaxe }" x-text="ordre.ordre"></span></code></pre>
                    </template>
                </div>
            </div>
        </template>
    </div>
    <div id="arrow" data-popper-arrow></div>
</div>

@once
    @push('page-js-script')
        <script>
            document.addEventListener('alpine:init', () => {
                Alpine.data('tooltip', () => ({
                    numMonde: 0,
                    tourPrec: false,
                    historique: false,
                    nomMonde: '',
                    crtxt: [],
                    ordres: [],
                    init() {
                        this.tooltip = new Tooltip()
                        document.addEventListener('tooltip-show', event => this.show(event))
                        document.addEventListener('tooltip-hide', event => this.hide(event))
                    },
                    show(e) {
                        this.numMonde = e.detail.numMonde
                        this.tourPrec = e.detail.tourPrec
                        this.crtxt = e.detail.crtxt ?? []
                        this.ordres = e.detail.ordres ?? []
                        this.nomMonde = e.detail.nomMonde
                        this.historique = e.detail.historique
                        this.tooltip.show(e.detail)
                    },
                    hide(e) {
                        this.tooltip.hide()
                    }
                }))
            })

        </script>
    @endpush
@endonce
