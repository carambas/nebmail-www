<x-layouts.app>
    <div class="container-fluid">
            @if (isset($boutonRetour) && $boutonRetour === true)
                <div class="row justify-content-center">
                    <div class="col">
                        <a href="javascript:history.back()" class="btn btn-primary mb-5">Retour ordres</a>
                    </div>
                </div>
            @endif
            @If(isset($error))
                <div class="row justify-content-center">
                    <div class="col">
                        <div class="alert alert-danger">{{ $error }}</div>
                    </div>
                </div>
            @Endif
            <pre style="font-size: 100%">{{ $cr ?? '' }}</pre>
            @if (isset($boutonRetour) && $boutonRetour === true)
                <div class="row justify-content-center">
                    <div class="col">
                        <a href="javascript:history.back()" class="btn btn-primary mt-5">Retour ordres</a>
                    </div>
                </div>
            @endif
    </div>
</x-layouts.app>
