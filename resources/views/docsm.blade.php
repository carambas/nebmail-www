<x-layouts.app>
    <div class="container">
    <h1>Serveur de mail V3</h1>


<h2>Vue d'ensemble du serveur</h2>
        <p>Ce serveur de mail va permettre aux joueurs des différentes parties de Nébula de discuter entre eux de façon anonyme et automatique par Email... Cela évitera notamment que les anciens joueurs se reconnaissent tout de suite et ainsi défavoriser les nouveaux joueurs face aux "barons".</p>
        <p>Le serveur de mail est également le lien entre l'arbitre et les joueurs. Vous pouvez lui envoyer des messages, vos ordres et c'est toujours le serveur de mail qui vous envoie votre compte-rendu à chaque tour.</p>
        <p>De plus le serveur vous permet d'envoyer des messages anonyme à une personne ou à tout le monde (Rumeur). Il envoie aussi automatiquement une copie de votre message à l'arbitre et vous permet de contacter un autre joueur simplement grâce à son numéro de joueur ou son pseudo (donc même s'il n'a pas mis un petit mot pour se présenter vous pourrez le joindre aisément).</p>
        <p>Une autre fonctionnalité du serveur est de pouvoir certifier l'arrivée de vos ordres. Ceux ci sont renvoyés vers l'arbitre et une copie locale est gardée au cas où ce dernier ne les ai pas reçus (il pourra les récupérer plus tard). Vous même recevrez un accusé de réception quand vos ordres sont stockés.</p>
        <p>Vous pouvez transmettre un fichier attaché avec vos messages au serveur de mail.</p>
        <p>L'adresse Email du serveur vous sera fournie par l'arbitre lors du lancement de chaque partie.</p>
        <p>Le serveur de mail possède par avance le pseudo et le numéro de joueur correspondant à chaque joueur. Pour le moment, un seul Email est lié à un joueur. Chacun d'entre vous possède un mot de passe, afin de pouvoir certifier l'identité de l'envoyeur et d'éviter
            les confusions quand 2 personnes ont la même adresse Email (boulot, famille, etc.).</p>

        <h2>Utilisation du serveur de mail</h2>
        <p>Vocabulaire : si on vous parle du "SM" il s'agit du Serveur de Mail.</p>
        <p>Un mail au serveur se compose de 2 parties, le sujet et le corps.</p>
        <p>Le sujet va contenir la commande, l'identification ainsi qu'éventuellement la liste des destinataires.
            Le corps contient le message ou bien une liste de commande (une par ligne). Ce qui se trouve dans le corps du mail doit systématiquement être entouré par DEBUT et FIN placés en début de ligne. Cela évite, par exemple, d'envoyer par erreur une signature non voulue à un autre joueur.</p>
        <p>Vous pouvez envoyer 4 types de messages au serveur :</p>

    <ul>
        <li>Envoyer un message</li>
        <li>Envoyer ses Ordres</li>
        <li>Changement de sa configuration</li>
        <li>Récupérer des informations</li>
    </ul>

        <h3>Message</h3>
        <p>Vous êtes le joueur J dans la partie PARTIE. Votre mot de passe est MotDePasse.</p>
        <pre class="text-white bg-secondary p-2"><code>Message PARTIE J MotDePasse {A|M} <{Num|Pseudo|All}[,...]></code></pre>
        <p>Vous pouvez spécifier optionnellement les fonctions particulières d'envoi A (votre message est anonyme) ou M (votre message ira à plusieurs destinataires. Cette liste n'est pas cachée et toute réponse à ce message ira à l'ensemble des destinataires originaux du message ainsi que son auteur).</p>
        <p>La liste des destinataires se trouvent entre < et >. Chaque joueur peut être présenté par son numéro de joueur (conseillé) ou par son pseudo (respectez les accents et les majuscules). La liste des destinataires est séparée par des virgules. Notez <All> qui représente l'intégralité des joueurs de la partie.</All>
        <p>Note : il est possible d'avoir des pièces-jointes dans le message. Cela pose quelques problèmes car certains logiciels de mails placent automatiquement une signature en attachement ou bien une carte de visite virtuelle qui pourrait identifier l'auteur d'un message. Pour ces raisons le serveur de mail filtre certains types d'attachement. Ne passeront pas les fichiers HTML attachés et certains types e fichiers texte..</p>
        <p class="mb-1"><strong>Exemple 1 :</strong> message du joueur 4 aux joueurs 2 et 3.</p>
        <pre class="text-white bg-secondary p-2"><code>Message KARNAJ 4 toto M <2,3>

DEBUT
On va gagner !
FIN</code></pre>
        <p>Si 3 répond à ce message la réponse sera transmise aux joueurs 2 et 4.</p>

        <p class="mb-1"><strong>Exemple 2 :</strong> Message anonyme du joueur 4 à tous les joueurs (on appelle ça une rumeur)</p>
        <pre class="text-white bg-secondary p-2"><code>Message KARNAJ 4 toto A &lt;All&gt;

DEBUT
C'est moi le plus fort
FIN</code></pre>

        <h3>OrdresTour</h3>
        <p>Vous êtes le joueur J dans la partie PARTIE. Votre mot de passe est MotDePasse. Vous envoyez vos ordres pour le tour Tour (tour de votre dernier CR+1).</p>
        <pre class="text-white bg-secondary p-2 mb-1"><code>OrdresTour PARTIE J MotDePasse Tour ("Message A Envoyer lors de l'AR")</code></pre>
        <p>Stocke les vos ordres contenus dans le corps du message, encadrés par DEBUT et FIN sur le serveur de mail.</p>
        <p>Vous recevrez un accusé de réception listant les ordres stockés et vous rappelant le message que vous avez passé dans le sujet. Ce message à saisir entre guillemets est optionnel. Il vous permettra de différencier les différents accusés de réceptions que vous recevrez si vous envoyez vos ordres plusieurs fois.</p>
        <p>Le SM va vérifier si le tour pour lequel vous envoyez vos ordres n'a pas déjà été évalué. Si c'est le cas vos ordres seront refusés. C'est une sécurité pour le cas où vos ordres arriveraient avec du retard, ça arrive parfois qu'un mail se perde en route et qu'il réapparaisse un jour plus tard.</p>
        <p>Note : Si vous envoyez vos ordres plusieurs fois, seule la dernière version de vos ordres reçue par le serveur de mail est stockée. Les anciens ordres sont oubliés. Prenez garde à bien envoyer l'intégralité de vos ordres à chaque fois.</p>
        <p>Faites attention aussi lorsque vous envoyez plusieurs version de vos ordres deux coups de suite il se peut que la deuxième version des ordres arrive au serveur de mail avant la première (ce sont les joies d'Internet). Dans ce cas c'est la première version de vos ordres qui sera stockée et la deuxième perdue. Surveillez bien vos accusés de réception.</p>

        <p><strong>Exemple :</strong></p>
        <pre class="text-white bg-secondary p-2"><code>ORDRESTOUR KARNAJ 4 toto 5

DEBUT
###############################################
# Ordres du joueur "Lucifer":4 pour le tour 5 #
# Ordres saisis avec Nebutil version 3.03     #
###############################################


J : 3 ; surprise !!!
F 5 * M
FIN</code></pre>

        <h3>Configuration</h3>
        <pre class="text-white bg-secondary p-2 mb-1"><code>Config PARTIE J MotDePasse</code></pre>
        <p>Permet de régler sa configuration et ses préférences pour la PARTIE.<br>
        Le corps du message contient une suite de commandes encadrées par DEBUT et FIN.</p>

        <p>Les commandes existantes sont les suivantes :</p>
        <pre class="text-white bg-secondary p-2 mb-1"><code>PASSWORD MonNouveauPassword</code></pre>
        <p>Vous permet de changer de mot de passe.</p>

        <pre class="text-white bg-secondary p-2 mb-1"><code>MSGGEN &lt;Y|N&gt;</code></pre>
        <p>Cette option permet de spécifier si vous voulez recevoir les messages généraux. Cette option est activée par défaut.</p>

        <pre class="text-white bg-secondary p-2 mb-1"><code>COPIE &lt;Y|N&gt;</code></pre>
        <p>Cette option permet de spécifier si vous voulez recevoir une copie de tous les mails que vous envoyez. Cette option est désactivée par défaut.</p>

        <pre class="text-white bg-secondary p-2 mb-1"><code>RUMEUR &lt;Y|N&gt;</code></pre>
        <p>Cette option permet de spécifier si vous voulez recevoir les rumeurs. Cette option est activée par défaut.</p>

        <pre class="text-white bg-secondary p-2 mb-1"><code>EMAIL MonNouvelEmail</code></pre>
        <p>Cette option vous permet de changer votre Email. Vous recevrez automatiquement un mail à cette nouvelle adresse pour confirmation. Vous devrez renvoyer ce mail afin d'activer votre changement. Vous pouvez spécifier plusieurs adresses Email d'un coup en les séparant par des virgules.</p>

        <pre class="text-white bg-secondary p-2 mb-1"><code>ADDEMAIL UnEmailDePlus</code></pre>
        <p>Cette option vous permet d'ajouter une nouvelle adresse email. De la même manière que EMAIL, vous devrez renvoyer le mail de confirmation.</p>

        <p"><strong class="text-danger">Attention :</strong><br>
        Lorsque l'option EMAIL ou ADDEMAIL est spécifiée il ne peut y avoir qu'une seule commande par mail. N'envoyez pas d'autre commande EMAIL ou ADDEMAIL avant d'avoir répondu à la confirmation.</p>
        <p>Exemples :</p>
        <pre class="text-white bg-secondary p-2"><code>Config KARNAJ 4 toto

DEBUT
RUMEURS Y
MSGGEN N
COPIE N
PASSWORD titi
FIN</code></pre>

        <pre class="text-white bg-secondary p-2"><code>Config KARNAJ 4 titi

DEBUT
EMAIL titi@tata.com
FIN</code></pre>

        <pre class="text-white bg-secondary p-2"><code>Config KARNAJ 4 titi

DEBUT
ADDEMAIL titi@club-internet.fr
FIN</code></pre>

        <p><strong>Note :</strong> un mail CONFIG sans commande dans le corps du message (DEBUT et FIN sans rien entre les deux) vous permet de récupérer des information sur votre configuration courante.</p>

        <p>D'autres commandes devraient être ajoutées comme par exemple une commande permettant de supprimer une adresse.</p>


<h3>Informations</h3>

        <pre class="text-white bg-secondary p-2 mb-1"><code>Info PARTIE J MotDePasse</code></pre>

        <p>Permet de récupérer différentes informations concernant la partie. Les différentes options sont à mettre dans le corps du message entre DEBUT et FIN en début de ligne. On peut mettre autant d'options que voulu dans un seul mail.</p>

        <p><strong>Les options sont :</strong></p>

        <pre class="text-white bg-secondary p-2 mb-1"><code>NBORDRES</code></pre>
        <p>renvoie le nombre d'ordres stocké sur le serveur pour le tour courant.</p>

        <pre class="text-white bg-secondary p-2 mb-1"><code>ORDRES</code></pre>
        <p>renvoie les ordres stockés sur le serveur.</p>

        <pre class="text-white bg-secondary p-2 mb-1"><code>CR</code></pre>
        <p>renvoie le CR du tour courant.</p>

        <p><strong>Exemple :</strong></p>

        <pre class="text-white bg-secondary p-2"><code>Info KARNAJ 4 toto

DEBUT
CR
NBORDRES
ORDRES
FIN</code></pre>
    </div>
</x-layouts.app>
