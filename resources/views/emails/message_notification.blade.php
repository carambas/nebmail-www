@component('mail::message')

@if (! $broadcast)
# {!! $message->thread->subject !!}
@endif


@if ($broadcast)
**message envoyé à tous les joueurs**
@elseif($message->recipients->count() > 1)
**message envoyé à plusieurs destinataires :** {{ $dest_str }}
@endif

@if (!$anonymous)
{!! $message->user->pseudoWithNumJou !!} a envoyé le message suivant :
@else
Une rumeur court en ce moment.
@endif

<pre style="white-space: pre-wrap;"><code>{{ $message->body }} </code></pre>

@if ($bouton_repondre)
@component('mail::button', ['url' => route('messages.show', $message->thread_id), 'color' => 'primary'])
    Répondre
@endcomponent
@endif

@endcomponent
