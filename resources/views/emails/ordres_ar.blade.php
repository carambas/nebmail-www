@component('mail::message')

@if (!$cc_arbitre)
# Accusé de réception de vos ordres
@else
# Ordres du joueur {{{$pseudo}}}:{{{$numjou}}}
@endif

@component('mail::table')
|        |           |
|--------|-----------|
|Partie  |**{{$partie}}**|
|Joueur  |**{{$pseudo}}:{{$numjou}}**|
|Date    |**{{ \Carbon\Carbon::now()->format('d/m/Y H:i')  }}**       |
@endcomponent

@if (count(Arr::flatten($ordres_checked['erreurs'])) > 0)
@component('mail::panel')
@if (count(Arr::flatten($ordres_checked['erreurs'])) == 1)
**L'ordre suivant a provoqué une erreur lors de la simulation d'évaluation :**
@elseif((count(Arr::flatten($ordres_checked['erreurs'])) > 1))
**Les ordres suivants ont provoqué une erreur lors de la simulation d'évaluation :**
@endif

@foreach ($ordres_checked['erreurs'] as $erreur_msg => $ordres_erreur)
@foreach ($ordres_erreur as $ordre_erreur)
<span style="color: red;">{{$ordre_erreur}} -> {{$erreur_msg}}</span><br>
@endforeach
@endforeach
@endcomponent
@endif

Ordres reçus :

```
{!! $ordres !!}
```

nombre d'ordres reçus : {{$ordres_checked['nbordres']}}


@endcomponent
