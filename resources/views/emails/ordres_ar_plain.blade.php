@if (!$cc_arbitre)
Accusé de réception de vos ordres
@else
Ordres du joueur {{{$pseudo}}}:{{{$numjou}}}
@endif
---------------------------------

{{{$partie}}}
{{{$pseudo}}}:{{{$numjou}}}
{{{ Carbon\Carbon::now()->format('d/m/Y H:i')  }}}
---
@if (count(Arr::flatten($ordres_checked['erreurs'])) > 0)
@if (count(Arr::flatten($ordres_checked['erreurs'])) == 1)
L'ordre suivant a provoqué une erreur lors de la simulation d'évaluation :
@elseif((count(Arr::flatten($ordres_checked['erreurs'])) > 1))
Les ordres suivants ont provoqué une erreur lors de la simulation d'évaluation :
@endif

@foreach ($ordres_checked['erreurs'] as $erreur_msg => $ordres_erreur)
@foreach ($ordres_erreur as $ordre_erreur)
{{{$ordre_erreur}}} -> {{{$erreur_msg}}}
@endforeach
@endforeach
@endif

{{{$ordres}}}

nombre d'ordres reçus : {{{$ordres_checked['nbordres']}}}

