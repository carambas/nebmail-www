<x-layouts.app>
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-3">
                <div class="card-body">
                    Bienvenue sur le site du serveur de mail de Nébula. Vous trouverez ici plein d'informations sur
                    son usage ainsi que des outils qui vous permettront d'éviter d'utiliser les emails dans pas
                    mal de cas et vous simplifieront votre vie de joueur.
                </div>
            </div>
            @auth
                <div class="card mb-3">
                    <div class="card-header">Partie {{ Auth::user()->partie_name }}</div>

                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            @foreach(config('nebmail.menu') as $key => $valeur)
                                @if ((!Auth::user()->isArbitre()) || (Auth::user()->isArbitre() && $valeur[4]))
                                <li class="list-group-item">
                                    <a href="{{ route($valeur[1], $valeur[2]) }}">
                                        <span class="{{ $valeur[3] ? '' : 'text-muted' }}">
                                            <iconify-icon icon="{{ $valeur[0] }}" inline="true" class="md-18 mr-1"></iconify-icon>&nbsp;{{ $key }}
                                        </span>
                                    </a>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endauth
                <div class="card mb -3">
                    <div class="card-header">Statistiques</div>

                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><a href="{{ url('stats/ordresrendus') }}">Ordres rendus</a>
                                @auth <livewire:ordres-rendus-partie :partieName="Auth::user()->partie_name" :nbOrdres="$nbOrdres"/> @endauth
                                @if ($equipeAvecOrdresCount->count() > 0)
                                    <table class="table table-sm table-borderless">
                                        <thead>
                                            <tr><th colspan=2>Dans mon équipe</th></tr>
                                        </thead>
                                        @foreach ($equipeAvecOrdresCount as $coequipier)
                                        <tr>
                                            <td>{{ $coequipier->pseudo }}</td>
                                            <td>@if($coequipier->ordre_count > 0)<span class="text-success">ordres rendus</span> @else <span class="text-warning">ordres non rendus</span> @endif</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                @endif
                            </li>
                            <li class="list-group-item"><a href="{{ url('stats/rumeurs') }}">Rumeurs et messages généraux</a></li>
                            <li class="list-group-item"><a href="{{ url('stats/messages') }}">Nombre de messages échangés</a></li>
                        </ul>
                    </div>
                </div>
        </div>
    </div>
</div>
</x-layouts.app>
