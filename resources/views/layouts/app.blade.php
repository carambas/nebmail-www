<x-layouts.base>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-primary shadow-sm">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto valign-center">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            Serveur de mail
                        </a>
                        <li class="nav-item dropdown">
                            <a id="navbarStatDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Statistiques <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarStatDropdown">
                                <a class="dropdown-item" href="{{ url('stats/ordresrendus') }}">
                                    Ordres rendus
                                </a>
                                <a class="dropdown-item" href="{{ url('stats/rumeurs') }}">
                                    Rumeurs et messages généraux
                                </a>
                                <a class="dropdown-item" href="{{ url('stats/messages') }}">
                                    Messages échangés
                                </a>
                            </div>
                        </li>
                        @auth
                            <li class="nav-item dropdown">

                                <a id="navbarPartieDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Interaction SM <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarPartieDropdown">
                                    @foreach(config('nebmail.menu') as $key => $valeur)
                                        @if ((!Auth::user()->isArbitre()) || (Auth::user()->isArbitre() && $valeur[4]))
                                            <a class="dropdown-item {{ $valeur[3] ? '' : 'text-muted' }} valign-center"
                                               href="{{ route($valeur[1], $valeur[2]) }}">
                                                <iconify-icon icon="{{ $valeur[0] }}" inline="true" class="md-18"></iconify-icon>&nbsp;{{ $key }}
                                            </a>

                                        @endif
                                    @endforeach
                                </div>
                            </li>
                            @if(Auth::user()->isArbitre())
                                <li class="nav-item {{ (Route::currentRouteName() == 'arbitre.dashboard') ? 'active' : ''}}">
                                    <a class="nav-link" href="{{ route('arbitre.dashboard') }}">Arbitre <span class="sr-only">(current)</span></a>
                                </li>
                            @Endif
                            {{-- Messages --}}
                            <li class="nav-item {{ (Route::currentRouteName() == 'messages' || Route::currentRouteName() == 'messages.show') ? 'active' : ''}}">
                                <a class="nav-link" href="{{route('messages')}}">
                                    <div class="valign-center">
                                </iconify-icon>
                                        <iconify-icon icon="ic:mail-outline" inline="true" class="mr-1 md-24"></iconify-icon>
                                        <livewire:messages-count/>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('messages.create')}}">
                                    <button type="button" class="btn btn-outline-light btn-sm d-none d-lg-block {{ (Route::currentRouteName() == 'messages.create') ? 'active' : ''}}">
                                        <div class="valign-center">
                                            <iconify-icon icon="ic:edit" inline="true" class="mr-1 md-24"></iconify-icon>Nouveau message
                                        </div>
                                    </button>
                                    <div class="valign-center d-lg-none">
                                        <iconify-icon icon="ic:edit" inline="true" class="mr-1 md-24"></iconify-icon>Message
                                    </div>
                                </a>
                            </li>
                            @if(request()->routeIs('nebutil*'))
                            <li>
                                <input type="file" id="file_input" class="d-none" accept=".nbt,.nba">
                                <button type="button" class="btn btn-outline-light btn-sm d-none d-lg-block" id="file_input_button">
                                    <div class="valign-center">
                                        <iconify-icon icon="ic:file-upload" inline="true" class="mr-1 md-24"></iconify-icon>NBT
                                    </div>

                                </button>
                            </li>
                            @endif
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Connexion</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarLoginDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->partie_name }} {{ Auth::user()->pseudo }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarLoginDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                        Déconnexion
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <x-notification />

        <main class="py-4">
            {{ $slot }}
        </main>
    </div>
</x-layouts.base>
