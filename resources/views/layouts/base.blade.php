<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Nebmail</title>
    <link rel="icon" href="/favicon.svg">

    <!-- Scripts -->
    <script>
        var echo_key = '{{ config('broadcasting.connections.pusher.key') }}';
        var echo_cluster = '{{ config('broadcasting.connections.pusher.options.cluster') }}';
        var userId = '{{ optional(Auth::user())->id }}';
    </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    @vite(['resources/js/app.js'])
    @stack('page-css-files')

    @livewireStyles
    @stack('page-styles')
</head>
<body>
{{ $slot }}


@stack('page-js-files')
@livewireScripts
</body>
@stack('page-js-script')

</html>

