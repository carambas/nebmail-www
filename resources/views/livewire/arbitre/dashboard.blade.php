<div class="container">
    <div class="row align-items-stretch">
        <div class="col-md-3">
            <x-card class="mb-3">
                <x-slot name="body">
                    Partie {{ $partie->name }} tour {{ $partie->tour }}
                    @isset($fichier_nba_dutour)
                    : <a href="{{ route('crnba') }}">{{ basename($fichier_nba_dutour) }}</a>
                    <a href="{{ route('plan') }}">Plan</a>
                    <a href="{{ route('nebutil') }}">Nebutil</a>
                    @endisset
                    @if(!$partie->diplomatie)<br><span class="text-warning">sans diplomatie</span>@endif
                    <livewire:arbitre.notifications-config/>

                </x-slot>
            </x-card>
        </div>
        <div class="col-md-5">
            <x-card class="mb-3">
                <x-slot name="body">
                    <span>Date limite pour le tour {{ $partie->tour + 1 }} :&nbsp;</span>
                    <span @if(($partie->deadline) < now())class="text-danger"@endif>
                        <a role="button" data-toggle="modal" data-target="#deadline" style="text-decoration: underline dotted;">
                        {{ $deadline_date ? Carbon\Carbon::parse($deadline_date)->format('d/m/Y') : 'Non définie'}}
                        {{ $deadline_heure }}
                        </a>
                    </span>
                </x-slot>
            </x-card>
        </div>
        @isset($partie->derniere_relance)
        <div class="col-md-4">
            <x-card class="mb-3">
                <x-slot name="body">
                    Dernière relance : {{ $partie->derniere_relance->format('d/m/Y H:i') }}
                </x-slot>
            </x-card>
        </div>
        @endisset
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="mb-3 card">
                <div class="card-header d-flex align-items-center">
                    <span>Liste des joueurs</span>
                    <div class="ml-auto d-flex">
                        <span>
                            @if($partie->joueurs_sans_arbitre->where('password', null)->count() > 0)
                            <button
                                wire:click="genererMdp"
                                class="btn btn-sm btn-primary"
                                wire:loading.attr="disabled"
                                >
                                Générer les mots de passe et les envoyer
                            </button>
                            @endif
                        </span>
                        <span class="ml-1">
                            @if($partie->tour === 0)
                            <button
                                class="btn btn-sm btn-primary"
                                data-toggle="modal"
                                data-target="#import-nba-modal">
                                Importer fichier .nba
                            </button>
                            @endif
                        </span>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <table class="table mt-3">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Pseudo</th>
                                    @if ($partie_par_equipe)<th class="text-center">Eq#</th>@endif
                                    <th>Email</th>
                                    <th class="text-center">Ordres rendus</th>
                                    @if ($partie->tour === 0)<th class="text-center">Mot de passe</th>@endif
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($partie->joueurs_sans_arbitre->with('ordre')->get() as $jou)
                                <tr wire:key="row-{{ $loop->index }}">
                                    <td class="align-middle">{{ $jou->numjou }}</td>
                                    <td class="align-middle">{{ $jou->pseudo }}</td>
                                    @if ($partie_par_equipe)<td class="text-center">{{ $jou->equipe ?? '❌' }}</td>@endif
                                    <td class="align-middle">
                                        @foreach($jou->email as $email)
                                            <div>{{ $email }}</div>
                                        @endforeach
                                    </td>
                                    <!-- ordres rendus / générer des ordres vides -->
                                    <td class="text-center align-middle">
                                        @if($jou->ordre)
                                        <x-badge wire:click="afficheOrdres('{{$jou->id}}')" type="success" variant="pill" role="button" data-toggle="modal" data-target="#joueur-ordres">
                                            {{ $jou->ordre->updated_at->diffForHumans() }}
                                        </x-badge>
                                        @else
                                        <a
                                        role="button"
                                        href="{{ route('ordres', ['user' =>  $jou->id]) }}"
                                        style="text-decoration: underline dotted;"
                                        >
                                        Saisir des ordres
                                        </a>
                                        @endif
                                    </td>

                                    @if ($partie->tour === 0)
                                    <!-- mot de passe -->
                                    <td class="text-center align-middle">
                                        @if(isset($jou->password))
                                        <div class="d-flex align-items-center justify-content-center"><iconify-icon icon="ic:outline-check-circle" class="md-18 text-success"></iconify-icon><span class="ml-1">OK</span></div>
                                        @else
                                        <div class="d-flex align-items-center justify-content-center"><iconify-icon icon="ic:outline-error-outline" inline="true" class="md-18 text-warning"></iconify-icon><span class="ml-1">Vide</span></div>
                                        @endif
                                    </td>
                                    @endif

                                    <!-- actions -->
                                    <td class="text-center align-middle">
                                        <button wire:click="details('{{$jou->id}}')" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#joueur-info" title="Détails"><iconify-icon icon="ci:search" class="md-16 valign-center"></iconify-icon></button>
                                        @if($partie->tour == 0)<button x-data x-on:click="if (confirm('Etes-vous certain de supprimer {{addslashes($jou->pseudo)}} ?')) $wire.delete('{{$jou->id}}')" class="ml-1 btn btn-sm btn-danger"  title="Supprimer"><iconify-icon icon="ic:outline-delete" class="md-16 valign-center"></iconify-icon></button>@endif
                                        <a href="{{ route("messages.create", ['dest' => $jou->numjou]) }}" class="ml-1 btn btn-sm btn-primary" title="Message"><iconify-icon icon="ic:outline-mail" class="md-16 valign-center"></iconify-icon></a>
                                        @isset($fichier_nba_dutour)<a href="{{ route("crtexte", ['user' => $jou->id]) }}" class="ml-1 btn btn-sm btn-primary" title="Voir CR"><iconify-icon icon="ic:description" class="md-16 valign-center"></iconify-icon></a>@endisset
                                        @isset($fichier_nba_dutour)<a href="{{ route("arbitre.nebutil", ['user' => $jou->id]) }}" class="ml-1 btn btn-sm btn-primary" title="Nebutil light"><iconify-icon icon="ci:dashboard" class="md-16 valign-center"></iconify-icon></a>@endisset
                                        @isset($fichier_nba_dutour)<button wire:click="renvoiCR('{{$jou->id}}')" class="ml-1 btn btn-sm btn-primary" title="renvoyer le CR par mail"><iconify-icon icon="ic:baseline-send" class="md-16 valign-center"></iconify-icon></button>@endisset
                                    </td>
                                </tr>
                                @empty
                                <tr><td colspan="5" class="text-center">Pas de joueurs</td></tr>
                                @endforelse

                            </tbody>

                        </table>
                    </div>

                </div>

        </div>
        <div class="row">
            <div class="col">
            </div>
        </div>
    </div>

        <!-- modal upload nba file -->
        <form wire:submit="uploadNba">
            <x-modal
                id="import-nba-modal"
                title="Importer les joueurs d'un fichier .nba"
                :centered="true"
                wire:ignore.self
                x-data=""
                x-ref="foo"
                x-on:close-upload-modal.window="$('#import-nba-modal').modal('hide');"
                >
                <x-slot name="body">
                    <x-alert type="danger">Cette action va remplacer la liste des joueurs actuelle. Il ne sera pas possible de revenir en arrière</x-alert>
                    <x-upload
                        type="file"
                        name="fichier_nba"
                        class="nebmail-file-input"
                        :label="[
                            'text' => 'Fichier à importer',
                        ]"
                        placeholder="{{ $label_upload }}"
                        wire:model.live="fichier_nba"
                        />
                </x-slot>
                <x-slot name="footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button class="btn btn-primary" @if(!$boutonImportActif)disabled @endif wire:submit>Importer</button>
                </x-slot>
            </x-modal>
        </form>

        <!-- modal joueur info -->
        <x-modal
            id="joueur-info"
            title="Information joueur"
            :centered="true"
            wire:ignore.self
            >
            <x-slot name="body">
                <table class="table table-sm table-borderless">
                    <tbody>
                        <tr><td>Prénom</td><td>{{ optional($user)->first_name }}</td></tr>
                        <tr><td>Nom de famille</td><td>{{ optional($user)->last_name }}</td></tr>
                        <tr><td>Numéro de joueur</td><td>{{ optional($user)->numjou }}</td></tr>
                        <tr><td>Pseudo</td><td>{{ optional($user)->pseudo }}</td></tr>
                        <tr><td>Mot de passe</td><td>{{ optional($user)->password_plain }}</td></tr>
                        <tr><td>Email 1</td><td>{{ $user?->email[0] }}</td></tr>
                        @if(isset($user?->email[1]))<tr><td>Email 2</td><td>{{ $user?->email[1] }}</td></tr>@endif
                        <tr><td>Code SM (obsolète)</td><td>{{ optional($user)->codesm }}</td></tr>
                        <tr><td>Copie des messages</td><td>{{ optional($user)->cc_str }}</td></tr>
                        <tr><td>Rumeurs</td><td>{{ optional($user)->rumeurs_str }}</td></tr>
                        <tr><td>Messages généraux</td><td>{{ optional($user)->msg_gen_str }}</td></tr>
                    </tbody>
                </table>
            </x-slot>
            <x-slot name="footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Quitter</button>
            </x-slot>
        </x-modal>

        <!-- modal ordres joueur -->
        <x-modal
            id="joueur-ordres"
            title="Ordres stockés"
            :centered="true"
            :scrollable="true"
            wire:ignore.self
            size="lg"
            >
            <x-slot name="body">
            <code><pre>{{ $ordres }}</pre></code>
            </x-slot>
            <x-slot name="footer">
                <div x-data="{ showOrdresButton: @entangle('showOrdresButton'), userId: @entangle('ordresIdJou').live }">
                    <a role="button" class="btn btn-primary" href="{{ route('ordres', ['user' => $ordresIdJou])}}">Modifier</a>
                    <a x-on:click="$wire.simulOrdres(userId)" x-show="!showOrdresButton" role="button" class="btn btn-primary">Simuler</a>
                    <a x-on:click="$wire.afficheOrdres(userId)" x-show="showOrdresButton" role="button" class="btn btn-primary">Ordres</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Quitter</button>
                </div>
            </x-slot>
        </x-modal>

        <!-- modal deadline -->
        <form wire:submit="deadline">
            <x-modal
                x-data=""
                id="deadline"
                title="Date limite pour le tour {{ $partie->tour + 1 }}"
                :centered="false"
                :scrollable="false"
                wire:ignore.self
                x-on:close-deadline.window="$('#deadline').modal('hide');"
                >
                <x-slot name="body">
                    <x-input name="deadline_date" type="date" wire:model.blur='deadline_date'></x-input>
                    <x-input name="deadline_heure" type="time" wire:model.blur='deadline_heure'></x-input>
                </x-slot>
                <x-slot name="footer">
                    <button wire:submit class="btn btn-primary">Sauver</button>
                </x-slot>
            </x-modal>
        </form>
</div>
