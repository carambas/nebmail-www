<div>
    <a role="button" data-toggle="modal" data-target="#notifications" style="text-decoration: underline dotted;">Configurer notifications</a>
    <x-modal
        x-data=""
        id="notifications"
        title="Test de notification"
        :centered="false"
        :scrollable="false"
        wire:ignore.self
        >
        <form wire:submit="notificationsFormSubmit">
            <x-slot name="body">
                <x-input name="webhook_ordres_rendus" type="url" wire:model.blur='webhook_ordres_rendus' :label="['text' => 'Webhook ordres rendus (URL Slack ou vide pour pas de notification)']"/>
                <x-input name="webhook_bot" type="url" wire:model.blur='webhook_bot' :label="['text' => 'Webhook bot (URL Discord)']"/>
                <x-input type="text" wire:model.blur='testTexte' :label="['text' => 'Texte de test (optionnel, sert aux actions de test)']"/>
                <button wire:click="testBot" class="btn btn-primary">Test Bot</button>
                <button wire:click="testNotificationMessage" class="btn btn-primary">Test notification ordres rendus</button>
            </x-slot>
            <x-slot name="footer">
                <button wire:click="notificationsFormSubmit" class="btn btn-success" data-dismiss="modal">Enregistrer</button>
                <button class="btn btn-warning" data-dismiss="modal">Annuler</button>
            </x-slot>
        </form>
    </x-modal>
</div>

@push('page-js-script')
<script type="module">
    document.addEventListener('livewire:initialized', function () {
        $('#notifications').on('hidden.bs.modal', function (e) {
            @this.notificationReset()
        })
    })
</script>
@endpush
