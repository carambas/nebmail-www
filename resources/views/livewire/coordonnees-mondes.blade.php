<x-modal
    id="coordonnees-modal"
    title="Coordonnées des mondes"
    :centered="true"
    :scrollable="false"
    size="lg"
    wire:ignore.self
    >
    <form wire:submit="save">
        @csrf
        <x-slot name="body">
            <p>Saisissez l'ensemble des coordonnées de vos mondes, 1 monde par ligne, sous la forme <code>m=x,y</code>. Où m est le numéro du monde concerné et x,y ses coordonnées. Exemple :</p>
            <p><code>5=10,15<br>25=11,3</code></p>
            <p>
                Ces coordonnées vont remplacer celles incluses dans le fichier .nbt stocké sur le serveur.
                Il faut rentrer dans ce champ de saisie l'intégralité des coordonnées que vous voulez voir remplacer,
                y compris celles que vous auriez déjà saisi au prélable.
            </p>
            <div class="form-group">
                <label for="message-text" class="col-form-label">Coordonnées :</label>
                <textarea rows="10" class="form-control" wire:model="coordonneesTexte"></textarea>
                @error('coordonneesTexte') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
        </x-slot>
        <x-slot name="footer">
            <button type="button" class="btn btn-success" wire:click="save" id="coordonnees-save-button">Envoyer</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Annuler</button>
        </x-slot>
</form>
</x-modal>

@push('page-js-script')
<script type="module">
    window.addEventListener('coordonnes-updated', event => {
        $("#coordonnees-modal").modal('hide')
    })

    document.addEventListener('disable-save-button', event => {
        document.querySelector("#coordonnees-save-button").disabled = false;
    })

    document.addEventListener('enable-save-button', event => {
        document.querySelector("#coordonnees-save-button").disabled = true;
    })
</script>
@endpush
