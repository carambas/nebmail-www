<div id="nebutil-cr" wire:ignore>
    <form>
        <div class="form-group">
            <textarea id="crtexte" rows="20" class="form-control CodeMirror">{{ $crtxt ?? '' }}</textarea>
        </div>
    </form>
</div>

@push('page-js-files')
@vite(['resources/js/ordres/codemirror.js'])
@endpush

@push('page-js-script')
<script type="module">
    let textAreaCr = document.getElementById('crtexte');
    if (textAreaCr != null) {
        var myCodeMirrorCr = CodeMirror.fromTextArea(textAreaCr, {
            lineNumbers: false,
            mode:  null,
            dragDrop: false,
            autofocus: true,
        });
    }
    else {
        alert('myTextarea est null');
    }

    window.addEventListener('dessine-plan', event => {
        if (myCodeMirrorCr) {
            const divcr = document.querySelector('#nebutil-cr')

            if (event.detail.simul) {
                divcr.classList.add('cr-simul')
            } else {
                divcr.classList.remove('cr-simul')
            }

            if (event.detail.ancientour) {
                divcr.classList.add('cr-tour-prec')
            } else {
                divcr.classList.remove('cr-tour-prec')
            }

            myCodeMirrorCr.doc.setValue(event.detail.crtxt);
        }
    })

    window.addEventListener('selection-monde', event => {
        if (!event.detail.from === 'cr') {
            return;
        }

        const num_monde = event.detail.monde
        let doc = myCodeMirrorCr.doc

        let num_ligne = getNumLigneFromNumMonde(num_monde)
        if (num_ligne) {
            if (--num_ligne < 0) {
                num_ligne = 0
            }

            doc.setCursor(doc.lineCount() - 1, 0, {scroll: true})
            doc.setCursor(num_ligne, 0, {scroll: true})
        }

    })

    function getNumLigneFromNumMonde(num_monde) {
        let doc = myCodeMirrorCr.doc
        let regexp = new RegExp('^Md?[#_]?' + num_monde + '\\s', '');
        for (let num_ligne = 0; num_ligne < doc.lineCount(); num_ligne++) {
            let ligne = doc.getLine(num_ligne)
            if (ligne.match(regexp)) {
                return num_ligne
            }
        }

        return null
    }



</script>
@endpush
