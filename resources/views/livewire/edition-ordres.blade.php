<div x-data="ordres" class="pt-2">
    <form wire:ignore>
        <div class="form-group">
            <textarea class="form-control CodeMirror" rows="20" spellcheck="false"
                name="ordres" id="ordres"></textarea>
        </div>
    </form>
    <div class="d-flex">
        <button id="envoi_button" name="envoi" x-on:click="envoi()" class="mr-2 btn btn-sm btn-primary">Envoyer mes
            ordres</button>
        <button id="brouillon_button" name="brouillon" x-on:click="brouillon()" class="mr-2 btn btn-sm btn-primary">Enregistrer
            le brouillon</button>
        <button id="simul_button" name="simul" x-on:click="simul()" class="mr-2 btn btn-sm btn-primary">Simulation
            d'évaluation</button>
        <button id="sortie_simul_button" x-cloak x-show="simul_eval" name="sortie_simul" x-on:click="sortieSimul()"
            class="mx-2 btn btn-sm btn-success">Revenir au CR d'origine</button>
        <button x-text="statuts[ordres_statut]"
            x-bind:class="{
                'btn-success': ordres_statut == 'sent',
                'btn-warning': ordres_statut == 'draft',
                'btn-danger': ordres_statut == 'changed'
            }"
            disabled
            id="ordres_statut"
            class="ml-auto btn btn-sm"></button>
    </div>
</div>

@push('page-js-script')
    @vite(['resources/js/ordres/ordres.mjs'])
    <script type="module">
        document.addEventListener('alpine:init', () => {
            Alpine.data('ordres', () => ({
                simul_eval: false,
                ordres_statut: "{{ $this->brouillon ? 'draft' : 'sent'}}",
                original_ordres_statut: '',
                statuts: {
                    sent: 'Envoyé',
                    draft: 'Brouillon',
                    changed: 'Non enregistré'
                },
                editionOrdres: null,

                init() {
                    Ordres.alpineInit(this, @this)
                },

                envoi() {
                    Ordres.envoi(this, @this)
                },

                brouillon() {
                    Ordres.brouillon(this, @this)
                },

                simul() {
                    Ordres.simul(this, @this)
                },

                sortieSimul() {
                    Ordres.sortieSimul(this)
                }
            }))
        })
    </script>
@endpush
