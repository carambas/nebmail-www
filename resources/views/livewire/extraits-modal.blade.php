<x-modal
    id="extraits-modal"
    title="Extraits de CR"
    :centered="true"
    :scrollable="false"
    size="xl"
    wire:ignore.self
    >
        <x-slot name="body">
            <x-notification/>
            <div wire:ignore>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a id="import-masse-tab" class="nav-link active" data-toggle="tab" href="#multi">Import en masse</a>
                    </li>
                    <li class="nav-item" role="presentation" id="crud-tab">
                    <a class="nav-link" data-toggle="tab" href="#crud">Liste des extraits stockés</a>
                    </li>
                </ul>
                <div class="tab-content mt-2" style="height: 75vh; overflow-y: auto;" wire:key="tab-content">
                    <div class="tab-pane fade show active h-100" id="multi" role="tabpanel" wire:key="multi">
                        {{-- Div  permettant d'occuper toute la hauteur et de coller les boutons tout en bas --}}
                        <div class="h-100 d-flex flex-column align-items-end">
                            <div>
                                <p>Les extraits de CR sont des fragments de CR fournis par d'autres joueurs qui vont compléter ce qui s'affiche sur votre plan et CR du tour. <span class="text-danger">Attention, les fragments ne sont pas pris en compte par le serveur pour la simulation d'évaluation.</span></p>
                                <p>Saisissez des extrait de CR d'un ou plusieurs mondes. Ils seront attribuées au tour courant à l'exception des mondes précédés par <code>### Information datant du tour n</code> qui seront attribués au tour indiqué.</p>
                                <p>S'il y a plusieurs mondes, assurez-vous qu'il y ait une ligne vide entre chaque extrait.</p>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Mondes :</label>
                                    <textarea id="extraits-input" rows="10" class="form-control text-monospace" wire:model.live.debounce.200ms="extraitsRaw"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Mondes trouvés dans les extraits</label>
                                    <input type="text" disabled class="form-control" wire:model.live="mondesTrouves"></textarea>
                                </div>
                            </div>
                            <div class="mt-auto">
                                <button type="button" class="btn btn-success" wire:click="sendExtraits">Envoyer</button>
                                <button type="button" class="btn btn-warning" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="crud" role="tabpanel" wire:key="crud">
                        <div x-data="extraits()" x-init="listen()">
                            <template x-if="extraits.length === 0">
                                <p>Pas d'extrait stocké</p>
                            </template>

                            <template x-for="tour in Object.keys(extraitsByTour).sort((a, b) => parseInt(b) - parseInt(a))" :key="tour">
                                <div class="card bg-secondary my-2">
                                    <div class="card-header bg-primary">Tour <span x-text="tour"></span></div>
                                    <div class="card-body">
                                        <template x-for="extrait in extraitsByTour[tour]" :key="'T' + tour + 'M' + extrait.numero">
                                            <div class="d-flex flex-row my-2">
                                                <div class="mr-3">
                                                    <iconify-icon icon="ic:edit" class="md-18 mr-1 menu_btn"
                                                        :data-extrait-tour="extrait.tour"
                                                        :data-extrait-numero="extrait.numero"
                                                        @click="onEditClick">
                                                    </iconify-icon>
                                                    <iconify-icon icon="ic:clear" class="md-18 mr-1 text-danger menu_btn"
                                                        :data-extrait-tour="extrait.tour"
                                                        :data-extrait-numero="extrait.numero"
                                                        @click="onDeleteClick">
                                                    </iconify-icon>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <pre x-text="extrait.content"
                                                        :class="{ 'bg-danger': extrait.erreur }"
                                                        class="p-1 mb-0" style="color: black; background: white; border-radius: 3px;">
                                                    </pre>
                                                </div>
                                            </div>
                                        </template>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </x-slot>
</x-modal>

@pushOnce('page-js-script')
<script>
    window.addEventListener('fermer-extraits-modal', event => {
        $("#extraits-modal").modal('hide')
    })

    var groupBy = function(xs = [], key) {
        return xs.reduce(function(rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    };

    function extraits() {
        return {
            extraits: [],
            extraitsByTour: [],

            listen() {
                window.addEventListener('dessine-plan', event => {
                    this.extraits = event.detail.extraits
                    if (this.extraits === undefined) {
                        this.extraits = []
                    }
                    this.extraitsByTour = this.getExtraitsByTour()
                    })
            },

            getExtraitsByTour() {
                return groupBy(this.extraits, 'tour')
            },

            onEditClick(e) {
                const tour = parseInt(e.target.getAttribute('data-extrait-tour'))
                const numero = parseInt(e.target.getAttribute('data-extrait-numero'))

                const extrait = this.extraits.find(elt => elt.tour === tour && elt.numero === numero)
                console.log(extrait)

                // Mise à jour du champ de saisie
                const extraitsInput = document.querySelector('#extraits-input')
                const extraitsRaw = '### Information datant du tour ' + tour + '\n' + extrait.content
                extraitsInput.value = extraitsRaw
                extraitsInput.dispatchEvent(new Event('input'));
                $('#import-masse-tab').tab('show')
            },

            onDeleteClick(e) {
                const tour = parseInt(e.target.getAttribute('data-extrait-tour'))
                const numero = parseInt(e.target.getAttribute('data-extrait-numero'))

                if (confirm(`Voulez vous supprimer l'extrait du monde ${numero} pour le tour ${tour} ?`)) {
                    @this.deleteExtrait(tour, numero)
                }
            },
        }
    }



</script>
@endPushOnce
