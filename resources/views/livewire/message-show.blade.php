<div class="container">
    <div class="col">
        <h3 class="mb-3">{{ $thread->subject }}</h3>

        <div>
        @if ($thread->participants_count > 2)
            <div class="alert alert-warning">Discussion à plusieurs avec {{ $thread->participantsString($user->id) }}</div>
        @endif
        </div>

        @foreach($thread->messages as $message)
        @php
            $class = $message->user->id != $user->id ? 'ml-5 bg-info' : 'mr-5'
        @endphp
        <div class="card mb-2 {{ $class }}" @if($loop->last) id="latest" @endif>
            <div class="card-header">
                @if (!$thread->anonymous)De : {{ $message->user->pseudoWithNumJou }}@else Une rumeur court en ce moment @endif <small>{{ $message->created_at->diffForHumans() }}</small>
                @if($loop->count > 1 && $loop->first) <a href="#latest"><iconify-icon icon="ic:vertical-align-bottom" class='mr-1 md-24 float-right' title='Aller en bas de la page'></iconify-icon></a> @endif
                @if($loop->count > 1 && $loop->last) <a href="#app"><iconify-icon icon="ic:vertical-align-top" class='mr-1 md-24 float-right' title='Aller en haut de la page'></iconify-icon></a> @endif
            </div>
            <div class="card-body">
                <pre style="white-space: pre-wrap; font-size: 90%;">{{ $message->body }}</pre>
            </div>
        </div>
        @endforeach


        <h3 class="mt-3">Répondre à la discussion</h3>
        <form method="post" action="{{ route('messages.update', [$thread->id]) }}">
            @csrf
            @method('PUT')

            <x-textarea
            name="message"
            required
            rows="15"
            class='text-monospace'
            :label="[
                'text' => 'Votre message',
            ]"
            />

            <button type="submit" class="btn btn-primary">Envoyer le message</button>
        </form>

    </div>
</div>
