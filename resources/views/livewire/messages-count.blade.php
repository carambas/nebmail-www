<span class="messages-count">
    @if ($this->count > 0)
    <span class="badge badge-pill badge-danger">{{ $this->count }}</span>
    @endif
</span>
