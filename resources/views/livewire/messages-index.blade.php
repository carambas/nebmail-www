<div>
    @if ($threads->count() > 0)
    <div x-data x-init="init_popover()" x-on:refresh-icons.window="init_popover()" @if($showDeleted) class="card" @endif>
        @if($showDeleted)
        <div class="card-header">Messages archivés</div>
        @endif
        @foreach($threads as $thread)
        @php
            $isUnread = $thread->isUnread($user->id);
            $bgclass = $isUnread ? 'bg-secondary' : '';
            $colclass = $isUnread ? 'msg-unread' : 'msg-read';
            $col_body = 'rgba(255,255,255,0.50)';
            $participants = $thread->participantsString($user->id);
            $ignoreNonRepondu = $thread->participants->where('user_id', $this->user->id)->first()?->ignore_non_repondu ?? false;
            $isNonRepondu = !$ignoreNonRepondu && !$thread->broadcast && !$isUnread && $thread->lastMessage->user_id !== $user->id;
        @endphp
            <div class="d-flex flex-row row-clickable {{ $bgclass }} pt-1 pb-1"
                @if(!$showDeleted)x-on:click="window.location='{{ route('messages.show', $thread->id)}}#latest'"@endif
                wire:key="{{$thread->id}}"
                >
                <div class="d-flex flex-column flex-md-row ellipsis w-100">
                    <div class="col-md-2 msg-subject {{ $colclass }} ellipsis" data-toggle="tooltip" data-placement="bottom" title="{{ $participants }}" >
                        {{ $participants }}
                    </div>
                    <div class="col-md-10 d-flex flex-md-row flex-column">
                        <div class="msg-subject {{ $colclass }}" style="white-space: nowrap">
                            @if ($isNonRepondu)
                            <span class="badge badge-danger badge-non-repondu" style="font-size:0.5rem;"
                                x-on:click="confirm('Supprimer le libellé non répondu pour ce message ?') || event.stopImmediatePropagation()"
                                wire:click.stop="removeNonLu({{$thread->id}})"
                                title='Supprimer la bulle non lu' data-toggle="tooltip">
                                Non répondu
                            </span>
                            @endif
                            <span>{{ $thread->subject }}</span>
                        </div>
                        <div class="msg-body {{ $colclass }} ellipsis" style="min-width:0"><span class="d-none d-md-inline-block">&nbsp;-&nbsp;</span>{{ Str::limit($thread->lastMessage->body, 150) }}</div>
                        <div class="msg-body {{ $colclass }} ml-auto d-md-block d-none" style="white-space: nowrap">
                            <span>{{ $thread->updated_at_human }}</span>
                            <span
                                x-data
                                id="archive-{{$thread->id}}"
                                data-toggle="tooltip"
                                data-placement="right"
                                title="{{!$showDeleted ? 'Archiver' : 'Replacer dans la boîte de reception'}}"
                                wire:key="archive-{{$thread->id}}"
                                wire:ignore
                                >
                                <iconify-icon
                                    wire:click.stop="{{!$showDeleted ? 'delete' : 'restore'}}({{$thread->id}})"
                                    x-on:click="$('#archive-{{$thread->id}}').tooltip('hide')"
                                    icon="ic:{{!$showDeleted ? 'archive' : 'unarchive'}}"
                                    inline="true"
                                    class="msg-subject msg-read md-18"
                                >
                                </iconify-icon>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="msg-body {{ $colclass }} ml-auto d-block d-md-none text-right" >
                    <span>{{ $thread->updated_at_human }}</span>
                    <span
                        x-data
                        id="archive-md-{{$thread->id}}"
                        data-toggle="tooltip"
                        data-placement="right"
                        title="{{!$showDeleted ? 'Archiver' : 'Replacer dans la boîte de reception'}}"
                        wire:key="archive-md-{{$thread->id}}"
                        wire:ignore
                    >
                        <iconify-icon
                            wire:click.stop="{{!$showDeleted ? 'delete' : 'restore'}}({{$thread->id}})"
                            x-on:click="$('#archive-{{$thread->id}}').tooltip('hide')"
                            icon="ic:{{!$showDeleted ? 'archive' : 'unarchive'}}"
                            inline="true"
                            class="msg-subject msg-read md-18"
                        >
                        </iconify-icon>
                    </span>
                </div>
            </div>
        @endforeach
    </div>
    @else
        <div>
            Aucun message.
        </div>
    @endif

    <div class="mt-3">
    {{ $threads->links() }}
    </div>
    <div class="mt-3">
        <button class="btn btn-primary btn-sm" wire:click="toggleDeleted">
            @if(!$showDeleted)
            Voir les messages archivés
            @else
            Revenir aux messages non archivés
            @endif
        </button>
    </div>
</div>

@push('page-js-script')
<script>
    function init_popover() {
        $('[data-toggle="tooltip"]').tooltip();
    }
</script>
@endpush


