<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Ordres rendus</div>
                <div class="card-body ">
                    <table class="table table-borderless table-sm">
                        <thead>
                            <tr>
                                <th>Partie</th>
                                <th class="text-center">Ordres</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ordres as $partie => $nbOrdres)
                                <tr>
                                    <td>{{ $partie }}</td>
                                    <td class="text-center">{{ $nbOrdres }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
