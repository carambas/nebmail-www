<div id="plan-redim">
    <x-tooltip />
    <div id="plan" wire:ignore>
        <svg id="svg_plan" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
            <defs>
                <pattern id="diagonal-stripe-4" patternUnits="userSpaceOnUse" width="10" height="10">
                    <svg xmlns='http://www.w3.org/2000/svg' width='10' height='10'>
                        <rect width='10' height='10' fill='black'/>
                        <path d='M-1,1 l2,-2
                                 M0,10 l10,-10
                                 M9,11 l2,-2' stroke='green' stroke-width='3'/>
                    </svg>
                </pattern>
                <symbol id="visibility_observe">
                    <svg class="explo" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="10" height="10" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                        <path d="M12 4.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zM12 17c-2.76 0-5-2.24-5-5s2.24-5 5-5s5 2.24 5 5s-2.24 5-5 5zm0-8c-1.66 0-3 1.34-3 3s1.34 3 3 3s3-1.34 3-3s-1.34-3-3-3z"/>
                    </svg>
                </symbol>
                <symbol id="visibility_sur_place">
                    <svg class="explo" xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" style="vertical-align: -0.125em;" width="10" height="10" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5a2.5 2.5 0 0 1 0-5a2.5 2.5 0 0 1 0 5z"/>
                    </svg>
                </symbol>
            </defs>
            <g id='svg_plan_group'>
            </g>
        </svg>
        <svg id="poi" viewBox="0 0 12 12" width="12" height="12" xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <circle cx="6" cy="6" r="6" fill="red" stroke="none"/>
        </svg>
    </div>
    <div id="plan-button-bar" class="d-flex md-24" style="position: absolute; left: 0; top: 0; width: 100%">
        <iconify-icon id="planFullscreenBtn" icon="ic:fullscreen" class="mr-1 menu_btn"></iconify-icon>
        <iconify-icon id="planModeBtn" icon="ic:looks-one" class="mr-1 menu_btn"></iconify-icon>
        <iconify-icon id="planObsBtn" icon="ic:visibility-off" class="mr-1 menu_btn"></iconify-icon>
        <iconify-icon id="planPilBtn" icon="ic:money-off" class="mr-1 menu_btn"></iconify-icon>
        <iconify-icon icon="ic:zoom-in" class="mr-1 menu_btn" onclick="document.dispatchEvent(new CustomEvent('PlanZoom', {detail: {mode: 'in' }}))"></iconify-icon>
        <iconify-icon icon="ic:zoom-out" class="mr-1 menu_btn" onclick="document.dispatchEvent(new CustomEvent('PlanZoom', {detail: {mode: 'out' }}))"></iconify-icon>
        <iconify-icon id="tour-down-button" icon="ic:arrow-downward" direction="down" class="d-none mr-1 menu_btn"></iconify-icon>
        <iconify-icon id="tour-up-button" icon="ic:arrow-upward" direction="up" class="d-none mr-1 menu_btn"></iconify-icon>
        <i class="ml-auto"></i>
        <iconify-icon id="parsing-cr-warning" icon="ic:warning" class="mr-1 menu_btn text-warning d-none" style="white-space: pre-wrap" title="Erreurs dans le parsing des cr :"></iconify-icon>
        <iconify-icon id="lock-button" icon="ic:outline-lock" class="mr-1 menu_btn" title="Empêche de modifier l'emplacement des mondes"></iconify-icon>
        <iconify-icon id="save-button" icon="ic:outline-save" class="mr-1 menu_btn_disabled" title="Empêche de modifier l'emplacement des mondes"></iconify-icon>
        <iconify-icon icon="ic:settings" class="mr-1 menu_btn" data-toggle="dropdown" title="Paramètres"></iconify-icon>
        <iconify-icon icon="ic:outline-help-outline" class="menu_btn" data-toggle="modal" data-target="#help-modal"></iconify-icon>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item" href="#coordonnees-modal" data-toggle="modal">Coordonnées des mondes</a>
            <div class="dropdown-divider"></div>
            <button class="dropdown-item" onclick="emptyStorage()">Rafraichir en vidant le cache</button>
            <a class="dropdown-item" href="#extraits-modal" data-toggle="modal">Extraits de CR</a>
            <a class="dropdown-item" href="#calculatrice-modal" data-toggle="modal">Calculatrice</a>
            <a class="dropdown-item" href="#relations-modal" data-toggle="modal">Relations avec les autres joueurs</a>
        </div>
    </div>

    <!-- modal doc shortcuts -->
    <x-modal
        id="help-modal"
        title="Interactions avec le plan et ordres"
        :centered="true"
        :scrollable="false"
        size="xl"
        >
        <x-slot name="body">
            <h5>Clavier</h5>
            <p>
                <kbd>Ctrl</kbd> + <kbd>M</kbd> : Recherche d'un monde<br>
                <kbd>Ctrl</kbd> + <kbd>L</kbd> : Recherche d'une flotte<br>
                <kbd>Ctrl</kbd> + <kbd>R</kbd> : Recherche d'un trésor<br>
                <kbd>Ctrl</kbd> + <kbd>+</kbd>, <kbd>Ctrl</kbd> + <kbd>&gt;</kbd> : Zoom avant sur le plan<br>
                <kbd>Ctrl</kbd> + <kbd>-</kbd>, <kbd>Ctrl</kbd> + <kbd>&lt;</kbd> : Zoom arrière sur le plan<br>
                <kbd>Ctrl</kbd> + <kbd>flèche</kbd> : Faire défiler le plan<br>
            </p>
            <h5>Souris</h5>
            <p>
                <kbd>Clic sur un monde</kbd> : affichage du CR du tour ainsi que des ordres saisis<br>
                <kbd>Clic sur la flèche</kbd> dans le CR d'un monde : positionne la saisie des ordres au niveau du monde en question<br>
                <kbd>Clic bouton du milieu sur un monde</kbd> : positionne la saisie des ordres au niveau du monde en question<br>
                <kbd>Clic-droit sur un monde</kbd> : menu contextuel proposant des actions liées au monde.<br>
            </p>
            <h5>Fenêtre des ordres</h5>
            <p>
                <kbd>Clic-droit</kbd> : menu déroulant permettant de saisir un numéro de joueur dans la liste des pseudos connus
            </p>
        </x-slot>
    </x-modal>

    <livewire:coordonnees-mondes/>
    <livewire:extraits-modal/>
    <x-calculatrice/>
    <livewire:relations-custom/>
</div>

@push('page-js-files')
@vite(['resources/js/plan/plan.mjs'])
@endpush
@push('page-js-script')
<script type="module">
    document.addEventListener('livewire:initialized', function () {
        const svg = document.getElementById('svg_plan')
        const plan = new Plan(svg, @this.nomPartie, @this.userId, @this.numJoueur, @this.tour, @this.crApiUrl, @this.apiToken, @this.customCoord)
        const btnFull=plan.buildButton(document.getElementById('planFullscreenBtn'),'PlanFullscreen',['ic:fullscreen-exit','ic:fullscreen'])
        const btnMode=plan.buildButton(document.getElementById('planModeBtn'),'PlanMode',['ic:baseline-view-compact-alt','ic:looks-one'])
        const btnObs=plan.buildButton(document.getElementById('planObsBtn'),'PlanObs',['ic:visibility','ic:visibility-off'])
        const btnPil=plan.buildButton(document.getElementById('planPilBtn'),'PlanPil',['ic:baseline-attach-money','ic:money-off'])
    })
</script>

<script>
    function emptyStorage() {
        document.dispatchEvent(new Event('empty-local-storage'))
    }

</script>
@endpush
