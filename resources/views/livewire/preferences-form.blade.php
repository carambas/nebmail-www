<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-3">
                <div class="card-header">Configuration</div>
                <div class="card-body">
                    <div>
                        <form wire:submit="save">
                            @csrf
                            <fieldset>
                                <legend>Préférences</legend>
                                <div class="row mb-3">
                                    <div class="col-sm-10 offset-sm-2">
                                        <div class="form-check">
                                            <input type="checkbox" wire:model="user.msg_gen" id="inp-msg_gen"
                                                   class="form-check-input">
                                            <label for="inp-msg_gen" class="form-check-label">Recevoir les messages généraux</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" wire:model="user.rumeurs" id="inp-rumeurs"
                                                   class="form-check-input">
                                            <label for="inp-rumeurs" class="form-check-label">Recevoir les rumeurs</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="checkbox" wire:model="user.cc" id="inp-cc" class="form-check-input">
                                            <label for="inp-cc" class="form-check-label">Recevoir une copie des messages que vous
                                                envoyez
                                                (indispensable si vous jouez à une partie débutant)</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inp-password" class="col-sm-2 col-form-label">Mot de passe</label>
                                    <div class="col-sm-10">
                                        <input type="text" wire:model.blur="password" id="inp-password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               aria-describedby="help-inp-password">
                                        @error('password')
                                        <small><span class="text-danger">{{ $message }}</span></small>
                                        @enderror
                                        <small id="help-inp-password" class="form-text text-muted">Minimum 8 caractères</small>
                                    </div>
                                </div>
                                @for ($i = 0; $i <= 1 ; $i++)
                                    <div class="form-group row">
                                        <label for="inp-emails[]" class="col-sm-2 col-form-label">Email {{ $i + 1 }}</label>
                                        <div class="col-sm-10">
                                            <input type="text" wire:model.blur="user.email.{{ $i }}" id="inp-emails[]"
                                                   class="form-control @error("user.email.$i") is-invalid @enderror">
                                            @error("user.email.$i")<small><span
                                                    class="text-danger">{{ $message }}</span></small>@enderror
                                        </div>
                                    </div>
                                @endfor
                            </fieldset>
                            <button type="submit" class="btn btn-primary">Envoyer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


