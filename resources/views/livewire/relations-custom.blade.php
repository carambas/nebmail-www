<x-modal
    id="relations-modal"
    title="relations avec les autres joueurs"
    :centered="true"
    :scrollable="false"
    size="xl"
    wire:ignore.self
    x-data="relations"
    x-init="listen()"
    >
        <x-slot name="body">
            <x-notification/>
            <div wire:ignore>
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th colspan="5"></th>
                            <th colspan="4" class="text-center">Le joueur vous a déclaré</th>
                            <th colspan="1"></th>
                        </tr>
                        <tr>
                            <th>Pseudo</th>
                            <th>Classe</th>
                            <th title="Niveau de défiance (entre 0 et 10)">Défiance</th>
                            <th>ATT</th>
                            <th>DEF</th>
                            <th class="text-center">Allié</th>
                            <th title="Chargeur" class="text-center">Ch</th>
                            <th title="Chargeur de population" class="text-center">ChP</th>
                            <th title="Déchargeur de population" class="text-center">DechP</th>
                            <th>Couleur</th>
                            {{-- <th></th> --}}
                        <tr>
                    </thead>
                        <template x-for="joueur in Object.values(joueurs).filter(j => j.numJou != numJou)" :key="joueur.numJou">
                            <tbody>
                                <tr>
                                    <td x-text="joueur.pseudo + ':' + joueur.numJou"></td>
                                    <td>
                                        <select id="classe" x-model.number="joueur.classe" class="form-control" class="col">
                                            <option value="0">???</option>
                                            <option value="1">Empereur</option>
                                            <option value="2">Marchand</option>
                                            <option value="3">Pirate</option>
                                            <option value="4">Antiquaire</option>
                                            <option value="5">Robotron</option>
                                            <option value="6">Missionnaire</option>
                                            <option value="7">Explorateur</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" x-model.number="joueur.confiance" class="form-control" min="0" max="10" step="1" size="2">
                                    </td>
                                    <td><input type="number" x-model.number="joueur.att" class="form-control" min="0" max="12" step="1" size="2"></td>
                                    <td><input type="number" x-model.number="joueur.def" class="form-control" min="0" max="12" step="1" size="2" ></td>
                                    <td class="text-center">
                                        <div class="form-check">
                                            <input type="checkbox" x-model.number="joueur.declAllie" class="form-check-input position-static">
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check">
                                            <input type="checkbox" x-model.number="joueur.declChargeur" class="form-check-input position-static">
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check">
                                            <input type="checkbox" x-model.number="joueur.declChargeurPop" class="form-check-input position-static">
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check">
                                            <input type="checkbox" x-model.number="joueur.declDechargeurPop" class="form-check-input position-static">
                                        </div>
                                    </td>
                                    <td>
                                        <input type="color" x-model="joueur.couleur">
                                    </td>
                                <tr>
                            </tbody>
                        </template>
                </table>
                <p class="small"><strong>Défiance : </strong> Indique le niveau de confiance que vous avez vis-à vis du joueur indiqué.
                    Plus le nombre est bas, plus vous avez confiance.<br>
                    Entre 1 et 2, correspond à un joueur allié, 3 correspond à une relation neutre (défaut), 4 et plus correspond à un ennemi.<br>
                    Cette information est utile pour les ordres de déplacement avec la flèche afin de trouver le chemin le plus sûr. Elle est également utilisée pour afficher les forces en présence sur le plan.</p>
            </div>
        </x-slot>
        <x-slot name="footer">
            <div>
                <input class="d-none" id="couleur-joueur" type="color" @change="colorInputChanged">
                <button type="button" class="btn btn-success" data-dismiss="modal" @click="modalResult = 'valider'">Valider</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal" @click="modalResult = 'annuler'">Annuler</button>
            </div>
        </x-slot>
</x-modal>

@pushOnce('page-js-script')
<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('relations', () => {
            let init = false
            let oldJoueurs = []
            const colorInput =  document.querySelector('#couleur-joueur')

            return {
                joueurs: [],
                proprioMondeClicked: 0,
                modalResult: '',
                numJou: 0,

                listen() {
                    document.addEventListener('relations-updated', event => {
                        oldJoueurs = _.cloneDeep(event.detail.joueurs)
                        this.joueurs = _.cloneDeep(event.detail.joueurs)
                    })

                    document.addEventListener('changer-couleur', this.onChangerCouleur.bind(this))
                    document.addEventListener('plan-data-set', this.onPlanDataSet.bind(this))
                    $('#relations-modal').on('hidden.bs.modal', this.onModalHidden.bind(this))
                },

                valider() {
                    if (! _.isEqual(oldJoueurs, this.joueurs)) {
                        // Persistance des données
                        @this.updateRelations(this.joueurs)
                        this.joueurs.forEach((joueur) => {
                            if (oldJoueurs.couleur != joueur.couleur) {
                                this.updateCouleur(joueur.numJou, joueur.couleur)
                            }
                        })
                        oldJoueurs = _.cloneDeep(this.joueurs)
                    }
                },

                annuler() {
                    this.joueurs = _.cloneDeep(oldJoueurs)
                },

                colorInputChanged(event) {
                    const couleur = event.target.value
                    if (this.proprioMondeClicked > 0) {
                        this.updateCouleur(this.proprioMondeClicked, couleur)
                    }
                },

                /**
                 * @param {number} numJou
                 */
                getFillMondeJoueurStyles(numJou) {
                    var styleBackground
                    var styleText

                    for (var styleSheet of document.styleSheets) {
                        const myrules = styleSheet.cssRules

                        for (var rule of myrules) {
                            if (rule.selectorText?.toLowerCase() === `.jou_${numJou}`){
                                styleBackground = rule
                            }

                            if (rule.selectorText?.toLowerCase() === `text.jou_${numJou}`){
                                styleText = rule
                            }

                            if (styleBackground && styleText) return { styleBackground, styleText }
                        }
                    }

                    return null
                },

                /**
                 * @param {Object} joueur
                 */
                getJoueurCouleur(joueur) {
                    var couleurStr
                    couleurStr = joueur.couleur

                    return Color(couleurStr ?? this.getFillMondeJoueurStyles(joueur.numJou).styleBackground.style.fill).hex()
                },

                setFillMondeJoueurStyle({ styleBackground, styleText }, color) {
                    if (!styleBackground) return

                    styleBackground.style.fill = color
                    styleText.style.fill = this.getColorFromBackground(styleBackground.style.fill)
                },

                getColorFromBackground(color) {
                    return Color(color).isLight() ? 'black' : 'white'
                },

                getJoueurData(joueur, relationsCustom) {
                    return {
                        pseudo: joueur.pseudo,
                        numJou: joueur.numJou,
                        couleur: relationsCustom?.couleur ?? this.getJoueurCouleur(joueur),
                        classe: relationsCustom?.classe ?? 0,
                        confiance: relationsCustom?.confiance ?? 3,
                        att: relationsCustom?.att ?? 0,
                        def: relationsCustom?.def ?? 0,
                        declAllie: relationsCustom?.declAllie ?? false,
                        declChargeur: relationsCustom?.declChargeur ?? false,
                        declChargeurPop: relationsCustom?.declChargeurPop ?? false,
                        declDechargeurPop: relationsCustom?.declDechargeurPop ?? false,
                    }
                },

                onChangerCouleur(event) {
                    this.proprioMondeClicked = plan.mondes[event.detail.numMonde].joueur_couleur
                    if (this.proprioMondeClicked > 0) {
                        const colorHex = this.proprioMondeClicked ? this.getJoueurCouleur(this.joueurs[this.proprioMondeClicked - 1]) : "#000000"
                        colorInput.value = colorHex
                        colorInput.click()
                    }
                },

                onPlanDataSet() {
                    if (init) return
                    init = true

                    const joueurs = Array.from(window.plan.joueurs.values()).filter(j => j).map(joueur => {
                        return {
                            pseudo: joueur.pseudo,
                            numJou: joueur.numJou
                        }
                    })
                    const relationsCustom = @js($relationsCustom)

                    this.numJou = @js($numJou)

                    for (index in joueurs) {
                        const numJou = joueurs[index].numJou
                        joueurs[index] = this.getJoueurData(joueurs[index], relationsCustom[numJou])
                        this.setFillMondeJoueurStyle(this.getFillMondeJoueurStyles(numJou), joueurs[index].couleur)
                    }

                    document.dispatchEvent(new CustomEvent('relations-updated', {
                        detail: { joueurs }
                    }))
                },

                onModalHidden() {
                    if (this.modalResult === 'valider') {
                        this.valider()
                    } else {
                        this.annuler()
                    }
                    this.modalResult = ''
                },

                updateCouleur(numJou, couleur) {
                    this.setFillMondeJoueurStyle(this.getFillMondeJoueurStyles(numJou), couleur)
                        this.joueurs[numJou - 1].couleur = couleur
                        window.plan.joueurs[numJou].couleur = couleur
                        @this.updateCouleur(numJou, couleur)
                        oldJoueurs[numJou - 1] = _.cloneDeep(this.joueurs[numJou - 1])
                }

            }
        })
    })
</script>
@endpushOnce
