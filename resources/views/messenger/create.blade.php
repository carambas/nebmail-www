<x-layouts.app>
    <div class="container">
        <h2>{{ __("messages.envoi $typeMessage") }}</h2>

        <form method="post" action="{{route('messages.store')}}">
            @csrf
            <input type="hidden" name="type_message" value="{{$typeMessage}}">

            <div class="row" x-data="{nb_checked: 0, type_message: '{{$typeMessage}}'}" x-init="nb_checked = countChecked()">
                @if ($typeMessage == 'message')
                    <div class="col-md-4 d-flex align-content-stretch">
                        <div class="card w-100">
                            <div class="card-header">
                                Destinataires
                                @if (Gate::allows('msg_general'))
                                <x-link class="btn btn-primary btn-sm" href="{{ route('messages.create', ['typeMessage' => 'mg']) }}" text="Message général"/>
                                @endif
                                @if (Gate::allows('msg_rumeur'))
                                <x-link class="btn btn-primary btn-sm" href="{{ route('messages.create', ['typeMessage' => 'rumeur']) }}" text="Rumeur"/>
                                @endif
                            </div>
                            <div class="card-body">
                                @error('recipients')
                                <x-alert type='danger'>{{ $message }}</x-alert>
                                @enderror
                                @error('recipients.*')
                                <x-alert type='danger'>{{ $message }}</x-alert>
                                @enderror
                                @foreach($users as $user)
                                @if ($dest === $user->numjou)
                                <x-checkbox
                                    name="recipients[{{$user->id}}]"
                                    :label="['text' => $user->pseudoWithNumJou]"
                                    :group="['style' => 'margin-bottom: 0rem']"
                                    checked
                                    class="dest"
                                    x-on:click="nb_checked = countChecked()"
                                    />
                                @else
                                <x-checkbox
                                    name="recipients[{{$user->id}}]"
                                    :label="['text' => $user->pseudoWithNumJou]"
                                    :group="['style' => 'margin-bottom: 0rem']"
                                    class="dest"
                                    x-on:click="nb_checked = countChecked()"
                                    />
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col">
                    <div class="">
                        @if ($typeMessage == 'message')
                            <x-input
                                name="subject"
                                type="text"
                                required
                                :label="[
                                    'text' => 'Objet',
                                ]"
                            />
                        @else
                            <a href="{{ route('messages.create', ['typeMessage' => 'message']) }}" class="btn btn-primary">Message classique</a>
                            @if (($typeMessage != 'rumeur') && Gate::allows('msg_rumeur'))
                                <x-link href="{{ route('messages.create', ['typeMessage' => 'rumeur']) }}" class="btn btn-primary" text="Rumeur"/>
                            @endif
                            @if (($typeMessage != 'mg') && Gate::allows('msg_general'))
                                <x-link href="{{ route('messages.create', ['typeMessage' => 'mg']) }}" class="btn btn-info" text="Message général"/>
                            @endif
                        @endif

                        @if ($typeMessage == 'rumeur' && !$diplomatie)<div class="alert alert-danger mt-3">Partie sans diplo, ne pas fournir d'information sur son déroulement</div>@endif
                        <x-textarea
                            name="message"
                            required
                            rows="20"
                            class='text-monospace'
                            :label="[
                                'text' => 'Votre message',
                            ]"
                        />

                        <button type="submit" class="btn btn-primary" x-bind:disabled="(type_message == 'message') && (nb_checked == 0)">Envoyer le message</button>
                    </div>
                </div>
            </div>

        </form>
    </div>

    @push('page-js-script')
    <script>
        function countChecked() {
            return document.querySelectorAll('.dest:checked').length
        }
    </script>
    @endpush
</x-layouts.app>

