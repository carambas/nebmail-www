<x-layouts.app>
    <div class="container-fluid p-0 position-absolute" style="top:82px;bottom:0;left:0;right:0; box-sizing: border-box;">
        <div id="nebutil-plan" class="" style="height: 100%; width:49%; float:left; overflow-y:auto; box-sizing: border-box;">
            <livewire:plan :userId='$userId' :numJoueur='$numJoueur'>
        </div>
        <div id="text_col" class="h-100" style="width:49%; margin-left:50%; box-sizing: border-box;">
            @if ($show_ordres)
            <div class="pt-2" style="height: 45%; box-sizing: border-box;">
                <div class="crtexte h-100" >
                    <livewire:crtexte :userId='$userId'/>
                </div>
            </div>
            <div class="h-50" style="height: 55%;  box-sizing: border-box;">
                <div class="nebutil-ordres ordres-form h-100" >
                    <livewire:edition-ordres :userId='$userId'/>
                </div>
            </div>
            @else
            <div class="h-100 pt-2" style=" box-sizing: border-box;">
                <div class="crtexte h-100" >
                    <livewire:crtexte :userId='$userId'/>
                </div>
            @endif
        </div>
    </div>
@push('page-js-files')
@vite(['resources/js/nebutil/ouvre-nbt.mjs'])
@endpush
@push('page-js-script')
<script type="module">
    document.addEventListener("PlanFullscreen",function(e){
        switch(e.detail.mode){
        case 0: //fullscreen
            document.getElementById('nebutil-plan').style.width="98%";
            document.getElementById('text_col').style.display="none";
            document.dispatchEvent(new CustomEvent("PlanResize"));
            break;
        default: // normal
            document.getElementById('nebutil-plan').style.width="49%";
            document.getElementById('text_col').style.display="block";
            document.dispatchEvent(new CustomEvent("PlanResize"));
        }
    });

    const config = {
        encoding: "{{ config('nebmail.files_encoding') }}",
        parseNbtUrl: "{{ route('api.parse-nbt') }}",
        nomPartie: "{{ $partieName }}",
        noJoueur: {{ $numJoueur }},
        apiToken: "{{ $apiToken }}",
    }
    new window.OuvreNbt(config)
</script>
@endpush
</x-layouts.app>

