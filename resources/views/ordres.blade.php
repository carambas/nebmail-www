<x-layouts.app>
<div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10 col-xl-6">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="alert alert-info" role="alert">
                            <div>Nombre de joueurs ayant rendus des ordres : {{ $nbOrdres }}</div>
                            <div>
                                @if (isset($dateOrdres))
                                    Dernière version d'ordres enregistrés le {{ $dateOrdres->format('d/m/Y H:i') }}
                                @else
                                    Pas d'ordres stockés pour ce tour
                                @endif
                            </div>

                        </div>
                        <div class='ordres-form'>
                        {!!Form::open()->post()->route('ordres', ['user' => $joueur_pour->id])!!}
                        {!!Form::textarea('ordres', (Auth::user()->isArbitre()) ? "Ordres <span class='text-warning'>de l'arbitre</span> pour le compte de <span class='text-warning'>{$joueur_pour->pseudo}</span>" : 'Mes ordres', $ordres)
                            ->id('ordres')
                            ->attrs([
                                'rows' => '20',
                                'spellcheck' => 'false',
                                'style' => 'font-family: monospace;'
                                ])
                            ->placeholder('##### Saisissez ici vos ordres')!!}
                        {!!Form::submit("Envoyer mes ordres")->attrs(['name' => 'envoi'])!!}
                        {!!Form::submit("Simulation d'évaluation")->attrs(['name' => 'simul'])!!}
                        {!!Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
            @if (isset($cr))
            <div class="col-md-10 col-xl-6">
                <div class="card">
                    <div class="card-header">Simulation d'évaluation</div>
                    <div class="card-body bg-white text-dark">
                        <div class="col text-nowrap">
                            <pre style="font-size: 100%">{{ $cr ?? '' }}</pre>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    @push('page-js-files')
        @vite(['resources/js/ordres/codemirror.js'])
    @endpush

    @push('page-js-script')
        <script type="module">
            let myTextArea = document.getElementById('ordres');
            if (myTextArea != null) {
                let myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                    lineNumbers: false,
                    mode:  'nebula',
                    dragDrop: false,
                    autofocus: true,
                });
                if (myCodeMirror.doc.lineCount() < 8) {
                    myCodeMirror.setCursor(5, 0);
                }
            }
            else {
                alert('myTextarea est null');
            }
        </script>
    @endpush
</x-layouts.app>

