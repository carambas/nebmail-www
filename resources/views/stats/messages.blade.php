<x-layouts.app>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2 class="text-center">Messages échangés</h2>
                <h3 class="text-center mt-4 text-warning">En cours de développement</h3>
                <div class="text-center mt-5"><a href="{{ config('nebmail.stat_nbmessages') }}" target="_blank" class="btn btn-primary">Accéder à l'ancien site</a></div>
            </div>
        </div>
    </div>
</x-layouts.app>
