<?php

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\CrController;
use App\Http\Controllers\Api\OrdreController;
use App\Http\Controllers\Api\RelationController;
use Illuminate\Support\Facades\Route;
use Spatie\HttpLogger\Middlewares\HttpLogger;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('guest')->group(function () {
    Route::get('/ordres/count/{partie?}', [OrdreController::class, 'count'])
        ->where('partie', '[A-Z]+')
        ->withoutMiddleware('auth');
});

Route::middleware('auth:api,api-token')->group(function () {
    Route::get('/user', [ApiController::class, 'user']);
    Route::get('/partie', [ApiController::class, 'partie']);

    Route::get('/ordres', [OrdreController::class, 'index']);
    Route::get('/ordres/{numjou}', [OrdreController::class, 'show'])
        ->where('numjou', '[0-9]+');
    Route::put('/ordres', [OrdreController::class, 'updateMany']);
    Route::put('/ordres/{numjou}', [OrdreController::class, 'update'])
        ->where('numjou', '[0-9]+');
    Route::get('/relation', [RelationController::class, 'index']);
    Route::put('/relation', [RelationController::class, 'update']);
    Route::post('/cr', [CrController::class, 'update'])
        ->middleware(HttpLogger::class);
    Route::get('/cr', [CrController::class, 'get'])->name('api.get.cr');
    Route::post('/parse-nbt', [CrController::class, 'parseNbt'])->name('api.parse-nbt');
});
