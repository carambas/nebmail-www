<?php

use App\Http\Controllers\CrController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MessagesController;
use App\Http\Controllers\OrdreController;
use App\Http\Controllers\StatsController;
use App\Livewire\Arbitre\Dashboard;
use App\Livewire\OrdresRendus;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::redirect('/', '/home');
Route::get('/home', [HomeController::class, 'index']);
Route::get('/stats/ordresrendus', OrdresRendus::class);
Route::get('/stats/rumeurs', [StatsController::class, 'rumeurs']);
Route::get('/stats/messages', [StatsController::class, 'messages']);

Route::middleware('auth')->group(function () {
    Route::get('/confpref', \App\Livewire\PreferencesForm::class)->name('confpref');
    Route::get('/ordres/{user?}', [OrdreController::class, 'edit'])->name('ordres')
        ->where(['user' => '[A-Z0-9]+\-[0-9]+']);
    Route::post('/ordres/{user}', [OrdreController::class, 'update'])
        ->where(['user' => '[A-Z0-9]+\-[0-9]+']);
    Route::get('/crtexte/{user?}', [CrController::class, 'crTexte'])->name('crtexte');
    Route::get('/cr', [CrController::class, 'crZip'])->name('cr');
    Route::get('/plan', [CrController::class, 'plan'])->name('plan');
    Route::get('/plan/{user}', [CrController::class, 'plan'])->name('arbitre.plan')
        ->where(['user' => '[A-Z0-9]+\-[0-9]+'])
        ->middleware('arbitre');
    Route::get('/plan', [CrController::class, 'plan'])->name('plan');
    Route::get('/nebutil', [CrController::class, 'nebutil'])->name('nebutil');
    Route::get('/nebutil/{user}', [CrController::class, 'nebutil'])->name('arbitre.nebutil')
        ->where(['user' => '[A-Z0-9]+\-[0-9]+'])
        ->middleware('arbitre');
});

Route::redirect('/arbitre', '/arbitre/dashboard');
Route::group(['prefix' => 'arbitre', 'middleware' => ['auth', 'arbitre']], function() {
    Route::get('/dashboard', Dashboard::class)->name('arbitre.dashboard');
    Route::get('/crnba', [CrController::class, 'crNba'])->name('crnba');
});

Route::group(['prefix' => 'messages'], function () {
    Route::get('/', [MessagesController::class, 'index'])->name('messages');
    Route::get('create/{typeMessage?}', [MessagesController::class, 'create'])
        ->name('messages.create')
        ->where(['typeMessage' => 'message|mg|rumeur']);
    Route::post('/', [MessagesController::class, 'store'])->name('messages.store');
    Route::get('{id}', [MessagesController::class, 'show'])->name('messages.show');
    Route::put('{id}', [MessagesController::class, 'update'])->name('messages.update');
});


