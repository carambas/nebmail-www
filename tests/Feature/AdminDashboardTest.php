<?php

namespace Tests\Feature;

use Livewire;
use Carbon\Carbon;
use Tests\GetUser;
use Tests\TestCase;
use App\Models\User;
use App\Models\Ordre;
use App\Models\Partie;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Livewire\Arbitre\Dashboard;
use App\Notifications\PasswordNotification;
use Illuminate\Support\Facades\Notification;

use function PHPUnit\Framework\assertEquals;

class AdminDashboardTest extends TestCase
{
    use GetUser;

    public function testAccesArbitreOk()
    {
        $arbitre = $this->getArbitre();
        $this->actingAs($arbitre)->get('/arbitre/dashboard')
            ->assertStatus(200);
    }

    public function testAccesJoueurKo()
    {
        $user = User::find('TEST-1');
        $this->actingAs($user)->get('/arbitre/dashboard')
            ->assertStatus(302);
    }

    public function testDashboardContent()
    {
        $user = $this->getArbitre();

        $this->actingAs($user);

        Livewire::test(Dashboard::class)->assertViewIs('livewire.arbitre.dashboard');

        $this->get('/arbitre/dashboard')
            ->assertSeeText("Partie {$user->partie->name} tour {$user->partie->tour}")
            ->assertSeeText("Date limite pour le tour")
            ->assertSeeText("Liste des joueurs");

    }

    public function testAffichageDateRelance()
    {
        $user = $this->getArbitre();
        $user->partie->derniere_relance = null;
        $user->partie->save();

        $this->actingAs($user)->get('/arbitre/dashboard')
            ->assertDontSeeText("Dernière relance");

        $user->partie->derniere_relance = now()->yesterday();
        $user->partie->save();

        $this->actingAs($user)->get('/arbitre/dashboard')
            ->assertSeeText("Dernière relance");

    }

    public function testVisibiliteBoutonSupprimer()
    {
        $partie = Partie::find('TEST');
        $partie->tour = 4;
        $partie->save();

        $user = $this->getArbitre();
        $this->actingAs($user)->get('/arbitre/dashboard')
            ->assertDontSee('Supprimer');

        $partie->tour = 0;
        $partie->save();

        $user = $this->getArbitre();
        $this->actingAs($user)->get('/arbitre/dashboard')
            ->assertSee('Supprimer');
    }

    public function testSupressionJoueur()
    {
        $joueurs = Partie::find('TEST')->joueurs_sans_arbitre;
        $joueur = $joueurs->first();
        $this->assertTrue($joueurs->count() === 4);
        $this->assertTrue($joueur->id === 'TEST-1');

        $this->actingAs($this->getArbitre());

        Livewire::test(Dashboard::class)
            ->assertSee($joueur->pseudo)
            ->call('delete', $joueur)
            ->assertDontSee($joueur->pseudo);

        $this->assertTrue($joueurs->count() === 3);
    }

    public function testDetails()
    {
        $this->actingAs($this->getArbitre());
        $user = User::find('TEST-1');

        Livewire::test(Dashboard::class)
            ->call('details', $user)
            ->assertSet('user', $user);

    }

    public function testUploadNba()
    {
        $this->actingAs($this->getArbitre());

        // On prépare une partie au tour 0 sans joueurs
        $partie = Partie::find('TEST');
        $partie->tour = 0;
        $partie->joueurs_sans_arbitre->get()->map(fn ($user) => $user->delete());
        $partie->save();
        $this->assertEquals(0, $partie->joueurs_sans_arbitre->count());

        Storage::fake('nba');

        // On upload le fichier .nba
        $nbaFileName =  'TEST04.nba';
        $nbaContent = Storage::disk('data')->get($nbaFileName);
        $file = UploadedFile::fake()->createWithContent($nbaFileName, $nbaContent);
        Livewire::test(Dashboard::class)
            ->set('fichier_nba', $file)
            ->call('uploadNba')
            ->assertSeeInOrder([
                'Joueur 1', 'Supprimer',
                'Joueur 2', 'Supprimer',
                'Joueur 3', 'Supprimer',
                'Joueur 4', 'Supprimer',
            ])
            ->assertSee("olivier@gangloff.cx");

        $partie->refresh();

        // On regarde le résultat dans la base
        $this->assertEquals(4, $partie->joueurs_sans_arbitre->count());

    }

    public function testAfficheOrdres()
    {
        $this->actingAs($this->getArbitre());
        $user = User::find('TEST-3');
        Livewire::test(Dashboard::class)
            ->call('afficheOrdres', $user)
            ->assertSet('ordres', $user->ordre->content)
            ->assertSet('ordresIdJou', $user->id)
            ->assertSet('showOrdresButton', false);
    }

    public function testOrdresRendus()
    {
        $this->actingAs($this->getArbitre());

        // On vide les ordres
        Ordre::all()->each->forceDelete();
        assertEquals(0, Ordre::count());

        // On vérifie qu'il n'y a pas d'ordres rendus
        Livewire::test(Dashboard::class)
            ->assertDontSeeHtml("afficheOrdres('TEST-1')")
            ->assertDontSeeHtml("afficheOrdres('TEST-2')")
            ->assertDontSeeHtml("afficheOrdres('TEST-3')")
            ->assertDontSeeHtml("afficheOrdres('TEST-4')");

        Ordre::create([
            'user_id' => 'TEST-1',
            'tour' => Partie::find('TEST')->tour + 1,
            'content' => "Ordres du joueur 1",
        ]);

        Ordre::create([
            'user_id' => 'TEST-1',
            'tour' => Partie::find('TEST')->tour,
            'content' => "Ordres du joueur 1 tour précédent",
            'deleted_at' => '2020-13-31',
        ]);

        Ordre::create([
            'user_id' => 'TEST-2',
            'tour' => Partie::find('TEST')->tour + 1,
            'content' => "Ordres du joueur 2 supprimé",
            'deleted_at' => '2020-13-31',
        ]);

        Ordre::create([
            'user_id' => 'TEST-3',
            'tour' => Partie::find('TEST')->tour + 1,
            'content' => "Ordres du joueur 3",
        ]);

        assertEquals(2, Ordre::count());

        // On vérifie qu'on voit les ordres ceéés et uniquement ceux-là
        Livewire::test(Dashboard::class)
            ->assertSeeHtml("afficheOrdres('TEST-1')")
            ->assertDontSeeHtml("afficheOrdres('TEST-2')")
            ->assertSeeHtml("afficheOrdres('TEST-3')")
            ->assertDontSeeHtml("afficheOrdres('TEST-4')");

        //$this->actingAs($this->getArbitre());

    }

    public function testDeadline()
    {
        $this->actingAs($this->getArbitre());
        $this->withoutExceptionHandling();

        Livewire::test(Dashboard::class)
            ->set('deadline_date', '28-02-2030')
            ->assertHasNoErrors('deadline_date')
            ->set('deadline_date', null)
            ->assertHasErrors('deadline_date');

        Livewire::test(Dashboard::class)
            ->set('deadline_heure', '20:00')
            ->assertHasNoErrors('deadline_heure')
            ->set('deadline_heure', null)
            ->assertHasErrors('deadline_heure');
    }

    public function testEnvoieMdp()
    {
        // On efface les mots de passe des joueurs de la liste $usersid
        $usersid = ['TEST-2', 'TEST-4'];
        $usersSansMdp = User::whereIn('id', $usersid)->get();
        $autresUsers = Partie::find('TEST')->joueur()->whereNotIn('id', $usersid)->get();

        $usersSansMdp->map(function (User $user) {
            $user->password = null;
            $user->api_token = null;
            $user->save();
        });

        $this->actingAs($this->getArbitre());

        Notification::fake();

        Livewire::test(Dashboard::class)
            ->call('genererMdp');

        Notification::assertSentTo($usersSansMdp, PasswordNotification::class);
        Notification::assertNotSentTo($autresUsers, PasswordNotification::class);

        // On vérifie qu'on n'a pas de joueur sans api_token
        $usersSansMdp->each(function ($user) {
            $user->refresh();
            $this->assertNotNull($user->api_token);
        });
    }

    public function testSimulOrdres()
    {
        // Il nous faut un joueur avec des ordres
        $user = $this->getJoueurAvecOrdres();

        $this->actingAs($this->getArbitre());
        Livewire::test(Dashboard::class)
            ->call('simulOrdres', $user->id)
            ->assertSet('showOrdresButton', true)
            ->assertNotSet('ordres', $user->ordre->content)
            ->assertSee('Résultat du tour No.');

    }
}
