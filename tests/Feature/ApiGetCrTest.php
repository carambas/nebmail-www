<?php

namespace Tests\Feature;

use Tests\GetUser;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class ApiGetCrTest extends TestCase
{
    use GetUser;

    protected function setUp(): void
    {
        parent::setUp();

        // On peuple le storage fake avec les fichiers .nba existants de la partie TEST
        $source = Storage::disk('data');
        Storage::fake('data');
        $dest = Storage::disk('data');

        for ($tour = 0; $tour <= 6; $tour++) {
            $fileName = sprintf("TEST/STOCK/TEST%02d.nba", $tour);
            $nbaContent = $source->get($fileName);
            $dest->put($fileName, $nbaContent);
        }
    }

    function testApiGetCrTxt()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user, 'api-token');
        $response = $this->json('GET', '/api/cr', [
            'data' => [
                'user' => $user->id,
                'tours' => [0, 1, 2, 4],
                'txt' => true,
                'nbt' => false,
            ],
        ]);

        $response
            ->assertOk()
            ->assertJsonCount(4, 'data.cr')
            ->assertJsonPath('data.cr.0.tour', 0)
            ->assertJsonPath('data.cr.1.tour', 1)
            ->assertJsonPath('data.cr.2.tour', 2)
            ->assertJsonPath('data.cr.3.tour', 4)
            ->assertJsonPath('data.cr.0.nbt', null)
            ->assertJsonPath('data.cr.1.nbt', null)
            ->assertJsonPath('data.cr.2.nbt', null)
            ->assertJsonPath('data.cr.3.nbt', null)
            ->assertJsonStructure([
                'data' => [
                    'cr' => [
                        '*' => [
                            'tour',
                            'txt'
                        ]
                    ]
                ]
            ]);

        $this->assertStringStartsWith("*** Compte rendu généré", $response['data']['cr'][0]['txt']);
        $this->assertStringStartsWith("*** Compte rendu généré", $response['data']['cr'][1]['txt']);
        $this->assertStringStartsWith("*** Compte rendu généré", $response['data']['cr'][2]['txt']);
        $this->assertStringStartsWith("*** Compte rendu généré", $response['data']['cr'][3]['txt']);
    }

    function testApiGetCrNbt()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user, 'api-token');
        $response = $this->json('GET', '/api/cr', [
            'data' => [
                'user' => $user->id,
                'tours' => [0, 1, 3, 4],
                'txt' => false,
                'nbt' => true,
            ],
        ]);

        $response
            ->assertOk()
            ->assertJsonCount(4, 'data.cr')
            ->assertJsonPath('data.cr.0.tour', 0)
            ->assertJsonPath('data.cr.1.tour', 1)
            ->assertJsonPath('data.cr.2.tour', 3)
            ->assertJsonPath('data.cr.3.tour', 4)
            ->assertJsonPath('data.cr.3.nbt.tour', 4)
            ->assertJsonPath('data.cr.3.nbt.numJoueur', $user->numjou)
            ->assertJsonPath('data.cr.0.txt', null)
            ->assertJsonPath('data.cr.1.txt', null)
            ->assertJsonPath('data.cr.2.txt', null)
            ->assertJsonPath('data.cr.3.txt', null)
            ->assertJsonStructure([
                'data' => [
                    'cr' => [
                        '*' => [
                            'tour',
                            'nbt'
                        ]
                    ]
                ]
            ]);
    }

    function testApiGetCrNbtEtTxt()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user, 'api-token');
        $response = $this->json('GET', '/api/cr', [
            'data' => [
                'user' => $user->id,
                'tours' => [1, 4],
                'txt' => true,
                'nbt' => true,
            ],
        ]);

        $response
            ->assertOk()
            ->assertJsonCount(2, 'data.cr')
            ->assertJsonPath('data.cr.0.tour', 1)
            ->assertJsonPath('data.cr.1.tour', 4)
            ->assertJsonStructure([
                'data' => [
                    'cr' => [
                        '*' => [
                            'tour',
                            'nbt',
                            'txt'
                        ]
                    ]
                ]
            ]);
    }

    function testApiGetCrMauvaisJoueur()
    {
        $user = User::find('TEST-1');
        $userCr = User::find('TEST-2');

        $this->actingAs($user, 'api-token');
        $this->json('GET', '/api/cr', [
            'data' => [
                'user' => $userCr->id,
                'tours' => [0],
                'txt' => false,
                'nbt' => false,
            ],
        ])
            ->assertForbidden();
    }

    function testApiGetCrArbitre()
    {
        $user = User::find('TEST-0');
        $userCr = User::find('TEST-2');

        $this->actingAs($user, 'api-token');
        $this->json('GET', '/api/cr', [
            'data' => [
                'user' => $userCr->id,
                'tours' => [0],
                'txt' => false,
                'nbt' => false,
            ],
        ])
            ->assertOk();
    }

    function testApiGetExtraits()
    {
        $user = User::find('TEST-2');

        $this->actingAs($user, 'api-token');
        $response = $this->json('GET', '/api/cr', [
            'data' => [
                'user' => $user->id,
                'tours' => [],
                'txt' => false,
                'nbt' => false,
                'extraits' => true,
                'extraitsAfter' => '2022-04-04 15:30:00',
            ],
        ]);

        $response
            ->assertOk()
            ->assertJsonCount(3, 'data')
            ->assertJsonCount(0, 'data.cr')
            ->assertJsonCount(3, 'data.extraits')
            ->assertJsonStructure([
                'data' => [
                    'extraits' => [
                        '*' => [
                            'tour',
                            'numero',
                            'content',
                        ]
                    ],
                    'lastExtraitAt',
                ]
            ])
            ->assertJsonPath('data.extraits.0.tour', 1)
            ->assertJsonPath('data.extraits.0.numero', 22)
            ->assertJsonPath('data.extraits.1.tour', 4)
            ->assertJsonPath('data.extraits.1.numero', 4)
            ->assertJsonPath('data.extraits.2.tour', 4)
            ->assertJsonPath('data.extraits.2.numero', 5);
    }
}
