<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Partie;
use Tests\GetUser;
use Tests\TestCase;
use App\Mail\OrdresAR;
use App\Jobs\CallSlack;
use App\Events\OrdresRendus;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Event;

class ApiOrdresTest extends TestCase
{
    use GetUser;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testApiGetOrdresOrdresNonRendus()
    {
        //$user = User::factory()->create();
        $user = $this->getJoueurSansOrdres();

        $this->actingAs($user, 'api')->getJson('/api/ordres/' . $user->numjou, ['Accept' => 'application/json'])
            ->assertOk()
            ->assertJsonPath('ordres.' . $user->numjou, '')
            ->assertJsonPath('tour', $user->partie->tour + 1);
    }

    public function testApiGetOrdresOrdresRendus()
    {
        $user = $this->getJoueurAvecOrdres();

        $this->actingAs($user, 'api')->getJson('/api/ordres/' . $user->numjou, ['Accept' => 'application/json'])
            ->assertOk()
            ->assertJsonStructure([
                "ordres" => [
                    $user->numjou
                ],
                "tour"
            ]);
    }

    public function testApiGetOrdresArbitre()
    {
        $user = User::find('TEST-0');

        $this->actingAs($user, 'api')->getJson('/api/ordres/1', ['Accept' => 'application/json'])
            ->assertOk()
            ->assertJsonStructure([
                "ordres" => [
                    '1'
                ],
                "tour"
            ]);
    }

    public function testApiGetAllOrdresArbitre()
    {
        $user = User::find('TEST-0');

        $this->actingAs($user, 'api')->getJson('/api/ordres', ['Accept' => 'application/json'])
            ->assertOk()
            ->assertJsonStructure([
                "ordres" => [
                    '1',
                    '3'
                ],
                "tour"
            ]);
    }

    public function testApiGetBadUserOrdresArbitre()
    {
        $user = User::find('TEST-0');

        $this->actingAs($user, 'api')->getJson('/api/ordres/17', ['Accept' => 'application/json'])
            ->assertNotFound();

    }

    public function testApiGetAllOrdresJoueur()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user, 'api')->getJson('/api/ordres', ['Accept' => 'application/json'])
            ->assertForbidden();

    }

    public function testApiGetOrdresBasicAuth()
    {
        $this->getJson('/api/ordres/1', [
            'Accept' => 'application/json',
            'HTTP_Authorization' => "Basic " . base64_encode('TEST-1:password'),
            ])
            ->assertOk();
    }

    public function testApiGetOrdresBadPassword()
    {
        $this->getJson('/api/ordres/1', [
            'Accept' => 'application/json',
            'HTTP_Authorization' => "Basic " . base64_encode('TEST-1:badpassword'),
            ])
            ->assertUnauthorized();
    }

    public function testApiGetOrdresCount()
    {
        $this->getjson('/api/ordres/count')
            ->assertOk()
            ->assertExactJson(['TEST' => 2]);
    }

    public function testApiGetOrdresCountPartie()
    {
        $this->getjson('/api/ordres/count/TEST')
            ->assertOk()
            ->assertExactJson(['TEST' => 2]);
    }

    public function testApiPutOrdresDejaRendus()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        $user = User::find('TEST-1');
        $ordres = '# Ordres joueur 1 par API.\nBlah; accents éà\nF 10 M 5\nF 35 C BOMBE ; Hello, World æ\n';

        $this->actingAs($user, 'api')->putJson('/api/ordres/' . $user->numjou, [
            'ordres' => $ordres,
            'tour' => 5
            ], [
                'Accept' => 'application/json'
            ])
            ->assertOK();

        Mail::assertQueued(OrdresAR::class, 2); // On attend 2 mails, un pour le joueur, un pour l'arbitre
        Bus::assertDispatched(CallSlack::class);
        Event::assertDispatched(OrdresRendus::class);

        $this->actingAs($user, 'api')->getJson('/api/ordres/' . $user->numjou, ['Accept' => 'application/json'])
            ->assertOk()
            ->assertJson([
                "ordres" => [
                    '1' => $ordres
                ],
                "tour" => 5
            ]);
    }

    public function testApiPutOrdresArCcArbitre()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        $user = User::find('TEST-1');

        $arbitre = User::find('TEST-0');
        $arbitre->cc = true;
        $arbitre->save();

        $ordres = '# Ordres vides';

        $this->actingAs($user, 'api')->putJson('/api/ordres/' . $user->numjou, [
            'ordres' => $ordres,
            'tour' => 5
            ], [
                'Accept' => 'application/json'
            ])
            ->assertOK();

        Mail::assertQueued(OrdresAR::class, 2);

        Mail::fake();
        $arbitre = User::find('TEST-0');
        $arbitre->cc = false;
        $arbitre->save();

        $this->actingAs($user, 'api')->putJson('/api/ordres/' . $user->numjou, [
            'ordres' => $ordres,
            'tour' => 5
            ], [
                'Accept' => 'application/json'
            ])
            ->assertOK();

        Mail::assertQueued(OrdresAR::class, 1);

    }

    public function testApiPutOrdresSansBody()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user, 'api')->putJson('/api/ordres/1', ['Accept' => 'application/json'])
            ->assertStatus(422);

    }

}
