<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Services\Cr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Testing\FileFactory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiParseNbtTest extends TestCase
{
    private string $nbtFileName;
    private string $nbaFileName;
    private string $nbtStoredFileName;

    protected function setUp(): void
    {
        parent::setUp();
        $this->nbtFileName = Cr::getNbtFileName('TEST', 3, 4, false);
        $this->nbaFileName = Cr::getNbaFileName('TEST', 4);
        $this->nbtStoredFileName = Cr::getNbtFileName('TEST', 3, 4, true);
    }

    private function setFakeStorageWithNbt()
    {
        $nbtContent = Storage::disk('data')->get($this->nbtFileName);
        Storage::fake('data');
        Storage::disk('data')->put($this->nbtFileName, $nbtContent);
    }

    public function test_post_nbt_sans_stockage()
    {
        $nbtContent = Storage::disk('data')->get($this->nbtFileName);
        Storage::fake('data');

        $user = User::find('TEST-3');

        $file = new FileFactory;
        $file = $file->createWithContent('bidon.nbt', $nbtContent);

        $this->json('POST', '/api/parse-nbt', [
            'api_token' => $user->api_token,
            'stocke_nbt' => 0,
            'nbt' => $file,
        ])
            ->assertJsonStructure(['nbt', 'crtxt']);

        Storage::disk('data')->assertMissing($this->nbtStoredFileName);
    }

    public function test_post_nbt_fichier_bien_stocke()
    {
        $nbtContent = Storage::disk('data')->get($this->nbtFileName);
        Storage::fake('data');

        $user = User::find('TEST-3');

        $file = new FileFactory;
        $file = $file->createWithContent('bidon.nbt', $nbtContent);

        $this->json('POST', '/api/parse-nbt', [
            'api_token' => $user->api_token,
            'stocke_nbt' => 1,
            'nbt' => $file,
        ])->assertOk();

        Storage::disk('data')->assertExists($this->nbtStoredFileName);
    }

    public function test_post_fichier_absence_nbt_provoque_erreur_de_validation()
    {
        $nbtContent = Storage::disk('data')->get($this->nbtFileName);
        Storage::fake('data');

        $user = User::find('TEST-2');

        $file = new FileFactory;
        $file = $file->createWithContent('bidon.nbt', $nbtContent);

        $this->json('POST', '/api/parse-nbt', [
            'api_token' => $user->api_token,
            'stocke_nbt' => 0,
            'nba' => $file,
        ])->assertInvalid(['nbt']);
    }


    public function test_post_fichier_pas_au_format_nbt_provoque_erreur_de_validation()
    {
        $nbaContent = Storage::disk('data')->get($this->nbaFileName);
        Storage::fake('data');

        $user = User::find('TEST-2');

        $file = new FileFactory;
        $file = $file->createWithContent('bidon.nba', $nbaContent);

        $this->json('POST', '/api/parse-nbt', [
            'api_token' => $user->api_token,
            'stocke_nbt' => 0,
            'nbt' => $file,
        ])->assertInvalid(['nbt_parsed' => "Le fichier n'est au format .nbt (pas de numéro de joueur de trouvé)"]);
    }

    public function test_post_fichier_mauvais_joueur_provoque_erreur_de_validation()
    {
        $nbtContent = Storage::disk('data')->get($this->nbtFileName);
        Storage::fake('data');

        $user = User::find('TEST-4');

        $file = new FileFactory;
        $file = $file->createWithContent('bidon.nbt', $nbtContent);

        $this->json('POST', '/api/parse-nbt', [
            'api_token' => $user->api_token,
            'stocke_nbt' => 0,
            'nbt' => $file,
        ])->assertInvalid(['nbt_parsed' => "Le fichier .nbt n'appartient pas au joueur 4 de la partie TEST"]);
    }

    public function test_post_fichier_mauvaise_partie_provoque_erreur_de_validation()
    {
        $nbtContent = Storage::disk('data')->get($this->nbtFileName);
        Storage::fake('data');

        $user = User::find('TEST-2');

        $file = new FileFactory;
        $nbtContent = Str::replace('NomPartie=TEST', 'NomPartie=TOTO', $nbtContent);
        $file = $file->createWithContent('bidon.nbt', $nbtContent);

        $this->json('POST', '/api/parse-nbt', [
            'api_token' => $user->api_token,
            'stocke_nbt' => 0,
            'nbt' => $file,
        ])->assertInvalid(['nbt_parsed' => "Le fichier .nbt n'appartient pas au joueur 2 de la partie TEST"]);
    }
}
