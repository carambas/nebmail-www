<?php

namespace Tests\Feature;

use Tests\GetUser;
use Tests\TestCase;
use App\Models\User;
use App\Services\Cr;
use App\Jobs\EnvoiCR;
use App\Models\Ordre;
use App\Events\CrSent;
use App\Models\Partie;
use App\Listeners\CrSentNotification;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Testing\FileFactory;

class ApiPostCrTest extends TestCase
{
    use GetUser;

    private function setFakeStorageWithNba()
    {
        $nbaContent = Storage::disk('data')->get('TEST/STOCK/TEST04.nba');
        Storage::fake('data');
        Storage::disk('data')->put('TEST/STOCK/TEST04.nba', $nbaContent);
    }

    function testEnvoiCrJobOneUser()
    {

        $user = User::find('TEST-1');
        $job = new EnvoiCR($user);

        $this->setFakeStorageWithNba();
        Event::fake([CrSent::class]);

        $job->handle();

        // Assert
        $this->assertEquals(true, $job->oneUser);
        $this->assertEquals($job->users->count(), 1);
        $this->assertEquals($job->partie->name, 'TEST');

        Event::assertDispatched(CrSent::class);

        Storage::disk('data')->assertExists('TEST/STOCK/NBT/TEST0104.nbt');
        Storage::disk('data')->assertMissing(['TEST/STOCK/NBT/TEST0204.nbt', 'TEST/STOCK/NBT/TEST0105.nbt']);

        Event::assertListening(
            CrSent::class,
            CrSentNotification::class
        );
    }

    function testEnvoiCrJobPartie()
    {
        $partie = Partie::find('TEST');

        $job = new EnvoiCR($partie);

        $this->setFakeStorageWithNba();
        Event::fake([CrSent::class]);

        $job->handle();

        // Assert
        $this->assertEquals(false, $job->oneUser);
        $this->assertEquals($job->users->count(), $partie->joueurs_sans_arbitre->count());
        $this->assertEquals($job->partie->name, 'TEST');

        Event::assertDispatched(CrSent::class);

        Storage::disk('data')->assertExists([
            'TEST/STOCK/NBT/TEST0104.nbt',
            'TEST/STOCK/NBT/TEST0204.nbt',
            'TEST/STOCK/NBT/TEST0304.nbt',
            'TEST/STOCK/NBT/TEST0404.nbt',
            ]);

        Storage::disk('data')->assertMissing('TEST/STOCK/NBT/TEST0205.nbt');

        Event::assertListening(
            CrSent::class,
            CrSentNotification::class
        );
    }

    function testEnvoiCrJobJoueurInconnuThrowsException()
    {
        $user = User::factory()->create();
        $job = new EnvoiCR($user);

        $this->setFakeStorageWithNba();
        Event::fake([CrSent::class]);

        $this->expectException(\Exception::class);
        $job->handle();

        Storage::disk('data')->assertMissing([
            'TEST/STOCK/NBT/TEST0104.nbt',
            'TEST/STOCK/NBT/TEST0504.nbt',
            ]);
    }

    function testApiEnvoiCr()
    {
        $partie = Partie::find('TEST');
        $nbaFileName = Cr::getNbaFileName($partie->name, $partie->tour + 1);
        $nbaContent = Storage::disk('data')->get($nbaFileName);

        $file = new FileFactory;
        $file = $file->createWithContent('bidon.nba', $nbaContent);

        $user= User::find('TEST-2');

        // On stocke un ordre et un autre en brouillon pour vérifier leur suppression après l'envoi du CR
        $ordre_definitif = Ordre::updateOrCreate([
            'user_id' => $user->id,
            'tour' => $partie->tour + 1,
            'brouillon' => false,
        ], [
            'content' => 'définitif',
        ]);

        $ordre_brouillon = Ordre::updateOrCreate([
            'user_id' => $user->id,
            'tour' => $partie->tour + 1,
            'brouillon' => true,
        ], [
            'content' => 'brouillon',
        ]);

        Queue::fake();
        Storage::fake('data');

        $this->actingAs($partie->arbitre, 'api');
        $response = $this->json('POST', '/api/cr', [
            'json' => json_encode([
                'data' => [
                    'tour' => 5,
                    'envoi_cr' => 1,
                ],
            ]),
            'nba' => $file,
        ]);

        $response
            ->assertExactJson([
                'file' => 'bidon.nba',
                'message' => 'OK',
                'tour' => 5,
                'envoi_cr' => 1,
                ]);

        // On vérifie qu'on a bien stocké le fichier .nba
        Storage::disk('data')->assertExists($nbaFileName);

        Queue::assertPushed(EnvoiCR::class, function (EnvoiCR $job) use ($partie) {
            return ($job->partie->name === $partie->name) && ($job->oneUser === false);
        });

        // On vérifie ci-dessous le bon nettoyage des ordres et brouillons suite au nouveau tour
        $user->refresh();
        $this->assertEmpty($user->ordre);
        $this->assertEmpty($user->ordreBrouillon);
        $this->assertSoftDeleted($ordre_definitif);
        $this->assertModelMissing($ordre_brouillon);
    }

    function testApiEnvoiCrMauvaisTour()
    {
        $partie = Partie::find('TEST');
        $nbaFileName = Cr::getNbaFileName($partie->name, $partie->tour + 1);
        $nbaContent = Storage::disk('data')->get($nbaFileName);

        $file = new FileFactory;
        $file = $file->createWithContent('bidon.nba', $nbaContent);

        $this->actingAs($partie->arbitre, 'api');
        $this->json('POST', '/api/cr', [
            'json' => json_encode([
                'data' => [
                    'tour' => 6,
                    'envoi_cr' => 1,
                ],
            ]),
            'nba' => $file,
        ])
        ->assertStatus(422)
        ->assertJsonStructure([
            "message",
             "errors" => [
                 'data.tour' => [
                     0
                 ]
             ]
         ]);
    }

    function testApiEnvoiCrMauvaisAuthent()
    {
        $partie = Partie::find('TEST');
        $nbaFileName = Cr::getNbaFileName($partie->name, $partie->tour + 1);
        $nbaContent = Storage::disk('data')->get($nbaFileName);

        $file = new FileFactory;
        $file = $file->createWithContent('bidon.nba', $nbaContent);

        $this->actingAs(User::find('TEST-1'), 'api');
        $this->json('POST', '/api/cr', [
            'json' => json_encode([
                'data' => [
                    'tour' => 5,
                    'envoi_cr' => 1,
                ],
            ]),
            'nba' => $file,
        ])
        ->assertStatus(403);
    }

}
