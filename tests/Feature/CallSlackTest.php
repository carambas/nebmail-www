<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Partie;
use Illuminate\Log\LogManager;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CallSlackTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_ordres_rendus_sans_webhook_n_appelle_pas_slack()
    {
        $message = "coucou";
        $partie = Partie::find('TEST');
        $partie->webhook_ordres_rendus = null;
        $partie->save();

        Log::spy();

        $job = new \App\Jobs\CallSlack($message, Partie::find('TEST'));
        $job->handle();

        Log::shouldHaveReceived('info')
            ->with($message)
            ->once();
    }

    public function test_notifiation_quand_webhook_configure()
    {
        $message = "coucou";
        $partie = Partie::find('TEST');
        $partie->webhook_ordres_rendus = "http://host/path";
        $partie->save();

        Log::spy();

        $job = new \App\Jobs\CallSlack(message: $message, partie: Partie::find('TEST'));

        $job->handle();

        Log::shouldHaveReceived('stack')->once();
    }

    public function test_pas_de_notifiation_quand_webhook_fourni_directement()
    {
        $message = "coucou";
        $webhook = "http://host/path";

        Log::spy();

        $job = new \App\Jobs\CallSlack(message: $message, url: $webhook);

        $job->handle();

        Log::shouldHaveReceived('stack')->once();
    }
}
