<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\GetUser;
use Tests\TestCase;
use Livewire\Livewire;
use App\Livewire\PreferencesForm;
use Illuminate\Foundation\Testing\WithFaker;
use function PHPUnit\Framework\assertContains;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConfPrefTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testComponentPreferencesFormPresent()
    {
        $user = User::find('TEST-1');

        $response = $this->actingAs($user)->get('/confpref');
        $response->assertSeeLivewire('preferences-form');
    }

    public function testPrefCheckboxes()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user);

        Livewire::test(PreferencesForm::class)
            ->set('user.msg_gen', false)
            ->set('user.rumeurs', true)
            ->set('user.cc', false)
            ->call('save');

        $user->refresh();
        $this->assertTrue($user->msg_gen == false);
        $this->assertTrue($user->rumeurs == true);
        $this->assertTrue($user->cc == false);

        Livewire::test(PreferencesForm::class)
            ->set('user.msg_gen', true)
            ->set('user.rumeurs', false)
            ->set('user.cc', true)
            ->call('save')
            ->assertDispatched('notify');

        $user = User::find('TEST-1');
        $this->assertTrue($user->msg_gen == true);
        $this->assertTrue($user->rumeurs == false);
        $this->assertTrue($user->cc == true);

        Livewire::test(PreferencesForm::class)
            ->set('password', 'toto')
            ->call('save')
            ->assertHasErrors('password');
    }

    public function testPrefPasswordSize()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user);

        Livewire::test(PreferencesForm::class)
            ->set('password', 'toto')
            ->call('save')
            ->assertHasErrors('password');

        Livewire::test(PreferencesForm::class)
            ->set('password', 'totototo')
            ->call('save')
            ->assertHasNoErrors('password');
    }

    public function testPrefOneEmail()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user);

        Livewire::test(PreferencesForm::class)
            ->set('user.email.0', 'toto@titi.fr')
            ->call('save')
            ->assertHasNoErrors('user.email.0');

        $user = User::find('TEST-1');
        $this->assertTrue($user->email[0] === 'toto@titi.fr');
    }

    public function testPrefTwoEmails()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user);

        Livewire::test(PreferencesForm::class)
            ->set('user.email.0', 'toto@titi.fr')
            ->set('user.email.1', 'toto2@titi.fr')
            ->call('save')
            ->assertHasNoErrors('user.email.0')
            ->assertHasNoErrors('user.email.1');

        $user = User::find('TEST-1');
        $this->assertTrue($user->email[0] === 'toto@titi.fr');
        $this->assertTrue($user->email[1] === 'toto2@titi.fr');
    }

    public function testPrefEmail2Empty()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user);

        Livewire::test(PreferencesForm::class)
            ->set('user.email.0', 'toto@titi.fr')
            ->set('user.email.1', 'toto2@titi.fr')
            ->call('save')
            ->set('user.email.1', '')
            ->call('save')
            ->assertHasNoErrors('user.email.0')
            ->assertHasNoErrors('user.email.1');

        $user->refresh();
        $this->assertTrue($user->email[0] === 'toto@titi.fr');
        $this->assertTrue(count($user->email) === 1);

    }

    public function testPrefEmailHasBadvalidation()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user);

        Livewire::test(PreferencesForm::class)
            ->set('user.email.0', 'tototiti.fr')
            ->call('save')
            ->assertHasErrors('user.email.0');
    }
}


