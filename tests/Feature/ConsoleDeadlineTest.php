<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Partie;
use Notification;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use App\Notifications\DeadlineJoueur;
use App\Notifications\DeadlineArbitre;

class ConsoleDeadlineTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private function initDeadline($heureDerniereRelance, $jours)
    {
        $partie = Partie::find('TEST');
        $partie->derniere_relance = Carbon::now()->setTime($heureDerniereRelance, 0);
        $partie->deadline = $partie->derniere_relance->addDays($jours)->setTime(20, 0);
        $partie->save();
    }

    private function initOrdres($users)
    {
        DB::table('ordres')->delete();
        foreach($users as $user) {
            $ordre = $user->ordre()->firstOrCreate([], [
                'tour' => $user->partie->tour + 1,
            ]);
            $ordre->content = "#Ordres du joueur {$user->pseudo}\nF5 M 12";
            $ordre->save();
        }

        return Partie::find('TEST')->joueurs_sans_arbitre->whereNotIn('id', $users->pluck('id'))->get();
    }

    public function testDeadlinePasDeDeadlineTrouvee()
    {
        Partie::where('deadline', '!=', null)
            ->update(['deadline' => Carbon::now()->yesterday()]);

        $this->artisan('nebula:deadline')
            ->expectsOutput('Aucune partie trouvée disposant d\'une date limite à venir')
            ->assertExitCode(1);
    }


    public function testDeadlineSurDerniereSemaine()
    {
        $relanceAttendue = array(false, false, false, true, false, true, true);

        // Eval effctuée aujourd'hui à 21j, éval suivante 7j plus tard à 20h
        $this->initDeadline(heureDerniereRelance: 21, jours: 7);

        // On se place le lendemain (premier jour) à 9h
        $this->travelTo(Carbon::tomorrow()->setTime(9,0));

        // Pour chaque jour venant on regarde si on a eu une relance ou non comme attendu
        for($jour = 1; $jour <= 7; $jour++) {
            $output = $relanceAttendue[$jour - 1]
                ? "TEST : relance"
                : "TEST : pas de relance";

            $this->artisan('nebula:deadline')
            ->assertExitCode(0)
            ->expectsOutput($output);

            $this->travel(1)->days();
        }
    }
    public function testMailRelance()
    {
        // On se place la veille de la DL à 9h pour provoquer une relance
        $this->initDeadline(heureDerniereRelance: 9, jours: 1);
        $this->travelTo(now()->setTime(9, 0));

        $usersOrdresRendus = User::whereIn('id', ['TEST-2'])->get();
        $usersONR = $this->initOrdres($usersOrdresRendus);

        Notification::fake();

        $this->withExceptionHandling()
            ->artisan('nebula:deadline')
            ->assertExitCode(0);

        Notification::assertNothingSent();

        $this->travel(1)->days();
        $this->withExceptionHandling()
            ->artisan('nebula:deadline')
            ->assertExitCode(0);

        Notification::assertSentTo($usersONR, DeadlineJoueur::class);
        Notification::assertNotSentTo($usersOrdresRendus, DeadlineJoueur::class);
        Notification::assertSentTo([User::find('TEST-0')], DeadlineArbitre::class);
        Notification::assertNotSentTo(Partie::find('TEST')->joueurs_sans_arbitre->get(), DeadlineArbitre::class);
        Notification::assertCount($usersONR->count() + 1);
    }

}
