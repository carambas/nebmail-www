<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use App\Notifications\PasswordNotification;
use Illuminate\Support\Facades\Notification;

class ConsolePartieTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreePartie()
    {
        Notification::fake();

        $this->artisan('nebula:partie LJMNP toto@titi.fr')
            ->expectsOutput('Partie LJMNP créée avec succès')
            ->assertExitCode(0);

        $this->assertDatabaseHas('parties', [
            'name' => 'LJMNP',
            'tour' => 0,
        ]);

        $this->assertDatabaseHas('users', [
            'id' => 'LJMNP-0',
            'pseudo' => 'Arbitre',
            'email' => json_encode(['toto@titi.fr']),
        ]);

        Notification::assertSentTo([User::find('LJMNP-0')], PasswordNotification::class);
    }

    public function testNomPartieDejaExistant()
    {
        $this->artisan('nebula:partie PARTIE toto@titi.fr')
            ->assertExitCode(0);

        $this->artisan('nebula:partie PARTIE toto@titi.fr')
            ->assertExitCode(1);
    }

    public function testNomPartieEnMinuscules()
    {
        $this->artisan('nebula:partie toto toto@titi.fr')
            ->assertExitCode(0);

        $this->artisan('nebula:partie toto toto@titi.fr')
            ->assertExitCode(1);

        $this->artisan('nebula:partie TOTO toto@titi.fr')
            ->assertExitCode(1);
    }

    public function testNomPartieSuffiqueNumerique()
    {
        $this->artisan('nebula:partie TOTO2 toto@titi.fr')
            ->assertExitCode(0);
    }

    public function testMauvaisFomatNomPartie()
    {
        $this->artisan('nebula:partie NOMDEPARTIETROPLONG toto@titi.fr')
            ->assertExitCode(1);

        $this->artisan('nebula:partie 2TOTO toto@titi.fr')
            ->assertExitCode(1);

        $this->artisan('nebula:partie TO2TO toto@titi.fr')
            ->assertExitCode(1);

        $this->artisan('nebula:partie H!HGT toto@titi.fr')
            ->assertExitCode(1);
    }

    public function testMauvaisFormatEmail()
    {
        $this->artisan('nebula:partie TOTO tototiti.fr')
            ->assertExitCode(2);
    }
}
