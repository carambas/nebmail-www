<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Livewire\Livewire;
use App\Rules\CoordonneesMondesRule;
use App\Livewire\CoordonneesMondes;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CoordonneesMondesModalTest extends TestCase
{
    use RefreshDatabase;

    public function test_update_coordonnees()
    {
        $user = User::find('TEST-3');

        $this->actingAs($user);

        $value = "5=12,27\n67=11,5";

        Livewire::test(CoordonneesMondes::class)
            ->assertSet('coordonneesTexte', '')
            ->set('coordonneesTexte', $value)
            ->call('save')
            ->assertSet('coordonneesTexte', $value)
            ->assertDispatched('coordonnes-updated', coord: [
                5 => [12, 27],
                67 => [11, 5]
            ]);

        $this->assertDatabaseHas('coordonnees', [
            'user_id' => $user->id,
            'value' => $value,
        ]);
    }

    public function test_validation_error()
    {
        $user = User::find('TEST-3');
        $this->actingAs($user);

        $value = "Toto";

        Livewire::test(CoordonneesMondes::class)
            ->assertSet('coordonneesTexte', '')
            ->set('coordonneesTexte', $value)
            ->call('save')
            ->assertHasErrors(['coordonneesTexte' => CoordonneesMondesRule::class]);

            $this->assertDatabaseMissing('coordonnees', [
                'user_id' => $user->id,
            ]);
    }
}
