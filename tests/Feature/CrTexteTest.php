<?php

namespace Tests\Feature;

use Tests\GetUser;
use Tests\TestCase;
use App\Models\User;
use App\Services\Cr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\WithFaker;
use function PHPUnit\Framework\assertContains;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CrTexteTest extends TestCase
{
    use GetUser;

    public function testCrTexte()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user)->get('/crtexte')
            ->assertOk()
            ->assertSeeText("*** Compte rendu généré à partir de nebula")
            ->assertSeeText("Fin des ordres du joueur");
    }

    public function testCrTexteAutreJoueur()
    {
        $user = User::find('TEST-1');

        $this->actingAs($user)->get('/crtexte/TEST-2')
            ->assertForbidden();
    }

    public function testCrTexteArbitre()
    {
        $user = User::find('TEST-0');

        $this->actingAs($user)->get('/crtexte/TEST-2')
            ->assertOk()
            ->assertSeeText("*** Compte rendu généré à partir de nebula")
            ->assertSeeText("Fin des ordres du joueur");
    }

    public function testCrTousLesMondes()
    {
        $user = User::find('TEST-0');

        $cr = Cr::getCr(user: $user, type: 'txt', tousLesMondes: true);
        $this->assertTrue(Str::startsWith($cr, 'M_1 '));

        $cr = Cr::getCr(user: $user, type: 'txt', tousLesMondes: false);
        $this->assertTrue(Str::startsWith($cr, "*** Compte rendu généré à partir de nebula"));
    }
}
