<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Livewire\Livewire;
use App\Livewire\ExtraitsModal;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExtraitsModalTest extends TestCase
{
    private string $extraitsRaw;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_modal_visible_sur_page_nebutil()
    {
        $user = User::find('TEST-2');

        $this->actingAs($user)->get('/nebutil')
            ->assertSeeLivewire('extraits-modal');
    }

    public function test_used_view()
    {
        $user = User::find('TEST-2');

        $this->actingAs($user);

        Livewire::test(ExtraitsModal::class)
            ->assertViewIs('livewire.extraits-modal');
    }

    public function test_event_dispatched()
    {
        $user = User::find('TEST-2');
        $this->actingAs($user);

        Livewire::test(ExtraitsModal::class)
            ->set('extraitsRaw', $this->extraitsRaw)
            ->call('sendExtraits')
            ->assertDispatched('nouveaux-extraits-cr');
    }

    public function test_import_est_stocke_en_base()
    {
        $user = User::find('TEST-2');
        $this->actingAs($user);

        Livewire::test(ExtraitsModal::class)
            ->set('extraitsRaw', $this->extraitsRaw)
            ->call('sendExtraits');

        $this->assertDatabaseHas('extraits', [
            'user_id' => 'TEST-2',
            'tour' => 4,
            'monde_id' => 50,
        ]);

        $this->assertDatabaseHas('extraits', [
            'user_id' => 'TEST-2',
            'tour' => 3,
            'monde_id' => 75,
        ]);
    }

    public function test_mondes_trouves_est_rempli()
    {
        $user = User::find('TEST-2');
        $this->actingAs($user);

        Livewire::test(ExtraitsModal::class)
            ->set('extraitsRaw', $this->extraitsRaw)
            ->assertSet('mondesTrouves', "50, 75(Tour 3)");
    }

    public function test_suppression_extrait()
    {
        $user = User::find('TEST-2');
        $this->actingAs($user);

        Livewire::test(ExtraitsModal::class)
            ->set('extraitsRaw', $this->extraitsRaw)
            ->call('sendExtraits')
            ->call('deleteExtrait', 3, 75)
            ->assertDispatched('supprimer-extrait-cr');;

        $this->assertDatabaseHas('extraits', [
            'user_id' => 'TEST-2',
            'tour' => 4,
            'monde_id' => 50,
        ]);

        $this->assertDatabaseMissing('extraits', [
            'user_id' => 'TEST-2',
            'tour' => 3,
            'monde_id' => 75,
        ]);
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->extraitsRaw = <<<EXTRAIT
        "Nom du monde 50"
        M_50 (1,100) "Mon joli pseudo"! I=1 P=50(150) MP=1(+3) [4,5]
                T_100 <La Chose>
            F_6 "Mon joli pseudo" [?]=2 du M_?
                T_67 <Le trésor qui brille>
        {F_100 "Mon joli pseudo" vers M_12}

        ### Information datant du tour 3
        M_75 (24,43) "Toto le grand:2" []** P=50(150) MP=(+3) Pi=2 PC=3 PE=600 E=7/8 [5,3]
        EXTRAIT;
    }
}
