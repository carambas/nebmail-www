<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Ordre;
use App\Models\Partie;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSlashRedirectToHome()
    {
        $response = $this->get('/');

        $response->assertRedirect('/home');
    }

    public function testHome()
    {
        $response = $this->get('/home');

        $response->assertStatus(200);
    }

    public function testHomeNbOrdres()
    {
        $this->actingAs(User::find('TEST-0'))->get('/home')
            ->assertSee('TEST : 2')
            ->assertDontSeeText('Dans mon équipe');

            $this->actingAs(User::find('TEST-1'))->get('/home')
            ->assertSee('TEST : 2')
            ->assertDontSeeText('Dans mon équipe');
    }

    public function testHomeNbOrdresEquipe()
    {
        $users = Partie::find('TEST')->joueur->where('numjou', '>', 0);
        DB::table('ordres')->delete();

        foreach($users as $user) {
            $user->equipe = 2 - $user->numjou %2;
            $user->save();
        }

        $ordre = new Ordre([
            'tour' => 5,
            'content' => 'ordres du tour 5'
            ]);

        User::find('TEST-1')->ordre()->save($ordre);

        $ordre = new Ordre([
            'tour' => 5,
            'content' => 'ordres du tour 5'
            ]);

        User::find('TEST-4')->ordre()->save($ordre);

        $this->actingAs(User::find('TEST-1'))->get('/home')
            ->assertSeeTextInOrder([
                'Dans mon équipe',
                'Joueur 1', 'ordres rendus',
                'Joueur 3', 'ordres non rendus',
                ]);

        $this->actingAs(User::find('TEST-4'))->get('/home')
            ->assertSeeTextInOrder([
                'Dans mon équipe',
                'Joueur 2', 'ordres non rendus',
                'Joueur 4', 'ordres rendus',
                ]);
    }
}
