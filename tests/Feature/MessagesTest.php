<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\User;
use App\Models\Partie;
use Livewire\Livewire;
use Cmgmyr\Messenger\Models\Thread;
use App\Livewire\MessagesIndex;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use App\Notifications\MessageNotification;
use function PHPUnit\Framework\assertEquals;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;

class MessagesTest extends TestCase
{
    use WithFaker;
    protected $users;
    protected Thread $thread;
    protected Message $message;

    public function testCreateMessageView()
    {
        $user = User::find('TEST-1');
        $users = User::whereIn('id', ['TEST-0', 'TEST-2'])->get();
        $this->actingAs($user)
            ->withViewErrors(['recipients' => null])
            ->view('messenger.create', [
                'users' => $users,
                'typeMessage' => 'message',
                'dest' => null,
                'diplomatie' => 1
            ])
            ->assertSeeText('Destinataires')
            ->assertSeeTextInOrder($users->pluck('pseudo')->toArray());
    }

    public function testCreateMessageCheckDestinatairesPasDeRelation()
    {
        $user =  User::find('TEST-1');

        // Test joueur sans aucune relation
        $user->relations()->detach();
        $users = Partie::find('TEST')->joueurs_sans_arbitre->get()->except($user->id);

        $this->actingAs($user)->get('/messages/create')
            ->assertOk()
            ->assertSeeText($user->pseudo)
            ->assertSeeText('Objet')
            ->assertSee('subject')
            ->assertSee('Message de ' . $user->pseudo_with_num_jou)
            ->assertSeeText('arbitre')
            ->assertDontSeeText($users->pluck('pseudo')->toArray());
    }

    public function testCreateMessageCheckDestinatairesMemeEquipe()
    {
        $user = User::find('TEST-1');
        $user2 = User::find('TEST-3');

        // Test joueur sans aucune relation
        $user->relations()->detach();
        $user->equipe = 1;
        $user->save();

        $user2->equipe = 1;
        $user2->save();

        $autreUsers = Partie::find('TEST')->joueurs_sans_arbitre->get()->except([$user->id, $user2->id]);

        $this->actingAs($user)->get('/messages/create')
            ->assertOk()
            ->assertSeeText($user->pseudo)
            ->assertSeeText('Objet')
            ->assertSee('subject')
            ->assertSee('Message de ' . $user->pseudo_with_num_jou)
            ->assertSeeText('arbitre')
            ->assertSeeText($user2->pseudo)
            ->assertDontSeeText($autreUsers->pluck('pseudo')->toArray());
    }

    public function testCreateMessageCheckDestinatairesUneRelation()
    {
        $user =  User::find('TEST-1');

        // Test joueur avec une relation
        $user->relations()->detach();
        $user2 = User::find('TEST-2');
        $user->relations()->attach($user2->id);
        $users = Partie::find('TEST')->joueurs_sans_arbitre->get()->except([$user->id, $user2->id]);

        $this->actingAs($user)->get('/messages/create')
            ->assertOk()
            ->assertSeeText('arbitre')
            ->assertSeeText($user2->pseudo)
            ->assertDontSeeText($users->pluck('pseudo')->toArray());
    }

    // Envoi d'un nouveau message (store new Thread)
    private function initMessageViaPost($user_from, $user_to, $messageSubject = null, $messageBody = null)
    {
        if (!isset($messageSubject)) $messageSubject = $this->faker->sentence(5);
        if (!isset($messageBody)) $messageBody = $this->faker->sentence();

        $this->withExceptionHandling();
        return $this->actingAs($user_from)->post('/messages', [
            'type_message' => 'message',
            'recipients' => [$user_to->id => null],
            'subject' => $messageSubject,
            'message' => $messageBody
        ]);
    }

    public function testStoreMessageJoueurConnu()
    {
        Notification::fake();

        $user_from =  User::find('TEST-1');
        $user_to =  User::find('TEST-2');
        $user_arbitre = $user_from->partie->arbitre;
        $messageSubject = $this->faker->sentence(5);
        $messageBody = $this->faker->sentence();

        $this->initMessageViaPost($user_from, $user_to, $messageSubject, $messageBody)
            ->assertRedirect('/messages');

        // On recherche le message dans la base de données
        $this->assertEquals($messageSubject, Thread::latest('id')->first()->subject);
        $this->assertEquals($messageBody, Thread::withLastMessage()->latest('id')->first()->lastMessage->body);

        // Vérifier notification + mail
        Notification::assertSentTo([$user_arbitre, $user_to], MessageNotification::class);
    }

    public function testStoreMessageJoueurInconnu()
    {
        Notification::fake();

        $user_from =  User::find('TEST-1');
        $user_to =  User::find('TEST-3');
        $user_arbitre = $user_from->partie->arbitre;
        $messageSubject = $this->faker->sentence(5);
        $messageBody = $this->faker->sentence();

        $this->initMessageViaPost($user_from, $user_to, $messageSubject, $messageBody)
            ->assertSessionHasErrors(['recipients.TEST-3' => 'Le champ destinataires est invalide.']);

        // On recherche le message dans la base de données
        $this->assertNotEquals($messageSubject, Thread::latest('id')->first()?->subject);
        $this->assertNotEquals($messageBody, Thread::withLastMessage()->latest('id')->first()?->lastMessage->body);

        // Vérifier notification + mail
        Notification::assertNotSentTo([$user_arbitre, $user_to], MessageNotification::class);
    }

    public function testStoreMessageJoueurInconnuMaisMemeEquipe()
    {
        Notification::fake();

        $user_from = User::find('TEST-1');
        $user_to = User::find('TEST-3');

        // Joueur sans aucune relation, from et to même équipe
        $user_from->relations()->detach();
        $user_from->equipe = 1;
        $user_from->save();
        $user_to->equipe = 1;
        $user_to->save();

        $messageSubject = $this->faker->sentence(5);
        $messageBody = $this->faker->sentence();

        $this->initMessageViaPost($user_from, $user_to, $messageSubject, $messageBody)
            ->assertRedirect('/messages');

        // On recherche le message dans la base de données
        $this->assertEquals($messageSubject, Thread::latest('id')->first()?->subject);
        $this->assertEquals($messageBody, Thread::withLastMessage()->latest('id')->first()?->lastMessage->body);

        // Vérifier notification + mail
        Notification::assertSentTo([$user_to], MessageNotification::class);
    }

    public function testStoreMessageSansDiploJoueurConnu()
    {
        Notification::fake();

        $partie = Partie::find('TEST');
        $partie->diplomatie = false;
        $partie->save();
        $user_from =  User::find('TEST-1');
        $user_to =  User::find('TEST-2');

        $this->initMessageViaPost($user_from, $user_to)
            ->assertSessionHasErrors(['recipients.TEST-2' => 'Le champ destinataires est invalide.']);
    }

    public function testStoreMessageSansDiploCoequipier()
    {
        Notification::fake();

        // Partie sans diplo
        $partie = Partie::find('TEST');
        $partie->diplomatie = false;
        $partie->save();

        $user_from = User::find('TEST-3');
        $user_to = User::find('TEST-2');

        // Joueurs en relation et même équipe
        $user_from->relations()->attach($user_to);
        $user_from->equipe = 1;
        $user_from->save();
        $user_to->equipe = 1;
        $user_to->save();

        $messageSubject = $this->faker->sentence(5);
        $messageBody = $this->faker->sentence();

        $this->initMessageViaPost($user_from, $user_to, $messageSubject, $messageBody)
            ->assertRedirect('/messages');

        // On recherche le message dans la base de données
        $this->assertEquals($messageSubject, Thread::latest('id')->first()?->subject);
        $this->assertEquals($messageBody, Thread::withLastMessage()->latest('id')->first()?->lastMessage->body);

        // Vérifier notification + mail
        Notification::assertSentTo([$user_to], MessageNotification::class);
    }

    // On s'assure que seuls les participants d'un thread le voient
    private function createMessageRandom()
    {
        $users = Partie::find('TEST')->joueur->shuffle();

        $thread = Thread::factory()->create();
        $thread->participants()->saveMany([
            Participant::factory(['user_id' => $users[0]->id])->make(),
            Participant::factory(['user_id' => $users[1]->id])->make(),
        ]);
        $this->message = Message::factory()->for($thread)->for($users[0])->create();

        $this->users = $users;
        $this->thread = $thread;
    }

    private function createMessage($user_from, $user_to)
    {
        $thread = Thread::factory()->create();
        $thread->participants()->saveMany([
            Participant::factory(['user_id' => $user_from->id])->make(),
            Participant::factory(['user_id' => $user_to->id])->make(),
        ]);
        $this->message = Message::factory()->for($thread)->for($user_from)->create();
        $this->thread = $thread;
    }

    public function testIndexMessage()
    {
        $this->createMessageRandom();

        $user1 = $this->users[0];
        $user2 = $this->users[1];
        $user_autre = $this->users[2];
        $thread = $this->thread;

        $this->actingAs($user1)->get('/messages')
            ->assertSeeText($thread->subject);

        $this->actingAs($user2)->get('/messages')
            ->assertSeeText($thread->subject);

        $this->actingAs($user_autre)->get('/messages')
            ->assertDontSeeText($thread->subject);
    }

    public function testIndexRemoveMessage()
    {
        $this->createMessageRandom();

        $this->actingAs($this->users[0])->get('/messages')
            ->assertSeeText($this->thread->subject);

        Livewire::test(MessagesIndex::class)
            ->assertViewIs('livewire.messages-index')
            ->assertSee($this->thread->subject)
            ->call('delete', $this->thread)
            ->assertDontSee($this->thread->subject)
            ->call('toggleDeleted')
            // On voit le thread supprimé sur la page des messages supprimés
            ->assertSee($this->thread->subject)
            ->call('toggleDeleted')
            ->assertDontSee($this->thread->subject);

        $this->createMessageRandom();

        $this->actingAs($this->users[0])->get('/messages')
            ->assertSeeText($this->thread->subject);

        Livewire::test(MessagesIndex::class)
            ->assertViewIs('livewire.messages-index')
            ->assertSee($this->thread->subject)
            ->call('delete', $this->thread)
            ->assertDontSee($this->thread->subject)
            ->call('toggleDeleted')
            // On voit le thread supprimé sur la page des messages supprimés
            ->assertSee($this->thread->subject)
            ->call('toggleDeleted')

            ->call('toggleDeleted')
            ->assertSee($this->thread->subject)
            // On restaure le thread supprimé
            ->call('restore', $this->thread)
            ->assertDontSee($this->thread->subject)
            ->call('toggleDeleted')
            // et on vérifie qu'il revient sur la vue principale
            ->assertSee($this->thread->subject);

        $this->actingAs($this->users[1])->get('/messages')
            ->assertSeeText($this->thread->subject);

        Livewire::test(MessagesIndex::class)
            ->assertViewIs('livewire.messages-index')
            ->assertSee($this->thread->subject)
            ->call('delete', $this->thread)
            ->assertDontSee($this->thread->subject)
            ->call('toggleDeleted')
            ->assertSee($this->thread->subject)
            ->call('toggleDeleted')
            ->assertDontSee($this->thread->subject);

        $this->get('/messages')
            ->assertDontSeeText($this->thread->subject);
    }

    public function getLastThreadId()
    {
        return Thread::latest('id')->first()->id;
    }

    public function getLastMessage()
    {
        return Message::latest('id')->first();
    }

    public function testReponseMessage()
    {
        Notification::fake();

        $user_from =  User::find('TEST-1');
        $user_to =  User::find('TEST-2');
        $messageBody = $this->faker->sentence();

        $this->createMessage($user_from, $user_to);
        $url = "/messages/{$this->thread->id}";

        $this->withExceptionHandling();
        $this->actingAs($user_from)
            ->put($url, [
                'message' => $messageBody
            ])
            ->assertSessionHasNoErrors();

        // On vérifie qu'on a bien un nouveau message
        $lastMessage = $this->getLastMessage();
        $this->assertEquals($lastMessage->id, $this->message->id + 1);
        $this->assertEquals($lastMessage->body, $messageBody);
    }

    public function testReponseMessagePasDansLesDestinataires()
    {
        Notification::fake();

        $user_from =  User::find('TEST-1');
        $user_to =  User::find('TEST-2');
        $user_ko =  User::find('TEST-3');
        $messageBody = $this->faker->sentence();

        $this->createMessage($user_from, $user_to);
        $url = "/messages/{$this->thread->id}";

        $this->withExceptionHandling();
        $this->actingAs($user_ko)
            ->put($url, [
                'message' => $messageBody
            ])
            ->assertSessionHas('error_message');

        // On vérifie qu'on n'a pas de nouveau message
        $lastMessage = $this->getLastMessage();
        $this->assertEquals($lastMessage->id, $this->message->id);
        $this->assertNotEquals($lastMessage->body, $messageBody);
    }

    public function testMessageMarqueLu()
    {
        $this->createMessageRandom();

        $this->assertNull($this->thread->participants[0]->last_read);
        $this->assertNull($this->thread->participants[1]->last_read);

        $this->actingAs($this->users[0])->get("/messages/{$this->thread->id}")
            ->assertSee($this->thread->subject);

        $this->thread->refresh();

        $this->assertNotNull($this->thread->participants[0]->last_read);
        $this->assertNull($this->thread->participants[1]->last_read);
    }

    public function testDestinatairesDiscussionAPlusieurs()
    {
        $auteur = User::find('TEST-0');
        $dest1 = User::find('TEST-1');
        $dest2 = User::find('TEST-2');

        $thread = Thread::factory()->create();

        $thread->participants()->saveMany([
            Participant::factory(['user_id' => $auteur->id])->make(),
            Participant::factory(['user_id' => $dest1->id])->make(),
            Participant::factory(['user_id' => $dest2->id])->make(),
        ]);

        Message::factory()->for($thread)->for($auteur)->create();

        $this->actingAs($auteur)->get("/messages/{$thread->id}")
            ->assertSee($thread->subject)
            ->assertSee("Discussion à plusieurs avec {$dest1->pseudo}, {$dest2->pseudo}");

        $this->actingAs($dest1)->get("/messages/{$thread->id}")
            ->assertSee($thread->subject)
            ->assertSee("Discussion à plusieurs avec {$auteur->pseudo}, {$dest2->pseudo}");

        $this->actingAs($dest2)->get("/messages/{$thread->id}")
            ->assertSee($thread->subject)
            ->assertSee("Discussion à plusieurs avec {$auteur->pseudo}, {$dest1->pseudo}");
    }

    public function testMessageRecuBadgeNonrepondu()
    {
        $user1 = User::find('TEST-1');
        $user2 = User::find('TEST-2');

        $thread = Thread::factory()->create();
        $thread->participants()->saveMany([
            Participant::factory(['user_id' => $user1->id])->make(),
            Participant::factory(['user_id' => $user2->id])->make(),
        ]);

        // user1 envoie un message à user2
        Message::factory()->for($thread)->for($user1)->create();

        $this->actingAs($user1)->get("/messages")
            ->assertDontSeeText('Non répondu');

        $this->actingAs($user2)->get("/messages")
            ->assertDontSeeText('Non répondu'); // Message non lu => le libellé "Non répondu" n'apparait pas

        $this->actingAs($user2)->get("/messages/{$thread->id}");

        $this->actingAs($user2)->get("/messages")
            ->assertSeeText('Non répondu'); // Message lu => le libellé "Non répondu" apparait

        // Réponse de user2
        $this->travel(5)->minutes();
        Message::factory()->for($thread)->for($user2)->create();

        $this->actingAs($user1)->get("/messages")
            ->assertSeeText($thread->subject)
            ->assertDontSeeText('Non répondu'); // Message non lu => le libellé "Non répondu" n'apparait pas

        $this->actingAs($user2)->get("/messages")
            ->assertSeeText($thread->subject)
            ->assertDontSeeText('Non répondu');

        $this->actingAs($user1)->get("/messages/{$thread->id}");

        $this->actingAs($user1)->get("/messages")
            ->assertSeeText($thread->subject)
            ->assertSeeText('Non répondu'); // Message lu => le libellé "Non répondu" apparait

    }

    public function testMessageGeneralAffichePasDeBadgeNonrepondu()
    {
        $thread = Thread::factory()
            ->messageGeneral()
            ->has(Participant::factory()->count(2))
            ->create();

        // Un joueur envoie un message général
        Message::factory()
            ->for($thread)
            ->for($thread->participants->first()->user)
            ->create();

        foreach ($thread->participants as $participant) {
            $this->actingAs($participant->user)->get("/messages/{$thread->id}");
            $this->actingAs($participant->user)->get("/messages")
                ->assertSeeText($thread->subject)
                ->assertDontSeeText('Non répondu');
        }
    }

    public function testRumeurAffichePasDeBadgeNonrepondu()
    {
        $thread = Thread::factory()
            ->rumeur()
            ->has(Participant::factory()->count(2))
            ->create();

        // Un joueur envoie une rumeur
        Message::factory()
            ->for($thread)
            ->for($thread->participants->first()->user)
            ->create();

        foreach ($thread->participants as $participant) {
            $this->actingAs($participant->user)->get("/messages/{$thread->id}");
            $this->actingAs($participant->user)->get("/messages")
                ->assertSeeText($thread->subject)
                ->assertDontSeeText('Non répondu');
        }
    }

    // Tests messages non répondu
    public function testFlagIgnorerNonReponduSupprimeAvecNouveauMessageDansLeThread()
    {
        $user1 = User::find('TEST-1');
        $user2 = User::find('TEST-2');

        $thread = Thread::factory()->create();
        $urlThread = "/messages/{$thread->id}";
        $thread->participants()->saveMany([
            Participant::factory(['user_id' => $user1->id])->make(),
            Participant::factory(['user_id' => $user2->id])->make(),
        ]);

        Message::factory()->for($thread)->for($user1)->create();    // user1 envoie un message à user2

        $this->actingAs($user2);
        $this->get($urlThread);                                     // user2 le lit

        Livewire::test(MessagesIndex::class)
            ->assertSeeText('Non répondu')
            ->call('removeNonLu', $thread)                          // clic sur le libellé Non lu pour l'ignorer
            ->assertDontSeeText('Non répondu');                     // Libellé disparu

        $this->actingAs($user1)                                     // user1 envoie un nouveau message sur le thread
            ->put($urlThread, [
                'message' => "Message"
            ]);

        $this->actingAs($user2);
        Livewire::test(MessagesIndex::class)
            ->assertSeeText('Non répondu');                         // Le libellé est revenu
    }
}
