<?php

namespace Tests\Feature;

use Tests\GetUser;
use Tests\TestCase;
use App\Models\User;
use App\Models\Ordre;
use App\Mail\OrdresAR;
use Livewire\Livewire;
use App\Jobs\CallSlack;
use App\Events\OrdresRendus;
use App\Scopes\NotDraftScope;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Bus;
use App\Livewire\EditionOrdres;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Event;

class NebutilOrdresTest extends TestCase
{
    use GetUser;

    public function testNebutilOrdresBrouillon()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        DB::table('ordres')->delete();

        $user = User::find('TEST-2');
        $this->actingAs($user);
        $ordre_content = "Brouillon ligne 1\nbrouillon ligne 2";

        Livewire::test(EditionOrdres::class, ['userId' => $user->id])
            ->call('save', $ordre_content, true);

        Mail::assertNothingQueued();
        Bus::assertNotDispatched(CallSlack::class);
        Event::assertNotDispatched(OrdresRendus::class);

        $this->assertDatabaseHas('ordres', [
            'user_id' => $user->id,
            'tour' => 5,
            'brouillon' => true,
        ]);

        $user->refresh();
        $this->assertEmpty($user->ordre);
        $this->assertNotEmpty($user->ordreBrouillon);
        $this->assertEquals($ordre_content, $user->ordreBrouillon->content);
        $this->assertEquals(0, $user->partie->ordre->count());
        $this->assertEquals(true, $user->ordre()->withoutGlobalScope(NotDraftScope::class)->first()->brouillon);
    }

    public function testNebutilOrdresEnvoi()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        DB::table('ordres')->delete();

        $user = User::find('TEST-2');
        $this->actingAs($user);

        Livewire::test(EditionOrdres::class, ['userId' => $user->id])
            ->call('save', "Coucou\nF 5 M 12", false);

        Mail::assertQueued(OrdresAR::class, 2); // On attend 2 mails, un pour le joueur, un pour l'arbitre
        Bus::assertDispatched(CallSlack::class);
        Event::assertDispatched(OrdresRendus::class);

        $this->assertDatabaseHas('ordres', [
            'user_id' => $user->id,
            'tour' => 5,
            'brouillon' => false,
        ]);

        $user->refresh();
        $this->assertNotEmpty($user->ordre);
        $this->assertEmpty($user->ordreBrouillon);
        $this->assertEquals(1, $user->partie->ordre->count());
        $this->assertEquals(false, $user->ordre()->withoutGlobalScope(NotDraftScope::class)->first()->brouillon);
    }

    public function testNebutilOrdresBrouillonPuisEnvoiPuisBrouillon()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        DB::table('ordres')->delete();

        $user = User::find('TEST-2');
        $this->actingAs($user);

        Livewire::test(EditionOrdres::class, ['userId' => $user->id])
            ->call('save', $content1 = "Brouillon", true);
        $user->refresh();
        $this->assertEquals($content1, $user->ordre()->withoutGlobalScope(NotDraftScope::class)->first()->content);
        $this->assertEquals(true, $user->ordre()->withoutGlobalScope(NotDraftScope::class)->first()->brouillon);
        $this->assertEquals($content1, $user->ordreBrouillon->content);
        $this->assertEmpty($user->ordre);
        $this->assertNotEmpty($user->ordreBrouillon);

        Livewire::test(EditionOrdres::class, ['userId' => $user->id])
            ->call('save', $content2 = "définitif", false);
        $user->refresh();
        $this->assertEquals($content2, $user->ordre()->withoutGlobalScope(NotDraftScope::class)->first()->content);
        $this->assertEquals(false, $user->ordre()->withoutGlobalScope(NotDraftScope::class)->first()->brouillon);
        $this->assertNotEmpty($user->ordre);
        $this->assertEmpty($user->ordreBrouillon);

        Livewire::test(EditionOrdres::class, ['userId' => $user->id])
            ->call('save', $ordres_brouillon_txt = "Brouillon2", true);
        $user->refresh();
        $this->assertEquals($ordres_brouillon_txt, $user->ordre()->withoutGlobalScope(NotDraftScope::class)->first()->content);
        $this->assertEquals($ordres_brouillon_txt, $user->ordreBrouillon->content);
        $this->assertEquals($content2, $user->ordre->content);
        $this->assertEquals(true, $user->ordre()->withoutGlobalScope(NotDraftScope::class)->first()->brouillon);
        $this->assertNotEmpty($user->ordre);
        $this->assertNotEmpty($user->ordreBrouillon);

        $this->assertDatabaseHas('ordres', [
            'user_id' => $user->id,
            'tour' => 5,
            'brouillon' => false,
        ]);

        $this->assertDatabaseHas('ordres', [
            'user_id' => $user->id,
            'tour' => 5,
            'brouillon' => true,
            'content' => $ordres_brouillon_txt
        ]);
    }
}
