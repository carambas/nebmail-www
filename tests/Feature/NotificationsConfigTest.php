<?php

namespace Tests\Feature;

use Tests\GetUser;
use Tests\TestCase;
use Livewire\Livewire;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;
use App\Livewire\Arbitre\NotificationsConfig;

class NotificationsConfigTest extends TestCase
{
    use GetUser;

    public function test_notification_modification_champs_webhooks()
    {
        $urlOrdres = 'http://host/ordres';
        $urlBot = 'http://host/bot';

        $this->actingAs($this->getArbitre());
        Livewire::test(NotificationsConfig::class)
            ->set('webhook_ordres_rendus', $urlOrdres)
            ->set('webhook_bot', $urlBot)
            ->call('notificationsFormSubmit')
            ->assertHasNoErrors(['webhook_ordres_rendus', 'webhook_bot']);

        $this->assertDatabaseHas('parties', [
            'name' => 'TEST',
            'webhook_ordres_rendus' => $urlOrdres,
            'discord_url' => $urlBot,
        ]);
    }

    public function test_notification_modification_champs_webhooks_erreur_validation()
    {
        $this->actingAs($this->getArbitre());
        Livewire::test(NotificationsConfig::class)
            ->set('webhook_ordres_rendus', 'url1')
            ->set('webhook_bot', 'url2')
            ->call('notificationsFormSubmit')
            ->assertHasErrors(['webhook_ordres_rendus', 'webhook_bot']);
    }

    public function test_notification_envoi_message_bot()
    {
        $urlOrdres = 'http://host/ordres';
        $urlBot = 'http://host/bot';
        $testTexte = "Hello, ceci est un test";

        $this->actingAs($this->getArbitre());

        Http::fake();

        Livewire::test(NotificationsConfig::class)
            ->set('webhook_ordres_rendus', $urlOrdres)
            ->set('webhook_bot', $urlBot)
            ->set('testTexte', $testTexte)
            ->call('testBot');

        Http::assertSent(fn (Request $request) => $request->url() === $urlBot);
    }
}
