<?php

namespace Tests\Feature;

use Tests\GetUser;
use Tests\TestCase;
use App\Models\User;
use App\Mail\OrdresAR;
use Livewire\Livewire;
use App\Jobs\CallSlack;
use App\Events\OrdresRendus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Bus;
use App\Livewire\EditionOrdres;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\WithFaker;
use function PHPUnit\Framework\assertContains;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrdresTest extends TestCase
{
    use GetUser;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetOrdresRendus()
    {
        $user = $this->getJoueurSansOrdres();
        $response = $this->actingAs($user)->get('/ordres');

        $response->assertStatus(200);

        $response->assertSeeTextInOrder([
            "Nombre de joueurs ayant rendus des ordres",
            "Pas d'ordres stockés pour ce tour",
        ], false);

        $response->assertSeeTextInOrder([
            "###########",
            "# Ordres du joueur ",
            "\"{$user->pseudo}:{$user->numjou}\"",
            "# Ordres saisis sur le site nebmail",
            "###########",
            ]);

        $response->assertSeeText("Mes ordres");
        $response->assertSeeText("Envoyer mes ordres");
        $response->assertSeeText("Simulation");
        $response->assertViewHasAll(['nbOrdres', 'dateOrdres', 'ordres', 'joueur_pour']);
    }

    public function testGetOrdresRendusArbitre()
    {
        $user = User::find('TEST-0');
        $user2 = User::find('TEST-1');
        $response = $this->actingAs($user)->get('/ordres/TEST-1');

        $response->assertStatus(200);

        $response->assertSeeTextInOrder([
            "Nombre de joueurs ayant rendus des ordres",
            "Ordres de l'arbitre pour le compte de {$user2->pseudo}",
        ], false);

        $response->assertViewHasAll(['nbOrdres', 'dateOrdres', 'ordres', 'joueur_pour']);

    }

    public function testGetOrdresNonRendus()
    {
        $user = $this->getJoueurAvecOrdres();

        $response = $this->actingAs($user)->get('/ordres');

        $response->assertStatus(200);
        $response->assertSeeTextInOrder([
            "Nombre de joueurs ayant rendus des ordres",
            "Dernière version d'ordres enregistrés",
            "Mes ordres",
        ], false);

        $response->assertViewHasAll(['nbOrdres', 'dateOrdres', 'ordres', 'joueur_pour']);
    }

    public function testGetOrdresAutreJoueur()
    {
        $user = User::find('TEST-1');

        $response = $this->actingAs($user)->get('/ordres/TEST-2');

        $response->assertForbidden();
    }

    public function testGetOrdresMemeJoueur()
    {
        $user = User::find('TEST-1');

        $response = $this->actingAs($user)->get('/ordres/TEST-1');

        $response->assertOk();
    }

    public function testPostOrdres()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        $user = User::find('TEST-1');
        $this->actingAs($user)
            ->post('/ordres/TEST-1', ['ordres' => 'Coucou\nF 5 M 12'])
            ->assertRedirect('/ordres');

        Mail::assertQueued(OrdresAR::class, 2); // On attend 2 mails, un pour le joueur, un pour l'arbitre
        Bus::assertDispatched(CallSlack::class);
        Event::assertDispatched(OrdresRendus::class);

    }

    public function testPostOrdreKOTropLong()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        $user = User::find('TEST-1');
        $this->actingAs($user)
            ->post('/ordres/TEST-1', ['ordres' => 'Coucou\nF 5 M 12\Cette ligne est beaucoup trop longue et va revenir à la ligne dans le récap des ordres en erreur\n'])
            ->assertRedirect('/ordres');

        Mail::assertQueued(OrdresAR::class, 2); // On attend 2 mails, un pour le joueur, un pour l'arbitre
        Bus::assertDispatched(CallSlack::class);
        Event::assertDispatched(OrdresRendus::class);

    }

    public function testPostOrdresAutreJoueur()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        $user = User::find('TEST-1');
        $response = $this->actingAs($user)->post('/ordres/TEST-2', ['ordres' => 'Coucou\nF 5 M 12']);
        $response->assertForbidden();
    }

    public function testPostOrdresArbitre()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        $user_arbitre = User::find('TEST-0');
        $user_pour = User::find('TEST-2');

        $response = $this->actingAs($user_arbitre)
            ->post("/ordres/{$user_pour->id}", ['ordres' => 'Coucou\nF 5 M 12'])
            ->assertRedirect("ordres/{$user_pour->id}");

        // On attend 2 mails, un pour le joueur, un pour l'arbitre
        Mail::assertQueued(OrdresAR::class, 2);

        Mail::assertQueued(OrdresAR::class, function ($mail) use ($user_pour) {
            return $mail->hasTo($user_pour->email);
        });

        Mail::assertQueued(OrdresAR::class, function ($mail) use ($user_arbitre) {
            return $mail->hasTo($user_arbitre->email);
        });

        Bus::assertDispatched(CallSlack::class);
        Event::assertDispatched(OrdresRendus::class);
    }

}
