<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Event;

class SimulEvalTest extends TestCase
{
    public function testPostSimulOrdres()
    {
        $user = User::find('TEST-1');
        $response = $this->actingAs($user)->post('ordres/TEST-1', [
            'simul' => 1,
            'ordres' => 'Coucou\nF 5 M 12'
            ]);
        $response->assertOk();
        $response->assertSee('Coucou')
            ->assertSee('F 5 M 12')
            ->assertViewIs('ordres')
            ->assertViewHasAll(['ordres', 'nbOrdres', 'dateOrdres', 'joueur_pour', 'cr', 'boutonRetour']);
    }

    public function testPostSimulOrdresAutreJoueur()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        $user = User::find('TEST-1');
        $response = $this->actingAs($user)->post('ordres/TEST-2', [
            'simul' => 1,
            'ordres' => 'Coucou\nF 5 M 12'
            ]);
        $response->assertForbidden();
    }

    public function testPostSimulOrdresArbitre()
    {
        Event::fake();
        Bus::fake();
        Mail::fake();

        $user = User::find('TEST-0');
        $response = $this->actingAs($user)->post('ordres/TEST-2', [
            'simul' => 1,
            'ordres' => 'Coucou\nF 5 M 12'
            ]);
            $response->assertOk();
            $response->assertSee('Coucou')
                ->assertSee('F 5 M 12')
                ->assertViewIs('ordres')
                ->assertViewHasAll(['ordres', 'nbOrdres', 'dateOrdres', 'joueur_pour', 'cr', 'boutonRetour']);
    }
}
