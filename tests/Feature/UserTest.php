<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\GetUser;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use function PHPUnit\Framework\assertContains;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUserHomePage()
    {
        //$user = User::factory()->create();
        $user = User::find('TEST-1');

        $response = $this->actingAs($user)->get('/home');

        $response->assertStatus(200);
        $response->assertSeeText("{$user->name_partie} {$user->pseudo}");
        $response->assertSeeText('Partie ' . $user->name_partie);
        $response->assertSeeTextInOrder([
            'Statistiques',
            'Ordres rendus',
            $user->name_partie
        ]);
    }


}
