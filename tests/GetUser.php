<?php

namespace Tests;

use App\Models\User;
use App\Models\Partie;

trait GetUser
{
    private function getJoueurSansOrdres()
    {
        return Partie::find('TEST')
                    ->joueurs_sans_arbitre
                    ->inRandomOrder()
                    ->get()
                    ->where('ordre', '=', null)
                    ->first();
    }

    private function getJoueurAvecOrdres()
    {
        return Partie::find('TEST')
                    ->joueurs_sans_arbitre
                    ->inRandomOrder()
                    ->get()
                    ->where('ordre', '!=', null)
                    ->first();
    }

    private function getArbitre()
    {
        return User::find('TEST-0')->first();
    }
}
