<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCaseWithoutDatabase extends BaseTestCase
{
    use CreatesApplication;

    protected $seed = true;

}
