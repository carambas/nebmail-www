<?php

namespace Tests\Unit;

use App\Services\CoordonneesService;
use PHPUnit\Framework\TestCase;

class CoordonneesTest extends TestCase
{
    public function test_basique()
    {
        $value = "5=10,15\n4=12,37";
        $this->assertTrue((new CoordonneesService($value))->checkFormat());
    }

    public function test_trim()
    {
        $value = " 5=10,15 \n \n\n4= 12, 37  ";
        $this->assertTrue((new CoordonneesService($value))->checkFormat());
    }

    public function test_float_ko()
    {
        $value = "5=10,15\n4=12,37.3";
        $this->assertFalse((new CoordonneesService($value))->checkFormat());
    }

    public function test_array()
    {
        $value = " 5=10,15 \n \n\n4= 12, 37  ";
        $result = (new CoordonneesService($value))->mondesAsKeys()->toArray();
        $this->assertIsArray($result);
        $this->assertEquals([
            5 =>  [10,15],
            4 => [12,37]
        ], $result);
    }

    public function test_string()
    {
        $value = " 5=10,15 \n \n\n4= 12, 37  ";
        $expected = "5=10,15\n4=12,37";
        $this->assertEquals($expected, (new CoordonneesService($value))->toString());
    }

    public function test_json()
    {
        $value = " 5=10,15 \n \n\n4= 12, 37  ";
        $json = (new CoordonneesService($value))->mondesAsKeys()->toJson();

        $this->assertJson($json);
        $this->assertJsonStringEqualsJsonString('{"5":[10,15],"4":[12,37]}', $json);

    }
}
