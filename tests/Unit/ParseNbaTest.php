<?php

namespace Tests\Unit;

use App\Services\NebData\Monde;
use App\Services\NebData\Flotte;
use App\Services\NebData\Joueur;
use App\Services\NebData\Tresor;
use App\Services\NebData\NebData;
use Tests\TestCaseWithoutDatabase;
use Illuminate\Support\Facades\Storage;


//class ParseNbaTest extends TestCaseWithoutDatabase
class ParseNbaTest extends TestCaseWithoutDatabase
{
    public function testSectionGeneral()
    {

        $tourVerrouille = random_int(0, 1);
        $piratageMultiClasse = random_int(0, 1);
        $affichageScore = random_int(0, 1);
        $affichageScore = random_int(0, 1);
        $bombesInterdites = random_int(0, 1);
        $mevaPourTous = random_int(0, 1);
        $diplomatieInterdite = random_int(0, 1);
        $pillagesInterdits = random_int(0, 1);
        $tirIndustries = random_int(0, 1);
        $emigrationSansHasard = random_int(0, 1);
        $partieCruel = random_int(0, 1);

        $rawGeneral = <<<GENERAL
[GENERAL]
NomPartie=DECONF
Tour=21
TourFinal=22
NomArbitre=Arbitre DECONF
EmailArbitre=toto@gmail.com
PasswordPartie=password
NbRotCode=5
DateDebut=30/04/2020
NbMondes=390
NbFlottes=410
NbJou=20
TaillePlanX=27
TaillePlanY=23
AdresseSM=nebmail@ludimail.net
Mds=10,20,30
Lock=$tourVerrouille
VersionSM=3
PiratageMultiClasse=$piratageMultiClasse
AffichageScore=$affichageScore
BombesInterdites=$bombesInterdites
MEVAPourTous=$mevaPourTous
DiploInterdite=$diplomatieInterdite
PillagesInterdits=$pillagesInterdits
TirIndustries=$tirIndustries
EmigrationSansHasard=$emigrationSansHasard
PartieCRUEL=$partieCruel
GENERAL;

        Storage::fake('data');
        Storage::disk('data')->put('test.nba', $rawGeneral);

        $nba = new NebData('test.nba');

        $this->assertEquals('DECONF', $nba->nomPartie);
        $this->assertEquals(21, $nba->tour);
        $this->assertEquals(22, $nba->tourFinal);
        $this->assertEquals('Arbitre DECONF', $nba->nomArbitre);
        $this->assertEquals("toto@gmail.com", $nba->emailArbitre);
        $this->assertEquals('password', $nba->passwordPartie);
        $this->assertEquals('30/04/2020', $nba->dateDebut);
        $this->assertEquals(0, $nba->numJoueur);
        $this->assertEquals(390, $nba->nbMondes);
        $this->assertEquals(410, $nba->nbFlottes);
        $this->assertEquals(20, $nba->nbJoueurs);
        $this->assertEquals(27, $nba->longX);
        $this->assertEquals(23, $nba->longY);
        $this->assertEquals([10,20,30], $nba->mondesDepartsSupplementaires);
        $this->assertEquals($tourVerrouille, $nba->tourVerrouille);
        $this->assertEquals($piratageMultiClasse, $nba->piratageMultiClasse);
        $this->assertEquals($affichageScore, $nba->affichageScore);
        $this->assertEquals($bombesInterdites, $nba->bombesInterdites);
        $this->assertEquals($mevaPourTous, $nba->mevaPourTous);
        $this->assertEquals($diplomatieInterdite, $nba->diplomatieInterdite);
        $this->assertEquals($pillagesInterdits, $nba->pillagesInterdits);
        $this->assertEquals(!$tirIndustries, $nba->tirIndustriesInterdits);
        $this->assertEquals($emigrationSansHasard, $nba->emigrationSansHasard);
        $this->assertEquals($partieCruel, $nba->partieCruel);
    }

    public function testSectionFlotte()
    {
        $numFlotte = 28;
        $chemin = [];
        for ($i = 0; $i < 9; $i++) {
            $chemin[] = random_int(1, 1024);
        }
        $chemin_str = implode(separator: ',', array: $chemin);

        $rawFlotte = <<<FLOTTE
[F_$numFlotte]
F=1,2,3,4,5,6,7,8,9,10,11,0,1,0,1,0,1,0,1
Chemin=$chemin_str
FLOTTE;

        $content = parse_ini_string($rawFlotte, true,  INI_SCANNER_RAW);
        ($flotte = new Flotte)->fillFromNba($numFlotte, $content['F_28']);

        $this->assertEquals($numFlotte, $flotte->numero);
        $this->assertEquals(1, $flotte->localisation);
        $this->assertEquals(2, $flotte->vc);
        $this->assertEquals(3, $flotte->vt);
        $this->assertEquals(4, $flotte->mp);
        $this->assertEquals(5, $flotte->populationsNonConverties);
        $this->assertEquals(6, $flotte->populationsConverties);
        $this->assertEquals(7, $flotte->robots);
        $this->assertEquals(8, $flotte->proprietaire);
        $this->assertEquals(9, $flotte->ancienProprietaire);
        $this->assertEquals(10, $flotte->ordreExclusif);
        $this->assertEquals(11, $flotte->cible);
        $this->assertEquals(false, $flotte->isEnPaix);
        $this->assertEquals(true, $flotte->isTransporteBombe);
        $this->assertEquals(false, $flotte->isCapturee);
        $this->assertEquals(true, $flotte->isPiratee);
        $this->assertEquals(false, $flotte->isCadeau);
        $this->assertEquals(true, $flotte->isEmbuscade);
        $this->assertEquals(false, $flotte->isDechagePc);
        $this->assertEquals(true, $flotte->isExploration);

        $this->assertEquals($chemin, $flotte->chemin);

    }

    public function testSectionJoueur()
    {
        $rawJoueur = <<<JOUEUR
[J_2]
Pseudo=Joueur 2
Nom=Nom Joueur 2
Email=gangloff@ludimail.org,toto@titi.fr
Equipe=1,2
Md=25
Score=255
J=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,50,100,260
ConNom=2
Contact=
Allies=1,3
Chargeurs=1,4
ChargeursPop=1,5
DechargeursPop=1,6
Pilleurs=1,7
DetailScore=91,74,58
Stat=
JOUEUR;

        $content = parse_ini_string($rawJoueur, true,  INI_SCANNER_RAW);
        ($joueur = new Joueur)->fillFromNba(2, $content['J_2']);

        $this->assertEquals('Joueur 2', $joueur->pseudo);
        $this->assertEquals('Nom Joueur 2', $joueur->nom);
        $this->assertEquals(['gangloff@ludimail.org', 'toto@titi.fr'], $joueur->email);
        $this->assertEquals(25, $joueur->md);
        $this->assertEquals(1, $joueur->classeId);
        $this->assertEquals(2, $joueur->tourJihad);
        $this->assertEquals(3, $joueur->numJouJihad);
        $this->assertEquals(4, $joueur->dep);
        $this->assertEquals(5, $joueur->att);
        $this->assertEquals(6, $joueur->def);
        $this->assertEquals(7, $joueur->rad);
        $this->assertEquals(8, $joueur->ali);
        $this->assertEquals(9, $joueur->car);
        $this->assertEquals(10, $joueur->depReste);
        $this->assertEquals(11, $joueur->attReste);
        $this->assertEquals(12, $joueur->defReste);
        $this->assertEquals(13, $joueur->radReste);
        $this->assertEquals(14, $joueur->aliReste);
        $this->assertEquals(15, $joueur->carReste);
        $this->assertEquals(50, $joueur->ca);
        $this->assertEquals(100, $joueur->cd);
        $this->assertEquals(260, $joueur->score);
        $this->assertEquals([2], $joueur->connuDeNom);
        $this->assertEquals([], $joueur->contacts);
        $this->assertEquals([1,3], $joueur->allies);
        $this->assertEquals([1,4], $joueur->chargeurs);
        $this->assertEquals([1,5], $joueur->chargeursPop);
        $this->assertEquals([1,6], $joueur->dechargeursPop);
        $this->assertEquals([1,7], $joueur->pilleurs);
        $this->assertEquals([91,74,58], $joueur->detailsScore);
    }

    public function testSectionMonde()
    {
        $rawMonde = <<<MONDE
[M_1]
Coord=3,9
C=5,12
M=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,1,,1,,1
Connu=2,3
AVuConnect=2,5,6
AVuCoord=,2,,5
StationMonde=2,4
1erPr=5
Possession=,2
Nom=Le nom de ce monde
[M_2]
Coord=5,7
C=13,29,40
MONDE;

        $content = parse_ini_string($rawMonde, true,  INI_SCANNER_RAW);
        ($monde = new Monde)->fillFromNba(1, $content['M_1']);

        $this->assertEquals($monde->x, 3);
        $this->assertEquals($monde->y, 9);
        $this->assertEquals($monde->connexions, [5, 12]);
        $this->assertEquals($monde->estVisible, [2, 3]);
        $this->assertEquals($monde->aStationne, [2, 4]);
        $this->assertEquals($monde->connexionsVues, [2, 5, 6]);
        $this->assertEquals($monde->possessionHisto, [0,2]);
        $this->assertEquals($monde->aVuCoord, [0,2,0,5]);
        $this->assertEquals($monde->proprietaire, 5);
        $this->assertEquals($monde->premierProprietaire, 5);
        $this->assertEquals($monde->duree, 1);
        $this->assertEquals($monde->proprietaireConvertis, 2);
        $this->assertEquals($monde->ancienProprietaire, 16);
        $this->assertEquals($monde->industries, 6);
        $this->assertEquals($monde->industriesActives, 17);
        $this->assertEquals($monde->vi, 12);
        $this->assertEquals($monde->actionVi, 0);
        $this->assertEquals($monde->cibleVi, 0);
        $this->assertEquals($monde->populations, 7);
        $this->assertEquals($monde->convertis, 8);
        $this->assertEquals($monde->robots, 9);
        $this->assertEquals($monde->limitePopulation, 10);
        $this->assertEquals($monde->vp, 13);
        $this->assertEquals($monde->actionVp, 0);
        $this->assertEquals($monde->populationsMortes, 0);
        $this->assertEquals($monde->convertisMorts, 0);
        $this->assertEquals($monde->robotsDetruits, 0);
        $this->assertEquals($monde->capaciteMiniere, 3);
        $this->assertEquals($monde->nbDechargementPc, 4);
        $this->assertEquals($monde->nbPcDecharges, 0);
        $this->assertEquals($monde->mp, 11);
        $this->assertEquals($monde->populationsDisponibles, 18);
        $this->assertEquals($monde->nbPillage, 14);
        $this->assertEquals($monde->toursRecupPillage, 15);
        $this->assertEquals($monde->isCapture, true);
        $this->assertEquals($monde->isCadeau, false);
        $this->assertEquals($monde->isTrouNoir, true);
        $this->assertEquals($monde->isBombe, false);
        $this->assertEquals($monde->isPille, true);
        $this->assertEquals($monde->isCorrompu, false);
        $this->assertEquals($monde->nbExplo, 0);
        $this->assertEquals($monde->nbExploCm, 0);
        $this->assertEquals($monde->nbExploPop, 0);
        $this->assertEquals($monde->nbExploLocalisation, 0);
        $this->assertEquals($monde->potentielExploration, 0);
        $this->assertEquals('Le nom de ce monde', $monde->nom);
    }

    public function testSectionTresor()
    {
        $numTresor = 157;
        $rawTresor = <<<TRESOR
[T_$numTresor]
T=6,M,390
Nom=La perle d'or
TRESOR;

        $content = parse_ini_string($rawTresor, true,  INI_SCANNER_RAW);
        ($tresor = new Tresor)->fillFromNba($numTresor, $content["T_$numTresor"]);

        $this->assertEquals($numTresor, $tresor->numero);
        $this->assertEquals(6, $tresor->proprietaire);
        $this->assertEquals(true, $tresor->sur_monde);
        $this->assertEquals(false, $tresor->sur_flotte);
        $this->assertEquals(390, $tresor->localisation);
        $this->assertEquals("La perle d'or", $tresor->nom);
    }

    public function testParseNba()
    {
        $nba = new NebData('TEST04.nba');

        $this->assertEquals(4, $nba->tour);
        $this->assertEquals(0, $nba->numJoueur);
        $this->assertEquals($nba->nbJoueurs, count($nba->joueurs));
        $this->assertEquals($nba->nbMondes, count($nba->mondes));
        $this->assertEquals($nba->nbFlottes, count($nba->flottes));
        $this->assertEquals(2, count($nba->tresors));
    }

    public function testParseNbt()
    {
        // $nba = new NebData('TEST/STOCK/NBT/TEST0104.nbt');
        $nbt = new NebData('TEST0104.nbt');

        $this->assertEquals(4, $nbt->tour);
        $this->assertEquals(1, $nbt->numJoueur);
        $this->assertTrue($nbt->nbJoueurs >= count($nbt->joueurs));
        $this->assertTrue($nbt->nbMondes >= count($nbt->mondes));
        $this->assertTrue($nbt->nbFlottes >= count($nbt->flottes));
        $this->assertEquals(0, count($nbt->tresors));
    }

}
