import { defineConfig } from 'vite'
import laravel from 'laravel-vite-plugin'
import livewire from '@defstudio/vite-livewire-plugin'
import path from 'path'

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/js/app.js',
                'resources/js/ordres/codemirror.js',
                'resources/js/plan/plan.mjs',
                'resources/js/ordres/ordres.mjs',
                'resources/js/nebutil/ouvre-nbt.mjs',
            ],
            refresh: true,
        }),
        livewire({
            refresh: ['resources/css/app.css'],
        }),
    ],
    resolve: {
        alias: {
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
        }
    },
    build: {
        sourcemap: true
    }
})
