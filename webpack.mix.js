const mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
const productionSourceMaps = false

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/ordres/codemirror.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .version()

mix.js('resources/js/plan/plan.mjs', 'public/js')
    .sourceMaps(productionSourceMaps, 'source-map')
    .version()

mix.js('resources/js/ordres/ordres.mjs', 'public/js')
    .sourceMaps(productionSourceMaps, 'source-map')
    .version()

mix.js('resources/js/nebutil/ouvre-nbt.mjs', 'public/js/')
    .sourceMaps(productionSourceMaps, 'source-map')
    .version()
